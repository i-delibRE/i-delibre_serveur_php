FROM php:7.1-fpm
MAINTAINER Libriciel TEAM

RUN apt-get update -qq && apt-get install -y zip pdftk libpq-dev libcurl4-gnutls-dev librtmp-dev libxml2-dev netcat ssmtp
RUN docker-php-ext-install pdo pdo_pgsql pgsql curl xml soap

COPY . /var/www/idelibre/
WORKDIR /var/www/idelibre/
RUN chown -Rf www-data:www-data /var/www/idelibre/*
COPY ./docker-ressources/idelibre.inc.docker.php /var/www/idelibre/app/Config/idelibre.inc.php
COPY ./docker-ressources/database.docker.php /var/www/idelibre/app/Config/database.php

RUN chmod -Rf u+rw,g+rw /var/www/idelibre*
RUN find ./ -type f -name "*.sh" -exec chmod -v u+x,g+x {} \;
RUN find ./ -type f -name "cake" -exec chmod -v u+x,g+x {} \;
RUN chmod -R 777 /var/www/idelibre/app/tmp
RUN mkdir -p /data/workspace/zip
RUN mkdir /data/token
RUN chown -R www-data: /data
RUN mkdir -p /var/www/socket/Connections/
RUN chown -R www-data: /var/www/socket/
COPY ./docker-ressources/zz-idelibre.conf /usr/local/etc/php-fpm.d/zz-idelibre.conf
COPY ./docker-ressources/zz-idelibre.conf /usr/local/etc/php-fpm.d/zz-idelibre.conf



# On doit pouvoir l'enlever ou est l'interet vu qu'on le map sur un volume sur le docker-compose ?
#VOLUME /var/www/idelibre


