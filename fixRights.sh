#!/bin/bash

echo "fix rights"

if [ -z "$1" ]
  then
    echo "preciser le repertoire"
    exit 1
fi

sudo chmod -R 664  ../$1
sudo chmod -R +X ../$1
sudo chown -R www-data: ../$1
sudo chmod 777 app/tmp
sudo chmod +x app/Console/cake
sudo find . -type f -name "*.sh" -exec chmod -v u+x,g+x {} \;


echo "Done !"


