#!/bin/bash

echo "clear tmp and cache"

rm ./app/tmp/*
rm ./app/tmp/cache/*
rm ./app/tmp/cache/models/*
rm ./app/tmp/cache/persistent/*
rm ./app/tmp/cache/views/*

chown www-data: -R *
chmod -R 664 *
chmod -R +X *
chmod -R 777 ./app/tmp
find ./ -type f -name "*.sh" -exec chmod -v u+x,g+x {} \;
find ./ -type f -name "cake" -exec chmod -v u+x,g+x {} \;



echo "OK"