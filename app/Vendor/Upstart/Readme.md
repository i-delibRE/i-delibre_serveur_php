1. Fichiers de configuration Upstart

Les fichiers présents dans ce répertoire sont des fichiers configuration pour Upstart. Après avoir modifié ces fichiers afin qu'ils correspondent à votre installation, vous devez les copier dans le répertoire d'Upstart.(en général /etc/init). Une fois fait, vous pouvez démarrer les services.

2. Démarrer les services :

    - i-delibRE Websocket Server : start idelibre-ws-server

3. Arrêter les services :

    - i-delibRE Websocket Server : stop idelibre-ws-server

Note :
    Lors de l'arrêt des services, les fichiers de cache de CakePHP (pour l'application i-delibRE) sont purgés.

3. Informations à propos des services :

    - i-delibRE Websocket Server : status idelibre-ws-server
