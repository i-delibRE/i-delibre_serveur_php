/**
 *
 * Vendor/SocketIO/config.default.js
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-22 10:10:01 +0200 (mar., 22 oct. 2013) $
 * $Revision: 303 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/webroot/js/dateHelper.js $
 * $Id: dateHelper.js 303 2013-10-22 08:10:01Z ssampaio $
 */

//include et init
var fs = require('fs');
var config = {};

//fichier de debug
config.logFile ='/var/www/idelibre/app/tmp/logs/node.log';


//niveau de debug (debug, info, warn, error 0 for no log);
config.logLevel = 'debug';



//var content = fs.readFileSync( __dirname + '/test.txt');
//console.log(content.toString());



//debug
//config.logLevel = 0; //niveau d'informations de debug (0: rien, ..., 3: max)

//root
config.root = '/var/www/idelibre';

config.tmp = config.root + '/app/tmp'; //peut rester ainsi
config.log = config.tmp + '/logs'; //peut rester ainsi

//Url publique du serveur
config.serverUrl = "localhost";

//log4js logfile
config.log4jsLogFile = config.log + '/ws-monitor.log'; //peut rester ainsi

//chemin du fichier cDebugUUID
config.cDebugUUID = config.tmp + '/cDebugUUID'; //peut rester ainsi

//port d ecoute
config.port = '8080';


//liste des connections des collectivités
//var ville = "postgres://idelibre:idelibre@localhost:5433/idelibre_coll3";
//var migration = "postgres://idelibre:idelibre@localhost:5433/idelibre_migration";
//var libriciel = "postgres://idelibre:idelibre@localhost:5432/idelibre_libriciel";



// tableau de correspondance suffixe connexion
config.conMap = {
};




try{
    var content = fs.readdirSync(__dirname + '/Connections');

    var res = [];
    content.forEach(function (name) {
        res.push(fs.readFileSync(__dirname + '/Connections/' + name, 'utf8'));
    });

    res.forEach(function (el) {
        var jsonConn = JSON.parse(el);
        var key = Object.keys(jsonConn)[0];
        config.conMap[key] = jsonConn[key];

    });
}catch(err){
    console.log(err);
}






//options ssl
config.sslOptions = {
    //key: fs.readFileSync(''),
    //cert: fs.readFileSync(''),
    //ca: fs.readFileSync('')
};

//export du module de config
module.exports = config;
