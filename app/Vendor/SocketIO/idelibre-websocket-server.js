/* global us */
/* global require */


//underscore js
var us = require('underscore');


//connexion postgresql
var pg = require('pg');





//  File d'attente
var queue = [];
//  includes
var express = require('express');
var uuid = require('node-uuid');
var fs = require('fs');
//  API Node.JS https
var https = require('https');
var http = require('http');
//  load config
var config = require('./config');




//  create server

//var app = require('http').createServer();
var app = module.exports.app = express();
var server = http.createServer(app);

var _ = require('lodash');


//var logger = require('logger');


//module de logging
var winston = require('winston');


//configuration du logging
//winston.add(winston.transports.File, {filename: config.logFile});
var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: config.logLevel,
            filename: config.logFile,
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            //maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: config.logLevel,
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});


logger.info('node start on : %s', config.port);



var io = require('socket.io').listen(server);
server.listen(config.port);

//  for testing
function handler(req, res) {
    res.writeHead(200);
    res.end("Welcome to i-delibRE websocket server\n");
}



// fonction de rollback en cas d'erreur sur une transacion
var rollback = function (client, done, res) {
    client.query('ROLLBACK', function (err) {
        res.end('err');
        return done(err);
    });
};


//Plus utiliser (à verifier)
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                    ? args[number]
                    : match
                    ;
        });
    };
}






/**
 * @argument {liste} liste des clients
 */
var client = {};


/**
 *  @function addClient
 *  @description Ajout d'un client dans la collection de clients
 *  @param {JSObject} client
 *  @returns {Void}
 */
var addClient = function (accounts, socketId) {
    //on parcours les accounts pour associé l'userId à la socketId
    for (var iA = 0, lnA = accounts.length; iA < lnA; iA++) {
        client[accounts[iA].id] = socketId;
    }

};


var getkeysFromValue = function (value, keyPairList) {
    var keys = [];


    us.each(keyPairList, function (v, k) {
        if (v === value) {
            keys.push(k);
        }
    });
    return keys;
};

var removeClient = function (socketId) {
    var keys = getkeysFromValue(socketId, client);

    for (var iK = 0, lnK = keys.length; iK < lnK; iK++) {
        delete client[keys[iK]];
    }

};


/**
 * retourne la socketid correspondant au client
 * @param {type} clientId
 * @returns {undefined}
 */
var getSocketfromClientId = function (clientId) {
    if (clientId) {
        return client[clientId];
    }
    return null;
};






/**
 * choix de la bdd utiliée
 * @param {type} socket
 * @param {type} data
 * @returns {undefined}
 */
var bddChoose = function (data) {
    //recupérer directement du client sa connection (qu'on lui stockera au moemnt de du lgobal auth, evitera ceci qui n'est pas propre du tout
    logger.debug(data.collectivite);
    logger.debug(config.conMap);

    if (data.collectivite) {
        var conn = config.conMap[data.collectivite];
        logger.debug('connection %s', conn);
        if (typeof (conn !== "undefined")) {
            return conn;
        }
    }
    return false;
};




/**
 * fonction de requete directement à la bdd et renvoie la reponse au client 
 * @param {type} conn
 * @param {type} request
 * @param {type} res
 * @returns {undefined}
 */
var bddReq = function (conn, request, res) {
    logger.debug('request %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {

            return logger.error('error fetching client from pool %s', err);
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();

            if (err) {
                return logger.error(err);
            }
            
            console.log(result.rows[0].data);
            
            var response = JSON.stringify(result.rows);
            
            res.end(response);

        });
    });
};


/**
 * insertion ou modification d'une annotation
 * @param {type} conn
 * @param {type} request1
 * @param {type} request3
 * @param {type} request4
 * @param {type} annot
 * @param {type} res
 * @returns {undefined}
 */
var bddInsertAnnot = function (conn, request1, request3, request4, annot, res) {
    logger.debug('BDDInsertAnnot');

    pg.connect(conn, function (err, client, done) {

        if (err) {
            console.log('conn');
            console.log(err);
            throw err;
        }
        client.query('BEGIN', function (err) {
            if (err) {
                console.log('begin');
                console.log(begin);
                return rollback(client, done, res);
            }
            process.nextTick(function () {
                var text = 'INSERT INTO annots(id, projet_id, json) values( $1, $2 , $3)';
                client.query(request1, function (err) {
                    console.log(request1);
                    if (err) {
                        console.log('supress');
                        console.log(err);
                        return rollback(client, done, res);
                    }
                    client.query(text, [annot.id, annot.projet_id, JSON.stringify(annot)], function (err) {
                        if (err) {
                            console.log('insert');
                            console.log(err);
                            return rollback(client, done, res);
                        }
                        client.query(request3, function (err) {
                            if (err) {
                                console.log('req3');
                                return rollback(client, done, res);
                            }
                            client.query(request4, function (err) {
                                if (err){
                                    console.log('req4');
                                    console.log(request4);
                                    console.log(err);
                                    
                                    return rollback(client, done, res);
                                }
                                console.log('commit');
                                client.query('COMMIT', done);
                                res.end('ok');
                            });
                        });
                    });
                });
            });
        });
    });
};



/**
 * fonction de récupération de l'intégralité des annotations
 * @param {type} conn
 * @param {type} request
 * @param {type} socket
 * @param {type} accountId
 * @returns {undefined}
 */
var bddAnnot = function (conn, request, socket, accountId) {
    logger.debug('request :  %s', request);
    pg.connect(conn, function (err, client, done) {
        if (err) {
            return logger.error('error fetching client from pool : %s', err);
        }
        client.query(request, function (err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                return logger.error(err);
            }
            //console.log(result.rows);
            //   var response = JSON.stringify(result.rows);
            var response = {json: result.rows, accountId: accountId};
            
            
            logger.info(response);
            io.to(socket.client.id).emit('all annotations feedback', response);
            // return response;
        });
    });
};




//recuperation d'une nouvelle connection de collectivite ajoutée sur le server
app.post('/nodejs/addCollectivite/', function (req, res) {


    var buffer = '';
    req.on('data', function (data) {
        buffer += data.toString();
    }).on('end', function () {

        try {
            var data = JSON.parse(buffer);
            var key = Object.keys(data)[0];
            config.conMap[key] = data[key];

        } catch (err) {
            logger.error(err);
        }

        res.send('ok');

    });



});





// renvoie les morceau d'un pdf
app.post('/nodejs/getpart/', function (req, res) {
    logger.debug('/nodejs/getpart');
    var buffer = '';
    req.on('data', function (data) {
        buffer += data.toString();
    }).on('end', function () {
        try {
            var data = JSON.parse(buffer);
            var conn = bddChoose(data);
            if (conn) {
                
                var request = "select * from pdfdatas where id='" + data.pdfdataId + "'";
                bddReq(conn, request, res);
            }
        } catch (err) {
            logger.error(err);
        }

    });
});
// renvoie la liste des seances archivées
app.post('/nodejs/archiveSeance/', function (req, res) {
    logger.debug('/nodejs/archiveSeance');
    var buffer = '';
    req.on('data', function (data) {
        buffer += data.toString();
    }).on('end', function () {
        try {
            var data = JSON.parse(buffer);
            var conn = bddChoose(data);

            if (conn) {
                var request = "select seances.name, seances.id, seances.date_seance, user_id from convocations join seances on convocations.seance_id = seances.id where convocations.user_id = '" + data.userid + "'  and convocations.seance_id in (select id from seances where archive = true);";
                bddReq(conn, request, res);
            }
        } catch (err) {
            logger.error(err);
        }

    });
});
// renvoie la liste des projets d'une seance archivée
app.post('/nodejs/archivedProjets/', function (req, res) {
    logger.debug('/nodejs/archivedProjet');
    var buffer = '';
    req.on('data', function (data) {
        buffer += data.toString();
    }).on('end', function () {
        try {
            var data = JSON.parse(buffer);
            var conn = bddChoose(data);
            if (conn) {
                //   var request = "select projets.id, projets.name, projets.rank ,pthemes.name as themeName , ptheme_id, document_id from projets join pthemes on projets.ptheme_id = pthemes.id where projets.seance_id = '" + data.seanceid + "';";
                var request = "select projets.id, projets.name, projets.rank ,pthemes.name as themeName , ptheme_id, document_id, annexes.name as annexe_name, annexes.id as annexe_id, annexes.type as annexe_type, annexes.rank as annexe_rank from projets left join pthemes on projets.ptheme_id = pthemes.id  left join annexes on projets.id = annexes.projet_id where projets.seance_id = '" + data.seanceid + "';";
                bddReq(conn, request, res);
            }
        } catch (err) {
            logger.error(err);
        }

    });
});


app.post('/nodejs/archivedConvocation/', function (req, res) {
    logger.debug('/nodejs/archivedConvocation');
    var buffer = '';
    req.on('data', function (data) {
        buffer += data.toString();
    }).on('end', function () {
        try {
            var data = JSON.parse(buffer);
            var conn = bddChoose(data);
            if (conn) {
                //   var request = "select projets.id, projets.name, projets.rank ,pthemes.name as themeName , ptheme_id, document_id from projets join pthemes on projets.ptheme_id = pthemes.id where projets.seance_id = '" + data.seanceid + "';";
                var request = "select document_id from seances where seances.id = '" + data.seanceid + "';";
                bddReq(conn, request, res);
            }
        } catch (err) {
            logger.error(err);
        }

    });
});



app.get('/nodejs/verif', function(req,res){
   
	res.send({success: 'works'}); 
});


// renvoie un document (pdf);
app.get('/nodejs/getDocument/:docId/:connection', function (req, res) {
    var docId = req.params.docId;
    var data = {};
    data.collectivite = req.params.connection;
    var conn = bddChoose(data);
    if (conn) {
        var request = "select content from documents  where documents.id = '" + docId + "';";
        bddReq(conn, request, res);
    }

});






app.get('/nodejs/jp', function (req, res) {
    res.send({"my": "object"});
});


// renvoie un document (pdf);
app.get('/nodejs/getAnnexe/:docId/:connection', function (req, res) {
    logger.debug('getANNEXE');
    logger.debug(req.params.connection);
    var docId = req.params.docId;
    var data = {};
    data.collectivite = req.params.connection;
    var conn = bddChoose(data);
    if (conn) {
        var request = "select content from annexes  where annexes.id = '" + docId + "';";
        bddReq(conn, request, res);
    }

});
app.post('/nodejs/deleteAnnot/', function (req, res) {
    console.log('deleteannot');
    logger.debug('/nodejs/delete');


    var buffer = '';
    req.on('data', function (data) {
        buffer += data.toString();
    }).on('end', function () {
        try {
            var data = JSON.parse(buffer);
        } catch (err) {
            logger.error(err);
        }
        // si il n'y a pas d'annotation à supprimer
        if (_.isEmpty(data.data)) {
            res.end(JSON.stringify({error: true}));
            return;
        }


        _.each(data.data, function (annot) {
            logger.debug(annot);
            var conn = bddChoose(annot);
            if (conn) {
                //supression de l'annotation
                var request = "delete from annots where id  = " + annot.id + " ;";
                bddReq(conn, request, res);
            }
        });
    });

});
// sauvegarde des annotations
app.post('/nodejs/saveAnnot/', function (req, res) {
    logger.debug('/nodejs/archivedProjet');
    var buffer = '';
    req.on('data', function (data) {
        buffer += data.toString();
    }).on('end', function () {
        var data = JSON.parse(buffer);

        // si il n'y a pas d'annotation à supprimer
        if (_.isEmpty(data.data)) {
            res.end(JSON.stringify({error: true}));
            return;
        }
        // pourchacune des annotations
        _.each(data.data, function (annot) {
            //on recupere la connexion 

            var data = {};
            data.collectivite = annot.collectivite;
            var conn = bddChoose(data);
            //supression des annotations si existantes
            var request1 = "DELETE FROM annots WHERE annots.id =" + annot.id + "; ";
            // ajout des annotations
            var request2 = "INSERT INTO annots (id, projet_id, json) VALUES (" + annot.id + ", '" + annot.projet_id + "' , '" + JSON.stringify(annot) + "'); ";
            //ajouts des utilisateurs avec qui l'annot est partagée
            var request3 = "";
            _.each(annot.shareArray, function (userId) {
                request3 += "insert into annots_users(user_id, annot_id) Values('" + userId + "' , " + annot.id + " ); ";
            });
            //utilisateur créateur de l'annotation
            var request4 = "insert into annots_users(user_id, annot_id) Values('" + annot.userId + "' , " + annot.id + " ); ";
            var request = request1 + request2 + request3 + request4;
            bddInsertAnnot(conn, request1, request3, request4, annot, res);

        });


    });
});

/**
 * traitement de l'authentification global
 * @param {Socket} socket
 * @param {Json} ans
 * @returns {}
 */
var globalAuthFeedback = function (socket, ans) {
    if (ans == "error") {
        io.to(socket.client.id).emit('global auth feedback', {error: true});
    } else {
        addClient(ans.accounts, socket.id);
        io.to(socket.client.id).emit('global auth feedback', ans);
    }
};
/**
 * traitement du retour des seances du serveur
 * @param {Socket} socket
 * @param {Json} ans
 * @returns {}
 */
var syncSeancesFeedback = function (socket, ans) {
    io.to(socket.client.id).emit('sync seances feedback', ans);
};
/**
 * traitment du retour des seances modifié du serveur 
 * @param {socket} socket
 * @param {Json} ans
 * @returns {}
 */
var reSyncSeancesFeedback = function (socket, ans) {
    io.to(getSocketfromClientId(ans.args.user_id)).emit('reSync seances feedback', ans);
};
/**
 * retour de l'accusé reception
 * @param {Socket} socket
 * @param {Json} ans
 * @returns {}
 */
var arFeedback = function (socket, ans) {
    var toSend = {
        success: ans.args.data.success
    };
    io.to(getSocketfromClientId(ans.args.user_id)).emit('ar feedback', toSend);
};
var sendToWebService = function (type, data, socket) {

    //  On concatène le type de la requête à data.
    var dataToSend = JSON.stringify({
        json: data,
        type: type
    });

    //  Configuration de la requête https
    var options = {
        //  Url interrogée
        hostname: config.serverUrl,
        //  API interrogée
        path: '/Clients/getData.json',
        //  Définition des headers
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(dataToSend)
        },
        rejectUnauthorized: false,
        method: 'POST'
    };
    //  Requête https vers le webservice
    var req = https.request(options, function (res) {
        //  res.setEncoding('utf8');
        var fullData = "";
        //  Le résultat de la requête arrive en plusieurs morceaux (chunks of data), on les concatène
        res.on('data', function (receivedData) {
            fullData += receivedData;
            //console.log('receivedData');
        });
        //  On écoute l'évènement end indiquant la fin du traitement de notre requête, et donc, de l'assemblage de nos chunks
        res.on('end', function () {
            try {
                var ans = JSON.parse(fullData);

                switch (type) {
                    case "global auth":
                        globalAuthFeedback(socket, ans);
                        break;
                    case "ar":
                        arFeedback(socket, ans);
                        break;
                    case "annot feedback":
                        annotFeedback(ans);
                        break;
                    case "sync seances":
                        syncSeancesFeedback(socket, ans);
                        break;
                    case "sync annots feedback":
                        syncAnnotationsFeedback(ans);
                        break;
                    case "reSync seances":
                        reSyncSeancesFeedback(socket, ans);
                        break;
                }


            } catch (err) {
                logger.error(err);
            }

        });
    });
    //  Gestion des erreurs
    req.on('error', function (e) {
        //console.log('problem with request: ' + e);
    });
    //  Envoi de la requête avec les données POST "dataToSend"
    req.write(dataToSend);
    req.end();
};
/**
 *   Connection Event
 *   @event Connection
 *   @description Détecte une connexion du client et enclenche les évènements associés
 *
 */
io.sockets.on('connection', function (socket) {
     console.log('nouvelle connection');
     
     console.log('nouveau client');
     
    io.to(socket.client.id).emit('hello');
   
   socket.on('new message', function (data) {
	console.log('test');
   });


   socket.on('android', function (data) {
        console.log('test');
        console.log(data);
	io.to(socket.client.id).emit('android server', data);
   });


   socket.on('getLogin object', function () {
        var loginJson = {name: 'test', id: 3};
        io.to(socket.client.id).emit('login object', loginJson);
    });

    socket.on('disconnect', function () {
        logger.debug('disconnect');
        removeClient(socket.id);
    });
    socket.on('sync accounts', function (data) {
        logger.debug('sync accounts');
        sendToWebService('global auth', data, socket);
    });
    socket.on('sync seances', function (data) {
        logger.debug('sync seances');
        //logger.debug(data);

        sendToWebService('sync seances', data, socket);
    });
    socket.on('resync seances', function (data) {
        logger.debug('resync seances');
        /* console.log(data);*/
        sendToWebService('reSync seances', data, socket);
    });
    socket.on('ar', function (data) {
        logger.debug('ar');
        logger.debug(data);
        sendToWebService('ar', data, socket);
    });
    //lors de la demande de toutes les annotations
    socket.on('all Annotations', function (data) {
        logger.info('all annotations');

        var conn = bddChoose(data);
        if (conn) {
            var request = "select * from annots join annots_users on annots.id = annots_users.annot_id where user_id ='" + data.userId + "' and annots.projet_id in (" + data.projetIds + ") ; "
            bddAnnot(conn, request, socket, data.userId);
        }
    });
////////////////////////////////////////////////////////////////////////////////////
//reception d'un evenement du serveur cakephp


    /**
     *  Seance Push Event
     *  @event Connection#seance_push
     *  @description Réception et redirection d'une séance envoyée par le serveur
     */
    socket.on('seance push', function (data) {
        logger.debug('seance push from server');
        io.to(getSocketfromClientId(data.userId)).emit('new seance pushed', data);
    });
    /**
     * seance repush event
     * lorqu'une seance à été modifiée sur le serveur
     */
    socket.on('seance repush', function (data) {
        winston.debug('seance repush from server');
        io.to(getSocketfromClientId(data.userId)).emit('modified seance', data);
    });
    /**
     * envoie d'un push de supression de seance 
     */
    socket.on('seance push delete', function (data) {
        logger.debug('seance push delete');
        io.to(getSocketfromClientId(data.userId)).emit('seance push delete from server', data);
    });


    socket.on('new annotation', function (data) {
        logger.debug('new annotation');
        if (data.shareArray) {
            for (var i = 0, l = data.shareArray.length; i < l; i++) {
                io.to(getSocketfromClientId(data.shareArray[i])).emit('push new shared annotation', data);
            }
        }
    });


    socket.on('delete annotation', function (data) {
        logger.debug('delete annotation');
        if (data.shareArray) {
            for (var i = 0, l = data.shareArray.length; i < l; i++) {
                io.to(getSocketfromClientId(data.shareArray[i])).emit('push delete shared annotation', data);
            }
        }
    });


    socket.on('collectivite change', function (data) {
        logger.debug('coll change');
        logger.debug(getSocketfromClientId(data.userId));

        io.to(getSocketfromClientId(data.userId)).emit('collectivite change push', data);
    });


});
