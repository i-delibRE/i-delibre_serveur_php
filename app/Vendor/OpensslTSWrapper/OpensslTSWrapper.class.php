<?php

/**
 *
 */
class OpensslTSWrapper {

	/**
	 *
	 * @var type
	 */
	private $opensslPath;

	/**
	 *
	 * @var type
	 */
	private $lastError;

	/**
	 *
	 * @param type $opensslPath
	 */
	public function __construct($opensslPath) {
		$this->opensslPath = $opensslPath;
	}

	/**
	 *
	 * @return type
	 */
	public function getLastError() {
		return $this->lastError;
	}

	/**
	 *
	 * @param type $command
	 * @return type
	 */
	private function execute($command) {
		return shell_exec($command);
	}

	/**
	 *
	 * @param type $data
	 * @return string
	 */
	private function getTmpFile($data = "") {
		$file_path = sys_get_temp_dir() . "/" . mt_rand();
		file_put_contents($file_path, $data);
		return $file_path;
	}

	/**
	 *
	 * @param type $data
	 * @return type
	 */
	public function getTimestampQuery($data) {
		$dataFilePath = $this->getTmpFile($data);
		$result = $this->execute($this->opensslPath . " ts -query -data $dataFilePath");
		unlink($dataFilePath);
		return $result;
	}

	/**
	 *
	 * @param type $timestampQuery
	 * @return type
	 */
	public function getTimestampQueryString($timestampQuery) {
		$timestampQueryFilePath = $this->getTmpFile($timestampQuery);
		$result = $this->execute($this->opensslPath . " ts -query -in $timestampQueryFilePath -text ");
		unlink($timestampQueryFilePath);
		return $result;
	}

	/**
	 *
	 * @param type $timestampReply
	 * @return type
	 */
	public function getTimestampReplyString($timestampReply) {
		$timestampReplyFilePath = $this->getTmpFile($timestampReply);
		$commande = $this->opensslPath . " ts -reply -in $timestampReplyFilePath -text ";
		$result = $this->execute($commande);
		unlink($timestampReplyFilePath);
		return $result;
	}

	/**
	 *
	 * @param type $data
	 * @param type $timestampReply
	 * @param type $CAFilePath
	 * @param type $certFilePath
	 * @return type
	 */
	public function verify($data, $timestampReply, $CAFilePath, $certFilePath) {
		$dataFilePath = $this->getTmpFile($data);
		$timestampReplyFilePath = $this->getTmpFile($timestampReply);

		$command = $this->opensslPath . " ts -verify " .
				" -data $dataFilePath " .
				" -in $timestampReplyFilePath " .
				" -CAfile $CAFilePath" .
				" -untrusted $certFilePath 2>&1 ";

		$result = trim($this->execute($command));

		unlink($dataFilePath);
		unlink($timestampReplyFilePath);

		$this->lastError = $result;
		return ($result == "Verification: OK");
	}

	/**
	 *
	 * @param type $timestampRequest
	 * @param type $signerCertificate
	 * @param type $signerKey
	 * @param type $signerKeyPassword
	 * @param type $configFile
	 * @return type
	 */
	public function createTimestampReply($timestampRequest, $signerCertificate, $signerKey, $signerKeyPassword, $configFile) {
		$timestampRequestFile = $this->getTmpFile($timestampRequest);
		$timestampReplyFile = $this->getTmpFile("");

		$command = $this->opensslPath . " ts -reply " .
				" -queryfile $timestampRequestFile" .
				" -signer " . $signerCertificate .
				" -inkey " . $signerKey .
				" -passin pass:" . $signerKeyPassword .
				" -out $timestampReplyFile " .
				" -config " . $configFile;
		shell_exec($command);

		$timestampReply = file_get_contents($timestampReplyFile);
		unlink($timestampRequestFile);
		unlink($timestampReplyFile);
		return $timestampReply;
	}

}