<?php

/**
 * Appchecks Utility Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-07-10 14:02:20 +0200 (jeu. 10 juil. 2014) $
 * $Revision: 505 $
 * $Author: genot $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Utility/DevTools.php $
 * $Id: DevTools.php 505 2014-07-10 12:02:20Z genot $
 *
 *
 */

class DevTools {


    //load connection
    public static function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }
        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }




    /**
     * Debug var in APP/tmp/logs/debug.log file
     *
     * @param mixed $var  Variable to debug
     * @param type $force Force logging even in debug mode 0
     */
    public static function ldebug($var, $force = false) {
        if (Configure::read('debug') > 0 || $force) {
            $debugvar = array(
                '================================================================',
                'date' => date('Y-m-d H:i:s'),
               // 'trace' => Debugger::trace(),
                'ligne' => __LINE__,
                'var' => $var,
                'type' => gettype($var)
            );
            $o = new CakeObject();
            $o->log(var_export($debugvar, true), 'debug');
        }
    }

    /**
     * Debug var in APP/tmp/logs/debug.log file
     *
     * @param mixed $var  Variable to debug
     * @param type $force Force logging even in debug mode 0
     */
    public static function debug($var, $force = false) {
        if (Configure::read('debug') > 0 || $force) {
            $debugvar = array(
                '================================================================',
                'date' => date('Y-m-d H:i:s'),
                'trace' => Debugger::trace(),
                'var' => $var,
                'type' => gettype($var)
            );

            debug($debugvar);
        }
    }

    /**
     *
     * @param type $var
     * @param type $forceq
     */
    public static function log($var, $force = false) {
        if (Configure::read('debug') > 0 || $force) {
            $o = new CakeObject();
            $o->log(var_export($var, true), 'debug');
        }
    }



    public static function logMe($var, $fileName ,$force = false) {
        if (Configure::read('debug') > 0 || $force){

            $bt = debug_backtrace();
            $caller = array_shift($bt);
            $file =  $caller['file'];
            $line =  $caller['line'];

            $debugvar = array(
                'file' => $line,
                'ligne' => $file,
                'var' => $var
            );
            $o = new CakeObject();
            $o->log(var_export($debugvar, true), $fileName);
        }
    }

}

?>
