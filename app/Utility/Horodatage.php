<?php

/**
 * Horodatage Utility Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-05-16 14:20:01 +0200 (ven. 16 mai 2014) $
 * $Revision: 421 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Utility/Horodatage.php $
 * $Id: Horodatage.php 421 2014-05-16 12:20:01Z ssampaio $
 *
 *
 */
App::uses('Security', 'Component');

class Horodatage {

    /**
     *
     * @var type
     */
    public $responseToText; //FIXME: bien que les données soient valides dans la classe Horodatage, dans le reste de l'appli, on ne peut y avoir accès

    /**
     * fonction de vérification du fonctionnement du service d'horodatage
     * @return boolean true, le service fonctionne, false dans le cas contraire
     */

    static public function check() {
        // initialisation
        $ret = false;

        // en fonction du type de service utilisé
        switch (Configure::read('Horodatage.type')) {
            case 'OPENSIGN':
                try {
                    $client = new SoapClient(Configure::read('Opensign.host'), array('wsdl_cache' => 0, 'trace' => 1));
                    $ret = $client->getOpensslVersion();
                    return (!empty($ret));
                } catch (Exception $e) {
                    return false;
                }
                break;
            case 'IAIK' :
            case 'CRYPTOLOG' :
                // création du fichier test temporaire à horodater
                $tmpTestFileUri = tempnam(sys_get_temp_dir(), rand());
                file_put_contents($tmpTestFileUri, 'as@lae : fichier temporaire de test d\'horodatage');
                if (!file_exists($tmpTestFileUri))
                    return false;
                // horodatage du fichier test
                $jh = Horodatage::contentHorodatageFichier($tmpTestFileUri);
                unlink($tmpTestFileUri);
                return ($jh !== false);
                break;
            default :
                return false;
        }
    }

    /**
     * fonction d'horodatage d'un fichier
     * @param string $fichierSource chemin du du fichier à horodater
     * @param string $repDest chemin du jeton d'horodatage (doit exister), si null, le jeton est produit dans le même répertoire que le fichier source
     */
    static public function horodateFichier($fichierSource, $repDest = null) {
        if(!Horodatage::check())
            return false;

        // si le nom du fichier est vide, on ne fait rien
        if (empty($fichierSource))
            return false;

        // initialisation du répertoire de destination
        if (empty($repDest))
            $repDest = dirname($fichierSource) . DS;

        $jh = Horodatage::contentHorodatageFichier($fichierSource);
        if ($jh !== false) {
            $outrep = $repDest . basename($fichierSource) . '.rep.tsa';
            file_put_contents($outrep, $jh['token']);
            return true;
        } else
            return false;
    }

    /**
     * fonction d'horodatage des fichiers d'un répertoire et de ses sous répertoires
     * les jetons d'horodatage peuvent être écrits dans un répertoire différent du répertoire source
     * @param string $repSource chemin du répertoire des fichiers à horodater
     * @param string $repDest chemin des jetons d'horodatage, si null, les jetons sont produit dans le même répertoire des sources
     * @param booleen $recursive traite également les sous-dossiers si true
     */
    static public function horodateRepertoire($repSource, $repDest = null, $recursive = true) {
        // si repertoire source est vide, on ne fait rien
        if (empty($repSource) || ($repSource === DS))
            return;

        // lecture du répertoire
        $fichiersSource = glob($repSource . '*');

        // initialisation du répertoire de destination
        if (empty($repDest))
            $repDest = $repSource;
        else
        if (!is_dir($repDest))
            mkdir($repDest);

        foreach ($fichiersSource as $fichierSource) {
            if (is_file($fichierSource)) {
                Horodatage::horodateFichier($fichierSource, $repDest);
            } else {
                // traitement du sous repertoire
                if ($recursive) {
                    Horodatage::horodateRepertoire($fichierSource . DS, $repDest . basename($fichierSource) . DS);
                }
            }
        }
    }

    /**
     * retourne le contenu du jeton d'horodatage d'un fichier
     * @param string $fileUri uri du fichier à horodater
     * @return mixed false si l'horodatege a échoué, bitstream du jeton d'horodatage en cas de succès
     */
    static public function contentHorodatageFichier($fileUri) {
        // si le nom du fichier est vide, on ne fait rien
        if (empty($fileUri))
            return false;

        // création de l'empreinte du fichier
        $sha1 = sha1_file($fileUri);

        // en fonction du type de service utilisé
        switch (Configure::read('Horodatage.type')) {
            case 'OPENSIGN' :
                $params = array('sha1' => $sha1);
                $client = new SoapClient(Configure::read('Opensign.host'));
                $requete = $client->__soapCall("createRequest", array('parameters' => $params));
                $params = array('request' => $requete);
                $response = $client->__soapCall("createResponse", array('parameters' => $params));
                $params = array('response' => $response);
                $token = $client->__soapCall("extractToken", array('parameter' => $params));

                //<< idelibre
                $responseToText = $client->__soapCall("responseToText", array('parameter' => $params));
                //idelibre >>

                if (empty($token))
                    return false;
                else
                    return array('token' => base64_decode($token), 'responseToText' => $responseToText);

                break;
            case 'IAIK' :
                require_once(APP . 'libs' . DS . 'TrustedTimestamps.php');
                $tsaUrl = Configure::read('IAIK.host');
                $requestFilePath = TrustedTimestamps::createRequestfile($sha1);

                try {
                    $token = TrustedTimestamps::signRequestfile($requestFilePath, $tsaUrl);
                } catch (Exception $e) {
                   // $this->log('Exception reçue lors du timestamping Iaik: ' . $e->getMessage());
                }

                if (empty($token))
                    return false;
                else
                    return base64_decode($token);

                break;
            case 'CRYPTOLOG' :
                require_once(APP . 'libs' . DS . 'TrustedTimestamps.php');
                $tsaUrl = Configure::read('CRYPTOLOG.host');
                $tsaUserPwd = Configure::read('CRYPTOLOG.userpass');
                $requestFilePath = TrustedTimestamps::createRequestfile($sha1);

                try {
                    $token = TrustedTimestamps::signRequestfile($requestFilePath, $tsaUrl, $tsaUserPwd);
                } catch (Exception $e) {
              //      $this->log('Exception reçue lors du timestamping Cryptolog: ' . $e->getMessage());
                }

                if (empty($token))
                    return false;
                else
                    return base64_decode($token);

                break;
            default :
                return false;
        }
    }

}

?>
