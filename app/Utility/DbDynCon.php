<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DbDynCon
 * Class de gestion dynamqiue des connections par ficheir de conf extern dans config/Conections
 *
 * @author rdubourget
 */
class DbDynCon {

    /**
     * 
     * @param type $name = collectivite.conn
     * @param type $database
     * @param type $suffix
     */
    public static function createStringTab($name, $database, $suffix) {


        $connection = "<?php " . "$" . "{$name} = array('datasource' => 'Database/Postgres',"
                . "'persistent' => false,"
                . "'host' =>'" . DATABASE_HOST . "',"
                . "'login' =>'" . DATABASE_LOGIN . "',"
                . "'password' =>'" . DATABASE_PASSWORD . "',"
                . "'database' => '{$database}',"
                . "'prefix' => '',"
                . "'port' =>" . DATABASE_PORT . ","
                . "'encoding' => 'utf8',"
                . "'inMenu' => true );";


        $file = new File(APP . 'Config/Connections/' . $name, true, 0777);
        $file->write($connection);


        $nodeConnection = '{"' . $suffix . '": "postgres:' . DATABASE_LOGIN . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . ':' . DATABASE_PORT . '/' . $database . '"' . '}';

        $fileNode = new File(APP . 'Vendor/SocketIO/Connections/' . $name, true, 0777);
        $fileNode->write($nodeConnection);




        $url = IDELIBRE_WS_PROTOCOL . '://' . IDELIBRE_WS_HOST . ':' . IDELIBRE_WS_PORT . '/' . 'addCollectivite/';

        $data = array(
            $suffix => 'postgres:' . DATABASE_LOGIN . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . ':' . DATABASE_PORT . '/' . $database
        );


        $jsonData = jSON_encode($data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Supprime els fichiers de conf de connection de la collectivite
     * @param type $name = collectivite.conn
     */
    public static function deleteFilesConfig($name) {
        $path = APP . 'Config/Connections/' . $name;
        $rp = realpath($path);
        if (is_writable($rp)) {
            unlink($rp);
        }

        $pathNode = APP . 'Vendor/SocketIO/Connections/' . $name;
        $rpNode = realpath($pathNode);
        if (is_writable($rpNode)) {
            unlink($pathNode);
        }
    }

    public static function loadAllConnections() {
        foreach (glob(APP . "Config/Connections/*") as $filename) {
            include_once $filename;




            $connectionName = basename($filename);
            try {
                ConnectionManager::create($connectionName, ${$connectionName});
            }
            catch(Exception $e){
                 CakeLog::write('DbDynCon', 'pas de connexion');
                CakeLog::write(var_export($e, true));


            }
        }

        // debug(ConnectionManager::sourceList());
        //  debug(ConnectionManager::enumConnectionObjects());
    }

    public static function loadConnection($name) {



        $filename = APP . "Config/Connections/" . $name;
        
        $fileExists = file_exists($filename);


        if ($fileExists) {
            include $filename;
            ConnectionManager::create($name, $fileExists ? ${$name} : array());
            return true;
        }
        return false;

    }
    
    
    
    
    
    
    

    public function addConnection($name, $database) {
        
    }

}
