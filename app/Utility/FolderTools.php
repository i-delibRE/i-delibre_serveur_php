<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FolderTools
 *
 * @author rdubourget
 */
class FolderTools {
    
    
     /**
     * Retourne un répertoire temporaire disponible dans le dossier passé en parametre
     * @param string $patchDir
     * @return bool|string
     */
    public static function newTmpDir($patchDir) {
        App::uses('Folder', 'Utility');
        $folder = new Folder($patchDir, true, 0777);
        //Création du répertoire temporaire par la fonction tempnam
        $outputDir = tempnam($folder->path, '');
        unlink($outputDir);
        $folder = new Folder($outputDir, true, 0777);
        return $folder->path;
    }
    
}
