<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConverstionBalise
 *
 * @author rdubourget
 */
class ConvertionBalise {

    /**
     * Retourne la chaîne de caractères $string dont les occurences de
     * #Model.champ# ont été remplacées par leur valeur extraite depuis $data.
     * sensible à la casse
     *
     * @param array $data
     * @param string $string
     * @return string
     */
    public static function evaluateString(array $data, $string) {
        if (strpos($string, '#') !== false) {
          //  if (preg_match_all('/(#[^#]+#)/', $string, $out)) {
            if (preg_match_all('/#[^\s]+#/', $string, $out)) {
                $tokens = $out[0];
                foreach (array_unique($tokens) as $token) {
                    $token = trim($token, '#');
                    $string = str_replace("#{$token}#", Hash::get($data, $token), $string);
                }
            }
        }

        return $string;
    }

    /**
     * Retourne le paramètre $mixed dont les occurences de #Model.champ# ont
     * été remplacées par leur valeur extraite depuis $data.
     *
     * @see Hash::get()
     *
     * @param array $data
     * @param string|array $mixed
     * @return string|array
     */
    public static function evaluate(array $data, $mixed) {
        if (is_array($mixed)) {
            $array = array();
            if (!empty($mixed)) {
                foreach ($mixed as $key => $value) {
                    $array[self::evaluateString($data, $key)] = self::evaluate($data, $value);
                }
            }
            return $array;
        }

        return self::evaluateString($data, $mixed);
    }

}
