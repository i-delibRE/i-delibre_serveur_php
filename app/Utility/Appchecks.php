<?php

/**
 * Appchecks Utility Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-08-01 11:49:31 +0200 (ven. 01 août 2014) $
 * $Revision: 550 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Utility/Appchecks.php $
 * $Id: Appchecks.php 550 2014-08-01 09:49:31Z ssampaio $
 */
class Appchecks {

    /**
     * Returns apache version
     * useless now
     *
     * @return string
     */
    public static function apacheVersion() {

        return function_exists("apache_get_version") ? apache_get_version() : 'false';
    }

    /**
     * Returns nginx version
     * @return string
     *
     */
    public static function nginxVersion() {
        if (isset($_SERVER['SERVER_SOFTWARE'])) {
            return $_SERVER['SERVER_SOFTWARE'];
        } else {
            return null;
        }
    }

    /**
     * Returns php version
     *
     * @return string
     */
    public static function phpVersion() {
        return phpversion();
    }

    /**
     * Return cakephp version
     *
     * @return string
     */
    public static function cakephpVersion() {
        return Configure::version();
    }

    /**
     * Return postgresql version
     *
     * @param boolean $full
     * @return string
     */
    public static function postgresVersion($full = false) {
        $model = ClassRegistry::init('Srvuser');
        $psqlVersion = $model->getDataSource($model->useDbConfig)->query('SELECT version();');
        $psqlVersion = $psqlVersion[0][0]["version"];

        if (!$full) {
            $psqlVersion = preg_replace('/.*PostgreSQL ([^ ]+) .*$/', '\1', $psqlVersion);
        }

        return $psqlVersion;
    }

    /**
     * Return info about apache, php, cakephp and postgresql versions
     *
     * @return array
     */
    public static function checkVersions() {


        $versions = array(
            'php' => Appchecks::phpVersion(),
            'cakephp' => Appchecks::cakephpVersion(),
            'postgres' => Appchecks::postgresVersion(),
            'nginx' => Appchecks::nginxVersion()
        );


        $phpCheck = false;
        $cakephpCheck = 0;
        $postgresCheck = false;
        $nginxCheck = false;



        //nginx check
        if (substr($versions['nginx'], 0, 5) == 'nginx') {
            $nginxVersion = substr($versions['nginx'], 6);
            $matches = array();
            preg_match("#([0-9]+)\.([0-9]+)\.[0-9]*.*#", $nginxVersion, $matches);

            if (count($matches) == 3 && intval($matches[1]) >= 1 && intval($matches[2]) >= 3) {
                $nginxCheck = true;
            }
        }



        //php check
        preg_match("#([0-9]+)\.([0-9]+)\.[0-9]*.*#", $versions['php'], $matches);
        if (count($matches) == 3 && intval($matches[1]) >= 7 && intval($matches[2]) >= 1) {
            $phpCheck = true;
        }

        //cakephp check
        preg_match("#([0-9]+)\.([0-9]+)\.([0-9]+)#", $versions['cakephp'], $matches);
        if (count($matches) == 4 && intval($matches[1]) >= 2 && intval($matches[2]) >= 3 && intval($matches[3]) >= 8) {
            $cakephpCheck = 2;
        }

        if (count($matches) == 4 && intval($matches[1]) >= 2 && intval($matches[2]) >= 4 && intval($matches[3]) >= 0) {
            $cakephpCheck = 1;
        }


        if(intval($versions['postgres']) == 10){
            $postgresCheck = true;

        }



        return array(
            'php' => array('version' => $versions['php'], 'check' => $phpCheck),
            'cakephp' => array('version' => $versions['cakephp'], 'check' => $cakephpCheck),
            'postgres' => array('version' => $versions['postgres'], 'check' => $postgresCheck),
            'nginx' => array('version' => substr($versions['nginx'], 6), 'check' => $nginxCheck)
        );
    }

    public static function return_bytes($val) {
        $val = trim($val);
        $last = strtolower($val[strlen($val) - 1]);
        switch ($last) {
            // Le modifieur 'G' est disponible depuis PHP 5.1.0
            case 'g':
                @$val *= 1024;
            case 'm':
                @$val *= 1024;
            case 'k':
                @$val *= 1024;
        }

        return $val;
    }

    /**
     * Returns existing and readable status about important app config files
     *
     * @return array
     */
    public static function checkConfigs() {
        $return = array(
            'php' => array(
                'file_uploads' => ini_get('file_uploads'),
                'memory_limit' => ini_get('memory_limit'),
                'enought_memory' => self::return_bytes(ini_get('post_max_size')) <= self::return_bytes(ini_get('memory_limit')),
                'enought_size' => true,//self::return_bytes(ini_get('upload_max_filesize')) * intval(ini_get('max_file_uploads')) <= self::return_bytes(ini_get('post_max_size')),
                'post_max_size' => ini_get('post_max_size'),
                'upload_max_filesize' => ini_get('upload_max_filesize'),
                'max_file_uploads' => ini_get('max_file_uploads')
            ),
            'idelibre' => array(
                'exists' => is_file(APP . "Config/idelibre.inc.php"),
                'readable' => is_readable(APP . "Config/idelibre.inc.php")
            ),
            'email' => array(
                'exists' => is_file(APP . "Config/email.php"),
                'readable' => is_readable(APP . "Config/email.php")
            )
        );
        return $return;
    }

    /**
     * Returns infos about conversion pdf tool (ghostscript)
     *
     * @return array
     */
    public static function checkPDFConvertTool() {
        $return = array('ghostscript' => array('present' => false));
        if (preg_match('#gs:\s\/(.*\/gs)#', exec('whereis gs'))) {
            $return['ghostscript']['present'] = true;
        }
        return $return;
    }

    /**
     * Returns info about websocket server and client data
     * @return array
     *
     */
    public static function checkWebsocket() {

        $return = array(
            'wsServer' => array(
                'installed' => true,
                'running' => true,
                'connected' => false
            )
        );

        $fd = @fsockopen(IDELIBRE_WS_HOST, IDELIBRE_WS_PORT, $errno, $errstr);
        if ($fd) {
            fclose($fd);
            $return['wsServer']['connected'] = true;
        }

        return $return;
    }

    /**
     * Return all infos
     *
     * @return array
     */
    public static function getAll() {
        $allChecks = array(
            'checks' => Appchecks::checkVersions(),
            'cfgs' => Appchecks::checkConfigs(),
            'pdfConvertTool' => Appchecks::checkPDFConvertTool(),
            'webSocket' => Appchecks::checkWebsocket(),
            'horodatage' => Appchecks::checkHorodatage()
        );

        $all = 'ok';
        foreach ($allChecks['checks'] as $key => $value) {
            if ($key != 'cakephp' && !$value['check'] || $key == "cakephp" && intval($value['check']) == 0) {
                $all = 'error';
            }
        }

        foreach ($allChecks['cfgs'] as $key => $value) {
            if ($key === "php") {
                if (!$value['file_uploads'] || !$value['enought_memory'] || !$value['enought_size']) {
                    $all = "error";
                }
            } else {
                if (!$value['exists'] || !$value['readable']) {
                    $all = 'error';
                }
            }
        }
//        foreach ($allChecks['pdfConvertTool'] as $value) {
//            if (!$value['present']) {
//
//debug('ooo'); die;
//
//                $all = 'error';
//            }
//        }


        foreach ($allChecks['webSocket'] as $key => $value) {
            if ($key == 'wsServer') {
                if (!$value['installed'] || !$value['running'] || !$value['connected']) {
                    $all = 'error';
                }
            }
        }

        if (!$allChecks['horodatage']) {
            $all = 'error';
        }

        $allChecks['all'] = $all;
        return $allChecks;
    }

    // check if webservice Horodatage running
    public static function checkHorodatage() {
        $return = false;
        try {
            $client = new SoapClient(Configure::read('Opensign.host'));
            $response = $client->WsEcho("ping");
            if ($response == 'ping') {
                $return = true;
            }
        } catch (\Exception $exc) {
          //  debug($exc->faultstring);
            DevTools::ldebug($exc->faultstring);
            return false;
        }
        //add
        $sha1 = "fb6ab7e60cb68a1a00355405180fbcf9d27b592a";
        $params =
            array(array('sha1' => $sha1)
            );

        try {
            $client = new SoapClient(Configure::read('Opensign.host'));
            $requete = $client->__soapCall("createRequest", $params);

            //  debug($requete);
            $params = array('request' => $requete);
            // debug($params);
            $response = $client->__soapCall("createResponse", array('parameters' => $params));
            //  debug($response);
            $params = array('response' => $response);
            $token = $client->__soapCall("extractToken", array('parameter' => $params));
            //  debug($token);
            //<< idelibre
            $responseToText = $client->__soapCall("responseToText", array('parameter' => $params));

            //endAdd
        }catch(\Exception $exc){
           // debug($exc->getMessage());
            DevTools::ldebug($exc->faultstring);
            return false;
        }

        return $return;
    }

}

?>
