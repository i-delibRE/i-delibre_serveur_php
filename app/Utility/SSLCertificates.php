<?php

/**
 * Password Utility Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Utility/SSLCertificates.php $
 * $Id: SSLCertificates.php 302 2013-10-21 15:57:34Z ssampaio $
 */
class SSLCertificates {

	const HEX2BIN_WS = " \t\n\r";

	/**
	 * Hexadecimal string to binary string conversion
	 * PHP < 5.4
	 *
	 * check hex2bin native function for PHP >= 5.4
	 * @link http://php.net/manual/en/function.hex2bin.php
	 *
	 *
	 * @param string $hex_string
	 * @return mixed
	 */
	public static function hex2bin($hex_string) {
		$result = false;
		if (function_exists("hex2bin")) {
			$result = hex2bin($hex_string);
		} else {
			$pos = 0;
			while ($pos < strlen($hex_string)) {
				if (strpos(SSLCertificates::HEX2BIN_WS, $hex_string{$pos}) !== FALSE) {
					$pos++;
				} else {
					$code = hexdec(substr($hex_string, $pos, 2));
					$pos = $pos + 2;
					$result .= chr($code);
				}
			}
		}
		return $result;
	}

	/**
	 *
	 * @param string $proof
	 * @param string $sign
	 * @param string $pem
	 * @return boolean
	 */
	public static function verifySSLSign($proof, $sign, $pem) {
		return openssl_verify($proof, $sign, openssl_pkey_get_public($pem));
	}

}

?>
