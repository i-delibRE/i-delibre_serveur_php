<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('DebugPanel', 'DebugKit.Lib');

/**
 * Provides debug information on the Current request params.
 *
 */
class RequestPanel extends DebugPanel {

/**
 * beforeRender callback - grabs request params
 *
 * @param Controller $controller
 * @return array
 */
	public function beforeRender(Controller $controller) {
		$out = array();
		$out['params'] = $controller->request->params;
		$out['url'] = $controller->request->url;
		$out['query'] = $controller->request->query;
		$out['data'] = $controller->request->data;
		if (isset($controller->Cookie)) {
			$out['cookie'] = $controller->Cookie->read();
		}
		$out['get'] = $_GET;
		$out['currentRoute'] = Router::currentRoute();
		return $out;
	}
}
