<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * A CakeLog listener which saves having to munge files or other configured loggers.
 *
 */
class DebugKitLog implements CakeLogInterface {

/**
 * logs
 *
 * @var array
 */
	public $logs = array();

/**
 * Makes the reverse link needed to get the logs later.
 *
 * @param $options
 * @return \DebugKitLog
 */
	public function __construct($options) {
		$options['panel']->logger = $this;
	}

/**
 * Captures log messages in memory
 *
 * @param $type
 * @param $message
 * @return void
 */
	public function write($type, $message) {
		if (!isset($this->logs[$type])) {
			$this->logs[$type] = array();
		}
		$this->logs[$type][] = array(date('Y-m-d H:i:s'), (string)$message);
	}
}
