<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('DebugPanel', 'DebugKit.Lib');

/**
 * Class TestPanel
 *
 * @since         DebugKit 0.1
 */
class TestPanel extends DebugPanel {

/**
 * Startup
 *
 * @param Controller $controller
 */
	public function startup(Controller $controller) {
		$controller->testPanel = true;
	}

}
