<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('DebugKitDebugger', 'DebugKit.Lib');
require_once CakePlugin::path('DebugKit') . 'Test' . DS . 'Case' . DS . 'TestFireCake.php';

/**
 * Test case for the DebugKitDebugger
 *
 * @since         debug_kit 0.1
 */
class DebugKitDebuggerTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		Configure::write('log', false);
		$this->firecake = FireCake::getInstance('TestFireCake');
		TestFireCake::reset();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		parent::tearDown();
		Configure::write('log', true);
		DebugKitDebugger::clearTimers();
		TestFireCake::reset();
	}

/**
 * test output switch to firePHP
 *
 * @return void
 */
	public function testOutput() {
		Debugger::getInstance('DebugKitDebugger');
		Debugger::addFormat('fb', array('callback' => 'DebugKitDebugger::fireError'));
		Debugger::outputAs('fb');

		set_error_handler('ErrorHandler::handleError');
		$foo .= '';
		restore_error_handler();

		$result = $this->firecake->sentHeaders;

		$this->assertRegExp('/GROUP_START/', $result['X-Wf-1-1-1-1']);
		$this->assertRegExp('/ERROR/', $result['X-Wf-1-1-1-2']);
		$this->assertRegExp('/GROUP_END/', $result['X-Wf-1-1-1-5']);

		Debugger::getInstance('Debugger');
		Debugger::outputAs('html');
	}
}
