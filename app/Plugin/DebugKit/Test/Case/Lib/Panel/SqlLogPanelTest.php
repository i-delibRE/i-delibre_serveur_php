<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('SqlLogPanel', 'DebugKit.Lib/Panel');
App::uses('Model', 'Model');
App::uses('Controller', 'Controller');

/**
 * Class SqlLogPanelTest
 *
 * @since         DebugKit 2.1
 */
class SqlLogPanelTest extends CakeTestCase {

/**
 * fixtures.
 *
 * @var array
 */
	public $fixtures = array('core.article');

/**
 * Setup
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->panel = new SqlLogPanel();
	}

/**
 * test the parsing of source list.
 *
 * @return void
 */
	public function testBeforeRender() {
		$Article = ClassRegistry::init('Article');
		$Article->find('first', array('conditions' => array('Article.id' => 1)));

		$controller = new Controller();
		$result = $this->panel->beforeRender($controller);

		$this->assertTrue(isset($result['connections'][$Article->useDbConfig]));
		$this->assertTrue(isset($result['threshold']));
	}
}
