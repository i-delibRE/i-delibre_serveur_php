<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('DebugKitDebugger', 'DebugKit.Lib');

/**
 * Class TimedBehaviorTestCase
 *
 * @since         DebugKit 1.3
 */
class TimedBehaviorTestCase extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('core.article');

/**
 * Start Test callback
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Article = ClassRegistry::init('Article');
		$this->Article->Behaviors->attach('DebugKit.Timed');
	}

/**
 * End a test
 *
 * @return void
 */
	public function tearDown() {
		parent::tearDown();
		unset($this->Article);
		ClassRegistry::flush();
		DebugKitDebugger::clearTimers();
	}

/**
 * Test find timers
 *
 * @return void
 */
	public function testFindTimers() {
		$timers = DebugKitDebugger::getTimers(false);
		$this->assertEquals(count($timers), 1);

		$this->Article->find('all');
		$result = DebugKitDebugger::getTimers(false);
		$this->assertEquals(count($result), 2);

		$this->Article->find('all');
		$result = DebugKitDebugger::getTimers(false);
		$this->assertEquals(count($result), 3);
	}

/**
 * Test save timers
 *
 * @return void
 */
	public function testSaveTimers() {
		$timers = DebugKitDebugger::getTimers(false);
		$this->assertEquals(count($timers), 1);

		$this->Article->save(array('user_id' => 1, 'title' => 'test', 'body' => 'test'));
		$result = DebugKitDebugger::getTimers(false);
		$this->assertEquals(count($result), 2);
	}
}
