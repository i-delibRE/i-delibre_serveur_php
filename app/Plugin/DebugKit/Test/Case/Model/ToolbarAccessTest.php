<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('ToolbarAccess', 'DebugKit.Model');

/**
 * Test case for ToolbarAccess model
 *
 * @since         DebugKit 1.3
 */
class ToolbarAccessTestCase extends CakeTestCase {

/**
 * Included fixtures
 *
 * @var array
 */
	public $fixtures = array('core.post');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Model = new ToolbarAccess();
	}

/**
 * tearDown
 *
 * @return void
 */
	public function tearDown() {
		parent::tearDown();
		unset($this->Model);
	}

/**
 * test that explain query returns arrays of query information.
 *
 * @return void
 */
	public function testExplainQuery() {
		$Post = new CakeTestModel(array('table' => 'posts', 'alias' => 'Post'));
		$db = $Post->getDataSource();
		$sql = 'SELECT * FROM ' . $db->fullTableName('posts') . ';';
		$result = $this->Model->explainQuery($Post->useDbConfig, $sql);

		$this->assertTrue(is_array($result));
		$this->assertFalse(empty($result));
	}
}
