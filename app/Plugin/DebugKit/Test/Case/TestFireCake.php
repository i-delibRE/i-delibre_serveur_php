<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('FireCake', 'DebugKit.Lib');

/**
 * TestFireCake class allows for testing of FireCake
 *
 * @since         DebugKit 0.1
 */
class TestFireCake extends FireCake {

/**
 * Headers that were sent
 *
 * @var array
 */
	public $sentHeaders = array();

/**
 * Send header
 *
 * @param $name
 * @param $value
 */
	protected function _sendHeader($name, $value) {
		$_this = FireCake::getInstance();
		$_this->sentHeaders[$name] = $value;
	}

/**
 * Skip client detection as headers are not being sent.
 *
 * @return boolean Always true
 */
	public static function detectClientExtension() {
		return true;
	}

/**
 * Reset FireCake
 *
 * @return void
 */
	public static function reset() {
		$_this = FireCake::getInstance();
		$_this->sentHeaders = array();
		$_this->_messageIndex = 1;
	}
}
