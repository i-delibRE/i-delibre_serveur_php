<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

require_once dirname(__FILE__) . DS . 'DebugkitGroupTestCase.php';

/**
 * DebugKitViewTestSuite class
 *
 * @since         DebugKit 1.0
 */
class AllDebugKitViewTest extends DebugkitGroupTestCase {

/**
 * Assemble Test Suite
 *
 * @return PHPUnit_Framework_TestSuite the instance of PHPUnit_Framework_TestSuite
 */
	public static function suite() {
		$suite = new self;
		$files = $suite->getTestFiles('View');
		$suite->addTestFiles($files);

		return $suite;
	}
}
