<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
?>
<h2><?php echo __d('debug_kit', 'Session'); ?></h2>
<?php echo $this->Toolbar->makeNeatArray($content);
