<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
?>
<h2> <?php echo __d('debug_kit', 'View Variables'); ?></h2>
<?php
$content['$this->validationErrors'] = $this->validationErrors;
$content['Loaded Helpers'] = $this->Helpers->attached();
echo $this->Toolbar->makeNeatArray($content);
