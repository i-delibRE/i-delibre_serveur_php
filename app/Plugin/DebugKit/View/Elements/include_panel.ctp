<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
?>
<h2> <?php echo __d('debug_kit', 'Included Files'); ?></h2>

<h4>Include Paths</h4>
<?php
	foreach ($content['paths'] as $i => $path) {
		if (strstr($path, CAKE)) {
			$content['paths'][$i] = '-> ' . $path;
			break;
		}
	}
	echo $this->Toolbar->makeNeatArray(array_filter($content['paths']));
	unset($content['paths']);
?>

<h4>Included Files</h4>
<?php echo $this->Toolbar->makeNeatArray($content);