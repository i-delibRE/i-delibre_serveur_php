<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

$panels = array();
foreach ($toolbarState as $panelName => $panel) {
	if (!empty($panel) && !empty($panel['elementName'])) {
		$panels[$panelName] = $this->element($panel['elementName'], array(
			'content' => $panel['content']
		), array(
			'plugin' => Inflector::camelize($panel['plugin'])
		));
	}
}
echo json_encode($panels);
Configure::write('debug', 0);
