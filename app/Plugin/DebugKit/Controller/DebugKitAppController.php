<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

/**
 * Class DebugKitAppController
 *
 * @since         DebugKit 0.1
 */
class DebugKitAppController extends AppController {

}
