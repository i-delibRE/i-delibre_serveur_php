<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');

/**
 * Class DebugKitAppModel
 *
 * @since         DebugKit 0.1
 */
class DebugKitAppModel extends AppModel {

}
