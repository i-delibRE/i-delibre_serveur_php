<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * In this file you set up your database connection details.
 *
 * @package       cake.config
 */

/**
 * Database configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * datasource => The name of a supported datasource; valid options are as follows:
 * 		Database/Mysql 		- MySQL 4 & 5,
 * 		Database/Sqlite		- SQLite (PHP5 only),
 * 		Database/Postgres	- PostgreSQL 7 and higher,
 * 		Database/Sqlserver	- Microsoft SQL Server 2005 and higher
 *
 * You can add custom database datasources (or override existing datasources) by adding the
 * appropriate file to app/Model/Datasource/Database.  Datasources should be named 'MyDatasource.php',
 *
 *
 * persistent => true / false
 * Determines whether or not the database should use a persistent connection
 *
 * host =>
 * the host you connect to the database. To add a socket or port number, use 'port' => #
 *
 * prefix =>
 * Uses the given prefix for all the tables in this database.  This setting can be overridden
 * on a per-table basis with the Model::$tablePrefix property.
 *
 * schema =>
 * For Postgres specifies which schema you would like to use the tables in. Postgres defaults to 'public'.
 *
 * encoding =>
 * For MySQL, Postgres specifies the character encoding to use when connecting to the
 * database. Uses database default not specified.
 *
 * unix_socket =>
 * For MySQL to connect via socket specify the `unix_socket` parameter instead of `host` and `port`
 */
class DATABASE_CONFIG {

    /**
     * Template database
     *
     * @var type
     */
    public $template = array(
        'datasource' => 'Database/Postgres',
        'persistent' => false,
        'host' => 'db',
        'login' => 'idelibre',
        'password' => 'idelibre',
        'database' => 'idelibre_template',
        'prefix' => '',
        'port' => 5432,
        'encoding' => 'utf8',
        'inMenu' => false
    );



    /**
     * Admin database
     *
     * @var type
     */
    public $default = array(
        'datasource' => 'Database/Postgres',
        'persistent' => false,
        'host' => 'db',
        'login' => 'idelibre',
        'password' => 'idelibre',
        'database' => 'idelibre_admin',
        'prefix' => '',
        'port' => 5432,
        'encoding' => 'utf8',
        'inMenu' => false
    );

}
