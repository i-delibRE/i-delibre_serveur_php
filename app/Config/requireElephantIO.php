<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

require_once(APP . "Vendor/wisembly/elephant.io/src/Client.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/EngineInterface.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/AbstractPayload.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Exception/SocketException.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Exception/MalformedUrlException.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Exception/ServerConnectionFailureException.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Exception/UnsupportedActionException.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Exception/UnsupportedTransportException.php");

require_once(APP . "Vendor/wisembly/elephant.io/src/Engine/AbstractSocketIO.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Engine/SocketIO/Session.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Engine/SocketIO/Version0X.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Engine/SocketIO/Version1X.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Payload/Decoder.php");
require_once(APP . "Vendor/wisembly/elephant.io/src/Payload/Encoder.php");
