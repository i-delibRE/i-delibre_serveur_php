<?php



/**
 * idelibre.inc.php.default
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding	  UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 16:27:59 +0200 (lun. 21 oct. 2013) $
 * $Revision: 301 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Controller/TypesController.php $
 * $Id: TypesController.php 301 2013-10-21 14:27:59Z ssampaio $
 *
 */

/**
 * App Title
 */
define('MAINTITLE', "i-delibRE DEV");


define('VERSION', "3.1.0");

/**
 * Upload webservice config
 */
define('IDELIBRE_WSIMPORT_THEME_SEPARATOR', ',');

/**
 * Upload webservice test config
 */
//Url
define('IDELIBRE_CURL_WSTEST_DOMAIN', 'nginx'); //(nginx, localhost)
//Public key
define('IDELIBRE_CURL_WSTEST_HTTPS_CERTIFICATE', '');

//Files to upload
define('IDELIBRE_CURL_WSTEST_FILE_EXEMPLE', '/var/www/idelibre/convocation.pdf');
define('IDELIBRE_CURL_WSTEST_LARGE_FILE_100_EXEMPLE', '/var/www/idelibre/convocation.pdf');

//User credentials
define('IDELIBRE_CURL_WSTEST_USER', 'secretaire');
define('IDELIBRE_CURL_WSTEST_PASS', 'idelibre');
define('IDELIBRE_CURL_WSTEST_CONN', 'agglo');

/**
 * Timestamping
 *
 * OPENSIGN, IAIK, CRYPTOLOG
 */
//token prefix
define('IDELIBRE_SYNC_STORE_TOKEN_PREFIX', 'idelibre_');
//Timestamping server type
Configure::write('Horodatage.type', 'OPENSIGN');
//Timestamping url
Configure::write('Opensign.host', 'http://horodatage.services.adullact.org/opensign.wsdl');
Configure::write('IAIK.host', '');
Configure::write('CRYPTOLOG.host', '');

/**
 * Websocket client data config
 */
//proof token
define('IDELIBRE_WS_CDUUID', 'cdUUID');
//proof token monitor
define('IDELIBRE_WS_CDEBUGUUID', 'cDebugUUID');
//monitor logfile
define('IDELIBRE_WS_MONITOR_LOG', TMP . 'logs/ws-monitor.log');
//protocol
define('IDELIBRE_WS_PROTOCOL', 'http'); //http
//Url
define('IDELIBRE_WS_HOST', 'node');  //nodejs , localhost
//port
//define('IDELIBRE_WS_PORT', '8080'); //8080
define('IDELIBRE_WS_PORT', 8080); //8080

/**
 *  Gestion des mandats
 */
define('IDELIBRE_ADMIN_GROUP_ADMIN_ID', '545ba2a9-53e1-43e5-8958-32dec528e8a0'); //superAdmin
define('IDELIBRE_ADMIN_GROUP_USER_ID', 'cde697d6-46b6-434d-8a68-35f099d0d324');
define('IDELIBRE_ADMIN_GROUP_WALLETMANAGER_ID', '53c67c1d-da9c-411c-9fbe-4dda293c0839');
define('IDELIBRE_GROUP_ADMIN_ID', '26b380d8-93b2-41e9-9f68-da98c55ccb30'); // Admin
define('IDELIBRE_GROUP_AGENT_ID', 'b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16');  // Acteur
define('IDELIBRE_GROUP_USER_ID', '9f803280-eb21-46b3-bcec-74ffc4d2aab8');  // Secretaire


define('IDELIBRE_GROUP_ADMINISTRATIF_ID', 'db68a3c7-0119-40f1-b444-e96f568b3d67');  // administratif
define('IDELIBRE_GROUP_INVITE_ID', 'aa130693-e6d0-4893-ba31-98892b98581f');  // invite

define('DEFAULT_EMAIL_INVITATION', '5a0ea5a3-2240-46e1-ba36-53b55cde535b');


//Configuration de la connexion pour la cration auto des configs
define('DATABASE_HOST', 'db');
define('DATABASE_LOGIN', 'idelibre');
define('DATABASE_PASSWORD', 'idelibre');
define('DATABASE_PORT', 5432);

define('NODEJS_CONNECTIONS', '/var/www/socket/Connections/');


//passphrase webservice
define('PASSPHRASE', 'passphrase');

define('WORKSPACE','/data/workspace/');
define('TOKEN','/data/token/');
define('ZIP','/data/workspace/zip/');

/**
 * regle de comparaison de rang des themes N'a rien à faire ici !
 * @param type $a
 * @param type $b
 * @return boolean
 */
function compareRank($a, $b) {
    $valA = $a['themeRank'];
    $valB = $b['themeRank'];

    if ($valA > $valB) {
        return 1;
    } else {
        return -1;
    }
}


//debug
define('ADD_SEANCE', 'addSeance');
define('EDIT_SEANCE', 'editSeance');
define ('IMPORT', "import");
define('CONNECTEUR', "connecteur");





