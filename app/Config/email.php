<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * In this file you set up your send email details.
 *
 * @package       cake.config
 */

/**
 * Email configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * transport => The name of a supported transport; valid options are as follows:
 * 		Mail 		- Send using PHP mail function
 * 		Smtp		- Send using SMTP
 * 		Debug		- Do not send the email, just return the result
 *
 * You can add custom transports (or override existing transports) by adding the
 * appropriate file to app/Network/Email.  Transports should be named 'YourTransport.php',
 * where 'Your' is the name of the transport.
 *
 * from =>
 * The origin email. See CakeEmail::from() about the valid values
 *
 */
class EmailConfig {

	/**
	 *
	 * @var type
	 */
	public $default = array(
		'transport' => 'Mail',
		'from' => 'you@localhost',
			//'charset' => 'utf-8',
			//'headerCharset' => 'utf-8',
	);

	/**
	 *
	 * @var type
	 */
	public $convocation = array(
		'transport' => 'Smtp',
		'from' => 'ne-pas-repondre@idelibre-api.fr',
		//'replyTo' => 'noreply@collectivite',
		'host' => 'mail',
		'port' => 25,
		'timeout' => 30,
		// 'username' => '',
		// 'password' => '',
		'client' => 'smtp_helo_hostname',
		'log' => false,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8'
	);

}
