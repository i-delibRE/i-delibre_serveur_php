<?php

/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
//Router::connect('/', array('controller' => 'srvusers', 'action' => 'login'));

/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();


//REST routing

/**
 * HTTP format	URL.format				Controller action invoked
 *
  GET			/controller.format		ControllerController::index()
  GET			/controller/123.format 	ControllerController::view(123)
  POST			/controller.format		ControllerController::add()
  PUT			/controller/123.format 	ControllerController::edit(123)
  DELETE		/controller/123.format 	ControllerController::delete(123)
  POST			/controller/123.format 	ControllerController::edit(123)
 */
Router::mapResources('seances');
Router::parseExtensions();


/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
