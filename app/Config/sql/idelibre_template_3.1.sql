--
-- PostgreSQL database dump
--

-- Dumped from database version 10.2 (Debian 10.2-1.pgdg90+1)
-- Dumped by pg_dump version 10.2 (Debian 10.2-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- Name: deletedocument(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deletedocument() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  DELETE FROM  documents where documents.id = OLD.document_Id;
  RETURN null;
END;$$;


ALTER FUNCTION public.deletedocument() OWNER TO postgres;

--
-- Name: rebuilt_sequences(); Type: FUNCTION; Schema: public; Owner: idelibre
--

CREATE FUNCTION rebuilt_sequences() RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE sequencedefs RECORD; c integer ;
  BEGIN
    FOR sequencedefs IN Select
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.column_name as columnname,
      replace(replace(columns.column_default,'''::regclass)',''),'nextval(''','') as sequencename
      from information_schema.constraint_column_usage, information_schema.columns
      where constraint_column_usage.table_schema ='public' AND
      columns.table_schema = 'public' AND columns.table_name=constraint_column_usage.table_name
      AND constraint_column_usage.column_name = columns.column_name
      AND columns.column_default is not null
   LOOP
      EXECUTE 'select max('||sequencedefs.columnname||') from ' || sequencedefs.tablename INTO c;
      IF c is null THEN c = 0; END IF;
      IF c is not null THEN c = c+ 1; END IF;
      EXECUTE 'alter sequence ' || sequencedefs.sequencename ||' restart  with ' || c;
   END LOOP;

   RETURN 1; END;
$$;


ALTER FUNCTION public.rebuilt_sequences() OWNER TO idelibre;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acos; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE acos (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer,
    foreign_key uuid
);


ALTER TABLE acos OWNER TO idelibre;

--
-- Name: TABLE acos; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE acos IS 'Gestion des ACL (CakePHP) - ressources';


--
-- Name: acos_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acos_id_seq OWNER TO idelibre;

--
-- Name: acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE acos_id_seq OWNED BY acos.id;


--
-- Name: annexes; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE annexes (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(500) NOT NULL,
    size integer DEFAULT 0 NOT NULL,
    type character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    projet_id uuid,
    checksum character varying(32),
    rank integer DEFAULT 0 NOT NULL,
    path character varying
);


ALTER TABLE annexes OWNER TO idelibre;

--
-- Name: TABLE annexes; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE annexes IS 'Gestion des annexes de projets de délibérations (pièces jointes)';


--
-- Name: annotationv3; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE annotationv3 (
    id uuid NOT NULL,
    authorid uuid NOT NULL,
    authorname character varying,
    page integer,
    rect character varying,
    text character varying,
    projet_id uuid,
    seance_id uuid,
    annexe_id uuid,
    shareduseridlist character varying,
    date bigint
);


ALTER TABLE annotationv3 OWNER TO postgres;

--
-- Name: annotationv3_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE annotationv3_users (
    annotationv3_id uuid NOT NULL,
    user_id uuid NOT NULL,
    isread boolean
);


ALTER TABLE annotationv3_users OWNER TO postgres;

--
-- Name: aros; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE aros (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer,
    foreign_key uuid
);


ALTER TABLE aros OWNER TO idelibre;

--
-- Name: TABLE aros; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE aros IS 'Gestion des ACL (CakePHP) - demandeurs';


--
-- Name: aros_acos; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE aros_acos (
    id integer NOT NULL,
    aro_id integer NOT NULL,
    aco_id integer NOT NULL,
    _create character varying(2) DEFAULT '0'::character varying NOT NULL,
    _read character varying(2) DEFAULT '0'::character varying NOT NULL,
    _update character varying(2) DEFAULT '0'::character varying NOT NULL,
    _delete character varying(2) DEFAULT '0'::character varying NOT NULL
);


ALTER TABLE aros_acos OWNER TO idelibre;

--
-- Name: TABLE aros_acos; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE aros_acos IS 'Gestion des ACL (CakePHP) - liaisons demandeurs / ressources';


--
-- Name: aros_acos_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE aros_acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aros_acos_id_seq OWNER TO idelibre;

--
-- Name: aros_acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE aros_acos_id_seq OWNED BY aros_acos.id;


--
-- Name: aros_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE aros_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aros_id_seq OWNER TO idelibre;

--
-- Name: aros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE aros_id_seq OWNED BY aros.id;


--
-- Name: convocations; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE convocations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    seance_id uuid NOT NULL,
    user_id uuid NOT NULL,
    read boolean DEFAULT false NOT NULL,
    presence boolean DEFAULT true NOT NULL,
    delegation boolean DEFAULT false NOT NULL,
    procuration boolean DEFAULT false NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    ar_horodatage timestamp without time zone,
    active boolean DEFAULT false NOT NULL,
    ar_horodatage_file bytea,
    ar_horodatage_token bytea,
    ar_received timestamp without time zone,
    ae_sent timestamp without time zone,
    ae_horodatage timestamp without time zone,
    ae_horodatage_file bytea,
    ae_horodatage_token bytea,
    presentstatus character varying
);


ALTER TABLE convocations OWNER TO idelibre;

--
-- Name: TABLE convocations; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE convocations IS 'Gestion des convocations';


--
-- Name: documents; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE documents (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    size integer DEFAULT 0 NOT NULL,
    type character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    checksum character varying(32),
    path character varying
);


ALTER TABLE documents OWNER TO idelibre;

--
-- Name: TABLE documents; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE documents IS 'Gestion des documents';


--
-- Name: emailconvocations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE emailconvocations (
    id integer NOT NULL,
    sujet character varying(200) NOT NULL,
    contenu character varying(2000) NOT NULL,
    type_id uuid,
    name character varying(255)
);


ALTER TABLE emailconvocations OWNER TO postgres;

--
-- Name: emailconvocations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE emailconvocations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE emailconvocations_id_seq OWNER TO postgres;

--
-- Name: emailconvocations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE emailconvocations_id_seq OWNED BY emailconvocations.id;


--
-- Name: emailinvitations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE emailinvitations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    type_id uuid,
    name character varying(255) NOT NULL,
    content character varying(2000),
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    sujet character varying(500) NOT NULL
);


ALTER TABLE emailinvitations OWNER TO postgres;

--
-- Name: groupepolitiques; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE groupepolitiques (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE groupepolitiques OWNER TO postgres;

--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE groupepolitiques_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groupepolitiques_id_seq OWNER TO postgres;

--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE groupepolitiques_id_seq OWNED BY groupepolitiques.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE groups (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    admin boolean DEFAULT false NOT NULL,
    inseance boolean DEFAULT false,
    superuser boolean DEFAULT false,
    baseuser boolean DEFAULT true
);


ALTER TABLE groups OWNER TO idelibre;

--
-- Name: TABLE groups; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE groups IS 'Gestion des groupes';


--
-- Name: invitations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE invitations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    seance_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    document_id uuid NOT NULL,
    ae_horodatage timestamp without time zone,
    ar_horodatage timestamp without time zone,
    isactive boolean DEFAULT false,
    isread boolean DEFAULT false
);


ALTER TABLE invitations OWNER TO postgres;

--
-- Name: logacteurs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE logacteurs (
    id integer NOT NULL,
    user_id uuid,
    created timestamp without time zone
);


ALTER TABLE logacteurs OWNER TO postgres;

--
-- Name: logacteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE logacteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE logacteurs_id_seq OWNER TO postgres;

--
-- Name: logacteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE logacteurs_id_seq OWNED BY logacteurs.id;


--
-- Name: projets; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE projets (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(500) NOT NULL,
    theme character varying(500),
    document_id uuid,
    vote character varying(10),
    ptheme_id integer,
    seance_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    rank integer DEFAULT 0 NOT NULL,
    user_id uuid,
    CONSTRAINT projets_vote_check CHECK (((vote)::text = ANY (ARRAY[('oui'::character varying)::text, ('non'::character varying)::text, ('nsp'::character varying)::text, ('abs'::character varying)::text])))
);


ALTER TABLE projets OWNER TO idelibre;

--
-- Name: TABLE projets; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE projets IS 'Gestion des projets de délibérations';


--
-- Name: pthemes; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE pthemes (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    parent_id integer,
    rght integer NOT NULL,
    lft integer NOT NULL,
    rank character varying(5),
    fullname character varying(1000)
);


ALTER TABLE pthemes OWNER TO idelibre;

--
-- Name: TABLE pthemes; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE pthemes IS 'Gestion des themes de projets de délibération';


--
-- Name: pthemes_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE pthemes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pthemes_id_seq OWNER TO idelibre;

--
-- Name: pthemes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE pthemes_id_seq OWNED BY pthemes.id;


--
-- Name: seances; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE seances (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    date_seance timestamp without time zone NOT NULL,
    rev numeric DEFAULT 1 NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    type_id uuid,
    document_id uuid NOT NULL,
    alias character varying(255) NOT NULL,
    archive boolean DEFAULT false,
    place character varying(128)
);


ALTER TABLE seances OWNER TO idelibre;

--
-- Name: TABLE seances; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE seances IS 'Gestion des séances';


--
-- Name: types; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE types (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE types OWNER TO idelibre;

--
-- Name: TABLE types; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE types IS 'Gestion des types de séances';


--
-- Name: types_autorized; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE types_autorized (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    type_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE types_autorized OWNER TO postgres;

--
-- Name: types_users; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE types_users (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    type_id uuid NOT NULL,
    user_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE types_users OWNER TO idelibre;

--
-- Name: TABLE types_users; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE types_users IS 'Liaison entre les types de séances et les utilisateurs';


--
-- Name: users; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE users (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    last_login timestamp without time zone,
    group_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    active boolean DEFAULT true NOT NULL,
    last_logout timestamp without time zone,
    last_import timestamp without time zone,
    mail character varying(255),
    pwdmodified boolean DEFAULT false NOT NULL,
    rev integer,
    rev_date timestamp without time zone,
    seance_sync_date timestamp without time zone,
    groupepolitique_id integer,
    owner boolean DEFAULT true,
    naissance timestamp without time zone,
    civilite integer,
    titre character varying(255)
);


ALTER TABLE users OWNER TO idelibre;

--
-- Name: TABLE users; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE users IS 'Gestion des utilisateurs';


--
-- Name: acos id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY acos ALTER COLUMN id SET DEFAULT nextval('acos_id_seq'::regclass);


--
-- Name: aros id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros ALTER COLUMN id SET DEFAULT nextval('aros_id_seq'::regclass);


--
-- Name: aros_acos id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros_acos ALTER COLUMN id SET DEFAULT nextval('aros_acos_id_seq'::regclass);


--
-- Name: emailconvocations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations ALTER COLUMN id SET DEFAULT nextval('emailconvocations_id_seq'::regclass);


--
-- Name: groupepolitiques id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupepolitiques ALTER COLUMN id SET DEFAULT nextval('groupepolitiques_id_seq'::regclass);


--
-- Name: logacteurs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs ALTER COLUMN id SET DEFAULT nextval('logacteurs_id_seq'::regclass);


--
-- Name: pthemes id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY pthemes ALTER COLUMN id SET DEFAULT nextval('pthemes_id_seq'::regclass);


--
-- Data for Name: acos; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY acos (id, parent_id, model, alias, lft, rght, foreign_key) FROM stdin;
35	24	\N	writeLoginSession	67	68	\N
58	49	\N	ldebugHr	113	114	\N
3	2	\N	getFile	3	4	\N
36	24	\N	ldebug	69	70	\N
4	2	\N	getFileJson	5	6	\N
24	1	\N	Collectivites	46	73	\N
5	2	\N	setConn	7	8	\N
37	24	\N	ldebugHr	71	72	\N
6	2	\N	processLogin	9	10	\N
7	2	\N	writeLoginConfig	11	12	\N
96	87	\N	setConn	189	190	\N
8	2	\N	writeUserConfig	13	14	\N
75	73	\N	add	147	148	\N
39	38	\N	setRead	75	76	\N
9	2	\N	writeLoginSession	15	16	\N
60	59	\N	index	117	118	\N
10	2	\N	ldebug	17	18	\N
2	1	\N	Annexes	2	21	\N
11	2	\N	ldebugHr	19	20	\N
40	38	\N	send	77	78	\N
13	12	\N	ping	23	24	\N
41	38	\N	sendJson	79	80	\N
14	12	\N	version	25	26	\N
61	59	\N	getAllJson	119	120	\N
15	12	\N	check	27	28	\N
42	38	\N	setConn	81	82	\N
16	12	\N	pub	29	30	\N
76	73	\N	delete	149	150	\N
17	12	\N	setConn	31	32	\N
43	38	\N	processLogin	83	84	\N
18	12	\N	processLogin	33	34	\N
62	59	\N	getJson	121	122	\N
19	12	\N	writeLoginConfig	35	36	\N
44	38	\N	writeLoginConfig	85	86	\N
20	12	\N	writeUserConfig	37	38	\N
102	87	\N	ldebugHr	201	202	\N
21	12	\N	writeLoginSession	39	40	\N
45	38	\N	writeUserConfig	87	88	\N
22	12	\N	ldebug	41	42	\N
63	59	\N	delete	123	124	\N
12	1	\N	Api300	22	45	\N
23	12	\N	ldebugHr	43	44	\N
46	38	\N	writeLoginSession	89	90	\N
25	24	\N	index	47	48	\N
77	73	\N	edit	151	152	\N
47	38	\N	ldebug	91	92	\N
26	24	\N	add	49	50	\N
64	59	\N	edit	125	126	\N
27	24	\N	edit	51	52	\N
38	1	\N	Convocations	74	95	\N
48	38	\N	ldebugHr	93	94	\N
28	24	\N	delete	53	54	\N
29	24	\N	info	55	56	\N
30	24	\N	infoJson	57	58	\N
88	87	\N	getEmails	173	174	\N
65	59	\N	add	127	128	\N
31	24	\N	setConn	59	60	\N
50	49	\N	getFile	97	98	\N
32	24	\N	processLogin	61	62	\N
33	24	\N	writeLoginConfig	63	64	\N
78	73	\N	getAllJson	153	154	\N
66	59	\N	setConn	129	130	\N
34	24	\N	writeUserConfig	65	66	\N
51	49	\N	getFileJson	99	100	\N
97	87	\N	processLogin	191	192	\N
67	59	\N	processLogin	131	132	\N
52	49	\N	setConn	101	102	\N
53	49	\N	processLogin	103	104	\N
79	73	\N	getJson	155	156	\N
68	59	\N	writeLoginConfig	133	134	\N
54	49	\N	writeLoginConfig	105	106	\N
55	49	\N	writeUserConfig	107	108	\N
89	87	\N	sendEmail	175	176	\N
69	59	\N	writeUserConfig	135	136	\N
56	49	\N	writeLoginSession	109	110	\N
80	73	\N	setConn	157	158	\N
57	49	\N	ldebug	111	112	\N
49	1	\N	Documents	96	115	\N
110	103	\N	setConn	217	218	\N
70	59	\N	writeLoginSession	137	138	\N
81	73	\N	processLogin	159	160	\N
71	59	\N	ldebug	139	140	\N
90	87	\N	emailRappel	177	178	\N
59	1	\N	Emailconvocations	116	143	\N
72	59	\N	ldebugHr	141	142	\N
82	73	\N	writeLoginConfig	161	162	\N
98	87	\N	writeLoginConfig	193	194	\N
74	73	\N	index	145	146	\N
91	87	\N	publipostage	179	180	\N
83	73	\N	writeUserConfig	163	164	\N
107	103	\N	params	211	212	\N
84	73	\N	writeLoginSession	165	166	\N
92	87	\N	index	181	182	\N
85	73	\N	ldebug	167	168	\N
99	87	\N	writeUserConfig	195	196	\N
73	1	\N	Emailinvitations	144	171	\N
86	73	\N	ldebugHr	169	170	\N
93	87	\N	add	183	184	\N
104	103	\N	index	205	206	\N
100	87	\N	writeLoginSession	197	198	\N
94	87	\N	edit	185	186	\N
116	103	\N	ldebugHr	229	230	\N
95	87	\N	delete	187	188	\N
105	103	\N	menu	207	208	\N
108	103	\N	stats	213	214	\N
101	87	\N	ldebug	199	200	\N
112	103	\N	writeLoginConfig	221	222	\N
87	1	\N	Emailrappels	172	203	\N
111	103	\N	processLogin	219	220	\N
106	103	\N	goToClient	209	210	\N
109	103	\N	statsJson	215	216	\N
115	103	\N	ldebug	227	228	\N
114	103	\N	writeLoginSession	225	226	\N
113	103	\N	writeUserConfig	223	224	\N
103	1	\N	Environnements	204	231	\N
118	117	\N	check	233	234	\N
119	117	\N	setConn	235	236	\N
120	117	\N	processLogin	237	238	\N
121	117	\N	writeLoginConfig	239	240	\N
122	117	\N	writeUserConfig	241	242	\N
123	117	\N	writeLoginSession	243	244	\N
124	117	\N	ldebug	245	246	\N
117	1	\N	Essai	232	249	\N
125	117	\N	ldebugHr	247	248	\N
1	\N	\N	controllers	1	642	\N
127	126	\N	index	251	252	\N
128	126	\N	listJson	253	254	\N
129	126	\N	add	255	256	\N
126	1	\N	Groupepolitiques	250	279	\N
130	126	\N	edit	257	258	\N
131	126	\N	detailJson	259	260	\N
132	126	\N	delete	261	262	\N
133	126	\N	deleteJson	263	264	\N
134	126	\N	setConn	265	266	\N
135	126	\N	processLogin	267	268	\N
136	126	\N	writeLoginConfig	269	270	\N
137	126	\N	writeUserConfig	271	272	\N
138	126	\N	writeLoginSession	273	274	\N
139	126	\N	ldebug	275	276	\N
140	126	\N	ldebugHr	277	278	\N
177	173	\N	delete	351	352	\N
142	141	\N	index	281	282	\N
230	227	\N	searchByDate	457	458	\N
143	141	\N	import	283	284	\N
178	173	\N	getList	353	354	\N
144	141	\N	setConn	285	286	\N
145	141	\N	processLogin	287	288	\N
204	189	\N	edition	405	406	\N
146	141	\N	writeLoginConfig	289	290	\N
179	173	\N	getListJson	355	356	\N
147	141	\N	writeUserConfig	291	292	\N
148	141	\N	writeLoginSession	293	294	\N
149	141	\N	ldebug	295	296	\N
141	1	\N	Importusers	280	299	\N
150	141	\N	ldebugHr	297	298	\N
180	173	\N	getJson	357	358	\N
152	151	\N	sendJson	301	302	\N
220	189	\N	setConn	437	438	\N
153	151	\N	sendAdministratifsJson	303	304	\N
181	173	\N	rankit	359	360	\N
154	151	\N	sendInvitesJson	305	306	\N
155	151	\N	sendMail	307	308	\N
205	189	\N	getSeanceDataJson	407	408	\N
156	151	\N	prepareTypeInvitation	309	310	\N
182	173	\N	setConn	361	362	\N
157	151	\N	setConn	311	312	\N
158	151	\N	processLogin	313	314	\N
159	151	\N	writeLoginConfig	315	316	\N
183	173	\N	processLogin	363	364	\N
160	151	\N	writeUserConfig	317	318	\N
161	151	\N	writeLoginSession	319	320	\N
162	151	\N	ldebug	321	322	\N
151	1	\N	Invitations	300	325	\N
163	151	\N	ldebugHr	323	324	\N
184	173	\N	writeLoginConfig	365	366	\N
165	164	\N	display	327	328	\N
206	189	\N	sendConvocations	409	410	\N
166	164	\N	setConn	329	330	\N
185	173	\N	writeUserConfig	367	368	\N
167	164	\N	processLogin	331	332	\N
168	164	\N	writeLoginConfig	333	334	\N
169	164	\N	writeUserConfig	335	336	\N
186	173	\N	writeLoginSession	369	370	\N
170	164	\N	writeLoginSession	337	338	\N
171	164	\N	ldebug	339	340	\N
164	1	\N	Pages	326	343	\N
172	164	\N	ldebugHr	341	342	\N
187	173	\N	ldebug	371	372	\N
174	173	\N	index	345	346	\N
175	173	\N	add	347	348	\N
207	189	\N	sendConvocationsJson	411	412	\N
176	173	\N	edit	349	350	\N
173	1	\N	Pthemes	344	375	\N
188	173	\N	ldebugHr	373	374	\N
221	189	\N	processLogin	439	440	\N
208	189	\N	refreshSeance	413	414	\N
190	189	\N	index	377	378	\N
191	189	\N	view	379	380	\N
192	189	\N	add	381	382	\N
209	189	\N	archive	415	416	\N
193	189	\N	edit	383	384	\N
194	189	\N	delete	385	386	\N
236	227	\N	writeLoginSession	469	470	\N
195	189	\N	addUsersSeance	387	388	\N
210	189	\N	check	417	418	\N
196	189	\N	modifyUserPresence	389	390	\N
197	189	\N	ajoutnew	391	392	\N
222	189	\N	writeLoginConfig	441	442	\N
198	189	\N	ajout	393	394	\N
211	189	\N	deleteJson	419	420	\N
199	189	\N	ajoutJson	395	396	\N
200	189	\N	liste	397	398	\N
201	189	\N	listeJson	399	400	\N
212	189	\N	archiveJson	421	422	\N
202	189	\N	details	401	402	\N
231	227	\N	searchByTheme	459	460	\N
203	189	\N	suppression	403	404	\N
223	189	\N	writeUserConfig	443	444	\N
213	189	\N	printInfoUsers	423	424	\N
214	189	\N	ColoredTable	425	426	\N
215	189	\N	printInfoUsersPdf	427	428	\N
224	189	\N	writeLoginSession	445	446	\N
216	189	\N	getzipSeance	429	430	\N
217	189	\N	genPdfSeance	431	432	\N
218	189	\N	editJson	433	434	\N
232	227	\N	setConn	461	462	\N
219	189	\N	getTokenAEJson	435	436	\N
225	189	\N	ldebug	447	448	\N
189	1	\N	Seances	376	451	\N
226	189	\N	ldebugHr	449	450	\N
240	239	\N	index	477	478	\N
228	227	\N	index	453	454	\N
237	227	\N	ldebug	471	472	\N
229	227	\N	searchByType	455	456	\N
233	227	\N	processLogin	463	464	\N
227	1	\N	Search	452	475	\N
234	227	\N	writeLoginConfig	465	466	\N
235	227	\N	writeUserConfig	467	468	\N
238	227	\N	ldebugHr	473	474	\N
243	239	\N	editSuperAdmin	483	484	\N
241	239	\N	add	479	480	\N
242	239	\N	edit	481	482	\N
244	239	\N	delete	485	486	\N
245	239	\N	get_proof	487	488	\N
246	239	\N	login	489	490	\N
247	239	\N	logout	491	492	\N
248	239	\N	mdplost	493	494	\N
249	239	\N	setConn	495	496	\N
250	239	\N	processLogin	497	498	\N
251	239	\N	writeLoginConfig	499	500	\N
252	239	\N	writeUserConfig	501	502	\N
253	239	\N	writeLoginSession	503	504	\N
254	239	\N	ldebug	505	506	\N
239	1	\N	Srvusers	476	509	\N
255	239	\N	ldebugHr	507	508	\N
257	256	\N	users	511	512	\N
258	256	\N	lastSeances	513	514	\N
256	1	\N	Statistics	510	529	\N
259	256	\N	setConn	515	516	\N
260	256	\N	processLogin	517	518	\N
261	256	\N	writeLoginConfig	519	520	\N
262	256	\N	writeUserConfig	521	522	\N
263	256	\N	writeLoginSession	523	524	\N
264	256	\N	ldebug	525	526	\N
265	256	\N	ldebugHr	527	528	\N
306	292	\N	writeUserConfig	609	610	\N
267	266	\N	getToken	531	532	\N
268	266	\N	setConn	533	534	\N
307	292	\N	writeLoginSession	611	612	\N
269	266	\N	processLogin	535	536	\N
270	266	\N	writeLoginConfig	537	538	\N
271	266	\N	writeUserConfig	539	540	\N
308	292	\N	ldebug	613	614	\N
272	266	\N	writeLoginSession	541	542	\N
273	266	\N	ldebug	543	544	\N
266	1	\N	Tokens	530	547	\N
274	266	\N	ldebugHr	545	546	\N
292	1	\N	Users	582	617	\N
309	292	\N	ldebugHr	615	616	\N
276	275	\N	index	549	550	\N
277	275	\N	add	551	552	\N
310	1	\N	AclExtras	618	619	\N
278	275	\N	edit	553	554	\N
279	275	\N	delete	555	556	\N
280	275	\N	users	557	558	\N
281	275	\N	usersJson	559	560	\N
282	275	\N	listJson	561	562	\N
283	275	\N	deleteJson	563	564	\N
284	275	\N	detailJson	565	566	\N
285	275	\N	setConn	567	568	\N
286	275	\N	processLogin	569	570	\N
313	312	\N	history_state	622	623	\N
287	275	\N	writeLoginConfig	571	572	\N
288	275	\N	writeUserConfig	573	574	\N
289	275	\N	writeLoginSession	575	576	\N
290	275	\N	ldebug	577	578	\N
275	1	\N	Types	548	581	\N
291	275	\N	ldebugHr	579	580	\N
314	312	\N	sql_explain	624	625	\N
293	292	\N	index	583	584	\N
294	292	\N	listJson	585	586	\N
295	292	\N	add	587	588	\N
315	312	\N	setConn	626	627	\N
296	292	\N	edit	589	590	\N
297	292	\N	getUserJson	591	592	\N
298	292	\N	listActorsJson	593	594	\N
299	292	\N	delete	595	596	\N
316	312	\N	processLogin	628	629	\N
300	292	\N	login	597	598	\N
301	292	\N	logout	599	600	\N
302	292	\N	mdplost	601	602	\N
303	292	\N	setConn	603	604	\N
317	312	\N	writeLoginConfig	630	631	\N
304	292	\N	processLogin	605	606	\N
305	292	\N	writeLoginConfig	607	608	\N
318	312	\N	writeUserConfig	632	633	\N
319	312	\N	writeLoginSession	634	635	\N
320	312	\N	ldebug	636	637	\N
311	1	\N	DebugKit	620	641	\N
312	311	\N	ToolbarAccess	621	640	\N
321	312	\N	ldebugHr	638	639	\N
\.


--
-- Data for Name: annexes; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY annexes (id, name, size, type, created, modified, projet_id, checksum, rank, path) FROM stdin;
\.


--
-- Data for Name: annotationv3; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY annotationv3 (id, authorid, authorname, page, rect, text, projet_id, seance_id, annexe_id, shareduseridlist, date) FROM stdin;
\.


--
-- Data for Name: annotationv3_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY annotationv3_users (annotationv3_id, user_id, isread) FROM stdin;
\.


--
-- Data for Name: aros; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY aros (id, parent_id, model, alias, lft, rght, foreign_key) FROM stdin;
1	\N	Group	Administrateurs	1	4	26b380d8-93b2-41e9-9f68-da98c55ccb30
2	\N	Group	Utilisateurs	5	6	9f803280-eb21-46b3-bcec-74ffc4d2aab8
3	\N	Group	Acteur	7	8	b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16
4	\N	Group	Invites	9	10	aa130693-e6d0-4893-ba31-98892b98581f
5	\N	Group	Administratifs	11	12	db68a3c7-0119-40f1-b444-e96f568b3d67
6	1	User	Administrateur	2	3	e64e47b6-9027-4d73-a49b-70fd2ee0475e
\.


--
-- Data for Name: aros_acos; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY aros_acos (id, aro_id, aco_id, _create, _read, _update, _delete) FROM stdin;
1	1	29	1	1	1	1
2	1	103	1	1	1	1
3	1	50	1	1	1	1
4	1	51	1	1	1	1
5	1	3	1	1	1	1
6	1	4	1	1	1	1
7	1	38	1	1	1	1
8	1	189	1	1	1	1
9	1	173	1	1	1	1
10	1	275	1	1	1	1
11	1	292	1	1	1	1
12	1	59	1	1	1	1
13	1	126	1	1	1	1
14	2	103	1	1	1	1
15	2	198	1	1	1	1
16	2	200	1	1	1	1
17	2	202	1	1	1	1
18	2	204	1	1	1	1
19	2	203	1	1	1	1
20	2	206	1	1	1	1
21	2	38	1	1	1	1
22	2	295	1	1	1	1
23	2	173	1	1	1	1
24	2	275	1	1	1	1
25	2	227	1	1	1	1
26	2	209	1	1	1	1
27	2	189	1	1	1	1
28	2	50	1	1	1	1
29	2	51	1	1	1	1
30	2	3	1	1	1	1
31	2	4	1	1	1	1
32	3	103	1	1	1	1
33	3	189	1	1	1	1
34	3	38	1	1	1	1
35	3	50	1	1	1	1
36	3	3	1	1	1	1
37	4	103	1	1	1	1
38	4	189	1	1	1	1
39	4	38	1	1	1	1
40	4	50	1	1	1	1
41	4	3	1	1	1	1
42	5	103	1	1	1	1
43	5	189	1	1	1	1
44	5	38	1	1	1	1
45	5	50	1	1	1	1
46	5	3	1	1	1	1
\.


--
-- Data for Name: convocations; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY convocations (id, seance_id, user_id, read, presence, delegation, procuration, created, modified, ar_horodatage, active, ar_horodatage_file, ar_horodatage_token, ar_received, ae_sent, ae_horodatage, ae_horodatage_file, ae_horodatage_token, presentstatus) FROM stdin;
\.


--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY documents (id, name, size, type, created, modified, checksum, path) FROM stdin;
\.


--
-- Data for Name: emailconvocations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY emailconvocations (id, sujet, contenu, type_id, name) FROM stdin;
1	convocation pour la seance #typeseance# le #dateseance# à #heureseance#	#prenom# #nom#,\n\nvous avez reçu une convocation pour la seance #typeseance# le #dateseance# à #heureseance#.\nVeuillez vous connecter sur votre application i-delibRE pour en prendre connaissance.	\N	\N
\.


--
-- Data for Name: emailinvitations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY emailinvitations (id, type_id, name, content, created, modified, sujet) FROM stdin;
\.


--
-- Data for Name: groupepolitiques; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY groupepolitiques (id, name) FROM stdin;
1	sans étiquette
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY groups (id, name, created, modified, admin, inseance, superuser, baseuser) FROM stdin;
26b380d8-93b2-41e9-9f68-da98c55ccb30	Administrateurs	2013-02-11 11:21:45.788881	2013-06-13 17:19:10	t	f	f	f
9f803280-eb21-46b3-bcec-74ffc4d2aab8	Utilisateurs	2013-02-11 11:21:45.832553	2013-06-26 13:36:49	f	f	t	f
b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16	Acteur	2013-02-11 11:21:45.805519	2013-06-26 13:36:31	f	t	f	t
aa130693-e6d0-4893-ba31-98892b98581f	Invites	\N	\N	f	f	f	t
db68a3c7-0119-40f1-b444-e96f568b3d67	Administratifs	\N	\N	f	f	f	t
\.


--
-- Data for Name: invitations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY invitations (id, user_id, seance_id, created, modified, document_id, ae_horodatage, ar_horodatage, isactive, isread) FROM stdin;
\.


--
-- Data for Name: logacteurs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY logacteurs (id, user_id, created) FROM stdin;
\.


--
-- Data for Name: projets; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY projets (id, name, theme, document_id, vote, ptheme_id, seance_id, created, modified, rank, user_id) FROM stdin;
\.


--
-- Data for Name: pthemes; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY pthemes (id, name, parent_id, rght, lft, rank, fullname) FROM stdin;
\.


--
-- Data for Name: seances; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY seances (id, name, date_seance, rev, created, modified, type_id, document_id, alias, archive, place) FROM stdin;
\.


--
-- Data for Name: types; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY types (id, name, created, modified) FROM stdin;
\.


--
-- Data for Name: types_autorized; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY types_autorized (id, user_id, type_id, created, modified) FROM stdin;
\.


--
-- Data for Name: types_users; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY types_users (id, type_id, user_id, created, modified) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY users (id, firstname, lastname, username, password, last_login, group_id, created, modified, active, last_logout, last_import, mail, pwdmodified, rev, rev_date, seance_sync_date, groupepolitique_id, owner, naissance, civilite, titre) FROM stdin;
e64e47b6-9027-4d73-a49b-70fd2ee0475e	Administrateur	Administrateur	Administrateur	mot de pass non valide	\N	26b380d8-93b2-41e9-9f68-da98c55ccb30	2013-11-07 15:54:40.942895	2013-11-07 15:54:40.942895	t	\N	\N	\N	f	\N	\N	\N	\N	t	\N	\N	\N
\.


--
-- Name: acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('acos_id_seq', 321, true);


--
-- Name: aros_acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('aros_acos_id_seq', 46, true);


--
-- Name: aros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('aros_id_seq', 6, true);


--
-- Name: emailconvocations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('emailconvocations_id_seq', 1, true);


--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('groupepolitiques_id_seq', 1, true);


--
-- Name: logacteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('logacteurs_id_seq', 1, false);


--
-- Name: pthemes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('pthemes_id_seq', 1, true);


--
-- Name: acos acos_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY acos
    ADD CONSTRAINT acos_pkey PRIMARY KEY (id);


--
-- Name: annexes annexes_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY annexes
    ADD CONSTRAINT annexes_pkey PRIMARY KEY (id);


--
-- Name: annotationv3 annotationv3_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3
    ADD CONSTRAINT annotationv3_pkey PRIMARY KEY (id);


--
-- Name: aros_acos aros_acos_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros_acos
    ADD CONSTRAINT aros_acos_pkey PRIMARY KEY (id);


--
-- Name: aros aros_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros
    ADD CONSTRAINT aros_pkey PRIMARY KEY (id);


--
-- Name: seances convocations_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT convocations_pkey PRIMARY KEY (id);


--
-- Name: convocations convocusers_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_pkey PRIMARY KEY (id);


--
-- Name: documents documents_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: emailconvocations emailconvocations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations
    ADD CONSTRAINT emailconvocations_pkey PRIMARY KEY (id);


--
-- Name: emailconvocations emailconvocations_unique_type_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations
    ADD CONSTRAINT emailconvocations_unique_type_id UNIQUE (type_id);


--
-- Name: groupepolitiques groupepolitiques_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupepolitiques
    ADD CONSTRAINT groupepolitiques_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: logacteurs logacteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs
    ADD CONSTRAINT logacteurs_pkey PRIMARY KEY (id);


--
-- Name: groups name_unique_idx; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT name_unique_idx UNIQUE (name);


--
-- Name: projets projets_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_pkey PRIMARY KEY (id);


--
-- Name: pthemes pthemes_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY pthemes
    ADD CONSTRAINT pthemes_pkey PRIMARY KEY (id);


--
-- Name: seances seances_alias_key; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT seances_alias_key UNIQUE (alias);


--
-- Name: types types_name_unique_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_name_unique_pkey UNIQUE (name);


--
-- Name: types types_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_pkey PRIMARY KEY (id);


--
-- Name: types_users types_users_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_pkey PRIMARY KEY (id);


--
-- Name: users user_username_unique_key; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT user_username_unique_key UNIQUE (username);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: aro_aco_key; Type: INDEX; Schema: public; Owner: idelibre
--

CREATE UNIQUE INDEX aro_aco_key ON aros_acos USING btree (aro_id, aco_id);


--
-- Name: invitations deleteinvitationtrigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER deleteinvitationtrigger AFTER DELETE ON invitations FOR EACH ROW EXECUTE PROCEDURE deletedocument();


--
-- Name: projets deleteprojettrigger; Type: TRIGGER; Schema: public; Owner: idelibre
--

CREATE TRIGGER deleteprojettrigger AFTER DELETE ON projets FOR EACH ROW EXECUTE PROCEDURE deletedocument();


--
-- Name: seances deleteseancetrigger; Type: TRIGGER; Schema: public; Owner: idelibre
--

CREATE TRIGGER deleteseancetrigger AFTER DELETE ON seances FOR EACH ROW EXECUTE PROCEDURE deletedocument();


--
-- Name: annexes annexes_projet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY annexes
    ADD CONSTRAINT annexes_projet_id_fkey FOREIGN KEY (projet_id) REFERENCES projets(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: annotationv3_users annotationv3_users_annotationv3_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3_users
    ADD CONSTRAINT annotationv3_users_annotationv3_id_fkey FOREIGN KEY (annotationv3_id) REFERENCES annotationv3(id) ON DELETE CASCADE;


--
-- Name: annotationv3_users annotationv3_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3_users
    ADD CONSTRAINT annotationv3_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: seances convocations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT convocations_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: convocations convocusers_convocation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_convocation_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: convocations convocusers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: emailconvocations emailconvocations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations
    ADD CONSTRAINT emailconvocations_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON DELETE SET NULL;


--
-- Name: emailinvitations emailinvitations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailinvitations
    ADD CONSTRAINT emailinvitations_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id);


--
-- Name: invitations invitations_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON DELETE CASCADE;


--
-- Name: invitations invitations_seance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_seance_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON DELETE CASCADE;


--
-- Name: invitations invitations_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: logacteurs logacteurs_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs
    ADD CONSTRAINT logacteurs_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: projets projets_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: projets projets_ptheme_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_ptheme_id_fkey FOREIGN KEY (ptheme_id) REFERENCES pthemes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: projets projets_seance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_seance_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: projets projets_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: seances seances_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT seances_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: types_autorized types_autorized_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY types_autorized
    ADD CONSTRAINT types_autorized_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON DELETE CASCADE;


--
-- Name: types_autorized types_autorized_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY types_autorized
    ADD CONSTRAINT types_autorized_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: types_users types_users_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: types_users types_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_groupepolitique_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_groupepolitique_id_fkey FOREIGN KEY (groupepolitique_id) REFERENCES groupepolitiques(id) ON DELETE SET NULL;




alter table convocations add istoken boolean default false;
alter table convocations add isemailed boolean default false;

--
-- PostgreSQL database dump complete
--

