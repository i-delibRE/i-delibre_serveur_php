CREATE TABLE zip_codes (
  id varchar(255),
  nom_voie varchar(1024),
  numero varchar(255),
  rep varchar(255),
  code_insee varchar(255),
  code_post varchar(255),
  nom_ld varchar(255),
  x varchar(255),
  y varchar(255),
  lon varchar(255),
  lat varchar(255),
  nom_commune varchar(255)
);