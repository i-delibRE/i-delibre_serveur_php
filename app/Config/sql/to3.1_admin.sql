-- dans idelibre_admin
-- Détruire tous les utilisateurs qui ne sont pas superadmin
delete from srvusers where srvrole_id != '545ba2a9-53e1-43e5-8958-32dec528e8a0';

-- Détruire les roles autre que superadmin
delete from srvroles where id != '545ba2a9-53e1-43e5-8958-32dec528e8a0';

--delete mandats
delete from mandats;
delete from srvusers_mandats;

--drop table mandats
drop table srvusers_mandats;
drop table mandats;
