
alter table documents add path varchar;
alter table annexes add path varchar;


create table annotationv3 (
id uuid not null,
authorId uuid not null, 
authorName varchar not null,
page integer,
rect varchar,
date bigint,
text varchar,
projet_id uuid,
seance_id uuid,
annexe_id uuid,    -- add foreign key
sharedUserIdList varchar
);

ALTER TABLE annotationv3 ADD CONSTRAINT annotationv3_pkey PRIMARY KEY (id);




create table annotationv3_users (
annotationv3_id uuid not null,
user_id uuid not null,
isread boolean 
);


ALTER TABLE annotationv3_users ADD  FOREIGN KEY(user_id) REFERENCES users(id) on delete cascade;
ALTER TABLE annotationv3_users ADD  FOREIGN KEY(annotationv3_id) REFERENCES annotationv3(id) on delete cascade;



--add presenceStatus
alter table convocations add  presentStatus varchar default null;





