--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;


--
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cake_sessions; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE cake_sessions (
    id character varying(255) NOT NULL,
    data text,
    expires integer
);


ALTER TABLE cake_sessions OWNER TO idelibre;

--
-- Name: collectivites; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE collectivites (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    conn character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    active boolean DEFAULT false NOT NULL,
    login_suffix character varying(255),
    use_cert boolean DEFAULT false NOT NULL,
    public_key bytea,
    CONSTRAINT collectivites_use_cert_chk CHECK ((((use_cert = false) AND (login_suffix IS NOT NULL)) OR ((use_cert = true) AND (public_key IS NOT NULL))))
);


ALTER TABLE collectivites OWNER TO idelibre;

--
-- Name: collectivites_srvusers; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE collectivites_srvusers (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    srvuser_id uuid NOT NULL,
    collectivite_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE collectivites_srvusers OWNER TO idelibre;

--
-- Name: mandats; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE mandats (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    collectivite_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE mandats OWNER TO idelibre;

--
-- Name: srvroles; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE srvroles (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE srvroles OWNER TO idelibre;

--
-- Name: srvusers; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE srvusers (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    last_login timestamp without time zone,
    srvrole_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    active boolean DEFAULT true NOT NULL,
    last_logout timestamp without time zone,
    mail character varying(255),
    lang character varying(10),
    naissance timestamp without time zone,
    rev integer DEFAULT 1 NOT NULL
);


ALTER TABLE srvusers OWNER TO idelibre;

--
-- Name: srvusers_mandats; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE srvusers_mandats (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    srvuser_id uuid,
    mandat_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE srvusers_mandats OWNER TO idelibre;

--
-- Data for Name: cake_sessions; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: collectivites; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: collectivites_srvusers; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: mandats; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: srvroles; Type: TABLE DATA; Schema: public; Owner: idelibre
--

INSERT INTO srvroles VALUES ('545ba2a9-53e1-43e5-8958-32dec528e8a0', 'Super administrateur', '2013-02-12 11:13:21.47237', '2013-02-12 11:13:21.47237');
INSERT INTO srvroles VALUES ('53c67c1d-da9c-411c-9fbe-4dda293c0839', 'Gestionnaire de mandats', '2014-07-16 15:20:29.13254', '2014-07-24 16:37:18');
INSERT INTO srvroles VALUES ('cde697d6-46b6-434d-8a68-35f099d0d324', 'Porte-feuille de mandat', '2013-02-12 11:13:27.41644', '2014-07-30 11:24:40');


--
-- Data for Name: srvusers; Type: TABLE DATA; Schema: public; Owner: idelibre
--

INSERT INTO srvusers VALUES ('e98219d4-1560-4e4b-8c77-efbc9c446068', 'Administrateur', 'Superadmin', 'superadmin', 'c49878922f7d75c9d6e3ea8098c702b97e170a4b', NULL, '545ba2a9-53e1-43e5-8958-32dec528e8a0', NULL, NULL, true, NULL, NULL, NULL, NULL, 1);


--
-- Data for Name: srvusers_mandats; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Name: cake_sessions cake_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY cake_sessions
    ADD CONSTRAINT cake_sessions_pkey PRIMARY KEY (id);


--
-- Name: collectivites collectivites_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY collectivites
    ADD CONSTRAINT collectivites_pkey PRIMARY KEY (id);


--
-- Name: collectivites_srvusers collectivites_srvusers_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY collectivites_srvusers
    ADD CONSTRAINT collectivites_srvusers_pkey PRIMARY KEY (id);


--
-- Name: collectivites conn_unique_idx; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY collectivites
    ADD CONSTRAINT conn_unique_idx UNIQUE (conn);


--
-- Name: mandats mandats_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY mandats
    ADD CONSTRAINT mandats_pkey PRIMARY KEY (id);


--
-- Name: collectivites name_unique_idx; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY collectivites
    ADD CONSTRAINT name_unique_idx UNIQUE (name);


--
-- Name: srvroles srvroles_name_unique_key; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvroles
    ADD CONSTRAINT srvroles_name_unique_key UNIQUE (name);


--
-- Name: srvroles srvroles_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvroles
    ADD CONSTRAINT srvroles_pkey PRIMARY KEY (id);


--
-- Name: srvusers_mandats srvusers_mandats_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvusers_mandats
    ADD CONSTRAINT srvusers_mandats_pkey PRIMARY KEY (id);


--
-- Name: srvusers srvusers_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvusers
    ADD CONSTRAINT srvusers_pkey PRIMARY KEY (id);


--
-- Name: srvusers srvusers_username_unique_key; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvusers
    ADD CONSTRAINT srvusers_username_unique_key UNIQUE (username);


--
-- Name: login_suffix_unique_idx; Type: INDEX; Schema: public; Owner: idelibre
--

CREATE UNIQUE INDEX login_suffix_unique_idx ON collectivites USING btree (login_suffix) WHERE ((login_suffix)::text <> ''::text);


--
-- Name: public_key_unique_idx; Type: INDEX; Schema: public; Owner: idelibre
--

CREATE UNIQUE INDEX public_key_unique_idx ON collectivites USING btree (public_key) WHERE (public_key <> '\x'::bytea);


--
-- Name: collectivites_srvusers collectivites_srvusers_collectivite_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY collectivites_srvusers
    ADD CONSTRAINT collectivites_srvusers_collectivite_id_fkey FOREIGN KEY (collectivite_id) REFERENCES collectivites(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: collectivites_srvusers collectivites_srvusers_srvuser_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY collectivites_srvusers
    ADD CONSTRAINT collectivites_srvusers_srvuser_id_fkey FOREIGN KEY (srvuser_id) REFERENCES srvusers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: srvusers_mandats srvusers_mandats_mandat_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvusers_mandats
    ADD CONSTRAINT srvusers_mandats_mandat_id_fkey FOREIGN KEY (mandat_id) REFERENCES mandats(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: srvusers_mandats srvusers_mandats_srvuser_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvusers_mandats
    ADD CONSTRAINT srvusers_mandats_srvuser_id_fkey FOREIGN KEY (srvuser_id) REFERENCES srvusers(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: srvusers srvusers_srvrole_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY srvusers
    ADD CONSTRAINT srvusers_srvrole_id_fkey FOREIGN KEY (srvrole_id) REFERENCES srvroles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


ALTER TABLE srvusers ADD bcrypt VARCHAR(255);


delete FROM  srvroles where id='53c67c1d-da9c-411c-9fbe-4dda293c0839';
delete FROM  srvroles where id='cde697d6-46b6-434d-8a68-35f099d0d324';

delete from srvroles where id ='53c67c1d-da9c-411c-9fbe-4dda293c0839';

delete from srvroles where id ='cde697d6-46b6-434d-8a68-35f099d0d324';

