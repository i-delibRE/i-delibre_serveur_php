--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- Name: rebuilt_sequences(); Type: FUNCTION; Schema: public; Owner: idelibre
--

CREATE FUNCTION rebuilt_sequences() RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE sequencedefs RECORD; c integer ;
  BEGIN
    FOR sequencedefs IN Select
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.column_name as columnname,
      replace(replace(columns.column_default,'''::regclass)',''),'nextval(''','') as sequencename
      from information_schema.constraint_column_usage, information_schema.columns
      where constraint_column_usage.table_schema ='public' AND
      columns.table_schema = 'public' AND columns.table_name=constraint_column_usage.table_name
      AND constraint_column_usage.column_name = columns.column_name
      AND columns.column_default is not null
   LOOP
      EXECUTE 'select max('||sequencedefs.columnname||') from ' || sequencedefs.tablename INTO c;
      IF c is null THEN c = 0; END IF;
      IF c is not null THEN c = c+ 1; END IF;
      EXECUTE 'alter sequence ' || sequencedefs.sequencename ||' restart  with ' || c;
   END LOOP;

   RETURN 1; END;
$$;


ALTER FUNCTION public.rebuilt_sequences() OWNER TO idelibre;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acos; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE acos (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer,
    foreign_key uuid
);


ALTER TABLE public.acos OWNER TO idelibre;

--
-- Name: TABLE acos; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE acos IS 'Gestion des ACL (CakePHP) - ressources';


--
-- Name: acos_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.acos_id_seq OWNER TO idelibre;

--
-- Name: acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE acos_id_seq OWNED BY acos.id;


--
-- Name: annexes; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE annexes (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(500) NOT NULL,
    size integer DEFAULT 0 NOT NULL,
    type character varying(255) NOT NULL,
    content bytea,
    created timestamp without time zone,
    modified timestamp without time zone,
    projet_id uuid,
    checksum character varying(32),
    rank integer DEFAULT 0 NOT NULL,
    path character varying
);


ALTER TABLE public.annexes OWNER TO idelibre;

--
-- Name: TABLE annexes; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE annexes IS 'Gestion des annexes de projets de délibérations (pièces jointes)';


--
-- Name: annotactions; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE annotactions (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    deltarevision_id uuid,
    created timestamp without time zone,
    modified timestamp without time zone,
    type character varying(1),
    annotation_id character varying(100),
    CONSTRAINT annotactions_type_in_chk CHECK (((type)::text = ANY (ARRAY[('C'::character varying)::text, ('U'::character varying)::text, ('D'::character varying)::text])))
);


ALTER TABLE public.annotactions OWNER TO idelibre;

--
-- Name: annotations; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE annotations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    annot_client_id uuid,
    imgpage_id uuid NOT NULL,
    content bytea NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    user_id uuid NOT NULL,
    shared boolean DEFAULT false NOT NULL,
    rank integer DEFAULT 0 NOT NULL,
    date timestamp without time zone,
    date_modif timestamp without time zone,
    height character varying(20),
    width character varying(20),
    x character varying(20),
    y character varying(20),
    comment character varying(255)
);


ALTER TABLE public.annotations OWNER TO idelibre;

--
-- Name: annotations_users; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE annotations_users (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    annotation_id uuid NOT NULL,
    read boolean DEFAULT false NOT NULL
);


ALTER TABLE public.annotations_users OWNER TO idelibre;

--
-- Name: annotationv3; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE annotationv3 (
    id uuid NOT NULL,
    authorid uuid NOT NULL,
    authorname character varying,
    page integer,
    rect character varying,
    text character varying,
    projet_id uuid,
    seance_id uuid,
    annexe_id uuid,
    shareduseridlist character varying,
    date bigint
);


ALTER TABLE public.annotationv3 OWNER TO postgres;

--
-- Name: annotationv3_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE annotationv3_users (
    annotationv3_id uuid NOT NULL,
    user_id uuid NOT NULL,
    isread boolean
);


ALTER TABLE public.annotationv3_users OWNER TO postgres;

--
-- Name: annots; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE annots (
    id bigint NOT NULL,
    projet_id uuid NOT NULL,
    liste_partage character varying(10000),
    json character varying(100000) NOT NULL
);


ALTER TABLE public.annots OWNER TO postgres;

--
-- Name: annots_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE annots_users (
    id integer NOT NULL,
    user_id uuid NOT NULL,
    annot_id bigint NOT NULL
);


ALTER TABLE public.annots_users OWNER TO postgres;

--
-- Name: annots_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE annots_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.annots_users_id_seq OWNER TO postgres;

--
-- Name: annots_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE annots_users_id_seq OWNED BY annots_users.id;


--
-- Name: aros; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE aros (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer,
    foreign_key uuid
);


ALTER TABLE public.aros OWNER TO idelibre;

--
-- Name: TABLE aros; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE aros IS 'Gestion des ACL (CakePHP) - demandeurs';


--
-- Name: aros_acos; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE aros_acos (
    id integer NOT NULL,
    aro_id integer NOT NULL,
    aco_id integer NOT NULL,
    _create character varying(2) DEFAULT '0'::character varying NOT NULL,
    _read character varying(2) DEFAULT '0'::character varying NOT NULL,
    _update character varying(2) DEFAULT '0'::character varying NOT NULL,
    _delete character varying(2) DEFAULT '0'::character varying NOT NULL
);


ALTER TABLE public.aros_acos OWNER TO idelibre;

--
-- Name: TABLE aros_acos; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE aros_acos IS 'Gestion des ACL (CakePHP) - liaisons demandeurs / ressources';


--
-- Name: aros_acos_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE aros_acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aros_acos_id_seq OWNER TO idelibre;

--
-- Name: aros_acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE aros_acos_id_seq OWNED BY aros_acos.id;


--
-- Name: aros_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE aros_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aros_id_seq OWNER TO idelibre;

--
-- Name: aros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE aros_id_seq OWNED BY aros.id;


--
-- Name: convocations; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE convocations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    seance_id uuid NOT NULL,
    user_id uuid NOT NULL,
    read boolean DEFAULT false NOT NULL,
    presence boolean DEFAULT true NOT NULL,
    delegation boolean DEFAULT false NOT NULL,
    procuration boolean DEFAULT false NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    ar_horodatage timestamp without time zone,
    active boolean DEFAULT false NOT NULL,
    ar_horodatage_file bytea,
    ar_horodatage_token bytea,
    ar_received timestamp without time zone,
    ae_sent timestamp without time zone,
    ae_horodatage timestamp without time zone,
    ae_horodatage_file bytea,
    ae_horodatage_token bytea,
    presentstatus character varying
);


ALTER TABLE public.convocations OWNER TO idelibre;

--
-- Name: TABLE convocations; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE convocations IS 'Gestion des convocations';


--
-- Name: deltarevisions; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE deltarevisions (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    num integer NOT NULL,
    delta bytea NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    user_id uuid NOT NULL
);


ALTER TABLE public.deltarevisions OWNER TO idelibre;

--
-- Name: deltarevisions_num_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE deltarevisions_num_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.deltarevisions_num_seq OWNER TO idelibre;

--
-- Name: deltarevisions_num_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE deltarevisions_num_seq OWNED BY deltarevisions.num;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE documents (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    size integer DEFAULT 0 NOT NULL,
    type character varying(255) NOT NULL,
    content bytea,
    created timestamp without time zone,
    modified timestamp without time zone,
    checksum character varying(32),
    path character varying
);


ALTER TABLE public.documents OWNER TO idelibre;

--
-- Name: TABLE documents; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE documents IS 'Gestion des documents';


--
-- Name: emailconvocations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE emailconvocations (
    id integer NOT NULL,
    sujet character varying(200) NOT NULL,
    contenu character varying(2000) NOT NULL
);


ALTER TABLE public.emailconvocations OWNER TO postgres;

--
-- Name: emailconvocations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE emailconvocations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emailconvocations_id_seq OWNER TO postgres;

--
-- Name: emailconvocations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE emailconvocations_id_seq OWNED BY emailconvocations.id;


--
-- Name: groupepolitiques; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE groupepolitiques (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE public.groupepolitiques OWNER TO postgres;

--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE groupepolitiques_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groupepolitiques_id_seq OWNER TO postgres;

--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE groupepolitiques_id_seq OWNED BY groupepolitiques.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE groups (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    admin boolean DEFAULT false NOT NULL,
    inseance boolean DEFAULT false,
    superuser boolean DEFAULT false,
    baseuser boolean DEFAULT true
);


ALTER TABLE public.groups OWNER TO idelibre;

--
-- Name: TABLE groups; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE groups IS 'Gestion des groupes';


--
-- Name: imgpages; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE imgpages (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    size integer DEFAULT 0 NOT NULL,
    type character varying(255) NOT NULL,
    content bytea,
    imgpagenum integer DEFAULT 0 NOT NULL,
    document_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    checksum character varying(32)
);


ALTER TABLE public.imgpages OWNER TO idelibre;

--
-- Name: TABLE imgpages; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE imgpages IS 'Gestion des pages des documents à annoter';


--
-- Name: logacteurs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE logacteurs (
    id integer NOT NULL,
    user_id uuid,
    created timestamp without time zone
);


ALTER TABLE public.logacteurs OWNER TO postgres;

--
-- Name: logacteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE logacteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logacteurs_id_seq OWNER TO postgres;

--
-- Name: logacteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE logacteurs_id_seq OWNED BY logacteurs.id;


--
-- Name: pdfdatas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pdfdatas (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    document_id uuid,
    data bytea,
    part integer
);


ALTER TABLE public.pdfdatas OWNER TO postgres;

--
-- Name: projets; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE projets (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(500) NOT NULL,
    theme character varying(500),
    document_id uuid,
    vote character varying(10),
    ptheme_id integer,
    seance_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    rank integer DEFAULT 0 NOT NULL,
    user_id uuid,
    CONSTRAINT projets_vote_check CHECK (((vote)::text = ANY (ARRAY[('oui'::character varying)::text, ('non'::character varying)::text, ('nsp'::character varying)::text, ('abs'::character varying)::text])))
);


ALTER TABLE public.projets OWNER TO idelibre;

--
-- Name: TABLE projets; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE projets IS 'Gestion des projets de délibérations';


--
-- Name: pthemes; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE pthemes (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    parent_id integer,
    rght integer NOT NULL,
    lft integer NOT NULL,
    rank character varying(5)
);


ALTER TABLE public.pthemes OWNER TO idelibre;

--
-- Name: TABLE pthemes; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE pthemes IS 'Gestion des themes de projets de délibération';


--
-- Name: pthemes_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE pthemes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pthemes_id_seq OWNER TO idelibre;

--
-- Name: pthemes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE pthemes_id_seq OWNED BY pthemes.id;


--
-- Name: seances; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE seances (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    date_seance timestamp without time zone NOT NULL,
    rev numeric DEFAULT 1 NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    type_id uuid,
    document_id uuid NOT NULL,
    alias character varying(255) NOT NULL,
    archive boolean DEFAULT false,
    place character varying(128)
);


ALTER TABLE public.seances OWNER TO idelibre;

--
-- Name: TABLE seances; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE seances IS 'Gestion des séances';


--
-- Name: types; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE types (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.types OWNER TO idelibre;

--
-- Name: TABLE types; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE types IS 'Gestion des types de séances';


--
-- Name: types_users; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE types_users (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    type_id uuid NOT NULL,
    user_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.types_users OWNER TO idelibre;

--
-- Name: TABLE types_users; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE types_users IS 'Liaison entre les types de séances et les utilisateurs';


--
-- Name: users; Type: TABLE; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE TABLE users (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    last_login timestamp without time zone,
    group_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    active boolean DEFAULT true NOT NULL,
    last_logout timestamp without time zone,
    last_import timestamp without time zone,
    mail character varying(255),
    pwdmodified boolean DEFAULT false NOT NULL,
    rev integer,
    rev_date timestamp without time zone,
    seance_sync_date timestamp without time zone,
    groupepolitique_id integer,
    owner boolean DEFAULT true,
    naissance timestamp without time zone
);


ALTER TABLE public.users OWNER TO idelibre;

--
-- Name: TABLE users; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE users IS 'Gestion des utilisateurs';


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY acos ALTER COLUMN id SET DEFAULT nextval('acos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annots_users ALTER COLUMN id SET DEFAULT nextval('annots_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros ALTER COLUMN id SET DEFAULT nextval('aros_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros_acos ALTER COLUMN id SET DEFAULT nextval('aros_acos_id_seq'::regclass);


--
-- Name: num; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY deltarevisions ALTER COLUMN num SET DEFAULT nextval('deltarevisions_num_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations ALTER COLUMN id SET DEFAULT nextval('emailconvocations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupepolitiques ALTER COLUMN id SET DEFAULT nextval('groupepolitiques_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs ALTER COLUMN id SET DEFAULT nextval('logacteurs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY pthemes ALTER COLUMN id SET DEFAULT nextval('pthemes_id_seq'::regclass);


--
-- Data for Name: acos; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY acos (id, parent_id, model, alias, lft, rght, foreign_key) FROM stdin;
75	68	\N	writeLoginConfig	147	148	\N
36	30	\N	setConn	69	70	\N
3	2	\N	getFile	3	4	\N
59	55	\N	getHorodatageToken	115	116	\N
4	2	\N	setConn	5	6	\N
37	30	\N	processLogin	71	72	\N
5	2	\N	processLogin	7	8	\N
6	2	\N	writeLoginConfig	9	10	\N
38	30	\N	writeLoginConfig	73	74	\N
7	2	\N	writeUserConfig	11	12	\N
60	55	\N	setConn	117	118	\N
8	2	\N	writeLoginSession	13	14	\N
39	30	\N	writeUserConfig	75	76	\N
9	2	\N	ldebug	15	16	\N
76	68	\N	writeUserConfig	149	150	\N
2	1	\N	Annexes	2	19	\N
10	2	\N	ldebugHr	17	18	\N
40	30	\N	writeLoginSession	77	78	\N
61	55	\N	processLogin	119	120	\N
12	11	\N	ping	21	22	\N
41	30	\N	ldebug	79	80	\N
13	11	\N	check	23	24	\N
87	80	\N	writeLoginSession	171	172	\N
14	11	\N	setConn	25	26	\N
30	1	\N	Collectivites	58	83	\N
42	30	\N	ldebugHr	81	82	\N
15	11	\N	processLogin	27	28	\N
16	11	\N	writeLoginConfig	29	30	\N
62	55	\N	writeLoginConfig	121	122	\N
17	11	\N	writeUserConfig	31	32	\N
77	68	\N	writeLoginSession	151	152	\N
18	11	\N	writeLoginSession	33	34	\N
44	43	\N	index	85	86	\N
19	11	\N	ldebug	35	36	\N
11	1	\N	Api300	20	39	\N
20	11	\N	ldebugHr	37	38	\N
63	55	\N	writeUserConfig	123	124	\N
45	43	\N	edit	87	88	\N
22	21	\N	getData	41	42	\N
90	1	\N	Environnements	178	203	\N
46	43	\N	saveSrvuser	89	90	\N
23	21	\N	setConn	43	44	\N
96	90	\N	setConn	189	190	\N
24	21	\N	processLogin	45	46	\N
64	55	\N	writeLoginSession	125	126	\N
47	43	\N	addCollectivite	91	92	\N
25	21	\N	writeLoginConfig	47	48	\N
26	21	\N	writeUserConfig	49	50	\N
78	68	\N	ldebug	153	154	\N
48	43	\N	setConn	93	94	\N
27	21	\N	writeLoginSession	51	52	\N
28	21	\N	ldebug	53	54	\N
65	55	\N	ldebug	127	128	\N
21	1	\N	Clients	40	57	\N
29	21	\N	ldebugHr	55	56	\N
49	43	\N	processLogin	95	96	\N
88	80	\N	ldebug	173	174	\N
31	30	\N	index	59	60	\N
50	43	\N	writeLoginConfig	97	98	\N
32	30	\N	add	61	62	\N
55	1	\N	Convocations	108	131	\N
66	55	\N	ldebugHr	129	130	\N
33	30	\N	edit	63	64	\N
51	43	\N	writeUserConfig	99	100	\N
34	30	\N	delete	65	66	\N
68	1	\N	Documents	134	157	\N
35	30	\N	info	67	68	\N
67	1	\N	ConvocationsController1	132	133	\N
52	43	\N	writeLoginSession	101	102	\N
79	68	\N	ldebugHr	155	156	\N
53	43	\N	ldebug	103	104	\N
43	1	\N	Confirmuser	84	107	\N
54	43	\N	ldebugHr	105	106	\N
69	68	\N	save_annotations	135	136	\N
56	55	\N	setRead	109	110	\N
80	1	\N	Emailconvocations	158	177	\N
57	55	\N	send	111	112	\N
89	80	\N	ldebugHr	175	176	\N
81	80	\N	index	159	160	\N
58	55	\N	getHorodatageFile	113	114	\N
70	68	\N	getFile	137	138	\N
71	68	\N	getFileFromClient	139	140	\N
102	90	\N	ldebugHr	201	202	\N
82	80	\N	edit	161	162	\N
72	68	\N	getAnnotations	141	142	\N
73	68	\N	setConn	143	144	\N
83	80	\N	setConn	163	164	\N
74	68	\N	processLogin	145	146	\N
97	90	\N	processLogin	191	192	\N
91	90	\N	index	179	180	\N
84	80	\N	processLogin	165	166	\N
85	80	\N	writeLoginConfig	167	168	\N
98	90	\N	writeLoginConfig	193	194	\N
86	80	\N	writeUserConfig	169	170	\N
92	90	\N	menu	181	182	\N
107	103	\N	writeLoginConfig	211	212	\N
93	90	\N	goToClient	183	184	\N
99	90	\N	writeUserConfig	195	196	\N
94	90	\N	params	185	186	\N
104	103	\N	testxml	205	206	\N
95	90	\N	stats	187	188	\N
110	103	\N	ldebug	217	218	\N
100	90	\N	writeLoginSession	197	198	\N
108	103	\N	writeUserConfig	213	214	\N
101	90	\N	ldebug	199	200	\N
105	103	\N	setConn	207	208	\N
116	112	\N	delete	229	230	\N
103	1	\N	Essai	204	221	\N
106	103	\N	processLogin	209	210	\N
113	112	\N	index	223	224	\N
109	103	\N	writeLoginSession	215	216	\N
111	103	\N	ldebugHr	219	220	\N
117	112	\N	setConn	231	232	\N
114	112	\N	add	225	226	\N
115	112	\N	edit	227	228	\N
118	112	\N	processLogin	233	234	\N
119	112	\N	writeLoginConfig	235	236	\N
120	112	\N	writeUserConfig	237	238	\N
121	112	\N	writeLoginSession	239	240	\N
122	112	\N	ldebug	241	242	\N
123	112	\N	ldebugHr	243	244	\N
112	1	\N	Groupepolitiques	222	245	\N
125	124	\N	index	247	248	\N
126	124	\N	add	249	250	\N
127	124	\N	edit	251	252	\N
128	124	\N	delete	253	254	\N
129	124	\N	setConn	255	256	\N
130	124	\N	processLogin	257	258	\N
124	1	\N	Groups	246	269	\N
165	154	\N	ldebugHr	327	328	\N
131	124	\N	writeLoginConfig	259	260	\N
132	124	\N	writeUserConfig	261	262	\N
133	124	\N	writeLoginSession	263	264	\N
134	124	\N	ldebug	265	266	\N
135	124	\N	ldebugHr	267	268	\N
227	225	\N	add	451	452	\N
190	189	\N	index	377	378	\N
167	166	\N	display	331	332	\N
137	136	\N	getFile	271	272	\N
138	136	\N	setConn	273	274	\N
168	166	\N	setConn	333	334	\N
139	136	\N	processLogin	275	276	\N
207	189	\N	processLogin	411	412	\N
191	189	\N	view	379	380	\N
140	136	\N	writeLoginConfig	277	278	\N
169	166	\N	processLogin	335	336	\N
141	136	\N	writeUserConfig	279	280	\N
142	136	\N	writeLoginSession	281	282	\N
170	166	\N	writeLoginConfig	337	338	\N
143	136	\N	ldebug	283	284	\N
219	213	\N	processLogin	435	436	\N
136	1	\N	Imgpages	270	287	\N
144	136	\N	ldebugHr	285	286	\N
192	189	\N	add	381	382	\N
171	166	\N	writeUserConfig	339	340	\N
146	145	\N	view	289	290	\N
172	166	\N	writeLoginSession	341	342	\N
147	145	\N	setConn	291	292	\N
208	189	\N	writeLoginConfig	413	414	\N
148	145	\N	processLogin	293	294	\N
193	189	\N	edit	383	384	\N
173	166	\N	ldebug	343	344	\N
149	145	\N	writeLoginConfig	295	296	\N
150	145	\N	writeUserConfig	297	298	\N
166	1	\N	Pages	330	347	\N
174	166	\N	ldebugHr	345	346	\N
151	145	\N	writeLoginSession	299	300	\N
152	145	\N	ldebug	301	302	\N
233	225	\N	writeUserConfig	463	464	\N
145	1	\N	Logacteur	288	305	\N
153	145	\N	ldebugHr	303	304	\N
194	189	\N	delete	385	386	\N
176	175	\N	index	349	350	\N
155	154	\N	index	307	308	\N
156	154	\N	start	309	310	\N
209	189	\N	writeUserConfig	415	416	\N
177	175	\N	add	351	352	\N
157	154	\N	stop	311	312	\N
195	189	\N	addUsersSeance	387	388	\N
158	154	\N	display	313	314	\N
178	175	\N	edit	353	354	\N
159	154	\N	setConn	315	316	\N
160	154	\N	processLogin	317	318	\N
220	213	\N	writeLoginConfig	437	438	\N
179	175	\N	delete	355	356	\N
161	154	\N	writeLoginConfig	319	320	\N
196	189	\N	ajout	389	390	\N
162	154	\N	writeUserConfig	321	322	\N
180	175	\N	getList	357	358	\N
163	154	\N	writeLoginSession	323	324	\N
164	154	\N	ldebug	325	326	\N
210	189	\N	writeLoginSession	417	418	\N
154	1	\N	Monitor	306	329	\N
197	189	\N	liste	391	392	\N
181	175	\N	rankit	359	360	\N
182	175	\N	setConn	361	362	\N
228	225	\N	edit	453	454	\N
198	189	\N	details	393	394	\N
183	175	\N	processLogin	363	364	\N
184	175	\N	writeLoginConfig	365	366	\N
211	189	\N	ldebug	419	420	\N
199	189	\N	suppression	395	396	\N
185	175	\N	writeUserConfig	367	368	\N
186	175	\N	writeLoginSession	369	370	\N
221	213	\N	writeUserConfig	439	440	\N
200	189	\N	compareRank	397	398	\N
187	175	\N	ldebug	371	372	\N
175	1	\N	Pthemes	348	375	\N
188	175	\N	ldebugHr	373	374	\N
189	1	\N	Seances	376	423	\N
212	189	\N	ldebugHr	421	422	\N
201	189	\N	edition	399	400	\N
244	237	\N	toRemoveMandat	485	486	\N
202	189	\N	sendConvocations	401	402	\N
222	213	\N	writeLoginSession	441	442	\N
203	189	\N	refreshSeance	403	404	\N
214	213	\N	index	425	426	\N
204	189	\N	archive	405	406	\N
229	225	\N	delete	455	456	\N
205	189	\N	printInfoUsers	407	408	\N
223	213	\N	ldebug	443	444	\N
206	189	\N	setConn	409	410	\N
215	213	\N	searchByType	427	428	\N
234	225	\N	writeLoginSession	465	466	\N
216	213	\N	searchByDate	429	430	\N
213	1	\N	Search	424	447	\N
224	213	\N	ldebugHr	445	446	\N
217	213	\N	searchByTheme	431	432	\N
230	225	\N	setConn	457	458	\N
218	213	\N	setConn	433	434	\N
238	237	\N	index	473	474	\N
231	225	\N	processLogin	459	460	\N
226	225	\N	index	449	450	\N
235	225	\N	ldebug	467	468	\N
241	237	\N	listeMandat	479	480	\N
225	1	\N	Srvroles	448	471	\N
232	225	\N	writeLoginConfig	461	462	\N
239	237	\N	addAdminColl	475	476	\N
236	225	\N	ldebugHr	469	470	\N
248	237	\N	editPortefeuilleMandat	493	494	\N
243	237	\N	authorizedCollectiviteId	483	484	\N
240	237	\N	add	477	478	\N
242	237	\N	authorizedCollectivite	481	482	\N
247	237	\N	editWalletManager	491	492	\N
245	237	\N	edit	487	488	\N
246	237	\N	editSuperAdmin	489	490	\N
249	237	\N	updateCollectivite	495	496	\N
250	237	\N	delete	497	498	\N
251	237	\N	get_proof	499	500	\N
252	237	\N	login	501	502	\N
253	237	\N	logout	503	504	\N
254	237	\N	WSTellGlobalAuth	505	506	\N
255	237	\N	mdplost	507	508	\N
256	237	\N	setConn	509	510	\N
257	237	\N	processLogin	511	512	\N
258	237	\N	writeLoginConfig	513	514	\N
259	237	\N	writeUserConfig	515	516	\N
1	\N	\N	controllers	1	612	\N
260	237	\N	writeLoginSession	517	518	\N
261	237	\N	ldebug	519	520	\N
237	1	\N	Srvusers	472	523	\N
262	237	\N	ldebugHr	521	522	\N
303	297	\N	writeUserConfig	602	603	\N
264	263	\N	index	525	526	\N
265	263	\N	add	527	528	\N
266	263	\N	edit	529	530	\N
267	263	\N	delete	531	532	\N
304	297	\N	writeLoginSession	604	605	\N
268	263	\N	users	533	534	\N
269	263	\N	setConn	535	536	\N
270	263	\N	processLogin	537	538	\N
271	263	\N	writeLoginConfig	539	540	\N
305	297	\N	ldebug	606	607	\N
272	263	\N	writeUserConfig	541	542	\N
273	263	\N	writeLoginSession	543	544	\N
296	1	\N	DebugKit	590	611	\N
274	263	\N	ldebug	545	546	\N
263	1	\N	Types	524	549	\N
275	263	\N	ldebugHr	547	548	\N
297	296	\N	ToolbarAccess	591	610	\N
306	297	\N	ldebugHr	608	609	\N
277	276	\N	index	551	552	\N
278	276	\N	confirm	553	554	\N
279	276	\N	addCollectivite	555	556	\N
280	276	\N	removeCollectivite	557	558	\N
281	276	\N	add	559	560	\N
282	276	\N	edit	561	562	\N
283	276	\N	delete	563	564	\N
284	276	\N	login	565	566	\N
285	276	\N	logout	567	568	\N
286	276	\N	mdplost	569	570	\N
287	276	\N	verif	571	572	\N
288	276	\N	setConn	573	574	\N
289	276	\N	processLogin	575	576	\N
290	276	\N	writeLoginConfig	577	578	\N
291	276	\N	writeUserConfig	579	580	\N
292	276	\N	writeLoginSession	581	582	\N
293	276	\N	ldebug	583	584	\N
276	1	\N	Users	550	587	\N
294	276	\N	ldebugHr	585	586	\N
295	1	\N	AclExtras	588	589	\N
298	297	\N	history_state	592	593	\N
299	297	\N	sql_explain	594	595	\N
300	297	\N	setConn	596	597	\N
301	297	\N	processLogin	598	599	\N
302	297	\N	writeLoginConfig	600	601	\N
\.


--
-- Name: acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('acos_id_seq', 306, true);


--
-- Data for Name: annexes; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY annexes (id, name, size, type, content, created, modified, projet_id, checksum, rank, path) FROM stdin;
\.


--
-- Data for Name: annotactions; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY annotactions (id, deltarevision_id, created, modified, type, annotation_id) FROM stdin;
\.


--
-- Data for Name: annotations; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY annotations (id, annot_client_id, imgpage_id, content, created, modified, user_id, shared, rank, date, date_modif, height, width, x, y, comment) FROM stdin;
\.


--
-- Data for Name: annotations_users; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY annotations_users (id, user_id, created, modified, annotation_id, read) FROM stdin;
\.


--
-- Data for Name: annotationv3; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY annotationv3 (id, authorid, authorname, page, rect, text, projet_id, seance_id, annexe_id, shareduseridlist, date) FROM stdin;
\.


--
-- Data for Name: annotationv3_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY annotationv3_users (annotationv3_id, user_id, isread) FROM stdin;
\.


--
-- Data for Name: annots; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY annots (id, projet_id, liste_partage, json) FROM stdin;
\.


--
-- Data for Name: annots_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY annots_users (id, user_id, annot_id) FROM stdin;
\.


--
-- Name: annots_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('annots_users_id_seq', 1, false);


--
-- Data for Name: aros; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY aros (id, parent_id, model, alias, lft, rght, foreign_key) FROM stdin;
1	\N	Group	Administrateurs	1	4	26b380d8-93b2-41e9-9f68-da98c55ccb30
2	\N	Group	Utilisateurs	5	6	9f803280-eb21-46b3-bcec-74ffc4d2aab8
3	\N	Group	Acteur	7	8	b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16
4	1	User	Administrateur	2	3	e64e47b6-9027-4d73-a49b-70fd2ee0475e
\.


--
-- Data for Name: aros_acos; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY aros_acos (id, aro_id, aco_id, _create, _read, _update, _delete) FROM stdin;
1	1	35	1	1	1	1
2	1	90	1	1	1	1
3	1	55	1	1	1	1
4	1	124	1	1	1	1
5	1	175	1	1	1	1
6	1	263	1	1	1	1
7	1	276	1	1	1	1
8	1	80	1	1	1	1
9	1	43	1	1	1	1
10	1	112	1	1	1	1
11	1	145	1	1	1	1
12	2	90	1	1	1	1
13	2	196	1	1	1	1
14	2	197	1	1	1	1
15	2	198	1	1	1	1
16	2	201	1	1	1	1
17	2	199	1	1	1	1
18	2	202	1	1	1	1
19	2	55	1	1	1	1
20	2	21	1	1	1	1
21	2	281	1	1	1	1
22	2	175	1	1	1	1
23	2	263	1	1	1	1
24	2	70	1	1	1	1
25	2	137	1	1	1	1
26	2	3	1	1	1	1
27	2	213	1	1	1	1
28	2	204	1	1	1	1
29	3	90	1	1	1	1
30	3	189	1	1	1	1
31	3	55	1	1	1	1
32	3	70	1	1	1	1
33	3	72	1	1	1	1
34	3	137	1	1	1	1
35	3	3	1	1	1	1
36	3	21	1	1	1	1
\.


--
-- Name: aros_acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('aros_acos_id_seq', 36, true);


--
-- Name: aros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('aros_id_seq', 4, true);


--
-- Data for Name: convocations; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY convocations (id, seance_id, user_id, read, presence, delegation, procuration, created, modified, ar_horodatage, active, ar_horodatage_file, ar_horodatage_token, ar_received, ae_sent, ae_horodatage, ae_horodatage_file, ae_horodatage_token, presentstatus) FROM stdin;
\.


--
-- Data for Name: deltarevisions; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY deltarevisions (id, num, delta, created, modified, user_id) FROM stdin;
\.


--
-- Name: deltarevisions_num_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('deltarevisions_num_seq', 1, false);


--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY documents (id, name, size, type, content, created, modified, checksum, path) FROM stdin;
\.


--
-- Data for Name: emailconvocations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY emailconvocations (id, sujet, contenu) FROM stdin;
1	convocation pour la seance #typeseance# le #dateseance# à #heureseance#	#prenom# #nom#,\n\nvous avez reçu une convocation pour la seance #typeseance# le #dateseance# à #heureseance#.\nVeuillez vous connecter sur votre application i-delibRE pour en prendre connaissance.
\.


--
-- Name: emailconvocations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('emailconvocations_id_seq', 1, true);


--
-- Data for Name: groupepolitiques; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY groupepolitiques (id, name) FROM stdin;
1	sans étiquette
\.


--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('groupepolitiques_id_seq', 1, true);


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY groups (id, name, created, modified, admin, inseance, superuser, baseuser) FROM stdin;
26b380d8-93b2-41e9-9f68-da98c55ccb30	Administrateurs	2013-02-11 11:21:45.788881	2013-06-13 17:19:10	t	f	f	f
9f803280-eb21-46b3-bcec-74ffc4d2aab8	Utilisateurs	2013-02-11 11:21:45.832553	2013-06-26 13:36:49	f	f	t	f
b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16	Acteur	2013-02-11 11:21:45.805519	2013-06-26 13:36:31	f	t	f	t
\.


--
-- Data for Name: imgpages; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY imgpages (id, name, size, type, content, imgpagenum, document_id, created, modified, checksum) FROM stdin;
\.


--
-- Data for Name: logacteurs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY logacteurs (id, user_id, created) FROM stdin;
\.


--
-- Name: logacteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('logacteurs_id_seq', 1, false);


--
-- Data for Name: pdfdatas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pdfdatas (id, document_id, data, part) FROM stdin;
\.


--
-- Data for Name: projets; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY projets (id, name, theme, document_id, vote, ptheme_id, seance_id, created, modified, rank, user_id) FROM stdin;
\.


--
-- Data for Name: pthemes; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY pthemes (id, name, parent_id, rght, lft, rank) FROM stdin;
\.


--
-- Name: pthemes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('pthemes_id_seq', 1, true);


--
-- Data for Name: seances; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY seances (id, name, date_seance, rev, created, modified, type_id, document_id, alias, archive, place) FROM stdin;
\.


--
-- Data for Name: types; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY types (id, name, created, modified) FROM stdin;
\.


--
-- Data for Name: types_users; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY types_users (id, type_id, user_id, created, modified) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: idelibre
--

COPY users (id, firstname, lastname, username, password, last_login, group_id, created, modified, active, last_logout, last_import, mail, pwdmodified, rev, rev_date, seance_sync_date, groupepolitique_id, owner, naissance) FROM stdin;
e64e47b6-9027-4d73-a49b-70fd2ee0475e	Administrateur	Administrateur	Administrateur	mot de pass non valide	\N	26b380d8-93b2-41e9-9f68-da98c55ccb30	2013-11-07 15:54:40.942895	2013-11-07 15:54:40.942895	t	\N	\N	\N	f	\N	\N	\N	\N	t	\N
\.


--
-- Name: acos_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY acos
    ADD CONSTRAINT acos_pkey PRIMARY KEY (id);


--
-- Name: annexes_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY annexes
    ADD CONSTRAINT annexes_pkey PRIMARY KEY (id);


--
-- Name: annotactions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY annotactions
    ADD CONSTRAINT annotactions_pkey PRIMARY KEY (id);


--
-- Name: annotationv3_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY annotationv3
    ADD CONSTRAINT annotationv3_pkey PRIMARY KEY (id);


--
-- Name: annots_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY annots
    ADD CONSTRAINT annots_id_key UNIQUE (id);


--
-- Name: aros_acos_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY aros_acos
    ADD CONSTRAINT aros_acos_pkey PRIMARY KEY (id);


--
-- Name: aros_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY aros
    ADD CONSTRAINT aros_pkey PRIMARY KEY (id);


--
-- Name: convocations_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT convocations_pkey PRIMARY KEY (id);


--
-- Name: convocusers_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_pkey PRIMARY KEY (id);


--
-- Name: deltarevisions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY deltarevisions
    ADD CONSTRAINT deltarevisions_pkey PRIMARY KEY (id);


--
-- Name: documents_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: emailconvocations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY emailconvocations
    ADD CONSTRAINT emailconvocations_pkey PRIMARY KEY (id);


--
-- Name: groupepolitiques_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY groupepolitiques
    ADD CONSTRAINT groupepolitiques_pkey PRIMARY KEY (id);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: imgpages_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY imgpages
    ADD CONSTRAINT imgpages_pkey PRIMARY KEY (id);


--
-- Name: logacteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY logacteurs
    ADD CONSTRAINT logacteurs_pkey PRIMARY KEY (id);


--
-- Name: name_unique_idx; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT name_unique_idx UNIQUE (name);


--
-- Name: projets_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_pkey PRIMARY KEY (id);


--
-- Name: pthemes_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY pthemes
    ADD CONSTRAINT pthemes_pkey PRIMARY KEY (id);


--
-- Name: seances_alias_key; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT seances_alias_key UNIQUE (alias);


--
-- Name: types_name_unique_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_name_unique_pkey UNIQUE (name);


--
-- Name: types_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_pkey PRIMARY KEY (id);


--
-- Name: types_users_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_pkey PRIMARY KEY (id);


--
-- Name: user_username_unique_key; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT user_username_unique_key UNIQUE (username);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: aro_aco_key; Type: INDEX; Schema: public; Owner: idelibre; Tablespace: 
--

CREATE UNIQUE INDEX aro_aco_key ON aros_acos USING btree (aro_id, aco_id);


--
-- Name: annexes_projet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY annexes
    ADD CONSTRAINT annexes_projet_id_fkey FOREIGN KEY (projet_id) REFERENCES projets(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: annotactions_deltarevision_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY annotactions
    ADD CONSTRAINT annotactions_deltarevision_id_fkey FOREIGN KEY (deltarevision_id) REFERENCES deltarevisions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: annotationv3_users_annotationv3_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3_users
    ADD CONSTRAINT annotationv3_users_annotationv3_id_fkey FOREIGN KEY (annotationv3_id) REFERENCES annotationv3(id) ON DELETE CASCADE;


--
-- Name: annotationv3_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3_users
    ADD CONSTRAINT annotationv3_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: annots_projet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annots
    ADD CONSTRAINT annots_projet_id_fkey FOREIGN KEY (projet_id) REFERENCES projets(id) ON DELETE CASCADE;


--
-- Name: annots_users_annot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annots_users
    ADD CONSTRAINT annots_users_annot_id_fkey FOREIGN KEY (annot_id) REFERENCES annots(id) ON DELETE CASCADE;


--
-- Name: annots_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annots_users
    ADD CONSTRAINT annots_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: convocations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT convocations_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: convocusers_convocation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_convocation_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: convocusers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: deltarevisions_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY deltarevisions
    ADD CONSTRAINT deltarevisions_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: imgpages_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY imgpages
    ADD CONSTRAINT imgpages_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: logacteurs_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs
    ADD CONSTRAINT logacteurs_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: pdfdatas_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pdfdatas
    ADD CONSTRAINT pdfdatas_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON DELETE CASCADE;


--
-- Name: projets_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: projets_ptheme_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_ptheme_id_fkey FOREIGN KEY (ptheme_id) REFERENCES pthemes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: projets_seance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_seance_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: projets_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: seances_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT seances_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: types_users_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: types_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_groupepolitique_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_groupepolitique_id_fkey FOREIGN KEY (groupepolitique_id) REFERENCES groupepolitiques(id) ON DELETE SET NULL;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

