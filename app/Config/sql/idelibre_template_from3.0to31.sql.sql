CREATE TABLE types_autorized (
  id uuid DEFAULT uuid_generate_v4() NOT NULL,
  user_id uuid NOT NULL,
  type_id uuid NOT NULL,
  created timestamp without time zone DEFAULT now() NOT NULL,
  modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE types_autorized ADD  FOREIGN KEY(user_id) REFERENCES users(id) on delete cascade;
ALTER TABLE types_autorized ADD  FOREIGN KEY(type_id) REFERENCES types(id) on delete cascade;


INSERT INTO groups (id, name) values ('aa130693-e6d0-4893-ba31-98892b98581f','Invites');
INSERT INTO groups (id, name) values ('db68a3c7-0119-40f1-b444-e96f568b3d67','Administratifs');


CREATE TABLE invitations (
  id uuid DEFAULT uuid_generate_v4() NOT NULL,
  user_id uuid NOT NULL,
  seance_id uuid NOT NULL,
  created timestamp without time zone DEFAULT now() NOT NULL,
  modified timestamp without time zone DEFAULT now() NOT NULL
);



ALTER TABLE invitations ADD COLUMN document_id uuid NOT NULL REFERENCES documents(id) on delete CASCADE;
ALTER TABLE invitations ADD COLUMN ae_horodatage TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE invitations ADD COLUMN ar_horodatage TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE invitations ADD COLUMN isactive BOOLEAN DEFAULT FALSE ;
ALTER TABLE invitations ADD COLUMN isread BOOLEAN DEFAULT FALSE ;


ALTER TABLE invitations ADD  FOREIGN KEY(user_id) REFERENCES users(id) on delete cascade;
ALTER TABLE invitations ADD  FOREIGN KEY(seance_id) REFERENCES seances(id) on delete cascade;


-- alter TABLE users add COLUMN civilite varchar(255);
alter TABLE users add COLUMN civilite INTEGER;
alter TABLE users add COLUMN titre varchar(255);




CREATE TABLE emailinvitations (
  id uuid DEFAULT uuid_generate_v4() NOT NULL,
  type_id uuid REFERENCES types(id),
  name varchar(255) NOT NULL,
  content varchar(2000),
  created timestamp without time zone DEFAULT now() NOT NULL,
  modified timestamp without time zone DEFAULT now() NOT NULL
);

ALTER TABLE emailinvitations ADD COLUMN sujet varchar(500) NOT NULL;

ALTER TABLE emailConvocations ADD COLUMN type_id uuid REFERENCES types(id) on delete set NULL;
ALTER TABLE emailConvocations ADD COLUMN name varchar(255);
ALTER TABLE emailconvocations ADD CONSTRAINT emailconvocations_unique_type_id  UNIQUE (type_id);

--TODO  inset 5a0ea5a3-2240-46e1-ba36-53b55cde535b  as default invitation email --

-- insert into types_autorized (user_id , type_id) values ('59ce0978-0a9c-4293-9d02-12f95cde535b', '5810a4a2-7ee4-46f6-a81c-189b5cde535b');
--
--
-- insert into types_autorized (user_id , type_id) values ('59ce0978-0a9c-4293-9d02-12f95cde535b', '5810a49a-7660-4eb3-a1e9-189c5cde535b');

--triggers to autodelete document
CREATE OR REPLACE FUNCTION deleteDocument() RETURNS trigger AS
$$BEGIN
  DELETE FROM  documents where documents.id = OLD.document_Id;
  RETURN null;
END;$$ LANGUAGE plpgsql;

CREATE TRIGGER deleteProjetTrigger
AFTER DELETE ON projets FOR EACH ROW
EXECUTE PROCEDURE deleteDocument();


CREATE TRIGGER deleteSeanceTrigger
AFTER DELETE ON seances FOR EACH ROW
EXECUTE PROCEDURE deleteDocument();

CREATE TRIGGER deleteInvitationTrigger
AFTER DELETE ON invitations FOR EACH ROW
EXECUTE PROCEDURE deleteDocument();


-- dans les table des collectivités idelibre_libriciel
delete from annotations;
delete from annotactions;
delete from annotations_users;
delete from annots;
delete from annots_users;
delete from deltarevisions;
delete from imgpages;
delete from pdfdatas;


drop table annotations_users;
drop table annotations;
drop table annotactions;
drop table annots_users;
drop table annots;
drop table deltarevisions;
drop table imgpages;
drop table pdfdatas;


--purge des clefs et des données inutiles !
alter table documents DROP COLUMN content;
alter table annexes DROP COLUMN content;


CREATE TABLE types_authorizeds (
  id uuid DEFAULT uuid_generate_v4() NOT NULL,
  user_id uuid NOT NULL,
  type_id uuid NOT NULL,
  created timestamp without time zone DEFAULT now() NOT NULL,
  modified timestamp without time zone DEFAULT now() NOT NULL
);

ALTER TABLE types_authorizeds ADD  FOREIGN KEY(user_id) REFERENCES users(id) on delete cascade;
ALTER TABLE types_authorizeds ADD  FOREIGN KEY(type_id) REFERENCES types(id) on delete cascade;


alter table convocations add istoken boolean default false;
alter table convocations add isemailed boolean default false;


insert INTO emailinvitations values ('5a0ea5a3-2240-46e1-ba36-53b55cde535b',null, 'Message par defaut', 'Invitation à une séance',now(), now(), 'Vous êtes invité à la séance #typeseance# du #dateseance#.');



ALTER TABLE users ADD bcrypt VARCHAR(255);

alter table emailconvocations drop constraint emailconvocations_type_id_fkey;
alter table emailconvocations add foreign key(type_id) REFERENCES types(id) on delete cascade;


alter table emailinvitations drop constraint emailinvitations_type_id_fkey;
alter table emailinvitations add foreign key(type_id) REFERENCES types(id) on delete cascade;


alter table users add blacklisted boolean default false;

alter table convocations add procuration_name varchar(255);


update convocations set istoken = true;
update convocations set isemailed = true;

insert into types_authorizeds (user_id , type_id) select users.id, types.id from users, types where users.group_id='9f803280-eb21-46b3-bcec-74ffc4d2aab8';
