alter table users drop constraint user_mail_unique_key;

alter table projets add user_id uuid;
alter table projets add foreign key (user_id) REFERENCES users(id);

alter table logacteurs drop constraint logacteurs_user_id_fkey;
alter table logacteurs add foreign key (user_id) REFERENCES users(id) on delete cascade;
