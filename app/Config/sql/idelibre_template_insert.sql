--
-- PostgreSQL database dump
--

-- Dumped from database version 10.2 (Debian 10.2-1.pgdg90+1)
-- Dumped by pg_dump version 10.2 (Debian 10.2-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- Name: deletedocument(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION deletedocument() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  DELETE FROM  documents where documents.id = OLD.document_Id;
  RETURN null;
END;$$;


ALTER FUNCTION public.deletedocument() OWNER TO postgres;

--
-- Name: rebuilt_sequences(); Type: FUNCTION; Schema: public; Owner: idelibre
--

CREATE FUNCTION rebuilt_sequences() RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE sequencedefs RECORD; c integer ;
  BEGIN
    FOR sequencedefs IN Select
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.column_name as columnname,
      replace(replace(columns.column_default,'''::regclass)',''),'nextval(''','') as sequencename
      from information_schema.constraint_column_usage, information_schema.columns
      where constraint_column_usage.table_schema ='public' AND
      columns.table_schema = 'public' AND columns.table_name=constraint_column_usage.table_name
      AND constraint_column_usage.column_name = columns.column_name
      AND columns.column_default is not null
   LOOP
      EXECUTE 'select max('||sequencedefs.columnname||') from ' || sequencedefs.tablename INTO c;
      IF c is null THEN c = 0; END IF;
      IF c is not null THEN c = c+ 1; END IF;
      EXECUTE 'alter sequence ' || sequencedefs.sequencename ||' restart  with ' || c;
   END LOOP;

   RETURN 1; END;
$$;


ALTER FUNCTION public.rebuilt_sequences() OWNER TO idelibre;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acos; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE acos (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer,
    foreign_key uuid
);


ALTER TABLE acos OWNER TO idelibre;

--
-- Name: TABLE acos; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE acos IS 'Gestion des ACL (CakePHP) - ressources';


--
-- Name: acos_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acos_id_seq OWNER TO idelibre;

--
-- Name: acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE acos_id_seq OWNED BY acos.id;


--
-- Name: annexes; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE annexes (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(500) NOT NULL,
    size integer DEFAULT 0 NOT NULL,
    type character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    projet_id uuid,
    checksum character varying(32),
    rank integer DEFAULT 0 NOT NULL,
    path character varying
);


ALTER TABLE annexes OWNER TO idelibre;

--
-- Name: TABLE annexes; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE annexes IS 'Gestion des annexes de projets de délibérations (pièces jointes)';


--
-- Name: annotationv3; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE annotationv3 (
    id uuid NOT NULL,
    authorid uuid NOT NULL,
    authorname character varying,
    page integer,
    rect character varying,
    text character varying,
    projet_id uuid,
    seance_id uuid,
    annexe_id uuid,
    shareduseridlist character varying,
    date bigint
);


ALTER TABLE annotationv3 OWNER TO postgres;

--
-- Name: annotationv3_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE annotationv3_users (
    annotationv3_id uuid NOT NULL,
    user_id uuid NOT NULL,
    isread boolean
);


ALTER TABLE annotationv3_users OWNER TO postgres;

--
-- Name: aros; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE aros (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer,
    foreign_key uuid
);


ALTER TABLE aros OWNER TO idelibre;

--
-- Name: TABLE aros; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE aros IS 'Gestion des ACL (CakePHP) - demandeurs';


--
-- Name: aros_acos; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE aros_acos (
    id integer NOT NULL,
    aro_id integer NOT NULL,
    aco_id integer NOT NULL,
    _create character varying(2) DEFAULT '0'::character varying NOT NULL,
    _read character varying(2) DEFAULT '0'::character varying NOT NULL,
    _update character varying(2) DEFAULT '0'::character varying NOT NULL,
    _delete character varying(2) DEFAULT '0'::character varying NOT NULL
);


ALTER TABLE aros_acos OWNER TO idelibre;

--
-- Name: TABLE aros_acos; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE aros_acos IS 'Gestion des ACL (CakePHP) - liaisons demandeurs / ressources';


--
-- Name: aros_acos_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE aros_acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aros_acos_id_seq OWNER TO idelibre;

--
-- Name: aros_acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE aros_acos_id_seq OWNED BY aros_acos.id;


--
-- Name: aros_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE aros_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aros_id_seq OWNER TO idelibre;

--
-- Name: aros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE aros_id_seq OWNED BY aros.id;


--
-- Name: convocations; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE convocations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    seance_id uuid NOT NULL,
    user_id uuid NOT NULL,
    read boolean DEFAULT false NOT NULL,
    presence boolean DEFAULT true NOT NULL,
    delegation boolean DEFAULT false NOT NULL,
    procuration boolean DEFAULT false NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    ar_horodatage timestamp without time zone,
    active boolean DEFAULT false NOT NULL,
    ar_horodatage_file bytea,
    ar_horodatage_token bytea,
    ar_received timestamp without time zone,
    ae_sent timestamp without time zone,
    ae_horodatage timestamp without time zone,
    ae_horodatage_file bytea,
    ae_horodatage_token bytea,
    presentstatus character varying
);


ALTER TABLE convocations OWNER TO idelibre;

--
-- Name: TABLE convocations; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE convocations IS 'Gestion des convocations';


--
-- Name: documents; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE documents (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    size integer DEFAULT 0 NOT NULL,
    type character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    checksum character varying(32),
    path character varying
);


ALTER TABLE documents OWNER TO idelibre;

--
-- Name: TABLE documents; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE documents IS 'Gestion des documents';


--
-- Name: emailconvocations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE emailconvocations (
    id integer NOT NULL,
    sujet character varying(200) NOT NULL,
    contenu character varying(2000) NOT NULL,
    type_id uuid,
    name character varying(255)
);


ALTER TABLE emailconvocations OWNER TO postgres;

--
-- Name: emailconvocations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE emailconvocations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE emailconvocations_id_seq OWNER TO postgres;

--
-- Name: emailconvocations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE emailconvocations_id_seq OWNED BY emailconvocations.id;


--
-- Name: emailinvitations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE emailinvitations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    type_id uuid,
    name character varying(255) NOT NULL,
    content character varying(2000),
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    sujet character varying(500) NOT NULL
);


ALTER TABLE emailinvitations OWNER TO postgres;

--
-- Name: groupepolitiques; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE groupepolitiques (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE groupepolitiques OWNER TO postgres;

--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE groupepolitiques_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groupepolitiques_id_seq OWNER TO postgres;

--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE groupepolitiques_id_seq OWNED BY groupepolitiques.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE groups (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    admin boolean DEFAULT false NOT NULL,
    inseance boolean DEFAULT false,
    superuser boolean DEFAULT false,
    baseuser boolean DEFAULT true
);


ALTER TABLE groups OWNER TO idelibre;

--
-- Name: TABLE groups; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE groups IS 'Gestion des groupes';


--
-- Name: invitations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE invitations (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    seance_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    document_id uuid NOT NULL,
    ae_horodatage timestamp without time zone,
    ar_horodatage timestamp without time zone,
    isactive boolean DEFAULT false,
    isread boolean DEFAULT false
);


ALTER TABLE invitations OWNER TO postgres;

--
-- Name: logacteurs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE logacteurs (
    id integer NOT NULL,
    user_id uuid,
    created timestamp without time zone
);


ALTER TABLE logacteurs OWNER TO postgres;

--
-- Name: logacteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE logacteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE logacteurs_id_seq OWNER TO postgres;

--
-- Name: logacteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE logacteurs_id_seq OWNED BY logacteurs.id;


--
-- Name: projets; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE projets (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(500) NOT NULL,
    theme character varying(500),
    document_id uuid,
    vote character varying(10),
    ptheme_id integer,
    seance_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    rank integer DEFAULT 0 NOT NULL,
    user_id uuid,
    CONSTRAINT projets_vote_check CHECK (((vote)::text = ANY (ARRAY[('oui'::character varying)::text, ('non'::character varying)::text, ('nsp'::character varying)::text, ('abs'::character varying)::text])))
);


ALTER TABLE projets OWNER TO idelibre;

--
-- Name: TABLE projets; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE projets IS 'Gestion des projets de délibérations';


--
-- Name: pthemes; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE pthemes (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    parent_id integer,
    rght integer NOT NULL,
    lft integer NOT NULL,
    rank character varying(5),
    fullname character varying(1000)
);


ALTER TABLE pthemes OWNER TO idelibre;

--
-- Name: TABLE pthemes; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE pthemes IS 'Gestion des themes de projets de délibération';


--
-- Name: pthemes_id_seq; Type: SEQUENCE; Schema: public; Owner: idelibre
--

CREATE SEQUENCE pthemes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pthemes_id_seq OWNER TO idelibre;

--
-- Name: pthemes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelibre
--

ALTER SEQUENCE pthemes_id_seq OWNED BY pthemes.id;


--
-- Name: seances; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE seances (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    date_seance timestamp without time zone NOT NULL,
    rev numeric DEFAULT 1 NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    type_id uuid,
    document_id uuid NOT NULL,
    alias character varying(255) NOT NULL,
    archive boolean DEFAULT false,
    place character varying(128)
);


ALTER TABLE seances OWNER TO idelibre;

--
-- Name: TABLE seances; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE seances IS 'Gestion des séances';


--
-- Name: types; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE types (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE types OWNER TO idelibre;

--
-- Name: TABLE types; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE types IS 'Gestion des types de séances';


--
-- Name: types_autorized; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE types_autorized (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    type_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE types_autorized OWNER TO postgres;

--
-- Name: types_users; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE types_users (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    type_id uuid NOT NULL,
    user_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE types_users OWNER TO idelibre;

--
-- Name: TABLE types_users; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE types_users IS 'Liaison entre les types de séances et les utilisateurs';


--
-- Name: users; Type: TABLE; Schema: public; Owner: idelibre
--

CREATE TABLE users (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    last_login timestamp without time zone,
    group_id uuid NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    active boolean DEFAULT true NOT NULL,
    last_logout timestamp without time zone,
    last_import timestamp without time zone,
    mail character varying(255),
    pwdmodified boolean DEFAULT false NOT NULL,
    rev integer,
    rev_date timestamp without time zone,
    seance_sync_date timestamp without time zone,
    groupepolitique_id integer,
    owner boolean DEFAULT true,
    naissance timestamp without time zone,
    civilite integer,
    titre character varying(255)
);


ALTER TABLE users OWNER TO idelibre;

--
-- Name: TABLE users; Type: COMMENT; Schema: public; Owner: idelibre
--

COMMENT ON TABLE users IS 'Gestion des utilisateurs';


--
-- Name: acos id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY acos ALTER COLUMN id SET DEFAULT nextval('acos_id_seq'::regclass);


--
-- Name: aros id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros ALTER COLUMN id SET DEFAULT nextval('aros_id_seq'::regclass);


--
-- Name: aros_acos id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros_acos ALTER COLUMN id SET DEFAULT nextval('aros_acos_id_seq'::regclass);


--
-- Name: emailconvocations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations ALTER COLUMN id SET DEFAULT nextval('emailconvocations_id_seq'::regclass);


--
-- Name: groupepolitiques id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupepolitiques ALTER COLUMN id SET DEFAULT nextval('groupepolitiques_id_seq'::regclass);


--
-- Name: logacteurs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs ALTER COLUMN id SET DEFAULT nextval('logacteurs_id_seq'::regclass);


--
-- Name: pthemes id; Type: DEFAULT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY pthemes ALTER COLUMN id SET DEFAULT nextval('pthemes_id_seq'::regclass);


--
-- Data for Name: acos; Type: TABLE DATA; Schema: public; Owner: idelibre
--

INSERT INTO acos VALUES (35, 24, NULL, 'writeLoginSession', 67, 68, NULL);
INSERT INTO acos VALUES (58, 49, NULL, 'ldebugHr', 113, 114, NULL);
INSERT INTO acos VALUES (3, 2, NULL, 'getFile', 3, 4, NULL);
INSERT INTO acos VALUES (36, 24, NULL, 'ldebug', 69, 70, NULL);
INSERT INTO acos VALUES (4, 2, NULL, 'getFileJson', 5, 6, NULL);
INSERT INTO acos VALUES (24, 1, NULL, 'Collectivites', 46, 73, NULL);
INSERT INTO acos VALUES (5, 2, NULL, 'setConn', 7, 8, NULL);
INSERT INTO acos VALUES (37, 24, NULL, 'ldebugHr', 71, 72, NULL);
INSERT INTO acos VALUES (6, 2, NULL, 'processLogin', 9, 10, NULL);
INSERT INTO acos VALUES (7, 2, NULL, 'writeLoginConfig', 11, 12, NULL);
INSERT INTO acos VALUES (96, 87, NULL, 'setConn', 189, 190, NULL);
INSERT INTO acos VALUES (8, 2, NULL, 'writeUserConfig', 13, 14, NULL);
INSERT INTO acos VALUES (75, 73, NULL, 'add', 147, 148, NULL);
INSERT INTO acos VALUES (39, 38, NULL, 'setRead', 75, 76, NULL);
INSERT INTO acos VALUES (9, 2, NULL, 'writeLoginSession', 15, 16, NULL);
INSERT INTO acos VALUES (60, 59, NULL, 'index', 117, 118, NULL);
INSERT INTO acos VALUES (10, 2, NULL, 'ldebug', 17, 18, NULL);
INSERT INTO acos VALUES (2, 1, NULL, 'Annexes', 2, 21, NULL);
INSERT INTO acos VALUES (11, 2, NULL, 'ldebugHr', 19, 20, NULL);
INSERT INTO acos VALUES (40, 38, NULL, 'send', 77, 78, NULL);
INSERT INTO acos VALUES (13, 12, NULL, 'ping', 23, 24, NULL);
INSERT INTO acos VALUES (41, 38, NULL, 'sendJson', 79, 80, NULL);
INSERT INTO acos VALUES (14, 12, NULL, 'version', 25, 26, NULL);
INSERT INTO acos VALUES (61, 59, NULL, 'getAllJson', 119, 120, NULL);
INSERT INTO acos VALUES (15, 12, NULL, 'check', 27, 28, NULL);
INSERT INTO acos VALUES (42, 38, NULL, 'setConn', 81, 82, NULL);
INSERT INTO acos VALUES (16, 12, NULL, 'pub', 29, 30, NULL);
INSERT INTO acos VALUES (76, 73, NULL, 'delete', 149, 150, NULL);
INSERT INTO acos VALUES (17, 12, NULL, 'setConn', 31, 32, NULL);
INSERT INTO acos VALUES (43, 38, NULL, 'processLogin', 83, 84, NULL);
INSERT INTO acos VALUES (18, 12, NULL, 'processLogin', 33, 34, NULL);
INSERT INTO acos VALUES (62, 59, NULL, 'getJson', 121, 122, NULL);
INSERT INTO acos VALUES (19, 12, NULL, 'writeLoginConfig', 35, 36, NULL);
INSERT INTO acos VALUES (44, 38, NULL, 'writeLoginConfig', 85, 86, NULL);
INSERT INTO acos VALUES (20, 12, NULL, 'writeUserConfig', 37, 38, NULL);
INSERT INTO acos VALUES (102, 87, NULL, 'ldebugHr', 201, 202, NULL);
INSERT INTO acos VALUES (21, 12, NULL, 'writeLoginSession', 39, 40, NULL);
INSERT INTO acos VALUES (45, 38, NULL, 'writeUserConfig', 87, 88, NULL);
INSERT INTO acos VALUES (22, 12, NULL, 'ldebug', 41, 42, NULL);
INSERT INTO acos VALUES (63, 59, NULL, 'delete', 123, 124, NULL);
INSERT INTO acos VALUES (12, 1, NULL, 'Api300', 22, 45, NULL);
INSERT INTO acos VALUES (23, 12, NULL, 'ldebugHr', 43, 44, NULL);
INSERT INTO acos VALUES (46, 38, NULL, 'writeLoginSession', 89, 90, NULL);
INSERT INTO acos VALUES (25, 24, NULL, 'index', 47, 48, NULL);
INSERT INTO acos VALUES (77, 73, NULL, 'edit', 151, 152, NULL);
INSERT INTO acos VALUES (47, 38, NULL, 'ldebug', 91, 92, NULL);
INSERT INTO acos VALUES (26, 24, NULL, 'add', 49, 50, NULL);
INSERT INTO acos VALUES (64, 59, NULL, 'edit', 125, 126, NULL);
INSERT INTO acos VALUES (27, 24, NULL, 'edit', 51, 52, NULL);
INSERT INTO acos VALUES (38, 1, NULL, 'Convocations', 74, 95, NULL);
INSERT INTO acos VALUES (48, 38, NULL, 'ldebugHr', 93, 94, NULL);
INSERT INTO acos VALUES (28, 24, NULL, 'delete', 53, 54, NULL);
INSERT INTO acos VALUES (29, 24, NULL, 'info', 55, 56, NULL);
INSERT INTO acos VALUES (30, 24, NULL, 'infoJson', 57, 58, NULL);
INSERT INTO acos VALUES (88, 87, NULL, 'getEmails', 173, 174, NULL);
INSERT INTO acos VALUES (65, 59, NULL, 'add', 127, 128, NULL);
INSERT INTO acos VALUES (31, 24, NULL, 'setConn', 59, 60, NULL);
INSERT INTO acos VALUES (50, 49, NULL, 'getFile', 97, 98, NULL);
INSERT INTO acos VALUES (32, 24, NULL, 'processLogin', 61, 62, NULL);
INSERT INTO acos VALUES (33, 24, NULL, 'writeLoginConfig', 63, 64, NULL);
INSERT INTO acos VALUES (78, 73, NULL, 'getAllJson', 153, 154, NULL);
INSERT INTO acos VALUES (66, 59, NULL, 'setConn', 129, 130, NULL);
INSERT INTO acos VALUES (34, 24, NULL, 'writeUserConfig', 65, 66, NULL);
INSERT INTO acos VALUES (51, 49, NULL, 'getFileJson', 99, 100, NULL);
INSERT INTO acos VALUES (97, 87, NULL, 'processLogin', 191, 192, NULL);
INSERT INTO acos VALUES (67, 59, NULL, 'processLogin', 131, 132, NULL);
INSERT INTO acos VALUES (52, 49, NULL, 'setConn', 101, 102, NULL);
INSERT INTO acos VALUES (53, 49, NULL, 'processLogin', 103, 104, NULL);
INSERT INTO acos VALUES (79, 73, NULL, 'getJson', 155, 156, NULL);
INSERT INTO acos VALUES (68, 59, NULL, 'writeLoginConfig', 133, 134, NULL);
INSERT INTO acos VALUES (54, 49, NULL, 'writeLoginConfig', 105, 106, NULL);
INSERT INTO acos VALUES (55, 49, NULL, 'writeUserConfig', 107, 108, NULL);
INSERT INTO acos VALUES (89, 87, NULL, 'sendEmail', 175, 176, NULL);
INSERT INTO acos VALUES (69, 59, NULL, 'writeUserConfig', 135, 136, NULL);
INSERT INTO acos VALUES (56, 49, NULL, 'writeLoginSession', 109, 110, NULL);
INSERT INTO acos VALUES (80, 73, NULL, 'setConn', 157, 158, NULL);
INSERT INTO acos VALUES (57, 49, NULL, 'ldebug', 111, 112, NULL);
INSERT INTO acos VALUES (49, 1, NULL, 'Documents', 96, 115, NULL);
INSERT INTO acos VALUES (110, 103, NULL, 'setConn', 217, 218, NULL);
INSERT INTO acos VALUES (70, 59, NULL, 'writeLoginSession', 137, 138, NULL);
INSERT INTO acos VALUES (81, 73, NULL, 'processLogin', 159, 160, NULL);
INSERT INTO acos VALUES (71, 59, NULL, 'ldebug', 139, 140, NULL);
INSERT INTO acos VALUES (90, 87, NULL, 'emailRappel', 177, 178, NULL);
INSERT INTO acos VALUES (59, 1, NULL, 'Emailconvocations', 116, 143, NULL);
INSERT INTO acos VALUES (72, 59, NULL, 'ldebugHr', 141, 142, NULL);
INSERT INTO acos VALUES (82, 73, NULL, 'writeLoginConfig', 161, 162, NULL);
INSERT INTO acos VALUES (98, 87, NULL, 'writeLoginConfig', 193, 194, NULL);
INSERT INTO acos VALUES (74, 73, NULL, 'index', 145, 146, NULL);
INSERT INTO acos VALUES (91, 87, NULL, 'publipostage', 179, 180, NULL);
INSERT INTO acos VALUES (83, 73, NULL, 'writeUserConfig', 163, 164, NULL);
INSERT INTO acos VALUES (107, 103, NULL, 'params', 211, 212, NULL);
INSERT INTO acos VALUES (84, 73, NULL, 'writeLoginSession', 165, 166, NULL);
INSERT INTO acos VALUES (92, 87, NULL, 'index', 181, 182, NULL);
INSERT INTO acos VALUES (85, 73, NULL, 'ldebug', 167, 168, NULL);
INSERT INTO acos VALUES (99, 87, NULL, 'writeUserConfig', 195, 196, NULL);
INSERT INTO acos VALUES (73, 1, NULL, 'Emailinvitations', 144, 171, NULL);
INSERT INTO acos VALUES (86, 73, NULL, 'ldebugHr', 169, 170, NULL);
INSERT INTO acos VALUES (93, 87, NULL, 'add', 183, 184, NULL);
INSERT INTO acos VALUES (104, 103, NULL, 'index', 205, 206, NULL);
INSERT INTO acos VALUES (100, 87, NULL, 'writeLoginSession', 197, 198, NULL);
INSERT INTO acos VALUES (94, 87, NULL, 'edit', 185, 186, NULL);
INSERT INTO acos VALUES (116, 103, NULL, 'ldebugHr', 229, 230, NULL);
INSERT INTO acos VALUES (95, 87, NULL, 'delete', 187, 188, NULL);
INSERT INTO acos VALUES (105, 103, NULL, 'menu', 207, 208, NULL);
INSERT INTO acos VALUES (108, 103, NULL, 'stats', 213, 214, NULL);
INSERT INTO acos VALUES (101, 87, NULL, 'ldebug', 199, 200, NULL);
INSERT INTO acos VALUES (112, 103, NULL, 'writeLoginConfig', 221, 222, NULL);
INSERT INTO acos VALUES (87, 1, NULL, 'Emailrappels', 172, 203, NULL);
INSERT INTO acos VALUES (111, 103, NULL, 'processLogin', 219, 220, NULL);
INSERT INTO acos VALUES (106, 103, NULL, 'goToClient', 209, 210, NULL);
INSERT INTO acos VALUES (109, 103, NULL, 'statsJson', 215, 216, NULL);
INSERT INTO acos VALUES (115, 103, NULL, 'ldebug', 227, 228, NULL);
INSERT INTO acos VALUES (114, 103, NULL, 'writeLoginSession', 225, 226, NULL);
INSERT INTO acos VALUES (113, 103, NULL, 'writeUserConfig', 223, 224, NULL);
INSERT INTO acos VALUES (103, 1, NULL, 'Environnements', 204, 231, NULL);
INSERT INTO acos VALUES (118, 117, NULL, 'check', 233, 234, NULL);
INSERT INTO acos VALUES (119, 117, NULL, 'setConn', 235, 236, NULL);
INSERT INTO acos VALUES (120, 117, NULL, 'processLogin', 237, 238, NULL);
INSERT INTO acos VALUES (121, 117, NULL, 'writeLoginConfig', 239, 240, NULL);
INSERT INTO acos VALUES (122, 117, NULL, 'writeUserConfig', 241, 242, NULL);
INSERT INTO acos VALUES (123, 117, NULL, 'writeLoginSession', 243, 244, NULL);
INSERT INTO acos VALUES (124, 117, NULL, 'ldebug', 245, 246, NULL);
INSERT INTO acos VALUES (117, 1, NULL, 'Essai', 232, 249, NULL);
INSERT INTO acos VALUES (125, 117, NULL, 'ldebugHr', 247, 248, NULL);
INSERT INTO acos VALUES (1, NULL, NULL, 'controllers', 1, 642, NULL);
INSERT INTO acos VALUES (127, 126, NULL, 'index', 251, 252, NULL);
INSERT INTO acos VALUES (128, 126, NULL, 'listJson', 253, 254, NULL);
INSERT INTO acos VALUES (129, 126, NULL, 'add', 255, 256, NULL);
INSERT INTO acos VALUES (126, 1, NULL, 'Groupepolitiques', 250, 279, NULL);
INSERT INTO acos VALUES (130, 126, NULL, 'edit', 257, 258, NULL);
INSERT INTO acos VALUES (131, 126, NULL, 'detailJson', 259, 260, NULL);
INSERT INTO acos VALUES (132, 126, NULL, 'delete', 261, 262, NULL);
INSERT INTO acos VALUES (133, 126, NULL, 'deleteJson', 263, 264, NULL);
INSERT INTO acos VALUES (134, 126, NULL, 'setConn', 265, 266, NULL);
INSERT INTO acos VALUES (135, 126, NULL, 'processLogin', 267, 268, NULL);
INSERT INTO acos VALUES (136, 126, NULL, 'writeLoginConfig', 269, 270, NULL);
INSERT INTO acos VALUES (137, 126, NULL, 'writeUserConfig', 271, 272, NULL);
INSERT INTO acos VALUES (138, 126, NULL, 'writeLoginSession', 273, 274, NULL);
INSERT INTO acos VALUES (139, 126, NULL, 'ldebug', 275, 276, NULL);
INSERT INTO acos VALUES (140, 126, NULL, 'ldebugHr', 277, 278, NULL);
INSERT INTO acos VALUES (177, 173, NULL, 'delete', 351, 352, NULL);
INSERT INTO acos VALUES (142, 141, NULL, 'index', 281, 282, NULL);
INSERT INTO acos VALUES (230, 227, NULL, 'searchByDate', 457, 458, NULL);
INSERT INTO acos VALUES (143, 141, NULL, 'import', 283, 284, NULL);
INSERT INTO acos VALUES (178, 173, NULL, 'getList', 353, 354, NULL);
INSERT INTO acos VALUES (144, 141, NULL, 'setConn', 285, 286, NULL);
INSERT INTO acos VALUES (145, 141, NULL, 'processLogin', 287, 288, NULL);
INSERT INTO acos VALUES (204, 189, NULL, 'edition', 405, 406, NULL);
INSERT INTO acos VALUES (146, 141, NULL, 'writeLoginConfig', 289, 290, NULL);
INSERT INTO acos VALUES (179, 173, NULL, 'getListJson', 355, 356, NULL);
INSERT INTO acos VALUES (147, 141, NULL, 'writeUserConfig', 291, 292, NULL);
INSERT INTO acos VALUES (148, 141, NULL, 'writeLoginSession', 293, 294, NULL);
INSERT INTO acos VALUES (149, 141, NULL, 'ldebug', 295, 296, NULL);
INSERT INTO acos VALUES (141, 1, NULL, 'Importusers', 280, 299, NULL);
INSERT INTO acos VALUES (150, 141, NULL, 'ldebugHr', 297, 298, NULL);
INSERT INTO acos VALUES (180, 173, NULL, 'getJson', 357, 358, NULL);
INSERT INTO acos VALUES (152, 151, NULL, 'sendJson', 301, 302, NULL);
INSERT INTO acos VALUES (220, 189, NULL, 'setConn', 437, 438, NULL);
INSERT INTO acos VALUES (153, 151, NULL, 'sendAdministratifsJson', 303, 304, NULL);
INSERT INTO acos VALUES (181, 173, NULL, 'rankit', 359, 360, NULL);
INSERT INTO acos VALUES (154, 151, NULL, 'sendInvitesJson', 305, 306, NULL);
INSERT INTO acos VALUES (155, 151, NULL, 'sendMail', 307, 308, NULL);
INSERT INTO acos VALUES (205, 189, NULL, 'getSeanceDataJson', 407, 408, NULL);
INSERT INTO acos VALUES (156, 151, NULL, 'prepareTypeInvitation', 309, 310, NULL);
INSERT INTO acos VALUES (182, 173, NULL, 'setConn', 361, 362, NULL);
INSERT INTO acos VALUES (157, 151, NULL, 'setConn', 311, 312, NULL);
INSERT INTO acos VALUES (158, 151, NULL, 'processLogin', 313, 314, NULL);
INSERT INTO acos VALUES (159, 151, NULL, 'writeLoginConfig', 315, 316, NULL);
INSERT INTO acos VALUES (183, 173, NULL, 'processLogin', 363, 364, NULL);
INSERT INTO acos VALUES (160, 151, NULL, 'writeUserConfig', 317, 318, NULL);
INSERT INTO acos VALUES (161, 151, NULL, 'writeLoginSession', 319, 320, NULL);
INSERT INTO acos VALUES (162, 151, NULL, 'ldebug', 321, 322, NULL);
INSERT INTO acos VALUES (151, 1, NULL, 'Invitations', 300, 325, NULL);
INSERT INTO acos VALUES (163, 151, NULL, 'ldebugHr', 323, 324, NULL);
INSERT INTO acos VALUES (184, 173, NULL, 'writeLoginConfig', 365, 366, NULL);
INSERT INTO acos VALUES (165, 164, NULL, 'display', 327, 328, NULL);
INSERT INTO acos VALUES (206, 189, NULL, 'sendConvocations', 409, 410, NULL);
INSERT INTO acos VALUES (166, 164, NULL, 'setConn', 329, 330, NULL);
INSERT INTO acos VALUES (185, 173, NULL, 'writeUserConfig', 367, 368, NULL);
INSERT INTO acos VALUES (167, 164, NULL, 'processLogin', 331, 332, NULL);
INSERT INTO acos VALUES (168, 164, NULL, 'writeLoginConfig', 333, 334, NULL);
INSERT INTO acos VALUES (169, 164, NULL, 'writeUserConfig', 335, 336, NULL);
INSERT INTO acos VALUES (186, 173, NULL, 'writeLoginSession', 369, 370, NULL);
INSERT INTO acos VALUES (170, 164, NULL, 'writeLoginSession', 337, 338, NULL);
INSERT INTO acos VALUES (171, 164, NULL, 'ldebug', 339, 340, NULL);
INSERT INTO acos VALUES (164, 1, NULL, 'Pages', 326, 343, NULL);
INSERT INTO acos VALUES (172, 164, NULL, 'ldebugHr', 341, 342, NULL);
INSERT INTO acos VALUES (187, 173, NULL, 'ldebug', 371, 372, NULL);
INSERT INTO acos VALUES (174, 173, NULL, 'index', 345, 346, NULL);
INSERT INTO acos VALUES (175, 173, NULL, 'add', 347, 348, NULL);
INSERT INTO acos VALUES (207, 189, NULL, 'sendConvocationsJson', 411, 412, NULL);
INSERT INTO acos VALUES (176, 173, NULL, 'edit', 349, 350, NULL);
INSERT INTO acos VALUES (173, 1, NULL, 'Pthemes', 344, 375, NULL);
INSERT INTO acos VALUES (188, 173, NULL, 'ldebugHr', 373, 374, NULL);
INSERT INTO acos VALUES (221, 189, NULL, 'processLogin', 439, 440, NULL);
INSERT INTO acos VALUES (208, 189, NULL, 'refreshSeance', 413, 414, NULL);
INSERT INTO acos VALUES (190, 189, NULL, 'index', 377, 378, NULL);
INSERT INTO acos VALUES (191, 189, NULL, 'view', 379, 380, NULL);
INSERT INTO acos VALUES (192, 189, NULL, 'add', 381, 382, NULL);
INSERT INTO acos VALUES (209, 189, NULL, 'archive', 415, 416, NULL);
INSERT INTO acos VALUES (193, 189, NULL, 'edit', 383, 384, NULL);
INSERT INTO acos VALUES (194, 189, NULL, 'delete', 385, 386, NULL);
INSERT INTO acos VALUES (236, 227, NULL, 'writeLoginSession', 469, 470, NULL);
INSERT INTO acos VALUES (195, 189, NULL, 'addUsersSeance', 387, 388, NULL);
INSERT INTO acos VALUES (210, 189, NULL, 'check', 417, 418, NULL);
INSERT INTO acos VALUES (196, 189, NULL, 'modifyUserPresence', 389, 390, NULL);
INSERT INTO acos VALUES (197, 189, NULL, 'ajoutnew', 391, 392, NULL);
INSERT INTO acos VALUES (222, 189, NULL, 'writeLoginConfig', 441, 442, NULL);
INSERT INTO acos VALUES (198, 189, NULL, 'ajout', 393, 394, NULL);
INSERT INTO acos VALUES (211, 189, NULL, 'deleteJson', 419, 420, NULL);
INSERT INTO acos VALUES (199, 189, NULL, 'ajoutJson', 395, 396, NULL);
INSERT INTO acos VALUES (200, 189, NULL, 'liste', 397, 398, NULL);
INSERT INTO acos VALUES (201, 189, NULL, 'listeJson', 399, 400, NULL);
INSERT INTO acos VALUES (212, 189, NULL, 'archiveJson', 421, 422, NULL);
INSERT INTO acos VALUES (202, 189, NULL, 'details', 401, 402, NULL);
INSERT INTO acos VALUES (231, 227, NULL, 'searchByTheme', 459, 460, NULL);
INSERT INTO acos VALUES (203, 189, NULL, 'suppression', 403, 404, NULL);
INSERT INTO acos VALUES (223, 189, NULL, 'writeUserConfig', 443, 444, NULL);
INSERT INTO acos VALUES (213, 189, NULL, 'printInfoUsers', 423, 424, NULL);
INSERT INTO acos VALUES (214, 189, NULL, 'ColoredTable', 425, 426, NULL);
INSERT INTO acos VALUES (215, 189, NULL, 'printInfoUsersPdf', 427, 428, NULL);
INSERT INTO acos VALUES (224, 189, NULL, 'writeLoginSession', 445, 446, NULL);
INSERT INTO acos VALUES (216, 189, NULL, 'getzipSeance', 429, 430, NULL);
INSERT INTO acos VALUES (217, 189, NULL, 'genPdfSeance', 431, 432, NULL);
INSERT INTO acos VALUES (218, 189, NULL, 'editJson', 433, 434, NULL);
INSERT INTO acos VALUES (232, 227, NULL, 'setConn', 461, 462, NULL);
INSERT INTO acos VALUES (219, 189, NULL, 'getTokenAEJson', 435, 436, NULL);
INSERT INTO acos VALUES (225, 189, NULL, 'ldebug', 447, 448, NULL);
INSERT INTO acos VALUES (189, 1, NULL, 'Seances', 376, 451, NULL);
INSERT INTO acos VALUES (226, 189, NULL, 'ldebugHr', 449, 450, NULL);
INSERT INTO acos VALUES (240, 239, NULL, 'index', 477, 478, NULL);
INSERT INTO acos VALUES (228, 227, NULL, 'index', 453, 454, NULL);
INSERT INTO acos VALUES (237, 227, NULL, 'ldebug', 471, 472, NULL);
INSERT INTO acos VALUES (229, 227, NULL, 'searchByType', 455, 456, NULL);
INSERT INTO acos VALUES (233, 227, NULL, 'processLogin', 463, 464, NULL);
INSERT INTO acos VALUES (227, 1, NULL, 'Search', 452, 475, NULL);
INSERT INTO acos VALUES (234, 227, NULL, 'writeLoginConfig', 465, 466, NULL);
INSERT INTO acos VALUES (235, 227, NULL, 'writeUserConfig', 467, 468, NULL);
INSERT INTO acos VALUES (238, 227, NULL, 'ldebugHr', 473, 474, NULL);
INSERT INTO acos VALUES (243, 239, NULL, 'editSuperAdmin', 483, 484, NULL);
INSERT INTO acos VALUES (241, 239, NULL, 'add', 479, 480, NULL);
INSERT INTO acos VALUES (242, 239, NULL, 'edit', 481, 482, NULL);
INSERT INTO acos VALUES (244, 239, NULL, 'delete', 485, 486, NULL);
INSERT INTO acos VALUES (245, 239, NULL, 'get_proof', 487, 488, NULL);
INSERT INTO acos VALUES (246, 239, NULL, 'login', 489, 490, NULL);
INSERT INTO acos VALUES (247, 239, NULL, 'logout', 491, 492, NULL);
INSERT INTO acos VALUES (248, 239, NULL, 'mdplost', 493, 494, NULL);
INSERT INTO acos VALUES (249, 239, NULL, 'setConn', 495, 496, NULL);
INSERT INTO acos VALUES (250, 239, NULL, 'processLogin', 497, 498, NULL);
INSERT INTO acos VALUES (251, 239, NULL, 'writeLoginConfig', 499, 500, NULL);
INSERT INTO acos VALUES (252, 239, NULL, 'writeUserConfig', 501, 502, NULL);
INSERT INTO acos VALUES (253, 239, NULL, 'writeLoginSession', 503, 504, NULL);
INSERT INTO acos VALUES (254, 239, NULL, 'ldebug', 505, 506, NULL);
INSERT INTO acos VALUES (239, 1, NULL, 'Srvusers', 476, 509, NULL);
INSERT INTO acos VALUES (255, 239, NULL, 'ldebugHr', 507, 508, NULL);
INSERT INTO acos VALUES (257, 256, NULL, 'users', 511, 512, NULL);
INSERT INTO acos VALUES (258, 256, NULL, 'lastSeances', 513, 514, NULL);
INSERT INTO acos VALUES (256, 1, NULL, 'Statistics', 510, 529, NULL);
INSERT INTO acos VALUES (259, 256, NULL, 'setConn', 515, 516, NULL);
INSERT INTO acos VALUES (260, 256, NULL, 'processLogin', 517, 518, NULL);
INSERT INTO acos VALUES (261, 256, NULL, 'writeLoginConfig', 519, 520, NULL);
INSERT INTO acos VALUES (262, 256, NULL, 'writeUserConfig', 521, 522, NULL);
INSERT INTO acos VALUES (263, 256, NULL, 'writeLoginSession', 523, 524, NULL);
INSERT INTO acos VALUES (264, 256, NULL, 'ldebug', 525, 526, NULL);
INSERT INTO acos VALUES (265, 256, NULL, 'ldebugHr', 527, 528, NULL);
INSERT INTO acos VALUES (306, 292, NULL, 'writeUserConfig', 609, 610, NULL);
INSERT INTO acos VALUES (267, 266, NULL, 'getToken', 531, 532, NULL);
INSERT INTO acos VALUES (268, 266, NULL, 'setConn', 533, 534, NULL);
INSERT INTO acos VALUES (307, 292, NULL, 'writeLoginSession', 611, 612, NULL);
INSERT INTO acos VALUES (269, 266, NULL, 'processLogin', 535, 536, NULL);
INSERT INTO acos VALUES (270, 266, NULL, 'writeLoginConfig', 537, 538, NULL);
INSERT INTO acos VALUES (271, 266, NULL, 'writeUserConfig', 539, 540, NULL);
INSERT INTO acos VALUES (308, 292, NULL, 'ldebug', 613, 614, NULL);
INSERT INTO acos VALUES (272, 266, NULL, 'writeLoginSession', 541, 542, NULL);
INSERT INTO acos VALUES (273, 266, NULL, 'ldebug', 543, 544, NULL);
INSERT INTO acos VALUES (266, 1, NULL, 'Tokens', 530, 547, NULL);
INSERT INTO acos VALUES (274, 266, NULL, 'ldebugHr', 545, 546, NULL);
INSERT INTO acos VALUES (292, 1, NULL, 'Users', 582, 617, NULL);
INSERT INTO acos VALUES (309, 292, NULL, 'ldebugHr', 615, 616, NULL);
INSERT INTO acos VALUES (276, 275, NULL, 'index', 549, 550, NULL);
INSERT INTO acos VALUES (277, 275, NULL, 'add', 551, 552, NULL);
INSERT INTO acos VALUES (310, 1, NULL, 'AclExtras', 618, 619, NULL);
INSERT INTO acos VALUES (278, 275, NULL, 'edit', 553, 554, NULL);
INSERT INTO acos VALUES (279, 275, NULL, 'delete', 555, 556, NULL);
INSERT INTO acos VALUES (280, 275, NULL, 'users', 557, 558, NULL);
INSERT INTO acos VALUES (281, 275, NULL, 'usersJson', 559, 560, NULL);
INSERT INTO acos VALUES (282, 275, NULL, 'listJson', 561, 562, NULL);
INSERT INTO acos VALUES (283, 275, NULL, 'deleteJson', 563, 564, NULL);
INSERT INTO acos VALUES (284, 275, NULL, 'detailJson', 565, 566, NULL);
INSERT INTO acos VALUES (285, 275, NULL, 'setConn', 567, 568, NULL);
INSERT INTO acos VALUES (286, 275, NULL, 'processLogin', 569, 570, NULL);
INSERT INTO acos VALUES (313, 312, NULL, 'history_state', 622, 623, NULL);
INSERT INTO acos VALUES (287, 275, NULL, 'writeLoginConfig', 571, 572, NULL);
INSERT INTO acos VALUES (288, 275, NULL, 'writeUserConfig', 573, 574, NULL);
INSERT INTO acos VALUES (289, 275, NULL, 'writeLoginSession', 575, 576, NULL);
INSERT INTO acos VALUES (290, 275, NULL, 'ldebug', 577, 578, NULL);
INSERT INTO acos VALUES (275, 1, NULL, 'Types', 548, 581, NULL);
INSERT INTO acos VALUES (291, 275, NULL, 'ldebugHr', 579, 580, NULL);
INSERT INTO acos VALUES (314, 312, NULL, 'sql_explain', 624, 625, NULL);
INSERT INTO acos VALUES (293, 292, NULL, 'index', 583, 584, NULL);
INSERT INTO acos VALUES (294, 292, NULL, 'listJson', 585, 586, NULL);
INSERT INTO acos VALUES (295, 292, NULL, 'add', 587, 588, NULL);
INSERT INTO acos VALUES (315, 312, NULL, 'setConn', 626, 627, NULL);
INSERT INTO acos VALUES (296, 292, NULL, 'edit', 589, 590, NULL);
INSERT INTO acos VALUES (297, 292, NULL, 'getUserJson', 591, 592, NULL);
INSERT INTO acos VALUES (298, 292, NULL, 'listActorsJson', 593, 594, NULL);
INSERT INTO acos VALUES (299, 292, NULL, 'delete', 595, 596, NULL);
INSERT INTO acos VALUES (316, 312, NULL, 'processLogin', 628, 629, NULL);
INSERT INTO acos VALUES (300, 292, NULL, 'login', 597, 598, NULL);
INSERT INTO acos VALUES (301, 292, NULL, 'logout', 599, 600, NULL);
INSERT INTO acos VALUES (302, 292, NULL, 'mdplost', 601, 602, NULL);
INSERT INTO acos VALUES (303, 292, NULL, 'setConn', 603, 604, NULL);
INSERT INTO acos VALUES (317, 312, NULL, 'writeLoginConfig', 630, 631, NULL);
INSERT INTO acos VALUES (304, 292, NULL, 'processLogin', 605, 606, NULL);
INSERT INTO acos VALUES (305, 292, NULL, 'writeLoginConfig', 607, 608, NULL);
INSERT INTO acos VALUES (318, 312, NULL, 'writeUserConfig', 632, 633, NULL);
INSERT INTO acos VALUES (319, 312, NULL, 'writeLoginSession', 634, 635, NULL);
INSERT INTO acos VALUES (320, 312, NULL, 'ldebug', 636, 637, NULL);
INSERT INTO acos VALUES (311, 1, NULL, 'DebugKit', 620, 641, NULL);
INSERT INTO acos VALUES (312, 311, NULL, 'ToolbarAccess', 621, 640, NULL);
INSERT INTO acos VALUES (321, 312, NULL, 'ldebugHr', 638, 639, NULL);


--
-- Data for Name: annexes; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: annotationv3; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: annotationv3_users; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: aros; Type: TABLE DATA; Schema: public; Owner: idelibre
--

INSERT INTO aros VALUES (1, NULL, 'Group', 'Administrateurs', 1, 4, '26b380d8-93b2-41e9-9f68-da98c55ccb30');
INSERT INTO aros VALUES (2, NULL, 'Group', 'Utilisateurs', 5, 6, '9f803280-eb21-46b3-bcec-74ffc4d2aab8');
INSERT INTO aros VALUES (3, NULL, 'Group', 'Acteur', 7, 8, 'b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16');
INSERT INTO aros VALUES (4, NULL, 'Group', 'Invites', 9, 10, 'aa130693-e6d0-4893-ba31-98892b98581f');
INSERT INTO aros VALUES (5, NULL, 'Group', 'Administratifs', 11, 12, 'db68a3c7-0119-40f1-b444-e96f568b3d67');
INSERT INTO aros VALUES (6, 1, 'User', 'Administrateur', 2, 3, 'e64e47b6-9027-4d73-a49b-70fd2ee0475e');


--
-- Data for Name: aros_acos; Type: TABLE DATA; Schema: public; Owner: idelibre
--

INSERT INTO aros_acos VALUES (1, 1, 29, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (2, 1, 103, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (3, 1, 50, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (4, 1, 51, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (5, 1, 3, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (6, 1, 4, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (7, 1, 38, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (8, 1, 189, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (9, 1, 173, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (10, 1, 275, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (11, 1, 292, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (12, 1, 59, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (13, 1, 126, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (14, 2, 103, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (15, 2, 198, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (16, 2, 200, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (17, 2, 202, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (18, 2, 204, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (19, 2, 203, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (20, 2, 206, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (21, 2, 38, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (22, 2, 295, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (23, 2, 173, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (24, 2, 275, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (25, 2, 227, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (26, 2, 209, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (27, 2, 189, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (28, 2, 50, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (29, 2, 51, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (30, 2, 3, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (31, 2, 4, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (32, 3, 103, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (33, 3, 189, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (34, 3, 38, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (35, 3, 50, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (36, 3, 3, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (37, 4, 103, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (38, 4, 189, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (39, 4, 38, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (40, 4, 50, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (41, 4, 3, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (42, 5, 103, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (43, 5, 189, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (44, 5, 38, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (45, 5, 50, '1', '1', '1', '1');
INSERT INTO aros_acos VALUES (46, 5, 3, '1', '1', '1', '1');


--
-- Data for Name: convocations; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: emailconvocations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO emailconvocations VALUES (1, 'convocation pour la seance #typeseance# le #dateseance# à #heureseance#', '#prenom# #nom#,

vous avez reçu une convocation pour la seance #typeseance# le #dateseance# à #heureseance#.
Veuillez vous connecter sur votre application i-delibRE pour en prendre connaissance.', NULL, NULL);


--
-- Data for Name: emailinvitations; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: groupepolitiques; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO groupepolitiques VALUES (1, 'sans étiquette');


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: idelibre
--

INSERT INTO groups VALUES ('26b380d8-93b2-41e9-9f68-da98c55ccb30', 'Administrateurs', '2013-02-11 11:21:45.788881', '2013-06-13 17:19:10', true, false, false, false);
INSERT INTO groups VALUES ('9f803280-eb21-46b3-bcec-74ffc4d2aab8', 'Utilisateurs', '2013-02-11 11:21:45.832553', '2013-06-26 13:36:49', false, false, true, false);
INSERT INTO groups VALUES ('b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16', 'Acteur', '2013-02-11 11:21:45.805519', '2013-06-26 13:36:31', false, true, false, true);
INSERT INTO groups VALUES ('aa130693-e6d0-4893-ba31-98892b98581f', 'Invites', NULL, NULL, false, false, false, true);
INSERT INTO groups VALUES ('db68a3c7-0119-40f1-b444-e96f568b3d67', 'Administratifs', NULL, NULL, false, false, false, true);


--
-- Data for Name: invitations; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: logacteurs; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: projets; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: pthemes; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: seances; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: types; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: types_autorized; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: types_users; Type: TABLE DATA; Schema: public; Owner: idelibre
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: idelibre
--

INSERT INTO users VALUES ('e64e47b6-9027-4d73-a49b-70fd2ee0475e', 'Administrateur', 'Administrateur', 'Administrateur', 'mot de pass non valide', NULL, '26b380d8-93b2-41e9-9f68-da98c55ccb30', '2013-11-07 15:54:40.942895', '2013-11-07 15:54:40.942895', true, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, true, NULL, NULL, NULL);


--
-- Name: acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('acos_id_seq', 321, true);


--
-- Name: aros_acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('aros_acos_id_seq', 46, true);


--
-- Name: aros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('aros_id_seq', 6, true);


--
-- Name: emailconvocations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('emailconvocations_id_seq', 1, true);


--
-- Name: groupepolitiques_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('groupepolitiques_id_seq', 1, true);


--
-- Name: logacteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('logacteurs_id_seq', 1, false);


--
-- Name: pthemes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelibre
--

SELECT pg_catalog.setval('pthemes_id_seq', 1, true);


--
-- Name: acos acos_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY acos
    ADD CONSTRAINT acos_pkey PRIMARY KEY (id);


--
-- Name: annexes annexes_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY annexes
    ADD CONSTRAINT annexes_pkey PRIMARY KEY (id);


--
-- Name: annotationv3 annotationv3_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3
    ADD CONSTRAINT annotationv3_pkey PRIMARY KEY (id);


--
-- Name: aros_acos aros_acos_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros_acos
    ADD CONSTRAINT aros_acos_pkey PRIMARY KEY (id);


--
-- Name: aros aros_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY aros
    ADD CONSTRAINT aros_pkey PRIMARY KEY (id);


--
-- Name: seances convocations_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT convocations_pkey PRIMARY KEY (id);


--
-- Name: convocations convocusers_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_pkey PRIMARY KEY (id);


--
-- Name: documents documents_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: emailconvocations emailconvocations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations
    ADD CONSTRAINT emailconvocations_pkey PRIMARY KEY (id);


--
-- Name: emailconvocations emailconvocations_unique_type_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations
    ADD CONSTRAINT emailconvocations_unique_type_id UNIQUE (type_id);


--
-- Name: groupepolitiques groupepolitiques_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groupepolitiques
    ADD CONSTRAINT groupepolitiques_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: logacteurs logacteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs
    ADD CONSTRAINT logacteurs_pkey PRIMARY KEY (id);


--
-- Name: groups name_unique_idx; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT name_unique_idx UNIQUE (name);


--
-- Name: projets projets_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_pkey PRIMARY KEY (id);


--
-- Name: pthemes pthemes_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY pthemes
    ADD CONSTRAINT pthemes_pkey PRIMARY KEY (id);


--
-- Name: seances seances_alias_key; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT seances_alias_key UNIQUE (alias);


--
-- Name: types types_name_unique_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_name_unique_pkey UNIQUE (name);


--
-- Name: types types_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_pkey PRIMARY KEY (id);


--
-- Name: types_users types_users_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_pkey PRIMARY KEY (id);


--
-- Name: users user_username_unique_key; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT user_username_unique_key UNIQUE (username);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: aro_aco_key; Type: INDEX; Schema: public; Owner: idelibre
--

CREATE UNIQUE INDEX aro_aco_key ON aros_acos USING btree (aro_id, aco_id);


--
-- Name: invitations deleteinvitationtrigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER deleteinvitationtrigger AFTER DELETE ON invitations FOR EACH ROW EXECUTE PROCEDURE deletedocument();


--
-- Name: projets deleteprojettrigger; Type: TRIGGER; Schema: public; Owner: idelibre
--

CREATE TRIGGER deleteprojettrigger AFTER DELETE ON projets FOR EACH ROW EXECUTE PROCEDURE deletedocument();


--
-- Name: seances deleteseancetrigger; Type: TRIGGER; Schema: public; Owner: idelibre
--

CREATE TRIGGER deleteseancetrigger AFTER DELETE ON seances FOR EACH ROW EXECUTE PROCEDURE deletedocument();


--
-- Name: annexes annexes_projet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY annexes
    ADD CONSTRAINT annexes_projet_id_fkey FOREIGN KEY (projet_id) REFERENCES projets(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: annotationv3_users annotationv3_users_annotationv3_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3_users
    ADD CONSTRAINT annotationv3_users_annotationv3_id_fkey FOREIGN KEY (annotationv3_id) REFERENCES annotationv3(id) ON DELETE CASCADE;


--
-- Name: annotationv3_users annotationv3_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY annotationv3_users
    ADD CONSTRAINT annotationv3_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: seances convocations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT convocations_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: convocations convocusers_convocation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_convocation_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: convocations convocusers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY convocations
    ADD CONSTRAINT convocusers_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: emailconvocations emailconvocations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailconvocations
    ADD CONSTRAINT emailconvocations_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON DELETE SET NULL;


--
-- Name: emailinvitations emailinvitations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY emailinvitations
    ADD CONSTRAINT emailinvitations_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id);


--
-- Name: invitations invitations_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON DELETE CASCADE;


--
-- Name: invitations invitations_seance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_seance_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON DELETE CASCADE;


--
-- Name: invitations invitations_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: logacteurs logacteurs_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY logacteurs
    ADD CONSTRAINT logacteurs_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: projets projets_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: projets projets_ptheme_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_ptheme_id_fkey FOREIGN KEY (ptheme_id) REFERENCES pthemes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: projets projets_seance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_seance_id_fkey FOREIGN KEY (seance_id) REFERENCES seances(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: projets projets_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY projets
    ADD CONSTRAINT projets_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: seances seances_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY seances
    ADD CONSTRAINT seances_document_id_fkey FOREIGN KEY (document_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: types_autorized types_autorized_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY types_autorized
    ADD CONSTRAINT types_autorized_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON DELETE CASCADE;


--
-- Name: types_autorized types_autorized_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY types_autorized
    ADD CONSTRAINT types_autorized_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: types_users types_users_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: types_users types_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY types_users
    ADD CONSTRAINT types_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_groupepolitique_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: idelibre
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_groupepolitique_id_fkey FOREIGN KEY (groupepolitique_id) REFERENCES groupepolitiques(id) ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--




CREATE TABLE types_authorizeds (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    type_id uuid NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);

ALTER TABLE types_authorizeds ADD  FOREIGN KEY(user_id) REFERENCES users(id) on delete cascade;
ALTER TABLE types_authorizeds ADD  FOREIGN KEY(type_id) REFERENCES types(id) on delete cascade;


alter table convocations add istoken boolean default false;
alter table convocations add isemailed boolean default false;



insert INTO emailinvitations values ('5a0ea5a3-2240-46e1-ba36-53b55cde535b',null, 'message par defaut', 'message par defaut',now(), now(), 'message par defaut');

ALTER TABLE users ADD bcrypt VARCHAR(255);


alter table emailconvocations drop constraint emailconvocations_type_id_fkey;
alter table emailconvocations add foreign key(type_id) REFERENCES types(id) on delete cascade;


alter table emailinvitations drop constraint emailinvitations_type_id_fkey;
alter table emailinvitations add foreign key(type_id) REFERENCES types(id) on delete cascade;


alter table users add blacklisted boolean default false;