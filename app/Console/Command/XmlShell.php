<?php

/**
 * CakePHP WsClient Shell
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Console/Command/XmlShell.php $
 * $Id: XmlShell.php 302 2013-10-21 15:57:34Z ssampaio $
 */
App::uses('ClassRegistry', 'Utility');
App::uses('Xml', 'Utility');

class XmlShell extends AppShell {

	public function main() {


		$xmlArray1 = Xml::toArray(Xml::build(file_get_contents("keepassx_merge1.xml")));
		$xmlArray2 = Xml::toArray(Xml::build(file_get_contents("keepassx_merge2.xml")));
		$xmlArray3 = Xml::toArray(Xml::build(file_get_contents("keepassx_merge3.xml")));
		$xmlArray = Set::merge($xmlArray1, $xmlArray2, $xmlArray3);

		file_put_contents("test.php", '<?php ' . var_export($xmlArray, true) . '; ?>');
		file_put_contents("test.xml", Xml::fromArray($xmlArray)->asXML());

		$this->showTreeGroup($xmlArray['database']);
	}

	public function showTreeGroup($node, $span = '') {

		$nextSpan = $span .= '     ';

		//groups
		if (!empty($node['group'])) {
			if (!empty($node['group']['title'])) {
				$this->out($span . ' Group : ');
				$this->out($span . '       _ title: ' . $node['group']['title']);
				$this->showTreeGroup($node['group'], $nextSpan);
			} else {
				for ($i = 0; $i < count($node['group']); $i++) {
					$this->out($span . ' Group : ');
					$this->out($span . '       _ title: ' . $node['group'][$i]['title']);
					$this->showTreeGroup($node['group'][$i], $nextSpan);
				}
			}
		}

		//entry
		if (!empty($node['entry'])) {
			if (!empty($node['entry']['title'])) {
				$this->out($span . ' Entry : ');
				$this->out($span . '       _ title: ' . $node['entry']['title']);
				$this->out($span . '       _ creation : ' . $node['entry']['creation']);
				$this->out($span . '       _ lastacess : ' . $node['entry']['lastaccess']);
				$this->out($span . '       _ lastmod : ' . $node['entry']['lastmod']);
			} else {
				for ($i = 0; $i < count($node['entry']); $i++) {
					$this->out($span . ' Entry : ');
					$this->out($span . '       _ title: ' . $node['entry'][$i]['title']);
					$this->out($span . '       _ creation: ' . $node['entry'][$i]['creation']);
					$this->out($span . '       _ lastaccess: ' . $node['entry'][$i]['lastaccess']);
					$this->out($span . '       _ lastmod : ' . $node['entry'][$i]['lastmod']);
				}
			}
		}
	}

}
