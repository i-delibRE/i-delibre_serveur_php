<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 22/08/18
 * Time: 10:52
 */



App::uses("Document", "Model");
App::uses("User", "Model");
App::uses("Collectivite", "Model");
App::uses("Convocation", "Model");
App::uses("ConnectionManager", "Model");
App::uses('DbDynCon', 'Utility');

class HorodateConvocationShell extends AppShell {

    public $collectiviteName = "Vitre Communaute";
    public $collectiviteId = "5a707a6c-fe90-4cb1-8187-0cbf5b79395d";
    public $username = "Technicien libriciel";
    public $seanceId = '5b7c1d02-988c-4e1c-98e6-6bb291ef9a8b';
    public $conn = 'vitrecommunauteorg';



    public function horodateAll(){
        $this->loadConnection($this->conn);
        $Convocation = ClassRegistry::init('Convocation');
        $Convocation->useDbConfig = $this->conn;

        $convocs = $Convocation->find('all', array(
            'conditions' => array(
                'seance_id' => $this->seanceId
            ),
            'fields' => array(
                'id'
            )
        ));

        foreach($convocs as $convoc){
            $this->out($convoc["Convocation"]['id']);
            $this->horodate($convoc["Convocation"]['id']);

        }

    }



    public function sendMailAll(){
        $this->loadConnection($this->conn);
        $Convocation = ClassRegistry::init('Convocation');
        $Convocation->useDbConfig = $this->conn;

        $convocs = $Convocation->find('all', array(
            'conditions' => array(
                'seance_id' => $this->seanceId
            )
        ));

        foreach($convocs as $convoc){
            $this->sendMail($convoc);
        }

    }




    private function horodate($convocation_id) {


        $this->loadConnection($this->conn);
        $Convocation = ClassRegistry::init('Convocation');
        $Convocation->useDbConfig = $this->conn;


        //récupération de la convocation
        $convocToSend = $Convocation->find('first', array('conditions' => array('Convocation.id' => $convocation_id)));
        //préparation des nouvelles informations de la convocation
        $convocToSend['Convocation']['active'] = true;


        $year = date("Y");
        $collId = $this->collectiviteId;

        $folder = TOKEN . $collId . DS . $year;
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $folder = TOKEN . $collId . DS . $year . DS . $this->seanceId;
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $TSFile = new File($folder . DS . 'idelibre_horodatage_' . Security::hash($convocation_id));


        //préparation des nouvelles informations de la convocation
        $convocToSend['Convocation']['active'] = true;
        $convocToSend['Convocation']['ae_received'] = date('Y-m-d H:i:s');

        //Ecriture du fichier d'horodatage
        $tsFileContent = "i-delibRE - Timestamp\n" .
            "controller : " . $this->name . "\n" .
            "action : " . "script" . "\n" .
            "organisation : " . $this->collectiviteName . " : " . $this->collectiviteId . "\n" .
            "user : " . $this->username . "\n" .
            "seance : " . $this->seanceId . "\n" .
            "convocation : " . $convocation_id . "\n" .
            "server timestamp : " . $convocToSend['Convocation']['ae_received'];
        $TSFile->write($tsFileContent);

        //Horodatagevim
        try {
            if (Horodatage::horodateFichier($TSFile->path)) { //si l horodatage est configuré et fonctionnel
                $return['horodatage']['success'] = true;
                $convocToSend['Convocation']['ae_horodatage'] = date('Y-m-d H:i:s');
                $this->out("success : " . $convocation_id);
            } else { //sinon on utilise la date systeme
                $convocToSend['Convocation']['ae_horodatage'] = date('Y-m-d H:i:s');
                $return['horodatage']['success'] = false;
                $this->out("success :  avec date systeme" . $convocation_id);
            }
        } catch (Exception $exc) {
            $return['horodatage']['message'] = __d('convocation', 'Convocation.horodatage.error');
            if (Configure::read('debug') > 0) {
                $return['horodatage']['exception'] = $exc->getTraceAsString();
                $return['horodatage']['success'] = false;
                $this->out("oups: " .  $convocation_id);
            }
        }


        $this->out('end : ' . $convocation_id);

    }





    public function sendMail($convocToSend) {

        $this->loadConnection($this->conn);
        $User = ClassRegistry::init('User');
        $User->useDbConfig = $this->conn;


        $User->id = $convocToSend['Convocation']['user_id'];
        $conv = $this->prepareTypeConvocation($convocToSend);
        $mailto = $User->field('mail');
        // Paramètres mail
        $Email = new CakeEmail('convocation');
        var_dump($conv['Emailconvocation']['contenu']);

        if (!empty($mailto)) {
            $Email->to($mailto);
            $Email->subject($conv['Emailconvocation']['sujet']);
        }
        //envoi
        $result = null;
        try {
            $result = $Email->send($conv['Emailconvocation']['contenu']);
        } catch (Exception $e) {
            $this->log($e->getMessage(), LOG_ERROR);
        }

        //traitement du retour
        return !empty($result);
    }





    public function prepareTypeConvocation($convocToSend) {


        $this->loadConnection($this->conn);
        $User = ClassRegistry::init('User');
        $User->useDbConfig = $this->conn;

        $Seance = ClassRegistry::init('Seance');
        $Seance->useDbConfig = $this->conn;




        $User->id = $convocToSend['Convocation']['user_id'];

        $this->setConn($this->conn);

        $seance = $Seance->find('first', array(
            'conditions' => array(
                'Seance.id' => $convocToSend['Convocation']['seance_id']
            ),
            'fields' => array(
                'Seance.name', 'Seance.place', 'Seance.date_seance', 'Seance.type_id'
            ),
            'contain' => array(
                'Type.name'
            )
        ));

        $typeId = $seance['Seance']['type_id'];
        //recupération de la convocation
        $Convoc = ClassRegistry::init('Emailconvocation');
        $Convoc->useDbConfig = $this->conn;

        $convoc = $Convoc->find('first', array(
            'conditions' => array(
                'type_id' => $typeId
            )
        ));
//si empty alors prendre le default !
        if(empty($convoc)){
            $convoc = $Convoc->find('first', array(
                'conditions' => array(
                    'type_id IS NULL'
                )
            ));
        }
        // variable de la convocation
        $evaluate = array();
        $evaluate['nom'] = $User->field('lastname');
        $evaluate['prenom'] = $User->field('firstname');
        $evaluate['titre'] = $User->field('titre');

        $civiliteInt = $User->field('civilite');

        if($civiliteInt == 1)
            $evaluate['civilite'] = "madame";
        elseif ($civiliteInt == 2)
            $evaluate['civilite'] = "monsieur";
        else
            $evaluate['civilite'] = "";

        $evaluate['nomseance'] = $seance['Seance']['name'];
        $evaluate['lieuseance'] = $seance['Seance']['place'];
        $evaluate['typeseance'] = $seance['Type']['name'];

        $dateTime = $seance['Seance']['date_seance'];
        //non compatible php ubuntu 12.04
        //$date = explode(' ', $dateTime)[0];
        $predate = explode(' ', $dateTime);
        $date = $predate[0];
        //non compatible php ubuntu 12.04
        //$time = explode(' ', $dateTime)[1];
        $pretime = explode(' ', $dateTime);
        $time = $pretime[1];

        $exploded = explode('-', $date);
        $formatedDate = $exploded[2] . '/' . $exploded[1] . '/' . $exploded[0];
        $exploded = explode(':', $time);
        $formatedTime = $exploded[0] . 'h' . $exploded[1];

        $evaluate['dateseance'] = $formatedDate;
        $evaluate['heureseance'] = $formatedTime;
        $res = ConvertionBalise::evaluate($evaluate, $convoc);
        return $res;
    }












    private function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }
        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }



    public function setConn($conn) {
        if (!empty($conn)) {
            $this->loadConnection($conn);
            Configure::write('conn', $conn);
            if ($conn != 'default') {
                Configure::write('Acl.database', $conn);
                $this->loadModel('Aro');
                $this->Aro->setDataSource($conn);
                $this->loadModel('Aco');
                $this->Aco->setDataSource($conn);
                $this->loadModel('Permission');
                $this->Permission->setDataSource($conn);
            }
        }
    }




}