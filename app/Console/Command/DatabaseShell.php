<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 02/03/18
 * Time: 14:03
 */
App::uses("Collectivite", "Model");
App::uses("ConnectionManager", "Model");



class DatabaseShell extends AppShell
{


    public function initDatabases(){
        if($this->isInit()){
            debug("La base de donnees est deja initialisee");
            return;
        }else{
            $this->initAdmin();
            $this->initTemplate();
        }
    }


    private function isInit(){
        $db = ConnectionManager::getDataSource('default');
        $res = null;
        try{
            $res = $db->rawQuery("select id from srvUsers;");
        }catch(Exception $e){
            debug("La base de donnees n est pas initialisee");
        }

        if($res){
            return true;
        }else{
            return false;
        }


    }



//
//    public function initDatabases(){
//        $this->initAdmin();
//        $this->initTemplate();
//    }
//
//
    public function initTemplate(){
        // Create database template
        $db = ConnectionManager::getDataSource('default');
        $db->rawQuery("create database idelibre_template owner idelibre;");
        // Populate database
        $source = ConnectionManager::getDataSource('template');
        $sqlTemplateFile = CONFIG . "sql" . DS . "idelibre_template_insert.sql";
        $this->importSql($source->config, $sqlTemplateFile );
    }



    public function initAdmin(){
        $source = ConnectionManager::getDataSource('default');
        $sqlAdminFile = CONFIG . "sql" . DS . "adminInsert.sql";
        $this->importSql($source->config, $sqlAdminFile );
    }


    private function importSql($conf, $sqlFilePath){
        $conn_string =
            "host=" . $conf["host"] . " port=" . $conf["port"] . " dbname=" . $conf["database"] . " user=" .$conf['login'] .  " password=" . $conf["password"];
        $dbconn = pg_connect($conn_string);

        if(!$dbconn){
            $this->out("erreur de connection");
        }
        $sql = file_get_contents($sqlFilePath);
        pg_query($dbconn, $sql);
    }


}