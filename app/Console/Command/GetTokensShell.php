<?php //

App::uses("Convocation", "Model");
App::uses("Collectivite", "Model");
App::uses("ConnectionManager", "Model");

class GetTokensShell extends AppShell {
//   ./cake BlobToFile transformFS -c newcollfr

    // add path to table documents

    public $WORKSPACE = "/tmp/tokens/";

    function transformFS() {

        $conn = $this->params['connection'];

        //get the collectiviteId
   /*     $Collectivite = ClassRegistry::init('Collectivite');
        $Collectivite->useDbConfig = "default";

        $collectivite = $Collectivite->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                'conn' => $conn
            )
        ));

        $collectiviteId = $collectivite['Collectivite']['id'];

        $this->out(var_export($collectiviteId, true));
*/




        $this->loadConnection($conn);



        // $Document = ClassRegistry::init('Document');
        $Convocation = ClassRegistry::init('Convocation');
        $Convocation->useDbConfig = $conn;

        $convocations = $Convocation->find('all', array(
            'conditions' => array(
                'Convocation.seance_id' => "5817597c-59cc-4910-8172-7cf6c18e9eb7"
            ),
            'fields' => array('id')
        ));



        $convocations = Hash::extract($convocations, '{n}.Convocation.id');


        $this->out(var_export($convocations, true));

        foreach ($convocations as $convocId) {
            $ar_horodatage_file = $Convocation->find('first', array(
                'fields' => array('ar_horodatage_file'),
                'conditions' => array('id' => $convocId)
            ));



            $folder = $this->WORKSPACE;
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }

            $res = file_put_contents($folder . DS . $convocId, $ar_horodatage_file['Convocation']['ar_horodatage_file']);
            if (!$res) {
                $errorConvert =array();
                $errorConvert['type'] = "ERROR";
                $errorConvert['conn'] = $conn;
                $this->out(var_export($errorConvert, true));
                $this->log(var_export($errorConvert, true), "transformFS");
                return;

            }
            $this->out($convocId);



        }
/*
        $successConvert = array();
        $successConvert['type'] ="SUCCESS";
        $successConvert['$conn'] = $conn;
ae_horodatage_file
ae_horodatage_token

        $successConvert['$collId'] = $collectiviteId;*/
        $this->log(var_export("OVER", true), "token");


    }

    //dynamically load connexion
    public function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }

        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }







    public function showAllConnections() {
        foreach (glob(APP . "Config/Connections/*") as $filename) {
            include_once $filename;

            $connectionName = basename($filename);
            ConnectionManager::create($connectionName, ${$connectionName});
        }

        $connectionList = ConnectionManager::enumConnectionObjects();
        foreach ($connectionList as $key => $val){
            $this->out($key);
        }
    }





    /**
     * Paramétrages et aides du shell.
     */
    public function getOptionParser() {
        $Parser = parent::getOptionParser();


        $Parser->addOptions(array(
                'connection' => array(
                    'short' => 'c',
                    'help' => 'précise la connection',
                    //'choices' => array('true', 'false'),
                    //'default' => 'true'
                )
            )
        );



        return $Parser;
    }


}
