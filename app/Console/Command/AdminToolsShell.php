<?php

/**
 *
 * AdminTools shell class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 *
 *
 * SVN Informations
 * $Date: 2014-08-11 15:26:57 +0200 (lun. 11 août 2014) $
 * $Revision: 553 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Console/Command/AdminToolsShell.php $
 * $Id: AdminToolsShell.php 553 2014-08-11 13:26:57Z ssampaio $
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 *
 */
App::uses('Cache', 'Cache');

App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
App::uses('ComponentCollection', 'Controller');

App::uses('AuthComponent', 'Controller/Component');
App::uses('AclComponent', 'Controller/Component');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

App::uses('Model', 'AppModel');
App::uses('DbAcl', 'Model');
App::uses('AclNode', 'Model');
App::uses('Aco', 'Model');
App::uses('Aro', 'Model');

App::uses('ConnectionManager', 'Model');

App::uses('DbDynCon', 'Utility');

class AdminToolsShell extends Shell {

    /**
     *
     * @var Model
     */
    private $User;

    /**
     *
     * @var Model
     */
    private $Collectivite;

    /**
     *
     * @var Model
     */
    private $Group;

    /**
     *
     * @var boolean
     */
    private $_showHelp;

    /**
     *
     * @var string
     */
    private $_bkpdir;

    /**
     *
     * @var string
     */
    private $_bkpdate;

    /**
     *
     * @var string
     */
    private $_fsOwner = "www-data";

    /**
     *
     * @var string
     */
    private $_fsGroup = "www-data";

    /**
     *
     * @var string
     */
    private $_conn;

    public function initialize() {
        parent::initialize();
        //chargement dynamique de toutes les collectivités
        DbDynCon::loadAllConnections();
    }

    /**
     * Démarrage
     *
     * @return void
     */
    public function startup() {
        parent::startup();

        //défini la connexion par défaut
        $this->_conn = 'default';
        //si précisé, défini la connexion vers une collectivité
        if (!empty($this->params['connection'])) {
            Configure::write('conn', $this->params['connection']);
            $this->_conn = $this->params['connection'];
        }

        //initialisation du modèle User
        ClassRegistry::init(('User'));
        $this->User = new User();
        $this->User->setDataSource($this->_conn); //configuration de la base à accéder

        if ($this->_conn != 'default') { //collectivité
            ClassRegistry::init(('Group'));
            $this->Group = new Group();
            $this->Group->setDataSource($this->_conn);

            $collection = new ComponentCollection();
            $this->Acl = new AclComponent($collection);
            $this->Acl->startup(new AppController());

            $this->Aco = $this->Acl->Aco;
            $this->Aco->setDataSource($this->_conn);

            $this->Aro = $this->Acl->Aro;
            $this->Aro->setDataSource($this->_conn);
        } else { //superadmin
            ClassRegistry::init(('Collectivite'));
            $this->Collectivite = new Collectivite();
            $this->Collectivite->setDataSource($this->_conn);
        }
    }

    /**
     * Point d'entrée du shell
     *
     * @return void
     */
    public function main() {
        $this->_showHelp = false;

        list($usec, $sec) = explode(' ', microtime());
        $script_start = (float) $sec + (float) $usec;


        //dispatcher
        if (!empty($this->args[0])) {
            if ($this->args[0] == "clear_cache") {
                $this->_clear_cache();
            } else if ($this->args[0] == "hash_pass") {
                $this->_hash_pass($this->args[1]);
            } else if ($this->args[0] == "rights") {
                $this->_rights();
            } else if ($this->args[0] == "clear_logs") {
                $this->_clear_logs();
            } else if ($this->args[0] == "clean_tree_tables") {
                if (!empty($this->args[1]) && $this->args[1] == "with_acl") {
                    $this->_clean_tree_tables(true);
                } else {
                    $this->_clean_tree_tables();
                }
            } else if ($this->args[0] == "clean") {
                $this->_clean();
            } else if ($this->args[0] == "reset") {
                $this->_reset();
            } else if ($this->args[0] == "install") {
                $this->_install();
            } else if ($this->args[0] == "fix_fs_rights") {
                $this->_fixFsRigths();
            } else if ($this->args[0] == "backup") {
                $this->_backup();
            } else if ($this->args[0] == "list_collectivites") {
                $this->_getListColl();
            } else {
                $this->_showHelp = true;
            }
        } else {
            $this->_showHelp = true;
        }

        if ($this->_showHelp) {
            $this->_displayHelp('');
        } else {
            list($usec, $sec) = explode(' ', microtime());
            $script_end = (float) $sec + (float) $usec;

            $elapsed_time = round($script_end - $script_start, 5);
            $this->out('Elapsed time : ' . $elapsed_time . 's');
            $this->out("<warning>Don't forget to check right access on APP/tmp.</warning>");
            $this->hr();
        }
    }

    /**
     * Affiche la liste des collectivités paramétrées
     *
     * @return void
     */
    private function _getListColl() {
        $qd = array(
            'fields' => array(
                'Collectivite.name',
                'Collectivite.id',
                'Collectivite.conn',
                'Collectivite.active'
            ),
            'recursive' => -1,
            'order' => array(
                'Collectivite.active',
                'Collectivite.name'
            )
        );
        $colls = $this->Collectivite->find('all', $qd);

        $this->out('Liste des collectivités paramétrées :');
        $this->out('identifiant - statut - nom - connexion');
        foreach ($colls as $coll) {
            $this->out($coll['Collectivite']['id'] . ' - ' . ($coll['Collectivite']['active'] ? 'activée' : 'désactivée') . ' - ' . $coll['Collectivite']['name'] . ' - ' . $coll['Collectivite']['conn']);
        }
    }

    /**
     * Réinitialise les droits du système de fichier
     *
     * @deprecated since version 1.1
     * @return void
     */
    private function _fixFsRigths() {
        if (!empty($this->params['owner'])) {
            $this->_fsOwner = $this->params['owner'];
        }
        if (!empty($this->params['group'])) {
            $this->_fsGroup = $this->params['group'];
        }
        $cmdCakeChown = "sudo chown " . $this->_fsOwner . ":" . $this->_fsGroup . " " . CAKE . " -Rvf";
        $cmdCakeChmod = "sudo chmod u+rw,g+rw,o-w " . CAKE . " -Rvf";
        $cmdAppChown = "sudo chown " . $this->_fsOwner . ":" . $this->_fsGroup . " " . ROOT . " -Rvf";
        $cmdAppChmod = "sudo chmod u+rw,g+rw,o-w " . ROOT . " -Rvf";
        passthru(escapeshellcmd($cmdCakeChown));
        passthru(escapeshellcmd($cmdCakeChmod));
        passthru(escapeshellcmd($cmdAppChown));
        passthru(escapeshellcmd($cmdAppChmod));
    }

    /**
     * Effectue un dump de la base indiquée
     *
     * @deprecated since version 1.1
     * @param string $connName
     * @return string
     */
    private function _backupDb($connName = "") {
        $conns = ConnectionManager::enumConnectionObjects();
        if ($connName == "") {
            $connName = $this->User->useDbConfig;
        }
        $conn = $conns[$connName];
        $filename = '';
        if ($conn) {
            $tmpBkpSchema = ROOT . "/idelibre_bd_" . $connName . "_schema_" . $this->_bkpdate . ".sql";
            $tmpBkpData = ROOT . "/idelibre_bd_" . $connName . "_data_" . $this->_bkpdate . ".sql";

            $cmdSchema = "pg_dump --host " . $conn['host'] . " --username " . $conn['login'] . (!empty($conn['port']) ? " --port " . $conn['port'] : "") . " -s " . $conn['database'] . " -f " . $tmpBkpSchema;
            $cmdData = "pg_dump --host " . $conn['host'] . " --username " . $conn['login'] . (!empty($conn['port']) ? " --port " . $conn['port'] : "") . " --insert --data-only " . $conn['database'] . " -f " . $tmpBkpData;

            passthru(escapeshellcmd($cmdSchema));
            passthru(escapeshellcmd($cmdData));

            $filename = $this->_bkpdir . "/idelibre_bd_" . $connName . "_" . $this->_bkpdate . ".tar.bz2";
            $cmd = "tar --create --bzip --remove-files --file " . $filename . " " . $tmpBkpSchema . " " . $tmpBkpData;
            passthru(escapeshellcmd($cmd));
        }
        return $filename;
    }

    /**
     * Effectue une sauvegarde des fichiers de l'application
     *
     * @deprecated since version 1.1
     * @return string
     */
    private function _backupApp() {
        $filename = $this->_bkpdir . "/idelibre_app_" . $this->_bkpdate . ".tar.bz2";
        $cmd = "tar --create --verbose --bzip --file " . $filename . " --exclude=.svn --exclude=*.tar.bz2 " . ROOT;
        passthru(escapeshellcmd($cmd));
        return $filename;
    }

    /**
     * Sauvegarde générale de l'application et des données
     *
     * @deprecated since version 1.1
     * @return void
     */
    private function _backup() {
        $this->_bkpdir = ROOT;
        if (!empty($this->params['backup_direcory']) && is_dir($this->params['backup_directory'])) {
            $this->_bkpdir = $this->params['backup_direcory'];
        }
        $this->_bkpdate = date('Ymd_His');
        if ($this->params['backup_mode'] == 'full') {
            $filenames = array($this->_backupApp());
            $conns = ConnectionManager::enumConnectionObjects();
            foreach ($conns as $kConn => $conn) {
                $filenames[] = $this->_backupDb($kConn);
            }
            $filename = $this->_bkpdir . "/idelibre_full_" . $this->_bkpdate . ".tar.bz2";
            $cmd = "tar --create --bzip --remove-files --file " . $filename;
            foreach ($filenames as $file) {
                $cmd .= " " . $file;
            }
            passthru(escapeshellcmd($cmd));
            $this->hr(1);
            $this->out("fichier généré : " . $filename);
            $this->hr(1);
        } else if ($this->params['backup_mode'] == 'db') {
            $filename = $this->_backupDb();
            $this->hr(1);
            $this->out("fichier généré : " . $filename);
            $this->hr(1);
        } else if ($this->params['backup_mode'] == 'all_db') {
            $conns = ConnectionManager::enumConnectionObjects();
            foreach ($conns as $kConn => $conn) {
                $filename = $this->_backupDb($kConn);
                $this->hr(1);
                $this->out("fichier généré : " . $filename);
                $this->hr(1);
            }
        } else if ($this->params['backup_mode'] == 'app') {
            $filename = $this->_backupApp();
            $this->hr(1);
            $this->out("fichier généré : " . $filename);
            $this->hr(1);
        }
    }

    /**
     * Installation de la base d'administration et création du superuser
     *
     * @return void
     */
    private function _install() {
        $this->hr(1);
        $this->out('Install starts. Please wait ...');
        $this->hr(1);

        //mise en place de la structure de la base admin
        $conns = ConnectionManager::enumConnectionObjects();
        $filename = APP . 'Config/sql/001_idelibre_admin.sql';
        $cmd = 'psql -U ' . $conns[$this->User->useDbConfig]['login'] . ' -p ' . $conns[$this->User->useDbConfig]['port'] . ' -h ' . $conns[$this->User->useDbConfig]['host'] . ' -f ' . $filename . ' ' . $conns[$this->User->useDbConfig]['database'];

        $this->hr(1);
        $this->out('<warning>Résultat de la commande</warning>');
        passthru(escapeshellcmd($cmd));
        $this->hr(1);

        //mise en place des données minimale de la base admin
        $filename = APP . 'Config/sql/001_idelibre_admin.sql';
        $cmd2 = 'psql -U ' . $conns[$this->User->useDbConfig]['login'] . ' -p ' . $conns[$this->User->useDbConfig]['port'] . ' -h ' . $conns[$this->User->useDbConfig]['host'] . ' -f ' . $filename . ' ' . $conns[$this->User->useDbConfig]['database'];

        $this->hr(1);
        $this->out('<warning>Résultat de la commande</warning>');
        passthru(escapeshellcmd($cmd2));
        $this->hr(1);

        //création de l'utilisateur superadmin
        $createUser = false;
        $this->out('Information du compte superadmin :');
        $username = $this->in(__d('user', 'User.username'));
        $password = $this->in(__d('user', 'User.password'));
        $nom = $this->in(__d('user', 'User.nom'));
        $prenom = $this->in(__d('user', 'User.prenom'));
        $mail = $this->in(__d('user', 'User.mail'));

        $user = array(
            'User' => array(
                'username' => $username,
                'password' => $password,
                'nom' => $nom,
                'prenom' => $prenom,
                'mail' => $mail
            )
        );
        $this->User->create($user);
        if ($this->User->save()) {
            $createUser = true;
        }

        $this->hr(1);
        $this->out('Install done.');
        if ($createUser) {
            $this->out("<info>L'utilisateur a été créé.</info>");
        } else {
            $this->out("<error>L'utilisateur n'a pas été créé.</error>");
        }
        $this->hr(1);
    }

    /**
     * Delete ACL Table's content (MySQL: autoincrement reseted, PostgreSQL: serial not reseted)
     *
     * @return boolean
     */
    private function _delete_db() {
        $ret = false;
        $this->out(__('<info> ACL tables delete starts. Please wait ... </info>'));
        $this->Aro->query("TRUNCATE table acos CASCADE;");
        $this->Aro->query("ALTER SEQUENCE acos_id_seq RESTART WITH 1;");
        $this->Aro->query("TRUNCATE table aros CASCADE;");
        $this->Aro->query("ALTER SEQUENCE aros_id_seq RESTART WITH 1;");
        $this->Aro->query("TRUNCATE table aros_acos CASCADE;");
        $this->Aro->query("ALTER SEQUENCE aros_acos_id_seq RESTART WITH 1;");
        $this->out(__('<success> ACL tables delete done. </success>'));
        $ret = true;

        return $ret;
    }

    /**
     * Synchronisation between Aro table and User/Group tables
     *
     * @return boolean
     */
    private function _aro_sync($admin = false) {
        $ret = false;
        $this->out(__('<info> Aro Sync starts. Please wait ... </info>'));
        $this->_aro_sync_group();
        $this->_aro_sync_user();
        $this->out(__('<success> Aro Sync done. </success>'));
        $ret = true;
        return $ret;
    }

    /**
     * Call of AclExtrasShell aco_sync method ( Sync the ACO table )
     *
     * @return boolean
     * @link http://mark-story.com
     */
    private function _aco_sync() {
        $ret = false;
        $this->out(__('<info> Aco Sync starts. Please wait ... </info>'));
        $this->dispatchShell('AclExtras.AclExtras aco_sync');
        $this->out(__('<success> Aco Sync done. </success>'));
        $ret = true;
        return $ret;
    }

    /**
     * ACL Rights initialisation
     *
     * @return boolean
     */
    private function _init_rights($admin = false) {
        $ret = false;
        $this->out(__('<info> Aros_acos init starts. Please wait ... </info>'));
        $this->out('Init function rights starting ...');
        $access_rights = array(
            "Administrateurs" => array(),
            "Secretaires" => array(),
            "Utilisateurs" => array()
        );
        $this->out("<info>* initArosAcos *</info>");

        /*
         * Administrateurs
         */
        $groups = $this->Group->find('all', array('fields' => array('Group.name'), 'conditions' => array('Group.admin' => true)));
        foreach ($groups as $group) {
            $access_rights[$group['Group']['name']][] = 'controllers/Collectivites/info';
            $access_rights[$group['Group']['name']][] = 'controllers/Environnements';
            $access_rights[$group['Group']['name']][] = 'controllers/Documents';
            $access_rights[$group['Group']['name']][] = 'controllers/Annexes';
            $access_rights[$group['Group']['name']][] = 'controllers/Convocations';
            $access_rights[$group['Group']['name']][] = 'controllers/Seances';
            $access_rights[$group['Group']['name']][] = 'controllers/Invitations';
            $access_rights[$group['Group']['name']][] = 'controllers/Emailrappels';
            $access_rights[$group['Group']['name']][] = 'controllers/Emailinvitations';
            $access_rights[$group['Group']['name']][] = 'controllers/Groups';
            $access_rights[$group['Group']['name']][] = 'controllers/Pthemes';
            $access_rights[$group['Group']['name']][] = 'controllers/Types';
            $access_rights[$group['Group']['name']][] = 'controllers/Users';
            $access_rights[$group['Group']['name']][] = 'controllers/Emailconvocations';
            $access_rights[$group['Group']['name']][] = 'controllers/Search';
            $access_rights[$group['Group']['name']][] = 'controllers/Groupepolitiques';
        }

        /*
         * Secretaire
         */
        $groups = $this->Group->find('all', array('fields' => array('Group.name'), 'conditions' => array('Group.superuser' => true)));
        foreach ($groups as $group) {
            $access_rights[$group['Group']['name']][] = 'controllers/Environnements';
            $access_rights[$group['Group']['name']][] = 'controllers/Seances';
            $access_rights[$group['Group']['name']][] = 'controllers/Emailrappels';
            $access_rights[$group['Group']['name']][] = 'controllers/Invitations/sendJson';
            $access_rights[$group['Group']['name']][] = 'controllers/Invitations/sendAdministratifsJson';
            $access_rights[$group['Group']['name']][] = 'controllers/Invitations/sendInvitesJson';
            $access_rights[$group['Group']['name']][] = 'controllers/Convocations';
            $access_rights[$group['Group']['name']][] = 'controllers/Pthemes/getListJson';
            $access_rights[$group['Group']['name']][] = 'controllers/Types/usersJson';
            $access_rights[$group['Group']['name']][] = 'controllers/Documents/getFileJson';
            $access_rights[$group['Group']['name']][] = 'controllers/Annexes/getFileJson';
        }

        /*
         * Utilisateurs
         */
        $groups = $this->Group->find('all', array('fields' => array('Group.name'), 'conditions' => array('Group.baseuser' => true)));
        foreach ($groups as $group) {
            $access_rights[$group['Group']['name']][] = 'controllers/Environnements';
        }

        //application des droits initiaux
        foreach ($access_rights as $key => $val) {
            foreach ($val as $key2 => $val2) {
                $valid = $this->Acl->allow($key, $val2);
                $this->out($key . " -> " . $val2 . ' : ' . ($valid ? '<success>ok</success>' : '<error>error</error>'));
            }
        }

        $this->out('Init function rights done.');
        $this->out(__('<success>Init rights: ok</success>'));
        $this->out(__('<success> Aros_acos init done. </success>'));
        $ret = true;
        return $ret;
    }

    /**
     * Full rigths init
     *
     * @return void
     */
    private function _rights() {
        $this->out(__('<info> Full rights init starts. Please wait ... </info>'));
        if ($this->_delete_db()) {
            if ($this->_aro_sync()) {
                if ($this->_aco_sync()) {
                    if ($this->_init_rights()) {
                        $this->out(__('<success> Full rights init done. </success>'));
                    }
                }
            }
        }
    }

    /**
     * Nettoyage de fichiers de log internes (APP/tmp/logs/*.log)
     *
     * @deprecated since version 1.1
     * @return void
     */
    private function _clear_logs() {
        App::uses('Folder', 'Utility');
        $logs_folder = new Folder(TMP . 'logs');
        $logs = $logs_folder->read(true, false, true);
        foreach ($logs[1] as $file) {
            if (file_put_contents($file, '') !== false) {
                $this->out('File ' . $file . ' cleared');
            }
        }
    }

    /**
     * CakePHP cache cleaner
     *
     * @deprecated since version 1.1
     * @return void
     */
    private function _clear_cache() {
        if (Cache::clear()) {
            $this->out('Cached data : cleared');
        }
        $cachePaths = array('js', 'css', 'menus', 'views', 'persistent', 'models');
        foreach ($cachePaths as $config) {
            if (clearCache(null, $config)) {
                $this->out('Cache ' . $config . ' : cleared');
            } else {
                $this->out('Cache ' . $config . ' : no files removed');
            }
        }
    }

    /**
     * Retourne le hash d'un mot de passe
     *
     * @param string $pass
     * @return void
     */
    private function _hash_pass($pass) {
        $this->out(AuthComponent::password($pass));
        $this->out(BlowfishPasswordHasher::hash($pass));
    }

    /**
     *
     * @param type $with_acl
     */
    private function _clean_tree_tables($with_acl = false) {
        $this->out(__('<info> Cleaning tree tables starts. Please wait ... </info>'));
        $modelNames = $this->_modelsList();
        foreach ($modelNames as $modelName) {
            if ($this->_hasTreeBehavior($modelName)) {
                $modelClass = ClassRegistry::init($modelName);
                $status = $modelClass->recover();
                $this->out($modelName . ': ' . ($status ? 'ok' : 'error'));
            }
        }

        if ($with_acl) {
            $this->Acl = new AclComponent(new ComponentCollection());
            $this->Acl->startup(null);
            $this->out('Aro :' . ($this->Acl->Aro->recover() ? 'ok' : 'error' ));
            $this->out('Aco :' . ($this->Acl->Aco->recover() ? 'ok' : 'error'));
            App::uses('DataAclComponent', 'DataAcl.Controller/Component');
            $this->DataAcl = new DataAclComponent(new ComponentCollection());
            $this->DataAcl->startup(null);
            $this->DataAcl->Daro->recover();
            $this->DataAcl->Daco->recover();
        }
        $this->out(__('<success> Cleaning tree table done. </success>'));
    }

    /**
     *
     * @param type $modelName
     * @return boolean
     */
    protected function _hasTreeBehavior($modelName) {
        /* if( in_array( $modelName, array( 'Aco', 'Aro', 'Daro', 'Daco' ) ) ) {
          return false; // FIXME
          } */
        $file = APP . DS . 'Model' . DS . $modelName . '.php';
        if (!file_exists($file)) {
            return false;
        }
        $modelClass = ClassRegistry::init($modelName);
        if (empty($modelClass->actsAs)) {
            return false;
        }
        $behaviors = array_keys(Set::normalize($modelClass->actsAs));
        return in_array('Tree', $behaviors);
    }

    /**
     *
     * @param type $accepted
     * @return type
     */
    protected function _modelsList($accepted = array()) {
        $models = array();
        $dirName = sprintf('%sModel' . DS, APP);
        $dir = opendir($dirName);
        while (( $file = readdir($dir) ) !== false) {
            $explose = explode('~', $file);
            if (( count($explose) == 1 ) && (!is_dir($dirName . $file) ) && (!in_array($file, array('empty', '.svn')) )) {
                $model = Inflector::classify(preg_replace('/\.php$/', '', $file));
                if (empty($accepted) || in_array($model, $accepted)) {
                    $models[] = $model;
                }
            }
        }
        closedir($dir);
        sort($models);
        return $models;
    }

    /**
     *
     */
    protected function _clean() {
        $this->out(__('<info> Running maintenance tasks, Please wait ... </info>'));
        $this->_clean_tree_tables(true);
        $this->_clear_logs();
        $this->_clear_cache();
        $this->out(__('<success> Maintenance tasks done. </success>'));
    }

    /**
     *
     */
    protected function _reset() {
        $this->out(__('<info> Running reset tasks, Please wait ... </info>'));
        $this->_clean();
        $this->_rights();
        $this->out(__('<success> Reset tasks done. </success>'));
    }

    /**
     *  Check if aro is not set
     *
     * @param array $aro Aro to check
     * @return boolean
     * */
    private function _aro_not_set($aro = null) {
        $retval = false;
        if ($aro != null) {
            if (!empty($aro['Aro']['model']) && !empty($aro['Aro']['foreign_key'])) {
                $conditions = array(
                    'model' => $aro['Aro']['model'],
                    'foreign_key' => $aro['Aro']['foreign_key']
                );
                if (!empty($aro['Aro']['alias'])) {
                    $conditions['alias'] = $aro['Aro']['alias'];
                }
                if (!empty($aro['Aro']['parent_id'])) {
                    $conditions['parent_id'] = $aro['Aro']['parent_id'];
                }
                $exists = $this->Aro->find('list', array('conditions' => $conditions));
                if (count($exists) == 0) {
                    $retval = true;
                }
            }
        }
        return $retval;
    }

    /**
     *  Create an Aro
     *
     * @param array $aro Aro to create
     * @return boolean
     * */
    private function _aro_create($aro = null) {
        $retval = false;
        if ($aro != null) {
            if ($this->_aro_not_set($aro)) {
                $this->Aro->create($aro);
                if ($this->Aro->save()) {
                    $this->out($aro['Aro']['model'] . ' ' . $aro['Aro']['alias'] . ' -> <success>ok</success>');
                    $retval = true;
                } else {
                    $this->out($aro['Aro']['model'] . ' ' . $aro['Aro']['alias'] . ' -> <error>error</error>');
                }
            }
        }
        return $retval;
    }

    /**
     *  Synchronisation between Aro table and Group table (here: Role)
     *
     * @return boolean
     * */
    private function _aro_sync_group() {
        $retval = true;
        $groups = $this->Group->find('list');
        foreach ($groups as $key => $group) {
            $aro = array('Aro' => array());
            $aro['Aro']['model'] = 'Group';
            $aro['Aro']['foreign_key'] = $key;
            $aro['Aro']['alias'] = $group;
            $this->_aro_create($aro);
        }
        return $retval;
    }

    /**
     *  Synchronisation between Aro table and User table (here: Utilisateur)
     *
     * @return boolean
     * */
    private function _aro_sync_user() {
        $retval = true;
        $users = $this->User->find('all');
        foreach ($users as $user) {
            $aro = array('Aro' => array());
            $aro['Aro']['model'] = 'User';
            $aro['Aro']['foreign_key'] = $user['User']['id'];
            $aro['Aro']['alias'] = $user['User']['username'];
            $aroParent = $this->Aro->find('first', array('conditions' => array('model' => 'Group', 'foreign_key' => $user['User']['group_id'])));
            $aro['Aro']['parent_id'] = $aroParent['Aro']['id'];
            $this->_aro_create($aro);
        }
        return $retval;
    }

    /**
     *
     * @return optionParser
     */
    public function getOptionParser() {

        $actions = array(
            'list_collectivites' => 'Récupère la liste des collectivités, leur identifiant et leur statut.',
            'rights' => 'Génération complète des droits fonction.',
            'clear_cache' => 'Nettoyage du cache CakePHP.',
            'hash_pass' => 'Renvoi la valeur hashée d\'une chaine passée en argument.',
            'clean_tree_tables' => 'Nettoie les tables utilisant le behavior Tree.',
            'clear_logs' => 'Vide les fichiers de logs.',
            'clean' => 'Opérations de maintenance (clean_tree_tables, clear_logs, clear_cache).',
            'reset' => 'Opérations de remises à zéro (clean, rights, data).',
            'install' => "Importation de la structure de la base de données d'administration et création du superadmin.",
            'backup' => "Sauvegarde de la base de données définie par la connexion, toutes les bases, l'application, ou toutes les bases et l'application dans un fichier .tar.bz2.\n" .
            " -m ou --backup_mode\n" .
            "  full (par défaut) : sauvegarde toute l'application et toutes les base de données de l'application\n" .
            "  db : sauvegarde toute la base de données définie par l'option -c ou --connection\n" .
            "  all_db : sauvegarde toutes les base de données de l'application\n" .
            "  app : sauvegarde les fichiers de l'application\n" .
            " -d ou --backup_directory\n" .
            "      répertoire de destination du fichier de sauvegarde (le répertoire courant par défaut)",
            "fix_fs_rights" => "Définis les droits sur les fichiers de l'application et de CakePHP\n" .
            " -o ou --owner : définis le propriétaire (par défaut : www-data)\n" .
            " -g ou --group : définis le groupe (par défaut : www-data)"
        );
        ksort($actions);
        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        foreach ($actions as $action => $description) {
            $optionParser->addSubcommand($action, array('help' => $description));
        }
        $optionParser->addOption('connection', array(
            'short' => 'c',
            'help' => 'connection',
            'default' => 'default',
            'choices' => array_keys(ConnectionManager::enumConnectionObjects())   //ajouter celles présentes dans les fichiers 
        ));
        $optionParser->addOption('backup_mode', array(
            'short' => 'm',
            'help' => 'backup_mode',
            'default' => 'full',
            'choices' => array('full', 'db', 'all_db', 'app')
        ));
        $optionParser->addOption('backup_directory', array(
            'short' => 'd',
            'help' => 'backup_directory'
        ));
        $optionParser->addOption('owner', array(
            'short' => 'o',
            'help' => 'propriétaires des fichiers suivant le schéma UNIX'
        ));
        $optionParser->addOption('group', array(
            'short' => 'g',
            'help' => 'groupe des fichiers suivant le schéma UNIX'
        ));
        return $optionParser;
    }

}
