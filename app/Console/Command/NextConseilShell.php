<?php

App::uses("Srvuser", "Model");
App::uses("User", "Model");
App::uses("Collectivite", "Model");
App::uses("ConnectionManager", "Model");

class NextConseilShell extends AppShell {

    function showPierre() {
        $connections = ConnectionManager::enumConnectionObjects();



        foreach ($connections as $conn => $config) {



            if (Hash::get($config, "inMenu") === true) {
                $this->out($conn);
            }
        }
        $this->log('fini');
    }

    // on retrouve tous les acteurs d'une collectivité
    function show() {
        $connections = ConnectionManager::enumConnectionObjects();

        //    $this->out(var_export($connections, true));
        $timestamp = date('Y-m-d G:i:s');

        foreach ($connections as $conn => $config) {
            $this->out('------------------------------');
            /* $this->out(var_export($conn, true));
              $this->out(var_export($config, true));
             */



            if (Hash::get($config, "inMenu") === true) {
                //    $this->out('xxxxxxxxxxxxxxxxxxxxxxxxx');
                $Seance = ClassRegistry::init('Seance');
                $Seance->useDbConfig = $conn;


                // $this->out(var_export($User, true));
                $seance = $Seance->find('all', array(
                    'conditions' => array(
                        'date_seance >' => $timestamp
                    ),
                    'fields' => array('date_seance', 'name')
                ));

                $seance = Hash::extract($seance, "{n}.Seance.date_seance");
                //$Seance = $User->find('all');

                if (!empty($seance)) {
                    $this->out($conn);
                    $this->out($seance);
                }
            }
        }
        $this->log('fini');
    }

//        //nom de la connexion
//        //$collectiviteConn = 'pyrenees_orientales';
//        $collectiviteConn = $this->params['connection'];
//        
//        
//        // recherche de l'id correspondant
//        $Collectivite = ClassRegistry::init('Collectivite');
//        $coll = $Collectivite->find('first', array(
//            'recursive' => -1,
//            'conditions' => array(
//                'conn'=>$collectiviteConn
//            ),
//            'fields' => array('id')
//        ));
//        
//        
//        
//        // si la connexion n'existe pas
//        if(!$coll){
//            $this->out('Connection non présente'); die;
//        }
//        
//        
//      
//        
//        $collectiveId = $coll['Collectivite']['id'];
//
//        
//        // on recupere les users de cette connection
//        $User = ClassRegistry::init('User');
//        $User->useDbConfig = $collectiviteConn;
//
//
//        $userListe = $User->find('all', array(
//            'recursive' => -1,
//            'conditions' => array(
//                'group_id' => IDELIBRE_GROUP_AGENT_ID
//            )
//        ));
//
//        
//        //on crée les Srvusers correspondant
//        foreach ($userListe as $user) {
//            $this->createSrvuser($collectiveId, $user);
//        }
    //   }
//    function createSrvuser($collectiviteId, $user) {
//        $Srvuser = ClassRegistry::init('Srvuser');
//
//        // on supprime la vérification de l'obligation d'avoir un apssword et une verif correspondante
//        unset($Srvuser->validate['password']['checkIdenticalValues']);
//
//
//
//
//
//        $srvuser = array();
//        $srvuser['firstname'] = $user['User']['firstname'];
//        $srvuser['lastname'] = $user['User']['lastname'];
//        $srvuser['mail'] = $user['User']['mail'];
//        $srvuser['username'] = $user['User']['username'];
//        $srvuser['password'] = $user['User']['password'];
//        //$srvuser['confirm'] = 'idelibre';
//        $srvuser['srvrole_id'] = IDELIBRE_ADMIN_GROUP_USER_ID;
//
//
//        $success = false;
//        
//           $Srvuser->create();
//        if ($Srvuser->save($srvuser)) {
//            $this->out('save ' . $srvuser['username']);
//            $success = true;
//        } else {
//            $this->log('ERROR SAVE ' . $srvuser['username'], LOG_ERROR);
//            $this->log($Srvuser->validationErrors, LOG_ERROR);
//        }
//
//
//
//        if ($success) {
//            
//            $mandat = $Srvuser->Mandat->create();
//            $mandat['Mandat']['user_id'] = $user['User']['id'];
//            $mandat['Mandat']['collectivite_id'] = $collectiviteId;
//            $mandat['Srvuser']['id'] = $Srvuser->id;
//            $successMandat = $Srvuser->Mandat->saveAll($mandat);
//            $result = $successMandat;
//            $this->out($result);
//            //$this->log($result, LOG_ERROR);
//        }
//        
//        
//        
//        
//        
    //  }

    /**
     * Paramétrages et aides du shell.
     */
    public function getOptionParser() {
        $Parser = parent::getOptionParser();

        $Parser->addOptions(array(
            'connection' => array(
                'short' => 'c',
                'help' => 'précise la connection',

            )
                )
        );
        return $Parser;
    }

}
