<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


App::uses("User", "Model");
App::uses("ConnectionManager", "Model");
App::uses("Collectivite", "Model");


class ImportcsvShell extends AppShell {

    public function importAll() {

        $filename = "/tmp/import.csv";
        $conn = "newcollfr";


        $this->loadConnection($conn);




        $User = ClassRegistry::init('User');
        $User->useDbConfig = $conn;
        $User->Aro->useDbConfig = $conn;


        $handle = fopen($filename, 'r');

        while (($row = fgetcsv($handle, 500, ';')) !== FALSE) {

            $User->create();
            // $User->useDbConfig = $conn;
            unset($User->validate['password']['checkIdenticalValues']);

            $user = array();

            $user['firstname'] = utf8_encode($row[1]);
            $user['lastname'] = $row[0];
            $user['mail'] = $row[4];
            $user['username'] = split("@", $row[2])[0];
            $user['password'] =  AuthComponent::password($row[3]);
            $user['group_id'] = IDELIBRE_GROUP_AGENT_ID;

            $res = $User->save($user);

            if($res){
                $this->out("success " . $row[0]);
            }else{
                $this->out("error " . $row[0]);
            }

        }
        fclose($handle);

    }


    //dynamically load connexion
    public function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }

        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }



}
