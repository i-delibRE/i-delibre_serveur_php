<?php

App::uses("Srvuser", "Model");
App::uses("Seance", "Model");
App::uses("Convocation", "Model");
App::uses("User", "Model");
App::uses("Collectivite", "Model");
App::uses("ConnectionManager", "Model");

class UnarchiveShell extends AppShell {

    function reactiveSeance() {
        $seanceId = "562f5385-936c-49b6-8c87-30edc18e9eb7";
        $conn ="legrandnarbonne";
        // $Document = ClassRegistry::init('Document');
        $Projet = ClassRegistry::init('Projet');
        $Projet->useDbConfig = $conn;

        $projets = $Projet->find('all', array(
            "conditions" => array(
                'Projet.seance_id' => $seanceId
            )
        ));

        $this->out(var_export($projets, true));
    }



    // reactive une seance archivée par erreur
    function reactive(){

        $conn = $this->params['connection'];
        $seanceId = $this->params['seanceId'];

        if($conn == null || $conn == null || $seanceId == null){
            $this->out("null param");
            return;
        }
        $this->loadConnection($conn);

        $Seance = ClassRegistry::init("Seance");
        $Seance->useDbConfig = $conn;

        $Seance->id = $seanceId;
        $Seance->saveField('archive', false);


        $db = ConnectionManager::getDataSource('default');
        $res = $db->rawQuery("update convocations set active = TRUE where seance_id ='" . $seanceId ."';");

        if($res)
            $this->out("success");
        else
            $this->out("error");

    }




    //dynamically load connexion
    public function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }
        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }


    /**
     * Paramétrages et aides du shell.
     */
    public function getOptionParser() {
        $Parser = parent::getOptionParser();


        $Parser->addOptions(array(
                'connection' => array(
                    'short' => 'c',
                    'help' => 'précise la connection',
                    //'choices' => array('true', 'false'),
                    //'default' => 'true'
                ),
                'seanceId' => array(
                    'short' => 's',
                    'help' => 'précise la seance_id',
                    //'choices' => array('true', 'false'),
                    //'default' => 'true'
                )
            )
        );



        return $Parser;
    }










}
