<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 16/05/18
 * Time: 09:24
 */


App::uses("Seance", "Model");
App::uses("ConnectionManager", "Model");



class GenzipShell extends AppShell
{


    public function setConn($conn) {
        if (!empty($conn)) {
            $this->loadConnection($conn);
            Configure::write('conn', $conn);
            if ($conn != 'default') {
                Configure::write('Acl.database', $conn);
                $this->loadModel('Aro');
                $this->Aro->setDataSource($conn);
                $this->loadModel('Aco');
                $this->Aco->setDataSource($conn);
                $this->loadModel('Permission');
                $this->Permission->setDataSource($conn);
            }
        }
    }



    //dynamically load connexion
    public function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }

        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }




    public function run()
    {

        define('ZIPShell','/tmp/zip/');
        $conn = "beaugency";
        $typeSeance = "582c5671-6e3c-4ea8-848e-739cc18e9eed";
        $collId = "beaugency";


        $this->loadConnection($conn);
        $this->setConn($conn);
        $Seance = ClassRegistry::init('Seance');
        $Seance->useDbConfig = $conn;
        //trouver toutes les séances Id dont le type est typeSeances

        $seances = $Seance->find('all', array(
            "conditions" => array(
                "type_id" => $typeSeance
            ),
            "fields" => array("id")
        ));

        $seanceIds = Hash::extract($seances, "{n}.Seance.id");

        $this->out(var_export($seanceIds, true));


        foreach ($seanceIds as $seanceId) {


            $seance = $Seance->find('first', array(
                "conditions" => array(
                    "Seance.id" => $seanceId
                ),
                "contain" => array(
                    "Document" => array(
                        "fields" => array("id", "path", "name")
                    ),
                    "Projet" => array(
                        "fields" => array("id", "name"),
                        "Document" => array(
                            "fields" => array("id", "path", "name")
                        ),
                        "Annex" => array(
                            "fields" => array("id", "path", "name")
                        )
                    ),
                )));


            $folder = ZIPShell . $collId . DS . $seanceId;
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }

            //copy convocationDocument
            copy($seance["Document"]["path"], $folder . DS . $seance["Document"]["name"]);


            //create a folder for each projet with project and annexes inside
            foreach ($seance["Projet"] as $projet) {
                //create directory
                $projetFolder = $folder . DS . $projet["name"];
                if (!file_exists($projetFolder)) {
                    mkdir($projetFolder, 0777, true);
                }
                //copy project file
                copy($projet["Document"]["path"], $projetFolder . DS . $projet["Document"]["name"]);

                foreach ($projet["Annex"] as $annexe) {
                    copy($annexe["path"], $projetFolder . DS . $annexe["name"]);
                }
            }
            $cmd = "nohup zip -r  " . ZIPShell . $collId . DS . $seance['Seance']['alias'] . ".zip " . ZIPShell . $collId . DS . $seanceId . DS . "* >/dev/null 2>&1 &&  rm -rf " . ZIPShell . $collId . DS . $seanceId . DS;
            shell_exec($cmd);
        }
    }

}