<?php //

App::uses("Document", "Model");
App::uses("Annexe", "Model");
App::uses("Collectivite", "Model");
App::uses("ConnectionManager", "Model");
App::uses('DbDynCon', 'Utility');

class BlobToFileShell extends AppShell {
//   ./cake BlobToFile transformFS -c newcollfr
    
    // add path to table documents

  	


    public $WORKSPACE = "/data/workspace/";


    //dynamically load connexion
    public function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }
        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }



    function transformAnnexe(){

    //DbDynCon::loadAllConnections();
	    $conn = $this->params['connection'];

	    if($conn == null){
	        return;
        }

        $this->loadConnection($conn);

        //get the collectiviteId
        $Collectivite = ClassRegistry::init('Collectivite');
        $Collectivite->useDbConfig = "default";

        $collectivite = $Collectivite->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                'conn' => $conn
            )
        ));

        $collectiviteId = $collectivite['Collectivite']['id'];

        $this->out(var_export($collectiviteId, true));


        $this->loadConnection($conn);

	 // $Document = ClassRegistry::init('Document');
        $Annexe = ClassRegistry::init('Annexe');
        $Annexe->useDbConfig = $conn;

        $annexes = $Annexe->find('all', array(
            'fields' => array('id')
        ));


      $annexes = Hash::extract($annexes, '{n}.Annexe.id');


        $this->out(var_export($annexes, true));


	 foreach ($annexes as $annexeId) {

            $docContent = $Annexe->find('first', array(
                'fields' => array('content'),
                'conditions' => array('id' => $annexeId)
            ));




            $folder = $this->WORKSPACE . $collectiviteId . DS . 'migration';
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }

            $res = file_put_contents($folder . DS . $annexeId, $docContent['Annexe']['content']);
            if (!$res) {
                $errorConvert =array();
                $errorConvert['type'] = "ERROR";
                $errorConvert['conn'] = $conn;
                $errorConvert['collId'] = $collectiviteId;
                $errorConvert['docId'] = $annexeId;
                $this->log(var_export($errorConvert, true), "transformFS");
                return;

            }
            $this->out($annexeId);


            //save the path into document
            $Annexe->id = $annexeId;

            $Annexe->saveField("path", $folder.DS.$annexeId);

        }

        $successConvert = array();
        $successConvert['type'] ="SUCCESS";
        $successConvert['$conn'] = $conn;
        $successConvert['$collId'] = $collectiviteId;
        $this->log(var_export($successConvert, true), "transformAnnexe");





}


    function transformFS() {


         $conn = $this->params['connection'];

        if($conn == null){
            return;
        }

        $this->loadConnection($conn);


        //get the collectiviteId
        $Collectivite = ClassRegistry::init('Collectivite');
        $Collectivite->useDbConfig = "default";

        $collectivite = $Collectivite->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                'conn' => $conn
            )
        ));

        $collectiviteId = $collectivite['Collectivite']['id'];

        $this->out(var_export($collectiviteId, true));



        $this->loadConnection($conn);
        
      

        // $Document = ClassRegistry::init('Document');
        $Document = ClassRegistry::init('Document');
        $Document->useDbConfig = $conn;

        $documents = $Document->find('all', array(
            'fields' => array('id')
        ));

        $documents = Hash::extract($documents, '{n}.Document.id');


        $this->out(var_export($documents, true));

        foreach ($documents as $docId) {

            $docContent = $Document->find('first', array(
                'fields' => array('content'),
                'conditions' => array('id' => $docId)
            ));

            
            
            
            $folder = $this->WORKSPACE . $collectiviteId . DS . 'migration';
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }

            $res = file_put_contents($folder . DS . $docId, $docContent['Document']['content']);
            if (!$res) {
                $errorConvert =array();
                $errorConvert['type'] = "ERROR";
                $errorConvert['conn'] = $conn;
                $errorConvert['collId'] = $collectiviteId;
                $errorConvert['docId'] = $docId;
                $this->log(var_export($errorConvert, true), "transformFS");
                return;
                
            }
            $this->out($docId);
                                    
            
            //save the path into document
            $Document->id = $docId;
            
            $Document->saveField("path", $folder.DS.$docId);
            
        }
        
        $successConvert = array();
        $successConvert['type'] ="SUCCESS";
        $successConvert['$conn'] = $conn;
        $successConvert['$collId'] = $collectiviteId;
        $this->log(var_export($successConvert, true), "transformFS");
        
        
    }


    
    

     
       public function showAllConnections() {
        foreach (glob(APP . "Config/Connections/*") as $filename) {
            include_once $filename;

            $connectionName = basename($filename);
            ConnectionManager::create($connectionName, ${$connectionName});
        }
        
        $connectionList = ConnectionManager::enumConnectionObjects();
        foreach ($connectionList as $key => $val){
            $this->out($key);
        }
    }

    
       
        
    
    /**
     * Paramétrages et aides du shell.
     */
    public function getOptionParser() {
        $Parser = parent::getOptionParser();


        $Parser->addOptions(array(
            'connection' => array(
                'short' => 'c',
                'help' => 'précise la connection',
                //'choices' => array('true', 'false'),
                //'default' => 'true'
            )
                )
        );



        return $Parser;
    }


}
