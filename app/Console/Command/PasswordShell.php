<?php

App::uses("Srvuser", "Model");
App::uses("User", "Model");
App::uses("Collectivite", "Model");
App::uses("Mandat", "Model");
App::uses("ConnectionManager", "Model");

class PasswordShell extends AppShell {

    // on retrouve tous les acteurs d'une collectivité
    function migrate() {

        //nom de la connexion
        //$collectiviteConn = 'pyrenees_orientales';
        $collectiviteConn = $this->params['connection'];


        // recherche de l'id correspondant
        $Collectivite = ClassRegistry::init('Collectivite');
        $coll = $Collectivite->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'conn'=>$collectiviteConn
            ),
            'fields' => array('id')
        ));

        // si la connexion n'existe pas
        if(!$coll){
            $this->out('Connection non présente'); die;
        }
        $collectiveId = $coll['Collectivite']['id'];

        $Mandat = ClassRegistry::init('Mandat');

        $mandats = $Mandat->find("all", array(
            "conditions" => array(
                "collectivite_id" => $collectiveId
            ),
            "fields" => array(
                "user_id"
            ),
            "contain" =>array(
                "Srvuser" =>array(
                    "fields" => array("password")
                )
            )

        ));

  //      $this->out(var_export($mandats, true));

        // on recupere les users de cette connection
        $this->loadConnection($collectiviteConn);
        $User = ClassRegistry::init(array('class' => 'User', 'ds' => $collectiviteConn));

      //  $User->useDbConfig = $collectiviteConn;
        $User->Behaviors->unload( 'Acl' );

        foreach ($mandats as $mandat) {
            $User->id = $mandat["Mandat"]["user_id"];
            $this->out(var_export($User->field('username'), true));
            if(isset($mandat["Srvuser"][0])) {
                $this->out(var_export($mandat["Srvuser"][0]["password"], true));
                $User->saveField("password", $mandat["Srvuser"][0]["password"], false);
            }else{
                $this->out("NOT SET");
            }
        }
    }


    //dynamically load connexion
    public function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }
        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }



    /**
     * Paramétrages et aides du shell.
     */
    public function getOptionParser() {
        $Parser = parent::getOptionParser();


        $Parser->addOptions(array(
                'connection' => array(
                    'short' => 'c',
                    'help' => 'précise la connection',
                    //'choices' => array('true', 'false'),
                    //'default' => 'true'
                )
            )
        );



        return $Parser;
    }

}
