<?php

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 29/06/17
 * Time: 14:18
 */
App::uses("Ptheme", "Model");
App::uses("Collectivite", "Model");
App::uses("ConnectionManager", "Model");

class FullThemeShell extends AppShell
{
    public function getOptionParser() {
        $Parser = parent::getOptionParser();

        $Parser->addOptions(array(
                'connection' => array(
                    'short' => 'c',
                    'help' => 'précise la connection',
                )
            )
        );
        return $Parser;
    }


    //dynamically load connexion
    public function loadConnection($name) {
        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);
        if ($fileExists) {
            include $filename;
        }
        ConnectionManager::create($name, $fileExists ? ${$name} : array());
    }


    public function addFullName(){
        $conn = $this->params['connection'];

        if($conn == null){
            return;
        }

        $this->loadConnection($conn);

        //get the collectiviteId
        $Collectivite = ClassRegistry::init('Collectivite');
        $Collectivite->useDbConfig = "default";

        $collectivite = $Collectivite->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                'conn' => $conn
            )
        ));

        $collectiviteId = $collectivite['Collectivite']['id'];
        $this->out(var_export($collectiviteId, true));
        $this->loadConnection($conn);

        $Ptheme = ClassRegistry::init('Ptheme');
        $Ptheme->useDbConfig = $conn;

        $Ptheme->query('alter table pthemes add fullname varchar(1000)');

    }



    public function create(){



        $conn = $this->params['connection'];

        if($conn == null){
            return;
        }

        $this->loadConnection($conn);

        //get the collectiviteId
        $Collectivite = ClassRegistry::init('Collectivite');
        $Collectivite->useDbConfig = "default";

        $collectivite = $Collectivite->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                'conn' => $conn
            )
        ));

        $collectiviteId = $collectivite['Collectivite']['id'];
        $this->out(var_export($collectiviteId, true));
        $this->loadConnection($conn);

        $Ptheme = ClassRegistry::init('Ptheme');
        $Ptheme->useDbConfig = $conn;


        $pthemes = $Ptheme->find('all', array(
            'fields' => array('id', 'name'),
        ));


        foreach ($pthemes as $ptheme) {

  
            $arrayThemeNames = Hash::extract($Ptheme->getPath($ptheme['Ptheme']['id']), "{n}.Ptheme.name");
            $fullThemeNames = implode(", ", $arrayThemeNames);

          //  $ptheme['Ptheme']['fullname'] = $fullThemeNames;

            $Ptheme->create();
            $Ptheme->id = $ptheme['Ptheme']['id'];

            $Ptheme->saveField("fullname", $fullThemeNames);



        }

    }



}


/*

#!/bin/bash

#connections contain the conn column

while read p; do

  echo $p
  conn=$p
  rm /var/www/v3dev/app/tmp/cache/models/*

 /var/www/v3dev/app/Console/cake FullTheme addFullName -c $conn
 /var/www/v3dev/app/Console/cake FullTheme create -c $conn
done <connections




*/

