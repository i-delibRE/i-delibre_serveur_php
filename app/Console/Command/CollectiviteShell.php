<?php

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 13/03/17
 * Time: 14:07
 */

App::uses("Collectivite", "Model");
class CollectiviteShell extends AppShell
{

    public function create(){

        $conn = $this->params['connection'];
        $login_suffix = $this->params['suffixe'];
        $name = $this->params['name'];
        $bdd = $this->params['bdd'];
        $Collectivite = ClassRegistry::init('Collectivite');

        if($conn !=null && $login_suffix !=null && $name !=null && $bdd !=null) {

            $res = $Collectivite->save(array(
                "name" => $name,
                "conn" => $conn,
                "login_suffix" => $login_suffix,
                "active" => true,
                "use_cert" => false
            ));

            if($res){
                $this->out("Success");


                // on defini la connection comme etant le suffix en minuscule et sans carractere speciaux

                //   debug($this->request->data); die;
                //création dynamique de la connexion à la bdd
                $this->createStringTab($conn, $bdd, $login_suffix);




            }else{
                $this->out("ERROR SAVE");
            }

        }else{
            $this->out("ERROR");
        }


    }


    public function createStringTab($name, $database, $suffix) {


        $connection = "<?php " . "$" . "{$name} = array('datasource' => 'Database/Postgres', \n"
            . "'persistent' => false, \n"
            . "'host' =>'" . DATABASE_HOST . "', \n"
            . "'login' =>'" . DATABASE_LOGIN . "',\n"
            . "'password' =>'" . DATABASE_PASSWORD . "',\n"
            . "'database' => '{$database}',\n"
            . "'prefix' => '',\n"
            . "'port' =>" . DATABASE_PORT . ",\n"
            . "'encoding' => 'utf8',\n"
            . "'inMenu' => true );";


        $file = new File(APP . 'Config/Connections/' . $name, true, 0777);
        $file->write($connection);


        $nodeConnection = '{"' . $suffix . '": "postgres:' . DATABASE_LOGIN . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . ':' . DATABASE_PORT . '/' . $database . '"' . '}';

        $fileNode = new File(NODEJS_CONNECTIONS . $name, true, 0777);
        $fileNode->write($nodeConnection);




        $url = IDELIBRE_WS_PROTOCOL . '://' . IDELIBRE_WS_HOST . ':' . IDELIBRE_WS_PORT . '/' . 'addCollectivite/';

        $data = array(
            $suffix => 'postgres:' . DATABASE_LOGIN . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . ':' . DATABASE_PORT . '/' . $database
        );

        $jsonData = jSON_encode($data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData))
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($jsonData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_exec($ch);
        curl_close($ch);
    }








    /**
     * Paramétrages  et aides du shell.
     */
    public function getOptionParser()
    {
        $Parser = parent::getOptionParser();


        $Parser->addOptions(array(
                'connection' => array(
                    'short' => 'c',
                    'help' => 'précise la connection',
                ),
                'suffixe' => array(
                    'short' => 's',
                    'help' => 'précise le suffixe',
                ),
                'name' => array(
                    'short' => 'n',
                    'help' => 'précise le nom connection',
                ),
                'bdd' => array(
                    'short' => 'b',
                    'help' => 'précise le nom de la base de donnée',
                )
            )
        );
        return $Parser;
    }





}

