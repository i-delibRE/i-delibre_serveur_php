<?php

/**
 * AppShell file
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * SVN Informations
 * $Date: 2014-08-11 15:26:57 +0200 (lun. 11 août 2014) $
 * $Revision: 553 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Console/Command/AppShell.php $
 * $Id: AppShell.php 553 2014-08-11 13:26:57Z ssampaio $
 */
App::uses('Shell', 'Console');
App::uses('ClassRegistry', 'Utility');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class AppShell extends Shell {

    /**
     * Debug variable in APP/tmp/logs/debug.log
     * @deprecated since version 1.1
     * @param mixed $var
     */
	public function ldebug($var) {
		$this->log(var_export($var, true), 'debug');
	}

}
