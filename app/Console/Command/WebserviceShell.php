<?php


App::uses('ClassRegistry', 'Cake.Utility');

class WebserviceShell extends AppShell
{

    public function main()
    {

    }

    public function send()
    {
        //Fichier d'exemple (pour l'exemple on envoi le meme fichier pour tous les projets, annexes et convocation
//        $file = PATH_TO_EXEMPLE_FILE;
        $file = IDELIBRE_CURL_WSTEST_FILE_EXEMPLE;


        $url = 'http://' . 'nginx' .'/seances.json';

        $jsonData = array(
            'place' => '8 rue de la Mairie',
            'type_seance' => 'Commission permanente (wsclient rapporteur)',
            'date_seance' => date('Y-m-d H:i' . (!empty($this->args[0]) && $this->args[0] === 'second' ? ':s' : '')) . (!empty($this->args[0]) && $this->args[0] === 'second' ? '' : ':00'),
            'acteurs_convoques' => '[{"Acteur":{"id":38,"typeacteur_id":7,"nom":"GUILLAUME","prenom":"Didier","salutation":"Monsieur","titre":"Pr\\u00e9sident","position":1,"date_naissance":null,"adresse1":"","adresse2":"","cp":"","ville":"","email":"","telfixe":"","telmobile":"","note":"","actif":true,"created":"2011-05-05 14:47:31","modified":"2012-12-28 09:48:27","suppleant_id":2},"Typeacteur":{"elu":true}},{"Acteur":{"id":2,"typeacteur_id":7,"nom":"GREGOIRE","prenom":"Michel","salutation":"Monsieur","titre":"1ER Vice-President","position":2,"date_naissance":null,"adresse1":"","adresse2":"","cp":"26000","ville":"Buis-les-Baronnies","email":"","telfixe":"","telmobile":"","note":"","actif":true,"created":"2008-07-24 17:24:04","modified":"2012-12-28 09:48:47","suppleant_id":7},"Typeacteur":{"elu":true}},{"Acteur":{"id":30,"typeacteur_id":3,"nom":"FAIVRE-PIERRET","prenom":"Jean-Charles","salutation":"Monsieur","titre":"","position":30,"date_naissance":"1970-01-01","adresse1":"","adresse2":"","cp":"","ville":"Valence IV","email":"","telfixe":"","telmobile":"","note":"","actif":true,"created":"2008-07-24 17:44:35","modified":"2008-07-24 17:44:35","suppleant_id":null},"Typeacteur":{"elu":false}},{"Acteur":{"id":31,"typeacteur_id":3,"nom":"GILLES","prenom":"Andre","salutation":"Monsieur","titre":"","position":31,"date_naissance":"1970-01-01","adresse1":"","adresse2":"","cp":"","ville":"Marsanne","email":"","telfixe":"","telmobile":"","note":"","actif":true,"created":"2008-07-24 17:45:03","modified":"2008-07-24 17:45:03","suppleant_id":null},"Typeacteur":{"elu":false}},{"Acteur":{"id":34,"typeacteur_id":3,"nom":"MOUTON","prenom":"Marie-Pierre","salutation":"Madame","titre":"","position":34,"date_naissance":"1970-01-01","adresse1":"","adresse2":"","cp":"","ville":"Pierrelatte","email":"","telfixe":"","telmobile":"","note":"","actif":true,"created":"2008-07-24 17:46:33","modified":"2008-07-24 17:46:33","suppleant_id":null},"Typeacteur":{"elu":false}},{"Acteur":{"id":33,"typeacteur_id":3,"nom":"LIMONTA","prenom":"Fabien","salutation":"Monsieur","titre":"","position":999,"date_naissance":null,"adresse1":"","adresse2":"","cp":"","ville":"Saint-Paul-Trois-Chateaux","email":"","telfixe":"","telmobile":"","note":"","actif":true,"created":"2008-07-24 17:46:06","modified":"2010-10-19 16:29:50","suppleant_id":null},"Typeacteur":{"elu":false}}]',
            'projets' => array(
                0 => array(
                    'ordre' => 0,
                    'libelle' => 'tarif cimetiere1',
                    'theme' => 'T1, STA',
                    ),
                2 => array(
                    'ordre' => 1,
                    'libelle' => 'tarif cimetiere2',
                    'theme' => 'T1, STB , sstb'
                  ),
                3 => array(
                    'ordre' => 2,
                    'libelle' => 'tarif cimetiere3',
                    'theme' => 'STA, ssta',
                    'Rapporteur' => array('rapporteurlastname' => 'GUILLAUME','rapporteurfirstname' => 'Didier')
                   ),
            )
        );

        $data = array(
            'username' => IDELIBRE_CURL_WSTEST_USER,
            'password' => IDELIBRE_CURL_WSTEST_PASS,
            'conn' => IDELIBRE_CURL_WSTEST_CONN,
            'jsonData' => json_encode($jsonData),
            'convocation' => new CurlFile($file),
            'projet_0_rapport' => new CurlFile($file),
            'projet_1_rapport' => new CurlFile($file),
            'projet_2_rapport' => new CurlFile($file),
//            'projet_0_0_annexe' => new CurlFile($file),
//            'projet_1_0_annexe' => new CurlFile($file),
//            'projet_1_1_annexe' => new CurlFile($file)
        );


        $ch = curl_init();


        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $result = curl_exec($ch);
        $this->out(var_export(array('url' => $url, 'data' => $data, 'result' => $result, 'error' => $result === false ? curl_error($ch) : ''), true));
        curl_close($ch);
    }

}