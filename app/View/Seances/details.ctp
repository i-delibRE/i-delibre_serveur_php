<?php
/**
 *
 * Seances/details View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-05-20 14:32:26 +0200 (mar. 20 mai 2014) $
 * $Revision: 424 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Seances/details.ctp $
 * $Id: details.ctp 424 2014-05-20 12:32:26Z ssampaio $
 *
 */
$modalOptions = array(
    'id' => 'seanceDeleteModal',
    'title' => __d('default', 'confirm.delete.title'),
    'message' => __d('default', 'confirm.delete.msg'),
    'btn.ok' => __d('default', 'btn.confirm'),
    'btn.ok.url' => Router::url(array('controller' => 'Seances', 'action' => 'suppression', $seance['Seance']['id'])),
    'btn.cancel' => __d('default', 'btn.close')
);
echo $this->BootstrapBox->modalBox($modalOptions);


$seance['Convocation'] = Hash::sort($seance['Convocation'], '{n}.User.lastname', 'asc');
$convocsSentStatus = Hash::extract($seance, "Convocation.{n}.active");



?>


<?php
//debug($seance);
// Stocke si la convocation a déjà été envoyé
$convocationSent = ($seance['Convocation'][0]['active']);
?>


<h1>
    <div class="pull-right">

        <!--        on ne fait apparaitre les boutons d'edition de visualisation et d'archivage que s'il s'agit d'une seance non archivée-->
        <?php if (!$seance['Seance']['archive']) { ?>
            <div class="btn-group">
                <a class="btn btn-inverse tooltiped active" data-toggle="" title="<?php echo __d('default', 'btn.details'); ?>" id="btnDetails" data-url="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'details', $seance['Seance']['id'])); ?>" data-original-title="<?php echo __d('default', 'btn.details'); ?>">
                    <i class="icon icon-eye-open icon-white"></i>
                </a>
                <a class="btn btn-inverse tooltiped <?php /* echo in_array(true, $convocsSentStatus, true) ? 'disabled' : ''; */ ?>" data-toggle="" title="<?php echo __d('default', 'btn.edit'); ?>" id="btnEdit" data-url="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'edition', $seance['Seance']['id'])); ?>" data-original-title="<?php echo __d('default', 'btn.edit'); ?>">
                    <i class="icon icon-edit icon-white"></i>
                </a>
            </div>
            <?php
            //Suppression de séance dans tout les cas (cependant il faudra mettre en place le mode archive
//        if (in_array(true, $convocsSentStatus, true)) {
            ?>
            <!--            <a class="tooltiped btn btn-danger disabled" data-original-title="Supprimer" title="" data-toggle="modal">
                            <i class="icon-trash icon-white"></i>
                        </a>-->



            <a onclick="archive('<?php echo $seance['Seance']['id'] ?>')" class="btn btn-warning tooltiped"  title="<?php echo __d('default', 'btn.archiver'); ?>" id="btnarchive"  data-original-title="<?php echo __d('default', 'btn.archive'); ?> ">
                <i class="icon icon-book icon-white"></i>
            </a>





        <?php } ?>

        <?php
        //        } else {
        echo $this->BootstrapBox->modalBtn('seanceDeleteModal', __d('default', 'btn.delete'), $this->Html->tag('i', '', array('class' => 'icon-trash icon-white')), 'btn-danger', $modalOptions['btn.ok.url']);


        //        }
        ?>
    </div>
    <?php echo __d('seance', 'Seance.details'); ?>
</h1>



<h2><?php echo __d('seance', 'Seance'); ?></h2>
<dl>
    <dt><?php echo __d('seance', 'Seance.type_id'); ?></dt><dd><?php echo h($seance['Seance']['name']); ?></dd>
    <dt><?php echo __d('seance', 'Seance.date_seance'); ?></dt><dd><?php echo CakeTime::format('d/m/Y (H\hi)', $seance['Seance']['date_seance']); ?></dd>
    <?php
    // seulement si le lieu est renseigné
    if (!empty($seance['Seance']['place'])) {
        ?>
        <dt><?php echo __d('seance', 'Seance.place'); ?></dt><dd><?php echo h($seance['Seance']['place']); ?></dd>
    <?php } ?>
</dl>
<?php
echo $this->Html->tag('h2', __d('seance', 'Seance.convoc.etats'));


//etat : envoyée

$globalConvocSent = '';
$dlContent = '';


//debug($convocsSentStatus);
if (!in_array(true, $convocsSentStatus, true)) {
    $globalConvocSent = $this->element('no', array('msg' => __d('seance', 'Seance.sent.no'))) . ' ' . $this->Html->tag('span', $this->Html->tag('i', '', array('class' => array('icon', 'icon-share-alt', 'icon-white'))) . ' ' . __d('seance', 'btn.sendAllConvocs'), array('id' => 'SeanceSendAllConvocs', 'class' => array('btn', 'btn-inverse', 'btn-label-action')));
} else if (!in_array(false, $convocsSentStatus, true)) {
    $globalConvocSent = $this->element('yes', array('msg' => __d('seance', 'Seance.sent.yes')));
    //$globalConvocSent = $this->element('yes', array('msg' => __d('seance', 'Seance.sent.yes'))) . ' ' . $this->Html->tag('span', $this->Html->tag('i', '', array('class' => array('icon', 'icon-share-alt', 'icon-white'))) . ' ' . __d('seance', 'btn.reSendAllConvocs'), array('id' => 'SeanceReSendAllConvocs', 'class' => array('btn', 'btn-inverse', 'btn-label-action')));
} else {
    $globalConvocSent = $this->element('inprogress', array('msg' => __d('seance', 'Seance.sent.inprogress'))) . ' ' . $this->Html->tag('span', $this->Html->tag('i', '', array('class' => array('icon', 'icon-share-alt', 'icon-white'))) . ' ' . __d('seance', 'btn.sendAllConvocs'), array('id' => 'SeanceSendAllConvocs', 'class' => array('btn', 'btn-inverse', 'btn-label-action')));
}

if (!$seance['Seance']['archive']) {
    $dlContent .= $this->Html->tag('dt', __d('seance', 'Convocations.sent')) . $this->Html->tag('dd', $globalConvocSent);
}

//etat : lue
$convocsReceivedStatus = Hash::extract($seance, "Convocation.{n}.read");
$globalConvocReceived = '';
if (!in_array(true, $convocsReceivedStatus, true)) {
    $globalConvocReceived = $this->element('no', array('msg' => __d('seance', 'Seance.received.no')));
} else if (!in_array(false, $convocsReceivedStatus, true)) {
    $globalConvocReceived = $this->element('yes', array('msg' => __d('seance', 'Seance.received.yes')));
} else {
    $globalConvocReceived = $this->element('inprogress', array('msg' => __d('seance', 'Seance.received.inprogress')));
}
$dlContent .= $this->Html->tag('dt', __d('seance', 'Convocations.red')) . $this->Html->tag('dd', $globalConvocReceived);

echo $this->Html->tag('dl', $dlContent);

// Liste des élus convoqués
echo $this->Html->tag('h2', __d('seance', 'details.elus'));
?>

<span class="pull-right">

    <a onclick="printInfoUsers('<?php echo $seance['Seance']['id'] ?>')" type="button" class=' btn  btn-inverse ' title="imprimer">
         <span class="icon icon-print icon-white"></span>
    </a>

    <a class="btn btn-inverse tooltiped "  title="modifier les présences" id="btnModifyUser" href="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'modifyUserPresence', $seance['Seance']['id'])); ?>" data-original-title="<?php echo __d('default', 'btn.edit'); ?>">
        <i class="icon icon-pencil icon-white"></i>
    </a>

</span>


<?php

if (!empty($seance['Convocation'])) {
    echo '<table>';
    echo $this->Html->tableHeaders(
        array(
            __d('user', 'User.name'),
            __d('convocation', 'Convocation.sent'),
            __d('convocation', 'Convocation.ae_horodatage'),
            __d('convocation', 'Convocation.read'),
            __d('convocation', 'Convocation.ar_horodatage'),
            __d('convocation', 'Convocation.present_status')
        )
    );

    //debug($seance['Convocation']); die;
    $seance['Convocation'] = Hash::sort($seance['Convocation'], '{n}.User.lastname', 'asc');


    foreach ($seance['Convocation'] as $convocation) {



        $date_tmp1 = explode(' ', $convocation['ar_horodatage']);
        $date_tmp2 = explode('-', $date_tmp1[0]);

        $pesenceIcon = $this->element('unknown');

        if($convocation['presentstatus'] == 'present'){
            $pesenceIcon = $this->element('yes');
        }
        else if($convocation['presentstatus'] == 'absent'){
            $pesenceIcon = $this->element('no');
        }


        $convocation['active'] = !empty($convocation['ae_horodatage']);


        echo $this->Html->tableCells(
            array(
                h($convocation['User']['name']),
                ($convocation['active'] ? $this->element('yes') : $this->element('no') . ' ' . $this->Html->tag('span', $this->Html->tag('i', '', array('class' => array('icon', 'icon-share-alt', 'icon-white'))), array('class' => array('btn', 'btn-inverse', 'btn-mini', 'tooltiped', 'btnSendConvoc'), 'title' => __d('convocation', 'btn.sendConvoc'), 'data-uuid' => $convocation['id']))),
                h($convocation['active'] && !empty($convocation['ae_horodatage']) ? h(CakeTime::i18nFormat($convocation['ae_horodatage'], '%x %X')) : ' - '),
                ( $convocation['read'] ? $this->element('yes') : $this->element('no') ),
                h($convocation['read'] && !empty($convocation['ar_horodatage']) ? h(CakeTime::i18nFormat($convocation['ar_horodatage'], '%x %X')) : ' - '),
                $pesenceIcon
            )
        );
    }
    echo '</table>';
}

echo $this->Seance->document($seance['Seance']['id'], $seance['Document']['id'], $seance['Document']['name'], false, 'Documents', __d('convocation', 'Seance'));

// Liste des projets
echo $this->Html->tag('h2', 'Ordre du jour - Rapports');

if (!empty($seance['Projet'])) {
    $ulContent = '';
    foreach ($seance['Projet'] as $projet) {
        $themeLabels = $this->Html->tag('span', implode(' / ', explode(',', $projet['theme'])), array('class' => 'label label-info'));
        $docRapport = '';
        if (isset($projet['Document']['id']) && isset($projet['Document']['name'])) {

            $docRapport = $this->Seance->document($seance['Seance']['id'], $projet['Document']['id'], $projet['Document']['name'], false, 'Documents', __d('convocation', 'Projet'));
            if(!empty($projet['user_id'])){

                $docRapport .= '<span class="doc icon-user"> rapporteur : ' . $userList[$projet['user_id']]['User']['firstname'] . ' ' . $userList[$projet['user_id']]['User']['lastname'] . '</span>';


            }
        }



        $PJs = '';
        foreach ($projet['Annex'] as $annex) {
            $PJs .= $this->Html->tag('li', $this->Seance->document($seance['Seance']['id'], $annex['id'], $annex['name'], false, 'Annexes', __d('convocation', 'Annexe')), array('class' => 'pj'));
        }
        $ulContent .= $this->Html->tag('li', $this->Html->tag('span', $themeLabels, array('class' => 'pull-right')) . $this->Html->tag('span', $projet['name'], array('class' => 'ordreDuJourItem')) . $docRapport . $this->Html->tag('ul', $PJs), array('class' => 'projet'));
    }
    echo $this->Html->tag('ol', $ulContent);
}
?>








<div class="modal fade hide" id="confirm-archive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Confirmation d'archivage</b>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir archiver cette séance ?
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <a href="#" id="btn-confirm-archive" class="btn btn-warning ">Confirmer</a>
            </div>
        </div>
    </div>
</div>





<script type="text/javascript">
    $('.tooltiped').tooltip();


    function prependWaitMsg() {
        var waiter = $('<div></div>').html(waitMsg);
        $('#seanceDetail .btn').addClass('disabled').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        $('#seanceDetail').prepend(waiter);
    }

    $('#SeanceSendAllConvocs').click(function () {
        prependWaitMsg();
        $.get('<?php echo Router::url(array('controller' => 'Seances', 'action' => 'sendConvocations', $seance['Seance']['id'])); ?>', function () {
            $('#listSeance li[data-uuid="<?php echo $seance['Seance']['id']; ?>"]').trigger('click');
        });
    });

    // inutile car on automatise la mise ajours des modifs au moemnt de la validation de l'edition
    /* $('#SeanceReSendAllConvocs').click(function() {
     prependWaitMsg();
     $.get('<?php echo Router::url(array('controller' => 'Seances', 'action' => 'refreshSeance', $seance['Seance']['id'])); ?>', function() {
     $('#listSeance li[data-uuid="<?php echo $seance['Seance']['id']; ?>"]').trigger('click');
     });
     });
     */

    $('.btnSendConvoc').click(function () {
        prependWaitMsg();
        $.get('<?php echo Router::url(array('controller' => 'Convocations', 'action' => 'send')); ?>' + '/' + $(this).attr('data-uuid'), function () {
            $('#listSeance li[data-uuid="<?php echo $seance['Seance']['id']; ?>"]').trigger('click');
        });
    });

    $('#btnEdit').click(function () {
        if (!$(this).hasClass('disabled')) {
            var url = $(this).attr('data-url');

            $('#seanceDetail').html(waitMsg);
            $.ajax({
                url: url,
                type: 'GET',
                success: function (data) {
                    $('#seanceDetail').html(data);
                },
                error: function (data) {
                    if (data.status === 403) {
                        $('#connexionForbidden').modal('show');
                    }
                }
            });
        }
    });



    var seanceId = null;
    function archive(id) {
        seanceId = id;
        $('#confirm-archive').modal('show');
    }

    $('#btn-confirm-archive').click(function () {
        $(location).attr('href', '<?php echo Router::url(array('controller' => 'Seances', 'action' => 'archive')); ?>' + '/' + seanceId);
    });


    function printInfoUsers(id){
        console.log(id);
        var url = '<?php echo Router::url(array('controller' => 'Seances', 'action' => 'printInfoUsers')); ?>' + '/'+ id;
        console.log(url);
        $(location).attr('href', url);
    }



</script>
