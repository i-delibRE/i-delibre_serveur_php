<?php
/**
 * Seances/liste View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Seances/liste.ctp $
 * $Id: liste.ctp 302 2013-10-21 15:57:34Z ssampaio $
 */
echo $this->Html->css('seances');
?>

<!--si on arrive de la creation d'une seance !-->
<?php if ($seanceredirectId) { ?>

    <script>

        $('html,body').scrollTop(0);

        $('#listSeance li').removeClass('btn-inverse').css('color', '#000');
        $('#listSeance li i').removeClass('icon-folder-open').addClass('icon-folder-close').removeClass('icon-white');

        $(this).addClass('btn-inverse').css('color', '#fff');
        $('i', this).removeClass('icon-folder-close').addClass('icon-folder-open').addClass('icon-white');

        var url = "<?php echo Router::url(array('controller' => 'seances', 'action' => 'details', $seanceredirectId)); ?>";

        $('#seanceDetail').html(waitMsg);
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                $('#seanceDetail').html(data);
            },
            error: function (data) {
                if (data.status === 403) {
                    $('#connexionForbidden').modal('show');
                }
            }
        });



        var beforeAjax = function () {
            $('#listSeance li').removeClass('btn-inverse').css('color', '#000');
            $('#listSeance li i').removeClass('icon-folder-open').addClass('icon-folder-close').removeClass('icon-white');
        };
        setAjaxLink($('#seanceAddLink'), $('#seanceDetail'), beforeAjax);
    </script>
    <?php
}
?>


<div class="row-fluid">
    <div class="span4" id="listSeance">
        <h1><?php echo __d('seance', 'Seance.list'); ?> <a href="#searchModal" role="button" class="btn btn-inverse" data-toggle="modal" title="rechercher"><i class="icon-search"></i></a> </h1>
        
        <ul  class="btn-group btn-group-vertical">
            <?php
            foreach ($seances as $seance) {
                $text = $seance['Seance']['name'] . ' du ' . CakeTime::format('d/m/Y (H\hi)', $seance['Seance']['date_seance']);
                $ico = $this->Html->tag('i', '', array('class' => 'icon-folder-close pull-right'));
                echo $this->Html->tag('li', $ico . $text, array('data-uuid' => $seance['Seance']['id'], 'class' => 'btn'));
            }
            ?>
        </ul>
        <br /><br />
        <a href="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'ajout')); ?>" class="btn btn-inverse" id="seanceAddLink" data-target-id="seanceDetail"><i class="icon-plus-sign icon-white"></i> <?php echo __d('seance', 'Seance.add'); ?></a>

    </div>
    <div class="span8" id="seanceDetail">
        <!-- Detail -->
    </div>
</div>



<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>Recherche</b>
            </div>
            <div class="modal-body">

                <!--                <div class="span1">zerezrz zrez rzer er zerze rzer zer</div>
                                <div class="span3">zerezrz zrez rzer er zerze rzer zer</div>
                                <div class="span1">z ere zrz zrez rzer er zer ze rzer zer</div>-->


                <div><b>Les séances contenant un projet de thème</b> <br><br></div>
                <?php
//recherche par theme de projet


                echo $this->Base->create(false, array(
                    'class' => 'form-inline',
                    'url' => array('controller' => 'Search', 'action' => 'searchByTheme')
                ));



                echo $this->Base->input('theme', array(
                    'options' => $listThemes,
                    'empty' => 'tous',
                    'div' => false,
                    'label' => false,
                    'class' => 'span5'
                ));

                echo $this->Base->end(array('label' => 'OK', 'class' => 'btn btn-primary', 'div' => false));


                echo '<hr>';
                ?>
                <div><b>Les séances de type</b> <br><br></div>

                <?php
                //recherche par type de seance
                echo $this->Base->create(false, array(
                    'class' => 'form-inline',
                    'url' => array('controller' => 'Search', 'action' => 'searchByType')
                ));



                echo $this->Base->input('type', array(
                    'options' => $listTypes,
                    'empty' => 'tous',
                    'div' => false,
                    'label' => false,
                    'class' => 'span5'
                ));

                echo $this->Base->end(array('label' => 'OK', 'class' => 'btn btn-primary', 'div' => false));

                echo '<hr>';
                ?>



                <div><b>Rechercher par date</b> <br><br></div>
                <?php
                //recherche par date
                echo $this->Base->create(false, array(
                    //'class' => 'form-inline',
                    'url' => array('controller' => 'Search', 'action' => 'searchByDate')
                ));
                ?>
                <span class="span1"><b> Du </b>  </span>
                <?php
                echo $this->Base->day('dateFrom', array('empty' => "Jour", 'class' => "span1"));
                echo $this->Base->month('dateFrom', array('empty' => "Mois", 'class' => "span2"));
                echo $this->Base->Year('dateFrom', date('Y') - 15, date('Y') - 0, array('empty' => "Année", 'class' => "span2"));
                echo '<br>';
                
                ?>
                <span class="span1"><b> Au </b>  </span>
                <?php
                
                echo $this->Base->day('dateTo', array('empty' => "Jour", 'class' => "span1"));
                echo $this->Base->month('dateTo', array('empty' => "Mois", 'class' => "span2"));
                echo $this->Base->Year('dateTo', date('Y') - 15, date('Y') - 0, array('empty' => "Année", 'class' => "span2"));


                echo $this->Base->end(array('label' => 'OK', 'class' => 'btn btn-primary pull-right', 'div' => false));


                echo '<hr>';
                ?>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="fa fa-times-circle" ></span>Annuler</button>
                
            </div>
        </div>
    </div>
</div>




<?php
$modalOptions = array(
    'id' => 'connexionForbidden',
    'title' => __d('default', 'connexion.forbidden.title'),
    'message' => __d('default', 'connexion.forbidden.msg'),
    'btn.ok' => __d('default', 'btn.reload'),
    'btn.ok.url' => Router::url(),
    'btn.cancel' => null
);
echo $this->BootstrapBox->modalBox($modalOptions);
?>

<script type="text/javascript">
    $('#listSeance li').click(function () {
        $('html,body').scrollTop(0);

        $('#listSeance li').removeClass('btn-inverse').css('color', '#000');
        $('#listSeance li i').removeClass('icon-folder-open').addClass('icon-folder-close').removeClass('icon-white');

        $(this).addClass('btn-inverse').css('color', '#fff');
        $('i', this).removeClass('icon-folder-close').addClass('icon-folder-open').addClass('icon-white');

        var url = "<?php echo Router::url(array('controller' => 'seances', 'action' => 'details')); ?>/" + $(this).attr('data-uuid');

        $('#seanceDetail').html(waitMsg);
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                $('#seanceDetail').html(data);
            },
            error: function (data) {
                if (data.status === 403) {
                    $('#connexionForbidden').modal('show');
                }
            }
        });
    });


    var beforeAjax = function () {
        $('#listSeance li').removeClass('btn-inverse').css('color', '#000');
        $('#listSeance li i').removeClass('icon-folder-open').addClass('icon-folder-close').removeClass('icon-white');
    };
    setAjaxLink($('#seanceAddLink'), $('#seanceDetail'), beforeAjax);
</script>