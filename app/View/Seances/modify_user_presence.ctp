
<a class="btn btn-inverse tooltiped active pull-right" data-toggle="" title="retour"
   id="btnRetour"
   href="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'liste')) ?>"
   data-original-title="Retour">
      Retour aux séances
</a>



<table class="table">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Presence</th>
            <th>Modifier en</th>
            <th></th>
        </tr>
    </thead>

   <tbody>


    <?php foreach ($convocations as $convoc){ ?>


    <tr>

        <?php echo $this->Form->create(null, array(
            'id' =>  $convoc['Convocation']['id'],
            'url' => array('controller' => 'Seances', 'action' => 'modifyUserPresence', $seanceId), 'id' => $convoc['Convocation']['id']));;


        ?>

        <td><?php echo $convoc["User"]["firstname"] . " " . $convoc["User"]["lastname"] ?></td>

         <?php
         $pesenceIcon = $this->element('unknown');

         if($convoc["Convocation"]['presentstatus'] == 'present'){
             $pesenceIcon = $this->element('yes');
         }
         else if($convoc["Convocation"]['presentstatus'] == 'absent'){
             $pesenceIcon = $this->element('no');
         }
         ?>
         <td> <?php echo $pesenceIcon ?> </td>

        <td>
            <select name="data[Convocation][value]">
                <option
                <option value= null text="Selectionner le rapporteur" disabled selected="selected">Selectionner un statut </option>
                <option value= "present">Présent </option>
                <option value= "absent">Absent </option>
                <option value= "">Non renseigné </option>

            </select>

        </td>

        <td>

            <input type="hidden" name="data[Convocation][id]" class="input-xlarge" value="<?php echo $convoc['Convocation']['id'] ?>" id="ConvocationId">
            <button type="submit" class="btn btn-default">Enregistrer</button>
        </td>

        <?php echo $this->Form->end() ?>

     </tr>

    <?php }?>

   </tbody>

</table>


