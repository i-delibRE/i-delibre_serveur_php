<?php
/**
 * Seances/view View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Seances/view.ctp $
 * $Id: view.ctp 302 2013-10-21 15:57:34Z ssampaio $
 */
?>
<div class="seances view">
	<h2><?php echo __('Seance'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($seance['Seance']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($seance['Seance']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Seance'); ?></dt>
		<dd>
			<?php echo h($seance['Seance']['date_seance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($seance['Seance']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($seance['Seance']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($seance['Type']['name'], array('controller' => 'types', 'action' => 'view', $seance['Type']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Document'); ?></dt>
		<dd>
			<?php echo $this->Html->link($seance['Document']['name'], array('controller' => 'documents', 'action' => 'view', $seance['Document']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Seance'), array('action' => 'edit', $seance['Seance']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Seance'), array('action' => 'delete', $seance['Seance']['id']), null, __('Are you sure you want to delete # %s?', $seance['Seance']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Seances'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Seance'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Documents'), array('controller' => 'documents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Document'), array('controller' => 'documents', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Convocations'), array('controller' => 'convocations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Convocation'), array('controller' => 'convocations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projets'), array('controller' => 'projets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Projet'), array('controller' => 'projets', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Convocations'); ?></h3>
	<?php if (!empty($seance['Convocation'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Seance Id'); ?></th>
				<th><?php echo __('User Id'); ?></th>
				<th><?php echo __('Read'); ?></th>
				<th><?php echo __('Presence'); ?></th>
				<th><?php echo __('Delegation'); ?></th>
				<th><?php echo __('Procuration'); ?></th>
				<th><?php echo __('Created'); ?></th>
				<th><?php echo __('Modified'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php
			$i = 0;
			foreach ($seance['Convocation'] as $convocation):
				?>
				<tr>
					<td><?php echo $convocation['id']; ?></td>
					<td><?php echo $convocation['seance_id']; ?></td>
					<td><?php echo $convocation['user_id']; ?></td>
					<td><?php echo $convocation['read']; ?></td>
					<td><?php echo $convocation['presence']; ?></td>
					<td><?php echo $convocation['delegation']; ?></td>
					<td><?php echo $convocation['procuration']; ?></td>
					<td><?php echo $convocation['created']; ?></td>
					<td><?php echo $convocation['modified']; ?></td>
					<td class="actions">
						<?php echo $this->Html->link(__('View'), array('controller' => 'convocations', 'action' => 'view', $convocation['id'])); ?>
						<?php echo $this->Html->link(__('Edit'), array('controller' => 'convocations', 'action' => 'edit', $convocation['id'])); ?>
		<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'convocations', 'action' => 'delete', $convocation['id']), null, __('Are you sure you want to delete # %s?', $convocation['id'])); ?>
					</td>
				</tr>
		<?php endforeach; ?>
		</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Convocation'), array('controller' => 'convocations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Projets'); ?></h3>
<?php if (!empty($seance['Projet'])): ?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Name'); ?></th>
				<th><?php echo __('Theme'); ?></th>
				<th><?php echo __('Document Id'); ?></th>
				<th><?php echo __('Vote'); ?></th>
				<th><?php echo __('Ptheme Id'); ?></th>
				<th><?php echo __('Seance Id'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php
			$i = 0;
			foreach ($seance['Projet'] as $projet):
				?>
				<tr>
					<td><?php echo $projet['id']; ?></td>
					<td><?php echo $projet['name']; ?></td>
					<td><?php echo $projet['theme']; ?></td>
					<td><?php echo $projet['document_id']; ?></td>
					<td><?php echo $projet['vote']; ?></td>
					<td><?php echo $projet['ptheme_id']; ?></td>
					<td><?php echo $projet['seance_id']; ?></td>
					<td class="actions">
						<?php echo $this->Html->link(__('View'), array('controller' => 'projets', 'action' => 'view', $projet['id'])); ?>
		<?php echo $this->Html->link(__('Edit'), array('controller' => 'projets', 'action' => 'edit', $projet['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'projets', 'action' => 'delete', $projet['id']), null, __('Are you sure you want to delete # %s?', $projet['id'])); ?>
					</td>
				</tr>
		<?php endforeach; ?>
		</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Projet'), array('controller' => 'projets', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
