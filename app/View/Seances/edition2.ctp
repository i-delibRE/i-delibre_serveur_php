<?php
/**
 * Seances/edition View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-08-01 09:40:19 +0200 (ven. 01 août 2014) $
 * $Revision: 545 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Seances/edition.ctp $
 * $Id: edition.ctp 545 2014-08-01 07:40:19Z ssampaio $
 */
echo $this->Html->css('/vendors/bootstrap-datepicker/datepicker');
echo $this->Html->script('globalHelper');
echo $this->Html->script('/vendors/bootstrap-datepicker/bootstrap-datepicker');
echo $this->Html->css('treelist');

$iconAdd = $this->html->tag('i', '', array('class' => 'icon icon-plus-sign icon-white')) . ' ';
$iconSubmit = $this->html->tag('i', '', array('class' => 'icon icon-ok-sign icon-white')) . ' ';

$modalOptions = array(
    'id' => 'seanceDeleteModal',
    'title' => __d('default', 'confirm.delete.title'),
    'message' => __d('default', 'confirm.delete.msg'),
    'btn.ok' => __d('default', 'btn.confirm'),
    'btn.ok.url' => Router::url(array('controller' => 'Seances', 'action' => 'suppression', $seance['Seance']['id'])),
    'btn.cancel' => __d('default', 'btn.close')
);
echo $this->BootstrapBox->modalBox($modalOptions);
?>

<?php
// Stocke si la convocation a déjà été envoyé
$convocationSent = ($seance['Convocation'][0]['active']);

//debug($seance);  
?> 






<script type="text/javascript">
    /**
     * Affichage d'information de liste vide
     *
     * @param {Object} list
     * @returns {undefined}
     */
    function showVoidListInfo(list) {
        if ($('li', list).length === 1) {
            $('li.li-void', list).css('display', 'list-item');
        } else {
            $('li.li-void', list).css('display', 'none');
        }
    }



    /**
     * Activation des boutons de déplacement des projets
     *
     * @returns {undefined} */
    function btnProjetActivation() {
        $('#finalProjets .projet').each(function () {
            var projet = $(this);
            $('.upBtn, .downBtn', projet).removeClass('disabled');
            if ($('#finalProjets .projet').first()[0] === projet[0]) {
                $('.upBtn', projet).addClass('disabled').tooltip('destroy');
            } else {
                $('.upBtn', projet).prop('data-toggle', 'tooltip').prop('title', '<?php echo __d('seance', 'Seance.projet.up'); ?>').tooltip();
            }

            if ($('#finalProjets .projet').last()[0] === projet[0]) {
                $('.downBtn', projet).addClass('disabled').tooltip('destroy');
            } else {
                $('.downBtn', projet).prop('data-toggle', 'tooltip').prop('title', '<?php echo __d('seance', 'Seance.projet.down'); ?>').tooltip();
            }
        });
    }

    /**
     * ajout d'un projet
     *
     * @param {object} projetInputName
     * @param {object} projetInputFile
     * @returns {undefined}	 */
    function addProjet(projetInputName, projetInputFile) {
        var projetName = $('#' + projetInputName).val();
        var isPresent = false;
        $('#finalProjets .projet').each(function () {


            if ($(this).prop('data-projet-name') === projetName) {
                isPresent = true;
            }
        });


        /*  if(isPresent === true){
         $('#modalAlreadyExistProject').modal('show');
         }*/

        if (isPresent !== true) {
            var deleteBtn = $('<span></span>')
                    .addClass('btn')
                    .addClass('btn-danger')
                    .addClass('btn-mini')
                    .addClass('pull-right')
                    .prop('data-toggle', 'tooltip')
                    .prop('title', '<?php echo __d('seance', 'Seance.projet.remove'); ?>')
                    .tooltip()
                    .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
                    .click(function () {
                        $(this).parent('.projet').remove();
                        showVoidListInfo($('#finalProjets'));
                        btnProjetActivation();
                    });
            var upBtn = $('<span></span>')
                    .addClass('upBtn')
                    .addClass('btn')
                    .addClass('btn-inverse')
                    .addClass('btn-mini')
                    .prop('data-toggle', 'tooltip')
                    .prop('title', '<?php echo __d('seance', 'Seance.projet.up'); ?>')
                    .tooltip()
                    .html($('<i></i>').addClass('icon-arrow-up').addClass('icon-white'))
                    .click(function () {
                        if (!$(this).hasClass('disabled')) {
                            var projet = $(this).parent().parent('.projet');
                            var prevProjet = projet.prev();
                            prevProjet.before(projet);
                            btnProjetActivation();
                        }
                    });
            var downBtn = $('<span></span>')
                    .addClass('downBtn')
                    .addClass('btn')
                    .addClass('btn-inverse')
                    .addClass('btn-mini')
                    .prop('data-toggle', 'tooltip')
                    .prop('title', '<?php echo __d('seance', 'Seance.projet.down'); ?>')
                    .tooltip()
                    .html($('<i></i>').addClass('icon-arrow-down').addClass('icon-white'))
                    .click(function () {
                        if (!$(this).hasClass('disabled')) {
                            var projet = $(this).parent().parent('.projet');
                            var nextProjet = projet.next();
                            nextProjet.after(projet);
                            btnProjetActivation();
                        }
                    });


            /*
             var docUploadUuid = UUID();
             
             var docUpload = $(projetInputFile).clone();
             docUpload.prop('name', docUploadUuid).addClass('doc_projet');
             var labelUpload = $('<label></label>').html('<?php echo __d('seance', 'Projets.document'); ?>').prop('for', docUploadUuid);
             var hintsDocUpload = $('<div></div>').addClass('hints').html('<?php echo __d('seance', 'Projets.document.filetype'); ?>');
             var blocDocUpload = $('<div></div>').append(labelUpload).append(docUpload).append(hintsDocUpload).addClass('doc');
             
             
             */

            var docUploadUuid = UUID();

            //preparation de l input file à cloner
            var docUpload = $('#' + projetInputFile);
            var inputFileParent = docUpload.parent();
            var cloneInputFile = docUpload.clone(true);


            docUpload.prop('name', docUploadUuid).addClass('doc_projet');
            var labelUpload = $('<label></label>').html('<?php echo __d('seance', 'Projets.document'); ?>').prop('for', docUploadUuid);
            var hintsDocUpload = $('<div></div>').addClass('hints').html('<?php echo __d('seance', 'Projets.document.filetype'); ?>');
            var blocDocUpload = $('<div></div>')
                    .append(labelUpload)
                    .append(docUpload) //on déplace l'input file vers le projet
                    .append(hintsDocUpload).addClass('doc');

            $(inputFileParent).prepend(cloneInputFile); //on replace le clone dans le formulaire de creation de projet




            var labelPj = $('<h6></h6>').html('<?php echo __d('seance', 'Projets.pjs') ?>');
            var listPjUuid = UUID();
            var listPj = $('<ul></ul>').prop('id', listPjUuid);
            var btnAddPj = $('<span></span>')
                    .addClass('btn')
                    .addClass('btn-inverse')
                    .html('<?php echo __d('seance', 'seance.form.projet.addAnnexe') ?>')
                    .click(function () {

                        var pjUploadUuid = UUID();
                        var pjUpload = $('<input />').prop('type', 'file').prop('name', pjUploadUuid).addClass('doc_pj');
                        var labelPjUpload = $('<label></label>').html('<?php echo __d('seance', 'Projets.pj.document'); ?>').prop('for', pjUploadUuid);
                        var hintsPjUpload = $('<div></div>').addClass('hints').html('<?php echo __d('seance', 'Projets.pj.document.filetype'); ?>');
                        var pjDeleteBtn = $('<span></span>')
                                .addClass('btn')
                                .addClass('btn-danger')
                                .addClass('btn-mini')
                                .addClass('pull-right')
                                .prop('data-toggle', 'tooltip')
                                .prop('title', '<?php echo __d('seance', 'Seance.projet.pj.remove'); ?>')
                                .tooltip()
                                .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
                                .click(function () {
                                    $(this).parent('.pj').remove();
                                    btnProjetActivation();
                                });
                        var liPj = $('<li></li>').append(pjDeleteBtn).append(labelPjUpload).append(pjUpload).append(hintsPjUpload).addClass('pj');
                        listPj.append(liPj);
                    });
            var blocPj = $('<div></div>').append(labelPj).append(listPj).append(btnAddPj).addClass('pjs');
            var upDownGroup = $('<span></span>').addClass('pull-left').addClass('btn-group').append(upBtn).append(downBtn);

            var projetUuid = UUID();

            var editableTitle = $('<div></div>').addClass('input-append').addClass('title');

            var editableTitleInput = $('<textarea></textarea>')
                    .prop('name', projetUuid + '_projetname')
                    .html(projetName)
                    .prop('title', '<?php echo __d('seance', 'Seance.projet'); ?>')
                    .prop('data-toggle', 'tooltip')
                    .tooltip().change(function () {
                $(this).html($(this).val());
            });

            editableTitleInput.change(function () {
                var newVal = $(this).html();
                $(this).parents('.projet').prop('data-projet-name', newVal);
            });
            var editableTitleAddOn = $('<span></span>').addClass('add-on').append($('<i></i>').addClass('icon-pencil'));
            editableTitle.append(editableTitleInput).append(editableTitleAddOn);



            var tagsList = $('<ul></ul>').addClass('ptheme-list').addClass('tree-list').prop('data-loaded', false);
            var tagsBtn = $('<a></a>').addClass('btn').addClass('btn-info').addClass('btn-mini').addClass('pull-right')
                    .css('margin-right', '5px')
                    .prop('title', '<?php echo __d('seance', 'Ptheme.list'); ?> ')
                    .prop('data-toggle', 'tooltip')
                    .tooltip()
                    .append($('<i></i>').addClass('icon-tags').addClass('icon-white'));

            tagsBtn.click(function () {
                if (!$(tagsList).prop('data-loaded')) {
                    $(tagsList).html($('<li></li>').append(waitMsg));
                    $.get('<?php echo Router::url(array('controller' => 'pthemes', 'action' => 'getList')); ?>', function (data) {
                        $(tagsList).html(data);
                        $(tagsList).prop('data-loaded', true);
                    });
                }

                if ($(tagsList).css('visibility') === 'hidden') {
                    $(tagsList).css('visibility', 'visible');
                } else {
                    $(tagsList).css('visibility', 'hidden');
                }

            });

            var tTagsList = $('<span></span>').addClass('tTagsList').addClass('pull-right').css('margin-right', '5px');

            $(document).on("click", ".list-data", function () {
                var tTags = JSON.parse($(this).attr('data-ptheme-path'));
                var selftTagsList = $('.tTagsList', $(this).parents('.projet'));
                var selfTagsList = $('.ptheme-list', $(this).parents('.projet'));
                selftTagsList.empty();
                var themepath = "";
                for (var i in tTags) {
                    var sep = '';
                    var viewSep = '';
                    if (tTags[i] !== tTags[tTags.length - 1 ]) {
                        sep = ',';
                        viewSep = ' / ';
                    }
                    themepath += tTags[i] + sep;
                    selftTagsList.append($('<span></span>').html(tTags[i])).append(viewSep);
                }
                selftTagsList.addClass('label').addClass('label-info');
                $(this).parents('.projet').prop('data-projet-theme', $.trim(themepath));
                $(selfTagsList).css('visibility', 'hidden');
            });

            var liProjet = $('<li></li>')
                    .addClass('projet')
                    .prop('data-projet-name', projetName)
                    .append(deleteBtn)
                    .append(tagsBtn)
                    .append(tTagsList)
                    .append(tagsList)
                    .append(editableTitle)
                    .prepend(upDownGroup)
                    .append(blocDocUpload)
                    .append(blocPj);
            $('#finalProjets').append(liProjet);
            btnProjetActivation();
        }
        showVoidListInfo($('#finalProjets'));
    }





    /* 
     
     
     function addProjet(projetInputName, projetInputFile) {
     
     
     
     var projetName = $('#' + projetInputName).val();
     
     
     
     var isPresent = false;
     $('#finalProjets .projet').each(function() {
     if ($(this).prop('data-projet-name') === projetName) {
     isPresent = true;
     }
     });
     
     if(isPresent === true){
     $('#modalAlreadyExistProject').modal('show');
     }
     
     if (isPresent !== true) {
     var deleteBtn = $('<span></span>')
     .addClass('btn')
     .addClass('btn-danger')
     .addClass('btn-mini')
     .addClass('pull-right')
     .prop('data-toggle', 'tooltip')
     .prop('title', '<?php echo __d('seance', 'Seance.projet.remove'); ?>')
     .tooltip()
     .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
     .click(function() {
     $(this).parent('.projet').remove();
     showVoidListInfo($('#finalProjets'));
     btnProjetActivation();
     });
     var upBtn = $('<span></span>')
     .addClass('upBtn')
     .addClass('btn')
     .addClass('btn-inverse')
     .addClass('btn-mini')
     .prop('data-toggle', 'tooltip')
     .prop('title', '<?php echo __d('seance', 'Seance.projet.up'); ?>')
     .tooltip()
     .html($('<i></i>').addClass('icon-arrow-up').addClass('icon-white'))
     .click(function() {
     if (!$(this).hasClass('disabled')) {
     var projet = $(this).parent().parent('.projet');
     var prevProjet = projet.prev();
     prevProjet.before(projet);
     btnProjetActivation();
     }
     });
     var downBtn = $('<span></span>')
     .addClass('downBtn')
     .addClass('btn')
     .addClass('btn-inverse')
     .addClass('btn-mini')
     .prop('data-toggle', 'tooltip')
     .prop('title', '<?php echo __d('seance', 'Seance.projet.down'); ?>')
     .tooltip()
     .html($('<i></i>').addClass('icon-arrow-down').addClass('icon-white'))
     .click(function() {
     if (!$(this).hasClass('disabled')) {
     var projet = $(this).parent().parent('.projet');
     var nextProjet = projet.next();
     nextProjet.after(projet);
     btnProjetActivation();
     }
     });
     var docUploadUuid = UUID();
     
     //preparation de l input file à cloner
     var docUpload = $('#' + projetInputFile);
     var inputFileParent = docUpload.parent();
     var cloneInputFile = docUpload.clone(true);
     
     
     docUpload.prop('name', docUploadUuid).addClass('doc_projet');
     var labelUpload = $('<label></label>').html('<?php echo __d('seance', 'Projets.document'); ?>').prop('for', docUploadUuid);
     var hintsDocUpload = $('<div></div>').addClass('hints').html('<?php echo __d('seance', 'Projets.document.filetype'); ?>');
     var blocDocUpload = $('<div></div>')
     .append(labelUpload)
     .append(docUpload) //on déplace l'input file vers le projet
     .append(hintsDocUpload).addClass('doc');
     
     $(inputFileParent).prepend(cloneInputFile); //on replace le clone dans le formulaire de creation de projet
     
     var labelPj = $('<h6></h6>').html('<?php echo __d('seance', 'Projets.pjs') ?>');
     var listPjUuid = UUID();
     var listPj = $('<ul></ul>').prop('id', listPjUuid);
     var btnAddPj = $('<span></span>')
     .addClass('btn')
     .addClass('btn-inverse')
     .html('<?php echo __d('seance', 'seance.form.projet.addAnnexe') ?>')
     .click(function() {
     
     var pjUploadUuid = UUID();
     var pjUpload = $('<input />').prop('type', 'file').prop('name', pjUploadUuid).addClass('doc_pj');
     var labelPjUpload = $('<label></label>').html('<?php echo __d('seance', 'Projets.pj.document'); ?>').prop('for', pjUploadUuid);
     var hintsPjUpload = $('<div></div>').addClass('hints').html('<?php echo __d('seance', 'Projets.pj.document.filetype'); ?>');
     var pjDeleteBtn = $('<span></span>')
     .addClass('btn')
     .addClass('btn-danger')
     .addClass('btn-mini')
     .addClass('pull-right')
     .prop('data-toggle', 'tooltip')
     .prop('title', '<?php echo __d('seance', 'Seance.projet.pj.remove'); ?>')
     .tooltip()
     .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
     .click(function() {
     $(this).parent('.pj').remove();
     btnProjetActivation();
     });
     var liPj = $('<li></li>').append(pjDeleteBtn).append(labelPjUpload).append(pjUpload).append(hintsPjUpload).addClass('pj');
     listPj.append(liPj);
     });
     var blocPj = $('<div></div>').append(labelPj).append(listPj).append(btnAddPj).addClass('pjs');
     var upDownGroup = $('<span></span>').addClass('pull-left').addClass('btn-group').append(upBtn).append(downBtn);
     var projetUuid = UUID();
     var editableTitle = $('<div></div>').addClass('input-append').addClass('title');
     var editableTitleInput = $('<textarea></textarea>')
     .prop('name', projetUuid + '_projetname')
     .html(projetName)
     .prop('title', '<?php echo __d('seance', 'Seance.projet'); ?>')
     .prop('data-toggle', 'tooltip')
     .tooltip().change(function() {
     $(this).html($(this).val());
     });
     editableTitleInput.change(function() {
     var newVal = $(this).html();
     $(this).parents('.projet').prop('data-projet-name', newVal);
     });
     var editableTitleAddOn = $('<span></span>').addClass('add-on').append($('<i></i>').addClass('icon-pencil'));
     editableTitle.append(editableTitleInput).append(editableTitleAddOn);
     var tagsList = $('<ul></ul>').addClass('ptheme-list').addClass('tree-list').prop('data-loaded', false);
     var tagsBtn = $('<a></a>').addClass('btn').addClass('btn-info').addClass('btn-mini').addClass('pull-right')
     .css('margin-right', '5px')
     .prop('title', '<?php echo __d('seance', 'Ptheme.list'); ?> ')
     .prop('data-toggle', 'tooltip')
     .tooltip()
     .append($('<i></i>').addClass('icon-tags').addClass('icon-white'));
     tagsBtn.click(function() {
     if (!$(tagsList).prop('data-loaded')) {
     $(tagsList).html($('<li></li>').append(waitMsg));
     $.get('<?php echo Router::url(array('controller' => 'pthemes', 'action' => 'getList')); ?>', function(data) {
     $(tagsList).html(data);
     $(tagsList).prop('data-loaded', true);
     });
     }
     
     if ($(tagsList).css('visibility') === 'hidden') {
     $(tagsList).css('visibility', 'visible');
     } else {
     $(tagsList).css('visibility', 'hidden');
     }
     
     });
     var tTagsList = $('<span></span>').addClass('tTagsList').addClass('pull-right').css('margin-right', '5px');
     $(document).on("click", ".list-data", function() {
     var tTags = JSON.parse($(this).attr('data-ptheme-path'));
     var selftTagsList = $('.tTagsList', $(this).parents('.projet'));
     var selfTagsList = $('.ptheme-list', $(this).parents('.projet'));
     selftTagsList.empty();
     var themepath = "";
     for (var i in tTags) {
     var sep = '';
     var viewSep = '';
     if (tTags[i] !== tTags[tTags.length - 1 ]) {
     sep = ',';
     viewSep = ' / ';
     }
     themepath += tTags[i] + sep;
     selftTagsList.append($('<span></span>').html(tTags[i])).append(viewSep);
     }
     selftTagsList.addClass('label').addClass('label-info');
     $(this).parents('.projet').prop('data-projet-theme', $.trim(themepath));
     $(selfTagsList).css('visibility', 'hidden');
     });
     var liProjet = $('<li></li>')
     .addClass('projet')
     .prop('data-projet-name', projetName)
     .append(deleteBtn)
     .append(tagsBtn)
     .append(tTagsList)
     .append(tagsList)
     .append(editableTitle)
     .prepend(upDownGroup)
     .append(blocDocUpload)
     .append(blocPj);
     $('#finalProjets').append(liProjet);
     btnProjetActivation();
     }
     showVoidListInfo($('#finalProjets'));
     }
     */
</script>


<h1>
    <div class="pull-right">
        <div class="btn-group">
            <a class="btn btn-inverse tooltiped" data-toggle="" title="<?php echo __d('default', 'btn.details'); ?>" id="btnDetails" data-url="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'details', $seance['Seance']['id'])); ?>" data-original-title="<?php echo __d('default', 'btn.details'); ?>">
                <i class="icon icon-eye-open icon-white"></i>
            </a>
            <a class="btn btn-inverse tooltiped active" data-toggle="" title="<?php echo __d('default', 'btn.edit'); ?>" id="btnEdit" data-url="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'edition', $seance['Seance']['id'])); ?>" data-original-title="<?php echo __d('default', 'btn.edit'); ?>">
                <i class="icon icon-edit icon-white"></i>
            </a>


        </div>
    
        
        
        <a class="btn btn-inverse tooltiped"  title="Ajouter des utilisateurs" id="btnAddUser" href="<?php echo Router::url(array('controller' => 'Seances', 'action' => 'addUsersSeance', $seance['Seance']['id'])); ?>" data-original-title="<?php echo __d('default', 'btn.details'); ?>">
                <i class="icon icon-user icon-white"></i>
            </a>







        <?php
        echo $this->BootstrapBox->modalBtn('seanceDeleteModal', __d('default', 'btn.delete'), $this->Html->tag('i', '', array('class' => 'icon-trash icon-white')), 'btn-danger', $modalOptions['btn.ok.url']);
        ?>
    </div>
    <?php echo __d('seance', 'Seance.edit'); ?>
</h1>

<?php
//echo $this->Html->tag('h1', __d('seance', 'Seance.edit') . ' : ' . $seance['Seance']['name']);
//start form
echo $this->Form->create('Seance', array('type' => 'file'));

//infos
$seanceInfos = $this->Html->tag('legend', __d('seance', 'Seance.form.infos'));


//
//$seanceInfos .= $this->Form->input('type_id', array('name' => 'type_id', 'label' => __d('seance', 'Seance.type'), 'options' => $types, 'empty' => true));
////$seanceInfos .= $this->Form->input('Seance.name', array('name' => 'type_seance', 'label' => __d('seance', 'Seance.name'), 'type' => 'hidden'));
//$confDateInput = array(
//	'type' => 'text',
//	'data-date-format' => 'dd/mm/yyyy',
//	'data-date-weekStart' => 1,
//	'div' => 'input-append',
//	'after' => '<span class="add-on"><i class="icon-calendar"></i></span>',
//	'name' => 'date_seance',
//	'label' => __d('seance', 'Seance.date_seance')
//);
//$seanceInfos .= $this->Form->input('Seance.date_seance', $confDateInput);
//$harray = range(6, 22);
//$hours = array_combine($harray, $harray);
//$marray = range(0, 50, 10);
//$mins = array_combine($marray, $marray);
//
//$labelHourMin = $this->Html->tag('label', __d('seance', 'Seance.heure_seance'), array('for' => 'data[Seance][date_seance_h]'));
//$inputHour = $this->Form->input('Seance.date_seance_h', array('type' => 'select', 'options' => $hours, 'label' => false, 'div' => array('class' => 'inline hourSelectDiv')));
//$inputMin = $this->Form->input('Seance.date_seance_m', array('type' => 'select', 'options' => $mins, 'label' => false, 'div' => array('class' => 'inline')));
//$seanceInfos .= $this->Html->tag('div', $labelHourMin . $inputHour . ' h ' . $inputMin, array('id' => 'hourSelect', 'class' => 'required'));

$seanceDlContent = $this->Html->tag('dt', __d('seance', 'Seance.type_id')) . $this->Html->tag('dd', $seance['Seance']['name']);
$seanceDlContent .= $this->Html->tag('dt', __d('seance', 'Seance.date_seance')) . $this->Html->tag('dd', h(CakeTime::i18nFormat($seance['Seance']['date_seance'], '%x %X')));

// seulement si le lieu est renseigné
if (!empty($seance['Seance']['place'])) {
    $seanceDlContent = $this->Html->tag('dt', __d('seance', 'Seance.place')) . $this->Html->tag('dd', $seance['Seance']['place']);
}
$seanceDlContent .= $this->Html->tag('dt', __d('seance', 'Seance.document') . $this->Html->tag('dd', $this->Html->link($this->Html->tag('i', '', array('class' => array('icon', 'icon-download-alt', 'icon-white'))), Router::url(array('controller' => 'Documents', 'action' => 'getFile', $seance['Document']['id'])), array('class' => array('btn', 'btn-inverse', 'btn-mini', 'pull-right'), 'escape' => false)) . ' ' . $seance['Document']['name']));
$seanceInfos .= $this->Html->tag('dl', $seanceDlContent);
$seanceInfos .= $this->Form->input('Seance.id', array('label' => false, 'value' => $seance['Seance']['id']));


//debug($seance);
// seulement si la convocation n'a pas encore été envoyée
if (!$convocationSent) {
    $seanceInfos .= $this->Form->input('Seance.document', array('type' => 'file', 'name' => 'convocation', 'label' => __d('seance', 'Seance.document.replace')));
}


echo $this->Html->tag('fieldset', $seanceInfos);


//users
$seanceUsers = $this->Html->tag('legend', __d('seance', 'Seance.form.users'));

$ulUsersContent = '';
foreach ($seance['Convocation'] as $convocation) {
    $ulUsersContent .= $this->Html->tag('li', $convocation['User']['name'], array('class' => array('user', 'user-from-type')));
}



$seanceUsers .= $this->Html->tag('ul', $ulUsersContent, array('id' => 'finalUsers'));
echo $this->Html->tag('fieldset', $seanceUsers);


//projets
?> 
<!--<a class="btn btn-inverse "  title="Ordre automatique" id="btnRank"> <i class="icon icon-resize-vertical icon-white"></i></a>-->
<?php
$seanceProjets = $this->Html->tag('legend', __d('seance', 'Seance.form.projets'));
$seanceProjets .= $this->Html->tag('h5', __d('seance', 'Projets.add'), array('class' => 'well-captioned-top'));
$seanceProjetsContent = $this->Html->tag('div', __d('seance', 'Projets.document.infos'), array('class' => 'alert alert-info'));
$seanceProjetsContent .= $this->Form->input('newProjetFile', array('type' => 'file', 'id' => 'newProjetFile', 'label' => __d('seance', 'Projets.document')));
$seanceProjetsContent .= $this->Html->tag('div', __d('seance', 'Projets.document.filetype'), array('class' => 'hints'));
$seanceProjetsContent .= $this->Form->input('newProjet', array('type' => 'text', 'id' => 'newProjet', 'label' => __d('seance', 'Projets.newprojet')));


$seanceProjetsContent .= $this->Html->tag('span', $iconAdd . __d('seance', 'seance.form.addNewProjet'), array('id' => 'btnAddNewProjet', 'class' => array('btn', 'btn-inverse')));
//$seanceProjetsRow = $this->Html->tag('div', $seanceProjetsContent, array('class' => 'span4 offset1'));
$seanceProjetsRow = $this->Html->tag('div', $seanceProjetsContent, array('id' => 'AddProjetForm'));
$seanceProjets .= $this->Html->tag('div', $seanceProjetsRow, array('class' => 'row-fluid well well-small well-captioned'));
$seanceProjets .= __d('seance', 'Projets.list'); 
$seanceProjets .= ' '; 
$seanceProjets .= '<a class="btn btn-inverse "  title="Ordre automatique" id="btnRank"> <i class="icon icon-resize-vertical icon-white"></i> Ordre Automatique</a>';


$ulContent = '';
foreach ($seance['Projet'] as $projet) {
    $themeLabels = $this->Html->tag('span', implode(' / ', explode(',', $projet['theme'])), array('class' => 'label label-info'));
    $docRapport = '';
    if (isset($projet['Document']['id']) && isset($projet['Document']['name'])) {
        $docRapport = $this->Seance->document($seance['Seance']['id'], $projet['Document']['id'], $projet['Document']['name'], false, 'Documents', __d('convocation', 'Projet'));
    }

    $PJs = '';
    foreach ($projet['Annex'] as $annex) {
        $PJs .= $this->Html->tag('li', $this->Seance->document($seance['Seance']['id'], $annex['id'], $annex['name'], false, 'Annexes', __d('convocation', 'Annexe')), array('class' => 'pj'));
    }
    $ulContent .= $this->Html->tag('li', $this->Html->tag('span', $themeLabels, array('class' => 'pull-right')) . $this->Html->tag('span', $projet['name'], array('class' => 'ordreDuJourItem')) . $docRapport . $this->Html->tag('ul', $PJs), array('class' => array('projet', 'notEditable'), 'data-uuid' => $projet['id']));
}
$seanceProjets .= $this->Html->tag('ol', $ulContent, array('id' => 'finalProjets'));
echo $this->Html->tag('fieldset', $seanceProjets);

//end form
echo $this->Html->tag('span', $iconSubmit . __d('default', 'btn.confirm'), array('id' => 'btnSubmit', 'class' => 'btn btn-inverse'));
echo $this->Form->end();
?>

<!-- Boîtes de dialogue : mesasges d'erreurs -->

<!-- Ajout Projet deja existant -->
<div id="modalAlreadyExistProject" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalAlreadyExistProjectLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.projet.already.exists.title') ?></h3>
    </div>
    <div class="modal-body">
        <p>
            <?php echo __d('seance', 'Seance.projet.already.exists.content') ?>

        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>



<!-- Ajout de projet -->
<div id="modalMissingInfoProjet" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalMissingInfoProjetLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.missingInfoProjet.title') ?></h3>
    </div>
    <div class="modal-body">
        <p>
            <span class='missinginfo missingName'><?php echo __d('seance', 'Seance.missingInfoProjet.name') ?><br /></span>
            <span class='missinginfo missingFile'><?php echo __d('seance', 'Seance.missingInfoProjet.file') ?></span>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>

<!-- Selection de fichier -->
<div id="modalBadFile" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalBadFileLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.Projet.add.badFileType.title') ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __d('seance', 'Seance.Projet.add.badFileType.message') ?></p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>

<!-- Envoi de la séance -->
<div id="modalMissingInfoSeance" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalMissingInfoSeanceLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.missingInfoSeance.title') ?></h3>
    </div>
    <div class="modal-body">
        <p>
            <span class='missinginfo missingProjet'><?php echo __d('seance', 'Seance.missingInfoSeance.projet') ?><br /></span>
            <span class='missinginfo missingTheme'><?php echo __d('seance', 'Seance.missingInfoSeance.projet.theme') ?><br /></span>
            <span class='missinginfo missingAnnexe'><?php echo __d('seance', 'Seance.missingInfoSeance.projet.annexe') ?></span>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>





















<script type="text/javascript">
    $('.tooltiped').tooltip();


    var rankIt = function () {
        $('#seanceDetail').html("test");
    };



    //clic sur le bouton d'ajout de projet
    $('#btnAddNewProjet').click(function () {
        var addValid = true;
        //verification du nom de projet
        $('#modalMissingInfoProjet .missinginfo').css('display', 'none');
        if ($('#newProjet').val() == '') {
            $('#modalMissingInfoProjet .missingName').css('display', 'inline');
            $('#newProjet').focus();
            addValid = false;
        }

        //verification du fichier
        if ($('#newProjetFile').val() == '') {
            $('#modalMissingInfoProjet .missingFile').css('display', 'inline');
            $('#newProjetFile').focus();
            addValid = false;
        }

        //traitement
        if (addValid) {
            //addProjet($('#newProjet'), $('#newProjetFile'));
            addProjet('newProjet', 'newProjetFile');
            $('#newProjet').val('');
            $('#newProjetFile').val('');
        } else {
            $('#modalMissingInfoProjet').modal('show');
        }
    });

    //clic sur le bouton d'envoi
    $('#btnSubmit').click(function (e) {
        e.preventDefault();
        e.stopPropagation();


        //initialisation des message d'erreur
        var missingInfoSeance = false;
        $('#modalMissingInfoSeance .missinginfo').css('display', 'none');



        //verfication des projets
        if ($('#SeanceEditionForm .projet').length === 0) {
            $('#modalMissingInfoSeance .missingProjet').css('display', 'inline');
            missingInfoSeance = true;
            $('#newProjet').focus();
        }



        //verfication des themes
        var firstProjetVoidTheme = null;
        var oneVoidTheme = false;
        $('.projet:not(.notEditable)').each(function () {
            if (typeof $(this).prop('data-projet-theme') === 'undefined') {
                if (oneVoidTheme === false) {
                    firstProjetVoidTheme = $(this);
                }
                oneVoidTheme = true;
            }
        });
        if (oneVoidTheme) {
            $('#modalMissingInfoSeance .missingTheme').css('display', 'inline');
            missingInfoSeance = true;
            $(firstProjetVoidTheme).focus();
        }

        //verfication des annexes
        var firstVoidAnnexe = null;
        var oneVoidAnnexe = false;
        $('.doc_pj').each(function () {
            if ($(this).val() === '') {
                if (oneVoidAnnexe === false) {
                    firstVoidAnnexe = $(this);
                }
                oneVoidAnnexe = true;
            }
        });
        if (oneVoidAnnexe) {
            $('#modalMissingInfoSeance .missingAnnexe').css('display', 'inline');
            missingInfoSeance = true;
            $(firstVoidAnnexe).focus();
        }

        //traitement du formulaire et envoi
        if (missingInfoSeance) {
            $('#modalMissingInfoSeance').modal('show');
        } else {

//			var strDate = $('#SeanceDateSeance').val().split('/');
//			var newStrDate = strDate[2] + '-' + strDate[1] + '-' + strDate[0];

            var dataToSend = {
                projets: []
            };

            var projetCpt = 0;
            $('.projet').each(function () {



                //ajout des projets aux données json
                var projet = {
                    ordre: projetCpt,
                    uuid: $(this).attr('data-uuid'),
                    libelle: $(this).prop('data-projet-name'),
                    theme: $(this).prop('data-projet-theme'),
                    annexes: []
                };
                //parametrage de l input file
                $('input[type="file"].doc_projet', this).prop('name', 'projet_' + projetCpt + '_rapport');


                //pièces jointes
                var pjCpt = 0;
                $('input[type="file"].doc_pj', this).each(function () {
                    projet.annexes.push({libelle: $(this).val(), ordre: pjCpt});
                    //parametrage de l input file
                    $(this).prop('name', 'projet_' + projetCpt + '_' + pjCpt + '_annexe');
                    pjCpt++;
                });
                projetCpt++;

                dataToSend.projets.push(projet);
            });
            var jsonData = $('<input></input>').prop('type', 'hidden').prop('name', 'jsonData').val(JSON.stringify(dataToSend));
            $('#SeanceEditionForm').append(jsonData);
            $('#SeanceEditionForm').submit();
        }
    });

    //Conversion du nom de fichier en nom de projet
    $('#newProjetFile').change(function () {
        var tmpName = $(this).val();
        if (tmpName != '' && $('#newProjet').val() == '') {

            //suppression du fakepath ajouter par webkit
            tmpName = tmpName.replace("C:\\fakepath\\", "");

            var ctrlName = tmpName.split('.');
            if (ctrlName[ctrlName.length - 1] != 'pdf') {
                $('#modalBadFile').modal('show');
                $(this).val('');
            } else {
                var newName = "";
                for (var i = 0; i < ctrlName.length - 1; i++) {
                    newName += (i === 0 ? '' : ' ') + ctrlName[i];
                }
                var newName = newName.replace(/_/g, ' ');
                $('#newProjet').val(newName[0].toUpperCase() + newName.substring(1));
            }
        }
    });


    $('#btnDetails').click(function () {
        var url = $(this).attr('data-url');

        $('#seanceDetail').html(waitMsg);
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                $('#seanceDetail').html(data);
            },
            error: function (data) {
                if (data.status === 403) {
                    $('#connexionForbidden').modal('show');
                }
            }
        });
    });

    $('#btnRank').click(function () {
      
        
        var url = "<?php echo Router::url(array('controller' => 'Seances', 'action' => 'edition', $seance['Seance']['id'], true)); ?>";


        $('#seanceDetail').html(waitMsg);
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                $('#seanceDetail').html(data);
            },
            error: function (data) {
                if (data.status === 403) {
                    $('#connexionForbidden').modal('show');
                }
            }
        });

    });




    $('.projet.notEditable').each(function () {
        var deleteBtn = $('<span></span>')
                .addClass('btn')
                .addClass('btn-danger')
                .addClass('btn-mini')
                .addClass('pull-right')
                .css('margin-left', '5px')
                .prop('data-toggle', 'tooltip')
                .prop('title', '<?php echo __d('seance', 'Seance.projet.remove'); ?>')
                .tooltip()
                .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
                .click(function () {
                    $(this).parent('.projet').remove();
                    showVoidListInfo($('#finalProjets'));
                    btnProjetActivation();
                });
        var upBtn = $('<span></span>')
                .addClass('upBtn')
                .addClass('btn')
                .addClass('btn-inverse')
                .addClass('btn-mini')
                .prop('data-toggle', 'tooltip')
                .prop('title', '<?php echo __d('seance', 'Seance.projet.up'); ?>')
                .tooltip()
                .html($('<i></i>').addClass('icon-arrow-up').addClass('icon-white'))
                .click(function () {
                    if (!$(this).hasClass('disabled')) {
                        var projet = $(this).parent().parent('.projet');
                        var prevProjet = projet.prev();
                        prevProjet.before(projet);
                        btnProjetActivation();
                    }
                });
        var downBtn = $('<span></span>')
                .addClass('downBtn')
                .addClass('btn')
                .addClass('btn-inverse')
                .addClass('btn-mini')
                .prop('data-toggle', 'tooltip')
                .prop('title', '<?php echo __d('seance', 'Seance.projet.down'); ?>')
                .tooltip()
                .html($('<i></i>').addClass('icon-arrow-down').addClass('icon-white'))
                .click(function () {
                    if (!$(this).hasClass('disabled')) {
                        var projet = $(this).parent().parent('.projet');
                        var nextProjet = projet.next();
                        nextProjet.after(projet);
                        btnProjetActivation();
                    }
                });
        var upDownGroup = $('<span></span>').addClass('pull-left').addClass('btn-group').append(upBtn).append(downBtn);

        $('.ordreDuJourItem', this).after(upDownGroup);
        $(this).prepend(deleteBtn);
        btnProjetActivation();

    });

</script>