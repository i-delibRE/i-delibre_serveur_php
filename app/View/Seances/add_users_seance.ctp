<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

?>

<div class="types form">
	<?php echo $this->Form->create('Seance', array('action' => "addUsersSeance")); ?>
	<fieldset>
		<legend><?php echo "Ajout d'acteur"; ?></legend>
		<?php
		echo $this->Form->input('User.User', array('type' => 'select', 'multiple' => 'checkbox'));
		?>
                <?php
                echo $this->Form->input('seanceid', array('value' => $seanceId, 'type' => 'hidden'));
		?>
	</fieldset>
	<?php echo $this->Form->end(array('editMode' => true)); ?>
</div>
