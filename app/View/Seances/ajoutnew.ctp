<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

?>


<div  ng-app="idelibreApp" ng-controller="GlobalCtrl">


test  {{2+3}}



</div>


<script>

    "use strict"



    angular.module('idelibreApp', []);
    angular.module('idelibreApp').controller('GlobalCtrl', function ($scope) {

    });




    //pour le datepicker
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    $('#SeanceDateSeance').datepicker().on('changeDate', function () {
        $('#SeanceDateSeance').datepicker('hide');
    }).datepicker('setValue', now);

    $('.icon-calendar').click(function(){
        $('#SeanceDateSeance').datepicker("show");
    });



    //Choix des utilisateurs associés à un type de séance
    $('#SeanceTypeId').change(function () {
        $('.user-from-type').remove();
        if ($('option:selected', $(this)).val() !== '') {
            $.get('<?php echo Router::url(array('controller' => 'Types', 'action' => 'users')); ?>/' + $('option:selected', $(this)).val(), function (data) {
                var users = JSON.parse(data);
                for (var i in users) {
                    addUser(users[i].firstname, users[i].lastname, true);
                }
            });
        } else {
            if ($('#finalUsers li').length === 0) {
                $('#finalUsers').append($('<li></li>').addClass('li-void').addClass('alert').html('<?php echo __d('seance', 'Users.list.void'); ?>'));
            }
        }
    });



    /**
     * Ajout d'un utilisateur
     *
     * @param {string} userFirstname
     * @param {string} userLastname
     * @param {boolean} fromType
     * @returns {undefined}
     */
    function addUser(userFirstname, userLastname, fromType, newUser) {

        var containerId = 'finalUsers';
        if (typeof newUser !== "undefined" && newUser) {
            containerId = 'finalUsersNew';
        }

        var isPresent = false;
        $('#finalUsers .user, #finalUsersNew .user').each(function () {
            if ($(this).prop('data-user-firstname') === userFirstname && $(this).prop('data-user-lastname') === userLastname) {
                if (fromType) {
                    $(this).remove();
                } else {
                    isPresent = true;
                }
            }
        });
        if (isPresent !== true) {
            var deleteBtn = $('<span></span>')
                .addClass('btn')
                .addClass('btn-danger')
                .addClass('btn-mini')
                .addClass('pull-right')
                .prop('data-toggle', 'tooltip')
                .prop('title', '<?php echo __d('seance', 'Users.remove'); ?>')
                .tooltip()
                .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
                .click(function () {
                    $(this).parent('.user').remove();
                    showVoidListInfo($('#finalUsers'));
                    showVoidListInfo($('#finalUsersNew'));
                });
            var liUser = $('<li></li>')
                .addClass('user')
                .prop('data-user-firstname', userFirstname)
                .prop('data-user-lastname', userLastname)
                .html(userFirstname + ' ' + userLastname);
            if (fromType !== true) {
                liUser.append(deleteBtn)
            } else if (fromType === true) {
                liUser.addClass('user-from-type');
            }
            $('#' + containerId).append(liUser);
        }
        //showVoidListInfo($('#' + containerId));
        showVoidListInfo($('#finalUsers'));
        showVoidListInfo($('#finalUsersNew'));
    }


   /* Affichage d'information de liste vide
    *
    * @param {Object} list
    * @returns {undefined}
    */
    function showVoidListInfo(list) {
        if ($('li', list).length === 1) {
            $('li.li-void', list).css('display', 'list-item');
        } else {
            $('li.li-void', list).css('display', 'none');
        }
    }


    //Ajout des utilisateurs sélectionné dans la liste en cliquant sur le boutton
    $('#btnAddSelectedUsers').click(function () {
        $('#UserUser option:selected').each(function () {
            var fullname = $(this).html();
            var nameItems = fullname.split(' ');
            addUser(nameItems[0], nameItems[1], false, true);
        });
    });

    //en doucle cliquant sur le nom de l'user
    //Doucble clic pour selection utilisateur dans la liste
    $('#UserUser').dblclick(function () {
        $('#btnAddSelectedUsers').trigger('click');
    });


    /**
     *
     * AJOUT DE PROJETS
     *
     */


   /* $('#newProjetFile').change(function () {
        var tmpName = $(this)[0].files;
        console.log(tmpName);
    //    alert("ok");
    });*/


   var projetFiles = [];
   //var tmpProjetFile = [];

    //Conversion du nom de fichier en nom de projet
    $('#newProjetFile').change(function () {

        var tmpFiles = $(this)[0].files

        for (var i = 0; i < tmpFiles.length; i++) {
            console.log(tmpFiles[i].name)
            //verification de l'extension pdf
            var ctrlName = tmpFiles[i].name.split('.');
            if (ctrlName[ctrlName.length - 1] != 'pdf') {
                $('#modalBadFile').modal('show');
                $(this).val('');
                return;
            }
            projetFiles.push(tmpFiles[i])
        }

        console.log(projetFiles)


        for(var i=0; i< projetFiles.length; i++){
            addProjet(projetFiles[i]);
        }




    //    return;
        /*
        for(var i=0; i< projetFiles.length; i++){
            $('#finalProjets').append(projetFiles[i].name +"<br>");
        }
*/


    });




</script>