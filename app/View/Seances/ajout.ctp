<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

//debug($users); die;

echo $this->Html->css('/vendors/bootstrap-datepicker/datepicker');
echo $this->Html->script('globalHelper');
echo $this->Html->script('/vendors/bootstrap-datepicker/bootstrap-datepicker');
echo $this->Html->css('treelist');



$iconAdd = $this->html->tag('i', '', array('class' => 'icon icon-plus-sign icon-white')) . ' ';
$iconSubmit = $this->html->tag('i', '', array('class' => 'icon icon-ok-sign icon-white')) . ' ';
?>





<script type="text/javascript">
    /**
     * Affichage d'information de liste vide
     *
     * @param {Object} list
     * @returns {undefined}
     */
    function showVoidListInfo(list) {
        if ($('li', list).length === 1) {
            $('li.li-void', list).css('display', 'list-item');
        } else {
            $('li.li-void', list).css('display', 'none');
        }
    }

    /**
     * Ajout d'un utilisateur
     *
     * @param {string} userFirstname
     * @param {string} userLastname
     * @param {boolean} fromType
     * @returns {undefined}
     */
    function addUser(userFirstname, userLastname, fromType, newUser) {

        var containerId = 'finalUsers';
        if (typeof newUser !== "undefined" && newUser) {
            containerId = 'finalUsersNew';
        }

        var isPresent = false;
        $('#finalUsers .user, #finalUsersNew .user').each(function () {
            if ($(this).prop('data-user-firstname') === userFirstname && $(this).prop('data-user-lastname') === userLastname) {
                if (fromType) {
                    $(this).remove();
                } else {
                    isPresent = true;
                }
            }
        });
        if (isPresent !== true) {
            var deleteBtn = $('<span></span>')
                    .addClass('btn')
                    .addClass('btn-danger')
                    .addClass('btn-mini')
                    .addClass('pull-right')
                    .prop('data-toggle', 'tooltip')
                    .prop('title', '<?php echo __d('seance', 'Users.remove'); ?>')
                    .tooltip()
                    .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
                    .click(function () {
                        $(this).parent('.user').remove();
//                        showVoidListInfo($('#' + containerId));
                        showVoidListInfo($('#finalUsers'));
                        showVoidListInfo($('#finalUsersNew'));
                    });
            var liUser = $('<li></li>')
                    .addClass('user')
                    .prop('data-user-firstname', userFirstname)
                    .prop('data-user-lastname', userLastname)
                    .html(userFirstname + ' ' + userLastname);
            if (fromType !== true) {
                liUser.append(deleteBtn)
            } else if (fromType === true) {
                liUser.addClass('user-from-type');
            }
            $('#' + containerId).append(liUser);
        }
        //showVoidListInfo($('#' + containerId));
        showVoidListInfo($('#finalUsers'));
        showVoidListInfo($('#finalUsersNew'));
    }

    /**
     * Activation des boutons de déplacement des projets
     *
     * @returns {undefined} */
    function btnProjetActivation() {
        $('#finalProjets .projet').each(function () {
            var projet = $(this);
            $('.upBtn, .downBtn', projet).removeClass('disabled');
            if ($('#finalProjets .projet').first()[0] === projet[0]) {
                $('.upBtn', projet).addClass('disabled').tooltip('destroy');
            } else {
                $('.upBtn', projet).prop('data-toggle', 'tooltip').prop('title', '<?php echo __d('seance', 'Seance.projet.up'); ?>').tooltip();
            }

            if ($('#finalProjets .projet').last()[0] === projet[0]) {
                $('.downBtn', projet).addClass('disabled').tooltip('destroy');
            } else {
                $('.downBtn', projet).prop('data-toggle', 'tooltip').prop('title', '<?php echo __d('seance', 'Seance.projet.down'); ?>').tooltip();
            }
        });
    }

    /**
     * ajout d'un projet
     *
     * @param {object} projetInputName
     * @param {object} projetInputFile
     * @returns {undefined}	 */
    function addProjet(projetInputName, projetInputFile) {



        var projetName = $('#' + projetInputName).val();



        var isPresent = false;
        $('#finalProjets .projet').each(function () {
            if ($(this).prop('data-projet-name') === projetName) {
                isPresent = true;
            }
        });

        if (isPresent === true) {
            $('#modalAlreadyExistProject').modal('show');
        }

        if (isPresent !== true) {
            var deleteBtn = $('<span></span>')
                    .addClass('btn')
                    .addClass('btn-danger')
                    .addClass('btn-mini')
                    .addClass('pull-right')
                    .prop('data-toggle', 'tooltip')
                    .prop('title', '<?php echo __d('seance', 'Seance.projet.remove'); ?>')
                    .tooltip()
                    .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
                    .click(function () {
                        $(this).parent('.projet').remove();
                        showVoidListInfo($('#finalProjets'));
                        btnProjetActivation();
                    });
            var upBtn = $('<span></span>')
                    .addClass('upBtn')
                    .addClass('btn')
                    .addClass('btn-inverse')
                    .addClass('btn-mini')
                    .prop('data-toggle', 'tooltip')
                    .prop('title', '<?php echo __d('seance', 'Seance.projet.up'); ?>')
                    .tooltip()
                    .html($('<i></i>').addClass('icon-arrow-up').addClass('icon-white'))
                    .click(function () {
                        if (!$(this).hasClass('disabled')) {
                            var projet = $(this).parent().parent('.projet');
                            var prevProjet = projet.prev();
                            prevProjet.before(projet);
                            btnProjetActivation();
                        }
                    });
            var downBtn = $('<span></span>')
                    .addClass('downBtn')
                    .addClass('btn')
                    .addClass('btn-inverse')
                    .addClass('btn-mini')
                    .prop('data-toggle', 'tooltip')
                    .prop('title', '<?php echo __d('seance', 'Seance.projet.down'); ?>')
                    .tooltip()
                    .html($('<i></i>').addClass('icon-arrow-down').addClass('icon-white'))
                    .click(function () {
                        if (!$(this).hasClass('disabled')) {
                            var projet = $(this).parent().parent('.projet');
                            var nextProjet = projet.next();
                            nextProjet.after(projet);
                            btnProjetActivation();
                        }
                    });
            var docUploadUuid = UUID();

            //preparation de l input file à cloner
            var docUpload = $('#' + projetInputFile);
            var inputFileParent = docUpload.parent();
            var cloneInputFile = docUpload.clone(true);


            docUpload.prop('name', docUploadUuid).addClass('doc_projet');
            var labelUpload = $('<label></label>').html('<?php echo __d('seance', 'Projets.document'); ?>').prop('for', docUploadUuid);
            var hintsDocUpload = $('<div></div>').addClass('hints').html('<?php echo __d('seance', 'Projets.document.filetype'); ?>');
            var blocDocUpload = $('<div></div>')
                    .append(labelUpload)
                    .append(docUpload) //on déplace l'input file vers le projet
                    .append(hintsDocUpload).addClass('doc');

            $(inputFileParent).prepend(cloneInputFile); //on replace le clone dans le formulaire de creation de projet

            var labelPj = $('<h6></h6>').html('<?php echo __d('seance', 'Projets.pjs') ?>');
            var listPjUuid = UUID();
            var listPj = $('<ul></ul>').prop('id', listPjUuid);
            var btnAddPj = $('<span></span>')
                    .addClass('btn')
                    .addClass('btn-inverse')
                    .html('<?php echo __d('seance', 'seance.form.projet.addAnnexe') ?>')
                    .click(function () {

                        var pjUploadUuid = UUID();
                        var pjUpload = $('<input />').prop('type', 'file').prop('name', pjUploadUuid).addClass('doc_pj');
                        var labelPjUpload = $('<label></label>').html('<?php echo __d('seance', 'Projets.pj.document'); ?>').prop('for', pjUploadUuid);
                        var hintsPjUpload = $('<div></div>').addClass('hints').html('<?php echo __d('seance', 'Projets.pj.document.filetype'); ?>');
                        var pjDeleteBtn = $('<span></span>')
                                .addClass('btn')
                                .addClass('btn-danger')
                                .addClass('btn-mini')
                                .addClass('pull-right')
                                .prop('data-toggle', 'tooltip')
                                .prop('title', '<?php echo __d('seance', 'Seance.projet.pj.remove'); ?>')
                                .tooltip()
                                .html($('<i></i>').addClass('icon-remove').addClass('icon-white'))
                                .click(function () {
                                    $(this).parent('.pj').remove();
                                    btnProjetActivation();
                                });
                        var liPj = $('<li></li>').append(pjDeleteBtn).append(labelPjUpload).append(pjUpload).append(hintsPjUpload).addClass('pj');
                        listPj.append(liPj);
                    });
            var blocPj = $('<div></div>').append(labelPj).append(listPj).append(btnAddPj).addClass('pjs');
            var upDownGroup = $('<span></span>').addClass('pull-left').addClass('btn-group').append(upBtn).append(downBtn);
            var projetUuid = UUID();
            var editableTitle = $('<div></div>').addClass('input-append').addClass('title');
            var editableTitleInput = $('<textarea></textarea>')
                    .prop('name', projetUuid + '_projetname')
                    .html(projetName)
                    .prop('title', '<?php echo __d('seance', 'Seance.projet'); ?>')
                    .prop('data-toggle', 'tooltip')
                    .tooltip().change(function () {
                $(this).html($(this).val());
            });
            editableTitleInput.change(function () {
                var newVal = $(this).html();
                $(this).parents('.projet').prop('data-projet-name', newVal);
            });
            var editableTitleAddOn = $('<span></span>').addClass('add-on').append($('<i></i>').addClass('icon-pencil'));
            editableTitle.append(editableTitleInput).append(editableTitleAddOn);
            var tagsList = $('<ul></ul>').addClass('ptheme-list').addClass('tree-list').prop('data-loaded', false);
            var tagsBtn = $('<a></a>').addClass('btn').addClass('btn-info').addClass('btn-mini').addClass('pull-right')
                    .css('margin-right', '5px')
                    .prop('title', '<?php echo __d('seance', 'Ptheme.list'); ?> ')
                    .prop('data-toggle', 'tooltip')
                    .tooltip()
                    .append($('<i></i>').addClass('icon-tags').addClass('icon-white'));
            tagsBtn.click(function () {
                if (!$(tagsList).prop('data-loaded')) {
                    $(tagsList).html($('<li></li>').append(waitMsg));
                    $.get('<?php echo Router::url(array('controller' => 'pthemes', 'action' => 'getList')); ?>', function (data) {
                        $(tagsList).html(data);
                        $(tagsList).prop('data-loaded', true);
                    });
                }

                if ($(tagsList).css('visibility') === 'hidden') {
                    $(tagsList).css('visibility', 'visible');
                } else {
                    $(tagsList).css('visibility', 'hidden');
                }

            });

            var optionsRapporteur = <?php echo json_encode($users) ?>;

            var selectRapporteur = $('<select></select>');

            $("<option />", {value: null, text: 'Selectionner le rapporteur', disabled: 'disabled', selected: 'selected'}).appendTo(selectRapporteur);
            $("<option />", {value: '', text: 'Aucun'}).appendTo(selectRapporteur);
            for (var val in optionsRapporteur) {
                $("<option />", {value: val, text: optionsRapporteur[val]}).appendTo(selectRapporteur);
            }


            var tTagsList = $('<span></span>').addClass('tTagsList').addClass('pull-right').css('margin-right', '5px');
            $(document).on("click", ".list-data", function () {
                var tTags = JSON.parse($(this).attr('data-ptheme-path'));
                var selftTagsList = $('.tTagsList', $(this).parents('.projet'));
                var selfTagsList = $('.ptheme-list', $(this).parents('.projet'));
                selftTagsList.empty();
                var themepath = "";
                for (var i in tTags) {
                    var sep = '';
                    var viewSep = '';
                    if (tTags[i] !== tTags[tTags.length - 1 ]) {
                        sep = ',';
                        viewSep = ' / ';
                    }
                    themepath += tTags[i] + sep;
                    selftTagsList.append($('<span></span>').html(tTags[i])).append(viewSep);
                }
                selftTagsList.addClass('label').addClass('label-info');
                $(this).parents('.projet').prop('data-projet-theme', $.trim(themepath));
                $(selfTagsList).css('visibility', 'hidden');
            });
            var liProjet = $('<li></li>')
                    .addClass('projet')
                    .prop('data-projet-name', projetName)
                    .append(deleteBtn)
                    .append(tagsBtn)
                    .append(tTagsList)
                    .append(tagsList)
                    .append(editableTitle)
                    .append(selectRapporteur)
                    .prepend(upDownGroup)
                    .append(blocDocUpload)
                    .append(blocPj);
            $('#finalProjets').append(liProjet);
            btnProjetActivation();
        }
        showVoidListInfo($('#finalProjets'));
    }
</script>


<?php echo $this->Html->tag('h1', __d('seance', 'Seance.add')); ?>
<div class="alert alert-info">
    <?php echo __d('seance', 'upload_params'); ?> :
    <ul>
        <li><?php echo __d('appchecks', 'Checks.php.upload_max_filesize');
    ?> : <?php echo ini_get('upload_max_filesize'); ?>o</li>
        <li><?php echo __d('appchecks', 'Checks.php.max_file_uploads'); ?> : <?php echo ini_get('max_file_uploads'); ?></li>
    </ul>
</div>
<?php
//start form
echo $this->Form->create('Seance', array('type' => 'file'));

//infos
$seanceInfos = $this->Html->tag('legend', __d('seance', 'Seance.form.infos'));
$seanceInfos .= $this->Form->input('type_id', array('name' => 'type_id', 'label' => __d('seance', 'Seance.type'), 'options' => $types, 'empty' => true));
//$seanceInfos .= $this->Form->input('Seance.name', array('name' => 'type_seance', 'label' => __d('seance', 'Seance.name'), 'type' => 'hidden'));
$confDateInput = array(
    'type' => 'text',
    'data-date-format' => 'dd/mm/yyyy',
    'data-date-weekStart' => 1,
    'div' => 'input-append',
    'after' => '<span class="add-on"><i class="icon-calendar"></i></span>',
    'name' => 'date_seance',
    'label' => __d('seance', 'Seance.date_seance')
);
$seanceInfos .= $this->Form->input('Seance.date_seance', $confDateInput);
$harray = range(6, 22);
$hours = array_combine($harray, $harray);
$marray = range(0, 55, 5);
$mins = array_combine($marray, $marray);

$labelHourMin = $this->Html->tag('label', __d('seance', 'Seance.heure_seance'), array('for' => 'data[Seance][date_seance_h]'));
$inputHour = $this->Form->input('Seance.date_seance_h', array('type' => 'select', 'options' => $hours, 'label' => false, 'div' => array('class' => 'inline hourSelectDiv')));
$inputMin = $this->Form->input('Seance.date_seance_m', array('type' => 'select', 'options' => $mins, 'label' => false, 'div' => array('class' => 'inline')));
$seanceInfos .= $this->Html->tag('div', $labelHourMin . $inputHour . ' h ' . $inputMin, array('id' => 'hourSelect', 'class' => 'required'));

$seanceInfos .= $this->Form->input('Seance.place', array(
    'label' => 'Lieu'
        ));

$seanceInfos .= $this->Form->input('Seance.document', array('type' => 'file', 'name' => 'convocation', 'label' => __d('seance', 'Seance.document')));

$seanceInfos .= $this->Html->tag('div', __d('seance', 'Projets.document.filetype'), array('class' => 'hints', 'style' => 'margin-left: 185px;'));
echo $this->Html->tag('fieldset', $seanceInfos);


//users
$seanceUsers = $this->Html->tag('legend', __d('seance', 'Seance.form.users'));

$seanceUsers .= $this->Html->tag('h5', __d('seance', 'Users.list'));
$seanceUsers .= $this->Html->tag('ul', $this->Html->tag('li', __d('seance', 'Users.list.void'), array('class' => 'li-void alert')), array('id' => 'finalUsers'));

$seanceUsers .= $this->Html->tag('h5', __d('seance', 'Users.add'), array('class' => 'well-captioned-top'));

$addUserFieldsetContentLeft = $this->Html->tag('h6', __d('seance', 'Users.existant'));
$addUserFieldsetContentLeft .= $this->Form->input('User.User', array('name' => 'acteurs_convoques', 'label' => false));
$addUserFieldsetContentLeft .= $this->Html->tag('span', $iconAdd . __d('seance', 'seance.form.addSelectedUsers'), array('id' => 'btnAddSelectedUsers', 'class' => array('btn', 'btn-inverse')));
/*
  $addUserFieldsetContentRight = $this->Html->tag('h6', __d('seance', 'Users.new'));
  $addUserFieldsetContentRight .= $this->Form->input('newUserFirstname', array('type' => 'text', 'id' => 'newUserFirstname', 'label' => __d('seance', 'Users.newuserfirstname')));
  $addUserFieldsetContentRight .= $this->Form->input('newUserLastname', array('type' => 'text', 'id' => 'newUserLastname', 'label' => __d('seance', 'Users.newuserlastname')));
  $addUserFieldsetContentRight .= $this->Html->tag('span', $iconAdd . __d('seance', 'seance.form.addNewUser'), array('id' => 'btnAddNewUser', 'class' => array('btn', 'btn-inverse')));
 */
$addUserFieldsetContent = $this->Html->tag('div', $addUserFieldsetContentLeft, array('class' => 'span4 offset1'));
//$addUserFieldsetContent .= $this->Html->tag('div', $addUserFieldsetContentRight, array('class' => 'span4 offset1 small-fields'));
$addUserFieldset = $this->Html->tag('div', $addUserFieldsetContent, array('class' => 'row-fluid well well-small well-captioned'));

$seanceUsers .= $addUserFieldset;






$seanceUsers .= $this->Html->tag('h5', __d('seance', 'Users.list.new'));
$seanceUsers .= $this->Html->tag('ul', $this->Html->tag('li', __d('seance', 'Users.list.void'), array('class' => 'li-void alert')), array('id' => 'finalUsersNew'));






echo $this->Html->tag('fieldset', $seanceUsers);


//projets
$seanceProjets = $this->Html->tag('legend', __d('seance', 'Seance.form.projets'));
$seanceProjets .= $this->Html->tag('h5', __d('seance', 'Projets.add'), array('class' => 'well-captioned-top'));
$seanceProjetsContent = $this->Html->tag('div', __d('seance', 'Projets.document.infos'), array('class' => 'alert alert-info'));
$seanceProjetsContent .= $this->Form->input('newProjetFile', array('type' => 'file', 'id' => 'newProjetFile', 'label' => __d('seance', 'Projets.document'), "multiple"));
$seanceProjetsContent .= $this->Html->tag('div', __d('seance', 'Projets.document.filetype'), array('class' => 'hints'));
$seanceProjetsContent .= $this->Form->input('newProjet', array('type' => 'text', 'id' => 'newProjet', 'label' => __d('seance', 'Projets.newprojet')));

$seanceProjetsContent .= $this->Html->tag('span', $iconAdd . __d('seance', 'seance.form.addNewProjet'), array('id' => 'btnAddNewProjet', 'class' => array('btn', 'btn-inverse')));
//$seanceProjetsRow = $this->Html->tag('div', $seanceProjetsContent, array('class' => 'span4 offset1'));
$seanceProjetsRow = $this->Html->tag('div', $seanceProjetsContent, array('id' => 'AddProjetForm'));
$seanceProjets .= $this->Html->tag('div', $seanceProjetsRow, array('class' => 'row-fluid well well-small well-captioned'));
$seanceProjets .= $this->Html->tag('h5', __d('seance', 'Projets.list'));
$seanceProjets .= $this->Html->tag('ol', $this->Html->tag('li', __d('seance', 'Projets.list.void'), array('class' => 'li-void alert')), array('id' => 'finalProjets'));
echo $this->Html->tag('fieldset', $seanceProjets);

//end form
echo $this->Html->tag('span', $iconSubmit . __d('seance', 'btn.submit'), array('id' => 'btnSubmit', 'class' => 'btn btn-inverse'));
echo $this->Form->end();
?>

<!-- Boîtes de dialogue : mesasges d'erreurs -->

<!-- Ajout Projet deja existant -->
<div id="modalAlreadyExistProject" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalAlreadyExistProjectLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.projet.already.exists.title') ?></h3>
    </div>
    <div class="modal-body">
        <p>
            <?php echo __d('seance', 'Seance.projet.already.exists.content') ?>

        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>




<!-- Ajout d'utilisateur -->
<div id="modalMissingInfoNewUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalMissingInfoNewUserLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.missingInfoNewUser.title') ?></h3>
    </div>
    <div class="modal-body">
        <p>
            <span class='missinginfo missingFirstName'><?php echo __d('seance', 'Seance.missingInfoNewUser.firstname') ?><br /></span>
            <span class='missinginfo missingLastName'><?php echo __d('seance', 'Seance.missingInfoNewUser.lastname') ?></span>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>

<!-- Ajout de projet -->
<div id="modalMissingInfoProjet" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalMissingInfoProjetLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.missingInfoProjet.title') ?></h3>
    </div>
    <div class="modal-body">
        <p>
            <span class='missinginfo missingName'><?php echo __d('seance', 'Seance.missingInfoProjet.name') ?><br /></span>
            <span class='missinginfo missingFile'><?php echo __d('seance', 'Seance.missingInfoProjet.file') ?></span>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>

<!-- Selection de fichier -->
<div id="modalBadFile" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalBadFileLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.Projet.add.badFileType.title') ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __d('seance', 'Seance.Projet.add.badFileType.message') ?></p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>



<!-- Selection de fichier -->
<div id="modalTooBigFile" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalTooBigFile" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Fichier trop volumineux</h3>
    </div>
    <div class="modal-body">
        <p>Votre fichier dépasse la taille maximum autorisée (<?php echo ini_get('upload_max_filesize'); ?>o)</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>



<!-- Envoi de la séance -->
<div id="modalMissingInfoSeance" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalMissingInfoSeanceLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo __d('seance', 'Seance.missingInfoSeance.title') ?></h3>
    </div>
    <div class="modal-body">
        <p>
            <span class='missinginfo missingType'><?php echo __d('seance', 'Seance.missingInfoSeance.type') ?><br /></span>
            <span class='missinginfo missingDate'><?php echo __d('seance', 'Seance.missingInfoSeance.date') ?><br /></span>
            <span class='missinginfo missingHeure'><?php echo __d('seance', 'Seance.missingInfoSeance.heure') ?><br /></span>
            <span class='missinginfo missingFile'><?php echo __d('seance', 'Seance.missingInfoSeance.convocation') ?><br /></span>
            <span class='missinginfo missingUser'><?php echo __d('seance', 'Seance.missingInfoSeance.user') ?><br /></span>
            <span class='missinginfo missingProjet'><?php echo __d('seance', 'Seance.missingInfoSeance.projet') ?><br /></span>
            <span class='missinginfo missingTheme'><?php echo __d('seance', 'Seance.missingInfoSeance.projet.theme') ?><br /></span>
            <span class='missinginfo missingAnnexe'><?php echo __d('seance', 'Seance.missingInfoSeance.projet.annexe') ?></span>
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo __d('default', 'btn.close') ?></button>
    </div>
</div>

<script type="text/javascript">
    //initilisation du datepicker
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    $('#SeanceDateSeance').datepicker().on('changeDate', function () {
        $('#SeanceDateSeance').datepicker('hide');
    }).datepicker('setValue', now);
    //Ajout des utilisateurs sélectionné dans la liste
    $('#btnAddSelectedUsers').click(function () {
        $('#UserUser option:selected').each(function () {
            var fullname = $(this).html();
            var nameItems = fullname.split(' ');
            addUser(nameItems[0], nameItems[1], false, true);
        });
    });
    //clic sur le bouton d'ajout d'utilisateur (Champs libres)
    $('#btnAddNewUser').click(function () {
        var addValid = true;
        $('#modalMissingInfoNewUser .missinginfo').css('display', 'none');
        //verification du prenom
        if ($('#newUserFirstname').val() == '') {
            $('#modalMissingInfoNewUser .missingFirstName').css('display', 'inline');
            $('#newUserFirstname').focus();
            addValid = false;
        }

        //verification du nom
        if ($('#newUserLastname').val() == '') {
            $('#modalMissingInfoNewUser .missingLastName').css('display', 'inline');
            $('#newUserLastname').focus();
            addValid = false;
        }

        //traitement
        if (addValid) {
            addUser($('#newUserFirstname').val(), $('#newUserLastname').val(), false, true);
            $('#newUserFirstname').val('');
            $('#newUserLastname').val('');
        } else {
            $('#modalMissingInfoNewUser').modal('show');
        }
    });
    //clic sur le bouton d'ajout de projet
    $('#btnAddNewProjet').click(function () {
        var addValid = true;
        //verification du nom de projet
        $('#modalMissingInfoProjet .missinginfo').css('display', 'none');
        if ($('#newProjet').val() == '') {
            $('#modalMissingInfoProjet .missingName').css('display', 'inline');
            $('#newProjet').focus();
            addValid = false;
        }

        //verification du fichier
        if ($('#newProjetFile').val() == '') {
            $('#modalMissingInfoProjet .missingFile').css('display', 'inline');
            $('#newProjetFile').focus();
            addValid = false;
        }

        //traitement
        if (addValid) {


//			addProjet($('#newProjet'), $('#newProjetFile'));
            addProjet('newProjet', 'newProjetFile');



            $('#newProjet').val('');
            $('#newProjetFile').val('');
        } else {
            $('#modalMissingInfoProjet').modal('show');
        }
    });
    //clic sur le bouton d'envoi
    $('#btnSubmit').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        //initialisation des message d'erreur
        var missingInfoSeance = false;
        $('#modalMissingInfoSeance .missinginfo').css('display', 'none');
        //verfication du type
        if ($('#SeanceTypeId option:selected').length == 0 || $('#SeanceTypeId option:selected').val() == '') {
            $('#modalMissingInfoSeance .missingType').css('display', 'inline');
            missingInfoSeance = true;
            $('#SeanceTypeId').focus();
            $('html,body').scrollTop(0);
        }

        //verfication de la date
        if ($('#SeanceDateSeance').val() == '') {
            $('#modalMissingInfoSeance .missingDate').css('display', 'inline');
            missingInfoSeance = true;
            $('#SeanceDateSeance').focus();
            $('html,body').scrollTop(0);
        }

        //verfication de l'heure
        if ($('#SeanceDateSeanceH').val() == '' || $('#SeanceDateSeanceM').val() == '') {
            $('#modalMissingInfoSeance .missingDate').css('display', 'inline');
            missingInfoSeance = true;
            $('#SeanceDateSeanceH').focus();
            $('html,body').scrollTop(0);
        }

        //verfication de la convocation
        if ($('#SeanceDocument').val() == '') {
            $('#modalMissingInfoSeance .missingFile').css('display', 'inline');
            missingInfoSeance = true;
            $('#SeanceDocument').focus();
            $('html,body').scrollTop(0);
        }

//        //verfication des projets
//        if ($('#SeanceAjoutForm .projet').length === 0) {
//            $('#modalMissingInfoSeance .missingProjet').css('display', 'inline');
//            missingInfoSeance = true;
//            $('#newProjet').focus();
//        }


        //verfication des utilisateurs
        if ($('#SeanceAjoutForm .user').length === 0) {
            $('#modalMissingInfoSeance .missingUser').css('display', 'inline');
            missingInfoSeance = true;
            $('#newProjet').focus();
        }

        //verfication des themes
        var firstProjetVoidTheme = null;
        var oneVoidTheme = false;
        $('.projet').each(function () {
            if (typeof $(this).prop('data-projet-theme') === 'undefined') {
                if (oneVoidTheme === false) {
                    firstProjetVoidTheme = $(this);
                }
                oneVoidTheme = true;
            }
        });
        if (oneVoidTheme) {
            $('#modalMissingInfoSeance .missingTheme').css('display', 'inline');
            missingInfoSeance = true;
            $(firstProjetVoidTheme).focus();
        }

        //verfication des annexes
        var firstVoidAnnexe = null;
        var oneVoidAnnexe = false;
        $('.doc_pj').each(function () {
            if ($(this).val() === '') {
                if (oneVoidAnnexe === false) {
                    firstVoidAnnexe = $(this);
                }
                oneVoidAnnexe = true;
            }
        });
        if (oneVoidAnnexe) {
            $('#modalMissingInfoSeance .missingAnnexe').css('display', 'inline');
            missingInfoSeance = true;
            $(firstVoidAnnexe).focus();
        }

        //traitement du formulaire et envoi
        if (missingInfoSeance) {
            $('#modalMissingInfoSeance').modal('show');
        } else {

            var strDate = $('#SeanceDateSeance').val().split('/');
            var newStrDate = strDate[2] + '-' + strDate[1] + '-' + strDate[0];
            var dataToSend = {
                type_id: $('#SeanceTypeId option:selected').val(),
                type_seance: $('#SeanceName').val(),
                date_seance: newStrDate + ' ' + parseInt($('#SeanceDateSeanceH').val()) + ':' + parseInt($('#SeanceDateSeanceM').val()) + ':00',
                acteurs_convoques: [],
                projets: []
            };
            $('.user').each(function () {
                var newUser = {
                    "Acteur": {
                        "nom": $(this).prop('data-user-lastname'),
                        "prenom": $(this).prop('data-user-firstname'),
                        "email": "",
                        "actif": true,
                        "suppleant_id": null
                    },
                    "Typeacteur": {
                        "elu": true
                    }
                };
                dataToSend.acteurs_convoques.push(newUser);
            });
            var projetCpt = 0;
            $('.projet').each(function () {
                var $select = ($(this).find('select'));
                var selectedId = $select.val();


                //ajout des projets aux données json
                var projet = {
                    ordre: projetCpt,
                    libelle: $(this).prop('data-projet-name'),
                    rapporteurId: selectedId,
                    theme: $(this).prop('data-projet-theme'),
                    annexes: []
                };
                //parametrage de l input file
                $('input[type="file"].doc_projet', this).prop('name', 'projet_' + projetCpt + '_rapport');
                //pièces jointes
                var pjCpt = 0;
                $('input[type="file"].doc_pj', this).each(function () {
                    projet.annexes.push({libelle: $(this).val(), ordre: pjCpt});
                    //parametrage de l input file
                    $(this).prop('name', 'projet_' + projetCpt + '_' + pjCpt + '_annexe');
                    pjCpt++;
                });
                projetCpt++;
                dataToSend.projets.push(projet);
            });
            var jsonData = $('<input></input>').prop('type', 'hidden').prop('name', 'jsonData').val(JSON.stringify(dataToSend));
            $('#SeanceAjoutForm').append(jsonData);

            $('#SeanceAjoutForm').submit();
        }
    });
    //Choix des utilisateurs associés à un type de séance
    $('#SeanceTypeId').change(function () {
        $('.user-from-type').remove();
        if ($('option:selected', $(this)).val() !== '') {
            $.get('<?php echo Router::url(array('controller' => 'Types', 'action' => 'users')); ?>/' + $('option:selected', $(this)).val(), function (data) {
                var users = JSON.parse(data);
                for (var i in users) {
                    addUser(users[i].firstname, users[i].lastname, true);
                }
            });
        } else {
            if ($('#finalUsers li').length === 0) {
                $('#finalUsers').append($('<li></li>').addClass('li-void').addClass('alert').html('<?php echo __d('seance', 'Users.list.void'); ?>'));
            }
        }
    });
    //Doucble clic pour selection utilisateur dans la liste
    $('#UserUser').dblclick(function () {
        $('#btnAddSelectedUsers').trigger('click');
    });
    //Conversion du nom de fichier en nom de projet
    $('#newProjetFile').change(function () {
        var tmpName = $(this).val();
         var maxFileSize = "<?php echo ini_get('upload_max_filesize'); ?>".slice(0, -1) * 1024 *1024;
        if (tmpName != '' && $('#newProjet').val() == '') {

            //suppression du fakepath ajouter par webkit
            tmpName = tmpName.replace("C:\\fakepath\\", "");

            //verification de l'extension pdf
            var ctrlName = tmpName.split('.');
            if (ctrlName[ctrlName.length - 1] != 'pdf') {
                $('#modalBadFile').modal('show');
                $(this).val('');
            } else {

                if ($(this).get(0).files.length > 0) { // only if a file is selected
                    var fileSize = $(this).get(0).files[0].size;
                    if (fileSize > maxFileSize) {
                        $('#modalTooBigFile').modal('show');
                        $(this).val('');
                        return;
                    }

                }

                var newName = "";
                for (var i = 0, ln = ctrlName.length - 1; i < ln; i++) {
                    newName += (i === 0 ? '' : ' ') + ctrlName[i];
                }
                var newName = newName.replace(/_/g, ' ');
                $('#newProjet').val((newName[0].toUpperCase() + newName.substring(1)).trim());
            }
        }
    });

    $('#SeanceDocument').change(function () {
        //verification de l'extension pdf
        var maxFileSize = "<?php echo ini_get('upload_max_filesize'); ?>".slice(0, -1) * 1024 *1024;

        //alert(maxFileSize);
        var ctrlName = $(this).val().split('.');
        if (ctrlName[ctrlName.length - 1] != 'pdf') {
            $('#modalBadFile').modal('show');
            $(this).val('');

        }
        else if ($(this).get(0).files.length > 0) { // only if a file is selected
            var fileSize = $(this).get(0).files[0].size;
            if (fileSize > maxFileSize) {
                $('#modalTooBigFile').modal('show');
                $(this).val('');
            }

        }
    });
</script>