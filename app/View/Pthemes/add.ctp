<?php
/**
 * Pthemes/add View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Pthemes/add.ctp $
 * $Id: add.ctp 302 2013-10-21 15:57:34Z ssampaio $
 */
?>
<div class="pthemes form">
	<?php echo $this->Form->create('Ptheme'); ?>
	<fieldset>
		<legend><?php echo __d('ptheme', 'Ptheme.add'); ?></legend>
		<?php
		echo $this->Form->input('name');
		echo $this->Form->input('parent_id', array('options' => $parentPthemes, 'empty' => true));
		echo $this->Form->input('rank', array('label' => __d('ptheme', 'Ptheme.rank')));
                
		?>
	</fieldset>
	<?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

