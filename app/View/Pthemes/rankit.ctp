<div class="span1"></div>
<div class="span9">
    <?php



    echo $this->Base->create('Ptheme', array());
    ?>




    <table class="table table-hover table-bordered table-striped">
        <tr>
            <th class="span9">Intitulé</th>
            <th>Rang</th>
        </tr>


        <?php foreach ($themes as $key => $theme) { ?>

            <?php
            echo $this->Base->input('Ptheme.' . $key . '.id', array(
                'value' => $theme['Ptheme']['id']
            ));
            ?>
            <tr><td>

                    <?php
                    echo '<b>' . $theme['Ptheme']['name'] . '</b>'
                    ?>


                </td><td>

                    <?php
                    echo $this->Base->input('Ptheme.' . $key . '.rank', array(
                        'label' => false,
                        'value' => $theme['Ptheme']['rank']
                    ));
                    ?>
                </td></tr>

        <?php }
        ?>
    </table>


    <div class="pull-right">
        <a href='/Pthemes/index' class='btn btn-inverse'>
            <span class="fa fa-times-circle"></span> Annuler
        </a>

        <button type="submit" class="btn btn-success"> <span class="fa fa-save"></span> Enregistrer </button>

    </div>

</div>

