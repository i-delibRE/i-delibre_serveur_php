<?php
/**
 * Pthemes/index View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-07-23 13:58:59 +0200 (mer. 23 juil. 2014) $
 * $Revision: 527 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Pthemes/index.ctp $
 * $Id: index.ctp 527 2014-07-23 11:58:59Z ssampaio $
 */

echo $this->Html->css('pthemes');

?>


<div class="<?php echo $controller; ?> <?php echo $action; ?>">
	<h2 name="top"><?php echo $title; ?></h2>

	<div class="table-header">
		<div class='control-group'>
			<a href='<?php echo $addurl; ?>' class='btn btn-inverse'>
				<i class='icon-plus-sign icon-white'></i> <?php echo $addtitle; ?>
			</a>
                    <a href='/Pthemes/rankit' class='btn btn-inverse'>
				<i class='icon-resize-vertical icon-white'></i> <?php echo __d('ptheme', 'Ptheme.rank'); ?>
			</a>
			<span class="pull-right">
				<span class="btn btn-more"><i class="icon-resize-full"></i> <?php echo __d('default', 'btn.table.td.more'); ?></span>
				<span class="btn btn-less"><i class="icon-resize-small"></i> <?php echo __d('default', 'btn.table.td.less'); ?></span>
			</span>
		</div>
	</div>

	<div class="table-container">
		<table class='table table-hover table-bordered'>
			<tr>
				<?php
				foreach ($fields as $field => $conf) {
					if (($field == 'id' && Configure::read('debug') > 0) || $field != 'id') {
						echo $this->Html->tag('th', $conf['label'], array('class' => !empty($conf['class']) ? $conf['class'] : array()));
					}
				}
				?>
                         
				<th class="actions"><?php echo __('Rang'); ?>  </th>
				<th class="actions"><?php echo __('Actions'); ?>  </th>
                                
			</tr>
			<?php
			foreach ($data as $item):
                            
				?>
				<tr>
					<?php
					foreach ($fields as $field => $conf) {
						if (($field == 'id' && Configure::read('debug') > 0) || $field != 'id') {
							echo $this->Html->tag('td', preg_replace("/_/", " <span class='submenu'>&#746;</span> ", $item[$model][$field]), array('class' => !empty($conf['class']) ? $conf['class'] : array()));
						}
					}
					?>
                                        
                                    <td class ="actions">
                                       <?php echo $item['Ptheme']['rank'] 
                                       
                                       ?> 
                                    </td>
                                    
					<td class="actions">
						<a
							data-toggle="tooltip"
							data-original-title="<?php echo __d('default', 'btn.edit'); ?>"
							class='tooltiped btn btn-inverse'
							href='<?php echo Router::url(array('action' => 'edit', $item[$model]['id'])); ?>'>
							<i class='fa fa-pencil icon-white'></i>
						</a>
						<?php
						echo $this->BootstrapBox->modalBtn('deleteModal', __d('default', 'btn.delete'), $this->Html->tag('i', '', array('class' => 'icon-trash icon-white')), 'btn-danger', Router::url(array('action' => 'delete', $item[$model]['id'])));
						?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<div class="table-footer">
	</div>
</div>

<a data-toggle="tooltip" data-placement="right" data-original-title="<?php echo __d('default', 'btn.gotop'); ?>" class='goTop' href="#top"><span class="triangle-up"></span></a>

<?php
$modalOptions = array(
	'id' => 'deleteModal',
	'title' => __d(Inflector::underscore($model), $model . '.delete.confirm.title'),
	'message' => __d(Inflector::underscore($model), $model . '.delete.confirm.message') . $this->Html->tag('div', $this->Html->tag('i', '', array('class' => array('icon', 'icon-warning-sign'))) . ' ' . __d(Inflector::underscore($model), $model . '.delete.confirm.message.tree'), array('class' => array('alert', 'alert-danger'))),
	'btn.ok' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-trash', 'icon-white'))) . ' ' . __d('default', 'btn.delete'),
	'btn.ok.url' => $deleteUrl,
	'btn.cancel' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-remove-sign'))) . ' ' . __d('default', 'btn.cancel')
);
echo $this->BootstrapBox->modalBox($modalOptions);
?>

<script type='text/javascript'>
	$('.goTop').hide();
	$(window).scroll(function() {
		if ($(window).scrollTop() > 0) {
			$('.goTop').show();
		} else {
			$('.goTop').hide();
		}
	});

	if ($("body").height() > $(window).height()) {
		alert("Vertical Scrollbar! D:");
	}


	function showmore() {
		$('.uuid, .datetime').addClass('show');
		$('.btn-more').hide();
		$('.btn-less').show();

		$('.pagination a').each(function() {
			if (typeof $(this).attr('href') != 'undefined' && $(this).attr('href') != '' && $(this).attr('href').search('/show:more') == -1) {
				$(this).attr('href', $(this).attr('href') + '/show:more');
			}
		});

	}

	function showless() {
		$('.uuid, .datetime').removeClass('show');
		$('.btn-less').hide();
		$('.btn-more').show();


		$('.pagination a').each(function() {
			if (typeof $(this).attr('href') != 'undefined' && $(this).attr('href') != '') {
				$(this).attr('href', $(this).attr('href').replace('/show:more', ''));
			}
		});
	}

<?php if (isset($this->params['named']) && isset($this->params['named']['show']) && ($this->params['named']['show'] == "more")) { ?>
		showmore();
<?php } else { ?>
		showless();
<?php } ?>

	$('.btn-more').click(function() {
		showmore();
	});
	$('.btn-less').click(function() {
		showless();
	});

	$('.tooltiped').tooltip();

	$('.goTop').tooltip({
		delay: {show: 300, hide: 0},
		container: $('.goTop')
	});
</script>