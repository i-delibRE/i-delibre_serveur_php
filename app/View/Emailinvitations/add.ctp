<div>

    <?php
    echo $this->Form->create('Emailinvitation');
    ?>

    <fieldset>
        <legend>Ajouter un email d'invitation</legend>

    <?php
    echo $this->Form->input('Emailinvitation.name', array(
        'label' => "Intitulé",
        'style' => 'width:100%;',
        'required' => true));
    ?>


    <?php
    echo $this->Form->input('Emailinvitation.sujet', array(
        'label' => 'Sujet',
        'style' => 'width:100%;',
        'required' => true));
    ?>

    <?php
    echo $this->Form->input('Emailinvitation.content', array(
        'style' => 'width:100%; height:200px;',
        "type" =>"textarea",
        'class' => 'form-control',
        'label' => 'Contenu',
        'required' => true));
    ?>


    <?php
    echo $this->Form->input('Type.id', array("label" => "Type",  "options" => $typeList , 'required' => true,'style' => 'width:100%;'));
    ?>

    </fieldset>

    <?php echo $this->Form->end(array('editMode' => true)); ?>


    <!--    <div>-->
<!--        <button type="submit" class="btn btn-success pull-right" title="Enregistrer"><span class="fa fa-floppy-o"></span> Enregistrer</button>-->
<!--    </div>-->
</div>

<br>

<div class="span3"></div>

<div class="span6 well">
    date de la séance : #dateseance# <br>
    heure de la séance : #heureseance# <br>
    lieu de la seance : #lieuseance# <br>
    type de la séance : #typeseance# <br>
    prénom de l'élu : #prenom# <br>
    nom de lélu : #nom# <br>
</div>
