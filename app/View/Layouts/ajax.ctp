<?php

/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
$this->Html->loadConfig('html5_tags');
echo $this->fetch('content');
?>
