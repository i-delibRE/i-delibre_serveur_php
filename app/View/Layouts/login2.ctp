<?php
/**
 * login Layout
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-11-21 17:52:55 +0100 (jeu. 21 nov. 2013) $
 * $Revision: 327 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Layouts/login.ctp $
 * $Id: login.ctp 327 2013-11-21 16:52:55Z ssampaio $
 *
 */
$this->Html->loadConfig('html5_tags');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>

		<?php
//		echo $this->Html->meta('icon');
//		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>

		<meta charset="utf-8">
		<title><?php echo MAINTITLE; ?></title>
		<meta name="description" content="<?php echo MAINTITLE; ?>">
		<meta name="author" content="Stéphane Sampaio - Adullact Projet">

		<link rel="shortcut icon" href="/ico/favicon.png">
		<link rel="apple-touch-icon" href="/ico/favicon.png"/>
		<link rel="apple-touch-icon-precomposed" href="/ico/favicon.png" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-touch-fullscreen" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta http-equiv="X-UA-Compatible" content="chrome=IE8">
		<link rel="apple-touch-startup-image" href="/ico/favicon.png" />

		<link rel="stylesheet" href="/vendors/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/vendors/bootstrap/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="/vendors/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/vendors/fullcalendar/fullcalendar.css" />
		<link rel="stylesheet" href="/css/main.css" />
		<link rel="stylesheet" href="/css/login.css" />

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--<script src="/jquery/jquery-1.8.3.min.js"></script>-->
		<script src="/vendors/jquery/jquery-2.0.0.min.js"></script>
		<script src="/vendors/jquery/jquery-migrate-1.1.1.min.js"></script>
		<script src="/vendors/bootstrap/js/bootstrap.min.js"></script>
		<script src="/vendors/fullcalendar/fullcalendar.min.js"></script>
		<script src="/js/jquery_override.js"></script>
		<script src="/js/dateHelper.js"></script>
		<script src="/js/main.js"></script>
	</head>

	<body>
		<div class="navbar navbar-inverse navbar-fixed-top app-ui-header">
			<div class="navbar-inner">
				<div class="container">
					<a class="brand" href="/"><img src="/img/logo-idelibre_inv.png" alt="Logo i-delibRE" title="i-delibRE" /></a>
					<div class="pull-left">
						<ul class="app-ui-nav-info">
							<li class="visible-desktop visible-tablet visible-phone">
								<i class="icon-globe icon-white"></i>
								<?php echo CakeSession::read('ctxTitle'); ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="app-ui-container">
				<div class="header"></div>
				<div class="content">
					<div id="nojs" class="alert alert-error">
						Votre navigateur ne prends pas en charge JavaScript.
					</div>
					<?php echo $this->Session->flash(); ?>
					<?php echo $this->fetch('content'); ?>
				</div>
				<div class="footer"></div>
			</div>
		</div>

		<div id="footer" class="app-ui-footer">
			<span id="versionInfo"><span class="bold">Version : </span><?php echo IDELIBREVERSION; ?></span>
			<span id="toolboxOpenBtn" class="btn btn-inverse"><i class="icon-chevron-up icon-white"></i></span>
			<span id="toolbox"></span>
		</div>


		<?php if (Configure::read('debug') > 0) { ?>
			<script src="/js/debugTools.js"></script>
		<?php } ?>

	</body>
</html>
