<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
?>
<?php echo $this->fetch('content');?>

This email was sent using the CakePHP Framework, http://cakephp.org.
