<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
$cakeDescription = 'i-delibRE';
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        <?php echo $cakeDescription ?>
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/vendors/jquery/jquery-2.0.0.min.js"></script>
    <script src="/vendors/jquery/jquery-migrate-1.1.1.min.js"></script>

    <?php
//    echo $this->Html->script('jquery-1.11.0.min');
    echo $this->Html->script('bootstrap.min.js');

    echo $this->Html->css('bootstrap.min.css');
    echo $this->Html->css('font-awesome-4.7.0/css/font-awesome.min.css');

    ?>



    <style>

        /*noinspection CssOptimizeSimilarProperties*/
        .libriciel-background {
            height: calc(101vh - (2 * 51px));
            background-color: #E0EFFA;
            background-image: linear-gradient(-1.2217rad, rgba(255, 255, 255, 0.5) 10%, transparent 10%),
            linear-gradient(1.2217rad, transparent 85%, rgba(255, 255, 255, 0.5) 85%),
            linear-gradient(-0.6981rad, rgba(255, 255, 255, 0.5) 15%, transparent 15%),
            linear-gradient(1.2217rad, rgba(255, 255, 255, 0.5) 20%, transparent 20%),
            linear-gradient(-0.7853rad, rgba(66, 161, 227, 0.08) 30%, transparent 30%),
            linear-gradient(0.6981rad, rgba(217, 236, 250, 0.5) 40%, transparent 40%),
            linear-gradient(1.3962rad, transparent 65%, rgba(255, 255, 255, 0.31) 65%),
            linear-gradient(-1.0471rad, rgba(217, 236, 250, 0.5) 50%, transparent 50%),
            linear-gradient(1.0471rad, rgba(255, 255, 255, 0.5) 60%, transparent 60%),
            linear-gradient(-1.3962rad, rgba(255, 255, 255, 0.5) 70%, transparent 70%),
            linear-gradient(1.3089rad, rgba(142, 199, 241, 0.5) 80%, transparent 80%),
            linear-gradient(-1.3089rad, rgba(58, 139, 215, 0.55) 90%, transparent 90%),
            linear-gradient(-1.3962rad, rgba(255, 255, 255, 0.5) 65%, transparent 70%);
            display: flex;
            align-items: center;
            margin-top: 51px;
        }

        /* Form */

        .login-block {
            border: 1px solid #080808;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
            background-color: white;
            display: flex;
            padding: 0;
            max-width: 576px;
        }

        .collectivity-block {
            background-color: #173A94;
            border-right: solid #080808 1px;
            padding: 24px;
            display: flex;
            align-items: center;
        }

        .collectivity-logo {
            max-height: 100%;
            max-width: 96px;
        }

        .form-block {
            width: 100%;
            padding: 24px 60px 48px 60px;
        }

        .main-logo {
            max-height: 96px;
            max-width: 100%;
        }

        .control-label.normal-left {
            text-align: left;
            font-weight: normal;
            padding-left: 0;
        }

        .color-inverse {
            background-color: #222;
            color: white;
        }

        .buffer-top {
            margin-top: 12px;
        }

        .double-buffer-top {
            margin-top: 24px;
        }

        /* /Form */

        /* Navbar */

        .navbar-font-buffer-top {
            padding-top: 12px;
        }

        .navbar-icon-selected {
            height: 34px;
        }

        .navbar-icon-reduced {
            opacity: 0.5;
            height: 29px;
        }

        .navbar-icons-buffer-top {
            padding-top: 9px;
        }

        .navbar-text-buffer-top {
            padding-top: 6px;
        }

        .navbar-font-reduced {
            font-size: 22px;
        }

        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        /* /Navbar */

    </style>
</head>

<body>
<!-- Header -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" style="padding-top: 14px" href="#" title="<?php echo $cakeDescription; ?>">
                <img src="/img/i-delibRE_white_h24px.png" alt="Logo i-delibRE" title="i-delibRE" />
            </a>
        </div>
    </div>
</nav>

<!-- Content -->
<div class="libriciel-background">
    <div class="col-xs-10
                        col-sm-7
                        col-md-6
                        col-lg-5
                        center-block login-block">

        <?php
        //        echo $this->Session->flash();
        echo $this->fetch('content');
        ?>
    </div>
</div>

<!-- Footer -->
<footer>
    <div class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container-fluid">
                    <span class="hidden-xs
                                 hidden-sm
                                 col-md-4
                                 col-lg-4
                                 navbar-left"
                          style="padding-left: 16px;">

                        <a data-toggle="tooltip"
                           title="as@lae"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/asalae"
                           target="_blank">
                            <?php
                            echo $this->Html->image('reduced_asalae.png');
                            ?>
                        </a>

                        <a data-toggle="tooltip"
                           title="i-delibRE"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/i-delibre"
                           target="_blank">
                            <?php
                            echo $this->Html->image('i-delibRE_white_h24px.png');
                            ?>
                        </a>

                        <a data-toggle="tooltip"
                           title="i-Parapheur"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/i-parapheur"
                           target="_blank">
                            <?php
                            echo $this->Html->image('reduced_iParapheur.png');
                            ?>
                        </a>

                        <a data-toggle="tooltip"
                           title="Pastell"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/pastell"
                           target="_blank">
                            <?php
                            echo $this->Html->image('reduced_Pastell.png');
                            ?>
                        </a>

                        <a data-toggle="tooltip"
                           title="S²LOW"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/s2low"
                           target="_blank">
                            <?php
                            echo $this->Html->image('reduced_S2low.png');
                            ?>
                        </a>

                        <a data-toggle="tooltip"
                           title="web-CIL"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/web-cil"
                           target="_blank">
                            <?php
                            echo $this->Html->image('reduced_WebCil.png');
                            ?>
                        </a>

                        <a data-toggle="tooltip"
                           title="web-delib"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/web-delib"
                           target="_blank">
                            <?php
                            echo $this->Html->image('reduced_WebDelib.png');
                            ?>
                        </a>

                        <a data-toggle="tooltip"
                           title="web-GFC"
                           class="navbar-font-buffer-top vcenter"
                           href="https://www.libriciel.fr/web-gfc"
                           target="_blank">
                            <?php
                            echo $this->Html->image('reduced_WebGFC.png');
                            ?>
                        </a>
                    </span>

            <span class="col-xs-12
                        hidden-sm
                        col-md-4
                        col-lg-4
                        text-center h5 text-muted navbar-text-buffer-top">
                        <?php
                        echo $cakeDescription . ' ' . VERSION;

                        ?>
                <span class="hidden-xs">
                            <?php
                            echo " / &copy; Libriciel SCOP 2006-2017 ";
                            ?>
                        </span>
                    </span>

            <span class="hidden-xs
                        col-sm-6
                        hidden-md
                        hidden-lg
                        h5 text-muted navbar-text-buffer-top">
                        <?php
                        echo $cakeDescription . ' ' . VERSION . " / &copy; Libriciel SCOP 2006-2017 ";
                        ?>
                    </span>

            <span class="hidden-xs
                        col-sm-6
                        col-md-4
                        col-lg-4
                        navbar-right navbar-icons-buffer-top"
                  style="padding-right: 32px;">
                        <a href="https://www.libriciel.fr"
                           target="_blank"
                           class="pull-right">
                            <?php
                            echo $this->Html->image('Libriciel_white_h24px.png');
                            ?>
                        </a>
                    </span>
        </div>
    </div>
</footer>

</body>
</html>


















<!---->
<!---->
<!---->
<?php
///**
// * login Layout
// *
// * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
// *
// * PHP version 5
// * @author Stéphane Sampaio
// * @copyright Adullact Projet
// * @link http://adullact.org/
// * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
// * @encoding UTF-8
// *
// * SVN Informations
// * $Date: 2013-11-21 17:52:55 +0100 (jeu. 21 nov. 2013) $
// * $Revision: 327 $
// * $Author: ssampaio $
// * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Layouts/login.ctp $
// * $Id: login.ctp 327 2013-11-21 16:52:55Z ssampaio $
// *
// */
//$this->Html->loadConfig('html5_tags');
//?>
<!--<!DOCTYPE html>-->
<!--<html lang="fr">-->
<!--	<head>-->
<!---->
<!--		--><?php
////		echo $this->Html->meta('icon');
////		echo $this->Html->css('cake.generic');
//
//		echo $this->fetch('meta');
//		echo $this->fetch('css');
//		echo $this->fetch('script');
//		?>
<!---->
<!--		<meta charset="utf-8">-->
<!--		<title>--><?php //echo MAINTITLE; ?><!--</title>-->
<!--		<meta name="description" content="--><?php //echo MAINTITLE; ?><!--">-->
<!--		<meta name="author" content="Stéphane Sampaio - Adullact Projet">-->
<!---->
<!--		<link rel="shortcut icon" href="/ico/favicon.png">-->
<!--		<link rel="apple-touch-icon" href="/ico/favicon.png"/>-->
<!--		<link rel="apple-touch-icon-precomposed" href="/ico/favicon.png" />-->
<!--		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" />-->
<!--		<meta name="apple-mobile-web-app-capable" content="yes" />-->
<!--		<meta name="apple-touch-fullscreen" content="yes">-->
<!--		<meta name="apple-mobile-web-app-status-bar-style" content="black" />-->
<!--		<meta http-equiv="X-UA-Compatible" content="chrome=IE8">-->
<!--		<link rel="apple-touch-startup-image" href="/ico/favicon.png" />-->
<!---->
<!--		<link rel="stylesheet" href="/vendors/bootstrap/css/bootstrap.min.css" />-->
<!--		<link rel="stylesheet" href="/vendors/bootstrap/css/bootstrap-responsive.min.css" />-->
<!--		<link rel="stylesheet" href="/vendors/font-awesome/css/font-awesome.min.css" />-->
<!--		<link rel="stylesheet" href="/vendors/fullcalendar/fullcalendar.css" />-->
<!--		<link rel="stylesheet" href="/css/main.css" />-->
<!--		<link rel="stylesheet" href="/css/login.css" />-->
<!---->
<!--		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->-->
<!--		<!--[if lt IE 9]>-->
<!--		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>-->
<!--		<![endif]-->-->
<!--		<!--<script src="/jquery/jquery-1.8.3.min.js"></script>-->-->
<!--		<script src="/vendors/jquery/jquery-2.0.0.min.js"></script>-->
<!--		<script src="/vendors/jquery/jquery-migrate-1.1.1.min.js"></script>-->
<!--		<script src="/vendors/bootstrap/js/bootstrap.min.js"></script>-->
<!--		<script src="/vendors/fullcalendar/fullcalendar.min.js"></script>-->
<!--		<script src="/js/jquery_override.js"></script>-->
<!--		<script src="/js/dateHelper.js"></script>-->
<!--		<script src="/js/main.js"></script>-->
<!--	</head>-->
<!---->
<!--	<body>-->
<!--		<div class="navbar navbar-inverse navbar-fixed-top app-ui-header">-->
<!--			<div class="navbar-inner">-->
<!--				<div class="container">-->
<!--					<a class="brand" href="/"><img src="/img/logo-idelibre_inv.png" alt="Logo i-delibRE" title="i-delibRE" /></a>-->
<!--					<div class="pull-left">-->
<!--						<ul class="app-ui-nav-info">-->
<!--							<li class="visible-desktop visible-tablet visible-phone">-->
<!--								<i class="icon-globe icon-white"></i>-->
<!--								--><?php //echo CakeSession::read('ctxTitle'); ?>
<!--							</li>-->
<!--						</ul>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!---->
<!--		<div class="container">-->
<!--			<div class="app-ui-container">-->
<!--				<div class="header"></div>-->
<!--				<div class="content">-->
<!--					<div id="nojs" class="alert alert-error">-->
<!--						Votre navigateur ne prends pas en charge JavaScript.-->
<!--					</div>-->
<!--					--><?php //echo $this->Session->flash(); ?>
<!--					--><?php //echo $this->fetch('content'); ?>
<!--				</div>-->
<!--				<div class="footer"></div>-->
<!--			</div>-->
<!--		</div>-->
<!---->
<!--		<div id="footer" class="app-ui-footer">-->
<!--			<span id="versionInfo"><span class="bold">Version : </span>--><?php //echo IDELIBREVERSION; ?><!--</span>-->
<!--			<span id="toolboxOpenBtn" class="btn btn-inverse"><i class="icon-chevron-up icon-white"></i></span>-->
<!--			<span id="toolbox"></span>-->
<!--		</div>-->
<!---->
<!---->
<!--		--><?php //if (Configure::read('debug') > 0) { ?>
<!--			<script src="/js/debugTools.js"></script>-->
<!--		--><?php //} ?>
<!---->
<!--	</body>-->
<!--</html>-->
