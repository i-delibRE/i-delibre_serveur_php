<?php
/**
 *
 * Srvusers/mdplost View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Srvusers/mdplost.ctp $
 * $Id: mdplost.ctp 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
?>



<div class="form-block">

    <?php echo $this->Session->flash(); ?>
    <div class="col-md-12 text-center" style="margin-top: 24px; margin-bottom: 24px;">
        <?php


        echo $this->Html->image('i-delibRE_color_h24px.png', [
            'class' => 'row center-block main-logo'
        ]);
        ?>

        <div class="row buffer-top text-center">
            En saisissant votre identifiant et votre adresse mail, à l'enregistrement, votre ancien mot de passe sera supprimé et un nouveau sera envoyé à l'adresse électronique renseignée.
        </div>

        <div class="row double-buffer-top">
            <?php
            echo $this->Form->create();
            ?>

            <div class="form-horizontal form-group">
                <label for="username"
                       class="col-md-4 control-label normal-left">
                    Identifiant *
                </label>

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-login">
                        <i class="fa fa-user fa-fw"
                           aria-hidden="true">
                        </i>
                    </span>
                    <?php
                    echo $this->Form->input('Srvuser.username', [
                        'class' => 'form-control',
                        'id' => 'username',
                        'aria-describedby' => 'sizing-addon-login',
                        'label' => false,
                        'require' => true
                    ]);
                    ?>
                </div>
            </div>



            <div class="form-horizontal form-group">
                <label for="password"
                       class="col-md-4 control-label normal-left">
                    E-mail *
                </label>

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-password">
                        <i class="fa fa-envelope fa-fw"
                           aria-hidden="true">
                        </i>
                    </span>
                    <?php
                    echo $this->Form->input('Srvuser.mail', [
                        'class' => 'form-control',
                        'id' => 'email',
                        'aria-describedby' => 'sizing-addon-password',
                        'label' => false,
                        'require' => true
                    ]);
                    ?>
                </div>
            </div>





            <div class="row form-horizontal form-group buffer-top pull-right" style="margin-top: 24px;">
                <?php
                echo $this->Form->button('<i class="fa fa-paper-plane fa-fw"> </i> Envoyer', [
                    'type' => 'submit',
                    'class' => 'btn btn-primary'
                ]);
                ?>


                <a href="/srvusers/login" class="btn btn-danger"><span class="fa fa-times-circle"></span> Annuler</a>
            </div>






            <?php
            echo $this->Form->end();
            ?>



        </div>

    </div>
</div>
















<!---->
<!---->
<!--<h1 class="homeTitle">-->
<!--	--><?php //echo __d('mdplost', 'Mdplost.lost'); ?>
<!--</h1>-->
<!---->
<!--<div class="hero-unit">-->
<!--	--><?php
//	echo __d('mdplost', 'Mdplost.beforesave');
//	echo $this->Html->tag('br');
//	echo $this->Html->tag('br');
//	echo $this->Form->create();
//	echo $this->Form->input('Srvuser.username', array('label' => __d('user', 'User.username')));
//	echo $this->Form->input('Srvuser.mail', array('label' => __d('mdplost', 'Srvuser.mail'), 'required' => true, 'type' => 'email'));
//	if (!empty($from) && $from == "client") {
//		echo $this->Form->input('from_client', array('type' => 'hidden', 'value' => '1'));
//	}
//
//	$strCtrl = $this->Html->tag('button', __d('login', 'login.submit'), array('class' => array('btn', 'btn-inverse'), 'type' => 'submit', 'div' => false, 'label' => false));
//	if (!empty($from) && $from == "client") {
//		$strCtrl .= $this->Html->tag('span', __d('default', 'btn.cancel'), array('class' => array('btn', 'btn-danger', 'from_client')));
//	} else {
//		$strCtrl .= $this->Html->link(__d('default', 'btn.cancel'), array('action' => 'login'), array('class' => array('btn', 'btn-danger')));
//	}
//
//	echo $this->Html->tag('div', $strCtrl, array('class' => 'btn-group'));
//
//	echo $this->Form->end();
//	?>
<!--</div>-->
<!---->
<!---->
<?php //if (!empty($from) && $from == "client") { ?>
<!--	<script type="text/javascript">-->
<!--		$('.from_client').click(function() {-->
<!--			window.location.href = '/idelibre_client';-->
<!--		});-->
<!--	</script>-->
<?php //} ?>
