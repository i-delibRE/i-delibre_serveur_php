<?php
/**
 *
 * Srvusers/edit View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-08-11 15:26:57 +0200 (lun. 11 août 2014) $
 * $Revision: 553 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Srvusers/edit.ctp $
 * $Id: edit.ctp 553 2014-08-11 13:26:57Z ssampaio $
 *
 */
echo $this->Html->css('srvusers');

//debug($srvroles);
//debug($srvuserColls);
//debug($activeUsersByColls);
//debug($mandats);



?>
<div class="srvusers form">
    <?php echo $this->Form->create('Srvuser'); ?>
    <fieldset>
        <legend><?php echo __d('srvuser', 'Srvuser.edit'); ?></legend>
        <?php
        echo $this->Form->input('id');
        
        echo $this->Form->input('firstname',array('disabled' => 'true'));
        echo $this->Form->input('firstname',array('type' => 'hidden'));
      
        echo $this->Form->input('lastname', array('disabled' => 'true') );
        echo $this->Form->input('lastname', array('type' => 'hidden') );
        
        echo $this->Form->input('srvrole_id', array('disabled' => 'true'));
        echo $this->Form->input('srvrole_id', array('type' => 'hidden'));
        
        echo $this->Form->input('mail');
        echo $this->Html->tag('hr');
        echo $this->Form->input('username' , array('disabled' => 'true'));
        echo $this->Form->input('username' , array('type' => 'hidden'));
        
        echo $this->Form->input('srvrole_id', array('disabled' => 'true'));
        echo $this->Form->input('srvrole_id', array('type' => 'hidden'));
        ?>
        <div id="group_user_infos">
            <?php
//            echo $this->Form->input('mandat_colls', array('type' => 'select', 'options' => $usersByColls, 'multiple' => 'checkbox'));
            echo $this->Form->input('createForColl', array('label' => __d('srvuser', 'Srvuser.allowed_colls.addUser'), 'type' => 'select', 'options' => $srvuserColls, 'multiple' => 'checkbox'));
            ?>
        </div>
<!--        <div id="group_wm_infos">-->
<!--            --><?php //echo $this->Form->input('Collectivite.Collectivite', array('label' => __d('srvuser', 'Srvuser.allowed_colls'), 'type' => 'select', 'options' => $srvuserColls, 'multiple' => 'checkbox')); ?>
<!--        </div>-->
        <?php
        echo $this->Html->tag('hr');
        echo $this->Form->input('newpass', array('type' => 'password', 'label' => __d('srvuser', 'Srvuser.password.new'), 'value' => '', 'required' => false, 'autocomplete' => 'off'));
        echo $this->Form->input('confirm', array('type' => 'password', 'autocomplete' => 'off'));
        ?>
    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

<?php
$settings = array(
    'id' => 'modalNotMatch',
    'title' => __d('srvuser', 'Srvuser.password.notmatch.title'),
    'message' => __d('srvuser', 'Srvuser.password.notmatch.message')
);
echo $this->BootstrapBox->msgBox($settings);
?>


<script type="text/javascript">
    var confirmOk = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-ok').addClass('icon-white')).addClass('label').addClass('label-success');
    var confirmNok = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-remove').addClass('icon-white')).addClass('label').addClass('label-important');

    function checkPassConfirm() {
        if ($('#SrvuserNewpass').val() !== $('#SrvuserConfirm').val() || $('#SrvuserNewpass').val() === '' || $('#SrvuserConfirm').val() === '') {
            $('#SrvuserConfirmInfo').empty().append(confirmNok);
        } else {
            $('#SrvuserConfirmInfo').empty().append(confirmOk);
        }
    }

    function showHideRoleUserInfo() {
        if ($('#SrvuserSrvroleId option:selected').val() === '<?php echo IDELIBRE_ADMIN_GROUP_USER_ID; ?>') {
            $('#group_user_infos').show();
        } else {
            $('#group_user_infos').hide();
        }


        if ($('#SrvuserSrvroleId option:selected').val() === '<?php echo IDELIBRE_ADMIN_GROUP_WALLETMANAGER_ID; ?>') {
            $('#group_wm_infos').show();
        } else {
            $('#group_wm_infos').hide();
        }
    }

    $('#SrvuserSrvroleId').change(function() {
        showHideRoleUserInfo();
    });
    showHideRoleUserInfo();

    $('#SrvuserConfirm').after($('<span></span>').attr('id', 'SrvuserConfirmInfo').css('margin-left', '5px')).on('keyup', function() {
        checkPassConfirm();
    });

    $('#SrvuserNewpass').on('keyup', function() {
        checkPassConfirm();
    });

    $('#SrvuserEditForm').on('submit', function(e) {
        if ($('#SrvuserNewpass').val() !== '' && $('#SrvuserNewpass').val() !== $('#SrvuserConfirm').val()) {
            e.preventDefault();
            e.stopPropagation();
            $('#modalNotMatch').modal('show');
            $('#SrvuserNewpass').focus();
        }
    });

    $('.controls').each(function() {
        if ($('div.input-xlarge', this).length > 0) {
            $(this).addClass('chkList');
        }
    })

    function switchDiv(checkbox, control) {
        if ($(checkbox).attr('checked') === "checked") {
            $(control).addClass('selected');
        } else {
            $(control).removeClass('selected');
        }
    }

    $('.controls div.input-xlarge input[type="checkbox"]').bind('change', function() {
        switchDiv(this, $(this).parent());
    });

    $('.controls div.input-xlarge').each(function() {
        switchDiv($('input[type="checkbox"]', $(this)), $(this));
    }).bind('click', function(e) {
        if ($(e.target).hasClass('input-xlarge')) {
            $('input[type="checkbox"]', $(e.target)).trigger('click');
        }
    });

    var mandats = new Array();
<?php foreach ($mandats as $mandat) { ?>
            mandats.push("<?php echo $mandat['Mandat']['collectivite_id']/* . '=' . $mandat['Mandat']['user_id'] */; ?>");
//        mandats.push("<?php echo $mandat['Mandat']['collectivite_id'] . '=' . $mandat['Mandat']['user_id']; ?>");
<?php } ?>


    for (var i = 0, ln = mandats.length; i < ln; i++) {
        $('.chkList .input-xlarge input').each(function() {
            if ($(this).val() === mandats[i]) {
                $(this).trigger('click');
            }
        });
    }


<?php
foreach ($activeUsersByColls as $coll => $user) {
    foreach ($user as $info => $active) {
        if (!$active) {
            ?>
                $('.chkList .input-xlarge input').each(function() {
                    if ($(this).val() === "<?php echo $info; ?>") {
                        $(this).parent().addClass('inactive');
                    }
                });
            <?php
        }
    }
}
?>

</script>
