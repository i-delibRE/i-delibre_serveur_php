<?php
/**
 *
 * Srvusers/add View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-07-30 12:00:41 +0200 (mer. 30 juil. 2014) $
 * $Revision: 542 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Srvusers/add.ctp $
 * $Id: add.ctp 542 2014-07-30 10:00:41Z ssampaio $
 *
 */
echo $this->Html->css('srvusers');
//debug($srvroles);
//debug($srvuserColls);
?>
<div class="srvusers form">
    <?php echo $this->Form->create('Srvuser', array('class' => 'form-horizontal')); ?>
    <fieldset>
        <legend><?php echo __d('srvuser', 'Srvuser.add'); ?></legend>
        <?php
        echo $this->Form->input('firstname');
        echo $this->Form->input('lastname');
        echo $this->Form->input('mail');
        echo $this->Html->tag('hr');
        echo $this->Form->input('username');
        echo $this->Form->input('srvrole_id');
        ?><div id="group_user_infos"><?php
        echo $this->Form->input('createForColl', array('label' => __d('srvuser', 'Srvuser.allowed_colls.addUser'), 'type' => 'select', 'options' => $srvuserColls, 'multiple' => 'checkbox'));
        ?></div><?php
        ?><div id="group_wm_infos"><?php
        echo $this->Form->input('Collectivite.Collectivite', array('label' => __d('srvuser', 'Srvuser.allowed_colls'), 'type' => 'select', 'options' => $srvuserColls, 'multiple' => 'checkbox'));
        ?></div>


        <div id="group_admin_collectivite">
            <?php
            echo $this->Form->input('CollAdmin.collId', array(
                'label' => __d('srvuser', 'Srvuser.choice_colls'),
                'type' => 'select',
                'options' => $srvuserColls,
            ));
            ?>
        </div>


        <?php
        echo $this->Html->tag('hr');
        echo $this->Form->input('password', array('autocomplete' => 'off'));
        echo $this->Form->input('confirm', array('type' => 'password', 'required' => true, 'autocomplete' => 'off'));
        ?>

    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

<?php
$settings = array(
    'id' => 'modalNotMatch',
    'title' => __d('srvuser', 'Srvuser.password.notmatch.title'),
    'message' => __d('srvuser', 'Srvuser.password.notmatch.message')
);
echo $this->BootstrapBox->msgBox($settings);
?>
<script type="text/javascript">
    var confirmOk = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-ok').addClass('icon-white')).addClass('label').addClass('label-success');
    var confirmNok = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-remove').addClass('icon-white')).addClass('label').addClass('label-important');

    function checkPassConfirm() {
        if ($('#SrvuserPassword').val() !== $('#SrvuserConfirm').val() || $('#SrvuserPassword').val() === '' || $('SrvuserConfirm').val() === '') {
            $('#SrvuserConfirmInfo').empty().append(confirmNok);
        } else {
            $('#SrvuserConfirmInfo').empty().append(confirmOk);
        }
    }

    function showHideRoleUserInfo() {
        if ($('#SrvuserSrvroleId option:selected').val() === '<?php echo IDELIBRE_ADMIN_GROUP_USER_ID; ?>') {
            $('#group_user_infos').show();
        } else {
            $('#group_user_infos').hide();
        }

        if ($('#SrvuserSrvroleId option:selected').val() === '<?php echo IDELIBRE_ADMIN_GROUP_WALLETMANAGER_ID; ?>') {
            $('#group_wm_infos').show();
        } else {
            $('#group_wm_infos').hide();
        }
        
        if ($('#SrvuserSrvroleId option:selected').val() === '<?php echo IDELIBRE_GROUP_ADMIN_ID; ?>') {
            $('#group_admin_collectivite').show();
        } else {
            $('#group_admin_collectivite').hide();
        }
        
        
    }


    $('#SrvuserSrvroleId').change(function() {
        showHideRoleUserInfo();
    });
    showHideRoleUserInfo();

    $('#SrvuserConfirm').after($('<span></span>').attr('id', 'SrvuserConfirmInfo').css('margin-left', '5px')).on('keyup', function() {
        checkPassConfirm();
    });

    $('#SrvuserPassword').on('keyup', function() {
        checkPassConfirm();
    });

    $('#SrvuserAddForm').on('submit', function(e) {
        if ($('#SrvuserPassword').val() !== $('#SrvuserConfirm').val()) {
            e.preventDefault();
            e.stopPropagation();
            $('#modalNotMatch').modal('show');
            $('#SrvuserPassword').focus();
        }
    });

    function switchDiv(checkbox, control) {
        if ($(checkbox).attr('checked') === "checked") {
            $(control).addClass('selected');
        } else {
            $(control).removeClass('selected');
        }
    }

    $('.controls div.input-xlarge input[type="checkbox"]').bind('change', function() {
        switchDiv(this, $(this).parent());
    });

    $('.controls div.input-xlarge').bind('click', function(e) {
        if ($(e.target).hasClass('input-xlarge')) {
            $('input[type="checkbox"]', $(e.target)).trigger('click');
        }
    });
</script>
