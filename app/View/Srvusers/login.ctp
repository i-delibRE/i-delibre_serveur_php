<?php
/**
 *
 * Srvusers/login View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-04-01 09:55:53 +0200 (mar. 01 avril 2014) $
 * $Revision: 376 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Srvusers/login.ctp $
 * $Id: login.ctp 376 2014-04-01 07:55:53Z ssampaio $
 *
 */
?>

<div class="form-block">

    <?php echo $this->Session->flash(); ?>
    <div class="col-md-12 text-center" style="margin-top: 24px; margin-bottom: 24px;">


        <?php
        echo $this->Html->image('adullact.png', [
            'class' => 'row center-block main-logo'
        ]);
        ?>
<br>
        <?php
        echo $this->Html->image('i-delibRE_color_h24px.png', [
            'class' => 'row center-block main-logo'
        ]);
        ?>


        <div class="row buffer-top text-center">
            Veuillez saisir vos identifiants de connexion
        </div>

        <div class="row double-buffer-top">
            <?php
            echo $this->Form->create('Srvuser', array("class" => "form-horizontal"));
            ?>

            <div class="form-horizontal form-group">
                <label for="username"
                       class="col-md-4 control-label normal-left">
                    Identifiant *
                </label>

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-login">
                        <i class="fa fa-user"
                           aria-hidden="true">
                        </i>
                    </span>
                    <?php
                    echo $this->Form->input('Srvuser.username', [
                        'class' => 'form-control',
                        'id' => 'username',
                        'aria-describedby' => 'sizing-addon-login',
                        'label' => false
                    ]);
                    ?>
                </div>
            </div>



            <div class="form-horizontal form-group">
                <label for="password"
                       class="col-md-4 control-label normal-left">
                    Mot de passe *
                </label>

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-password">
                        <i class="fa fa-lock"
                           aria-hidden="true">
                        </i>
                    </span>
                    <?php
                    echo $this->Form->input('Srvuser.password', [
                        'class' => 'form-control',
                        'id' => 'password',
                        'aria-describedby' => 'sizing-addon-password',
                        'label' => false
                    ]);
                    ?>
                </div>
            </div>





            <div class="row form-horizontal form-group buffer-top" style="margin-top: 24px;">
                <?php
                echo $this->Form->button('<i class="fa fa-sign-in fa-fw"> </i> Se connecter', [
                    'type' => 'submit',
                    'class' => 'pull-right btn btn-primary'
                ]);
                ?>
            </div>



            <?php
            echo $this->Form->end();
            ?>



        </div>
        <?php
        echo $this->Html->link('Mot de passe oublié ?', array('action' => 'mdplost'));
        ?>
    </div>
</div>
