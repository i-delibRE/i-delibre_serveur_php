<?php

/**
 *
 * Srvusers/json/login View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-11-26 17:25:53 +0100 (mar. 26 nov. 2013) $
 * $Revision: 330 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Srvusers/json/logout.ctp $
 * $Id: logout.ctp 330 2013-11-26 16:25:53Z ssampaio $
 *
 */
$data = json_encode(array_merge(array('jsonView' => 'logout'), $userInfos));
echo $isJsonp ? $this->element('jsonp', array('data' => $data)) : $data;
?>