<?php

/**
 * Srvusers/json/get_proof View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-29 09:29:52 +0100 (mar. 29 oct. 2013) $
 * $Revision: 308 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Srvusers/json/get_proof.ctp $
 * $Id: get_proof.ctp 308 2013-10-29 08:29:52Z ssampaio $
 */
?>
<?php

$data = json_encode(array('view' => 'get_proof', 'proof' => $proof));
echo $isJsonp ? $this->element('jsonp', array('data' => $data)) : $data;
?>