<?php

/**
 * Tree Helper Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-07-23 13:58:59 +0200 (mer. 23 juil. 2014) $
 * $Revision: 527 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Helper/TreeHelper.php $
 * $Id: TreeHelper.php 527 2014-07-23 11:58:59Z ssampaio $
 *
 * @package AclUtilities
 * @package View.Helper
 */
class TreeHelper extends AppHelper {

	/**
	 * Helpers utilisés.
	 *
	 * @var array
	 */
	public $helpers = array('Html', 'Form');

	/**
	 *
	 * @var type
	 */
	private $_treeList;

	/**
	 *
	 * @param type $items
	 * @param type $aliasPath
	 * @param type $prefix
	 * @return null
	 */
	public function treeList($items, $options = array(), $lvl = 0, $path = array()) {
		$return = null;

		if (!empty($items)) {
			//si niveau initial, on vide la variable contenant la liste ul
			if ($lvl == 0) {
				$this->_treeList = '';
			}

			//parcours de informations
			foreach ($items as $item) {
				//Traitement
				$newpath = $path;
				$newpath[] = $item['Ptheme']['name'];
				$this->_treeList .= '<li class="list-data" data-ptheme-path="' . htmlentities(json_encode($newpath)) . '" data-id="' . $item['Ptheme']['id'] . '"><i class="pull-right icon-check"></i> ' . $item['Ptheme']['name'] . '</li>';
				//Recursivité
				if (!empty($item['children'])) {
					$this->_treeList .= '<li class="list-placeholder"><ul class="lvl' . $lvl . '">';
					$this->treeList($item['children'], $options, $lvl + 1, $newpath);
					$this->_treeList .= '</ul></li>';
				}
			}

			//si niveau initial, on renvoi une liste ul
			if ($lvl == 0) {
				if (!empty($options['listContent'])) {
					$return = $this->_treeList;
				} else {
					$return = $this->Html->tag('ul', $this->_treeList, array('class' => 'tree-list dropdown-menu', 'role' => 'menu', 'aria-labelledby' => 'dLabel'));
				}
			}
		}
		return $return;
	}

}

?>