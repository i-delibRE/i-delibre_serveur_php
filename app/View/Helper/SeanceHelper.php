<?php

/**
 * Seance Helper Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Helper/SeanceHelper.php $
 * $Id: SeanceHelper.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
class SeanceHelper extends AppHelper {

	/**
	 *
	 * @var type
	 */
	public $helpers = array('Html', 'Form', 'Bootstrap');

	/**
	 *
	 * Generate Document div with buttons
	 *
	 * @param type $convocationId
	 * @param type $documentId
	 * @param type $documentName
	 * @return type
	 */
	public function document($convocationId = null, $documentId = null, $documentName = null, $deletable = false, $controller, $kind = 'Document') {
		$return = null;
		if (!empty($convocationId) && !empty($documentId) && !empty($documentName)) {
			//document preparation
			$documentUrl = Router::url(array('controller' => $controller, 'action' => 'getFile', $documentId));
			$baseName = basename($documentName);

			//Download Button
			$btnDownloadOptions = array(
				'title' => __d('default', 'btn.download'),
				'data-toggle' => 'tooltip',
				'class' => 'btn-mini btn btn-inverse tooltiped',
				'escape' => false
			);
			$btnDownloadDoc = $this->Html->link('<i class="icon-download-alt icon-white"></i>', $documentUrl, $btnDownloadOptions);

			$btnDeleteDoc = '';
			if ($deletable) {
				//Delete Button
				$btnDeleteUrl = array(
					'controller' => 'Convocations',
					'action' => 'deletedoc',
					$convocationId,
					$documentId
				);

				$btnDeleteOptions = array(
					'title' => __d('default', 'btn.delete'),
					'data-toggle' => 'tooltip',
					'class' => 'btn-mini btn btn-danger tooltiped',
					'escape' => false
				);
				$btnDeleteDoc = $this->Html->link('<i class="icon-trash icon-white"></i>', $btnDeleteUrl, $btnDeleteOptions);
			}

			//Button group
			$btnGroupDoc = $this->Html->tag('span', $btnDownloadDoc . ' ' . $btnDeleteDoc, array('class' => 'app-ui-hidden pull-right '));

			//Document Div
			$return = $this->Html->tag('div', $btnGroupDoc . '<i class="icon-file"></i> ' . $kind . ' : ' . $baseName, array('class' => 'doc'));
		}
		return $return;
	}

}

?>
