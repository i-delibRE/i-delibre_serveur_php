<?php

/**
 * Bootstrap Form Helper Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Helper/BootstrapFormHelper.php $
 * $Id: BootstrapFormHelper.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
App::uses('FormHelper', 'View/Helper');

/**
 *
 */
class BootstrapFormHelper extends FormHelper {

	/**
	 *
	 * @var type
	 */
	private $_hasRequiredField;

	/**
	 *
	 * @param type $model
	 * @param type $options
	 * @return type
	 */
	public function create($model = null, $options = array()) {
		return parent::create($model, $this->addClass($options, 'form-horizontal'));
	}

	/**
	 *
	 * @param type $fieldName
	 * @param type $options
	 * @return type
	 */
	public function input($fieldName, $options = array()) {
		//recupération des informations de la div principale
		$this->setEntity($fieldName);
		$options = $this->_parseOptions($options);
		$divOptions = $this->_divOptions($options);

		//contruction du label
		$label = '';
		if (!empty($options['label'])) {
			$labelStr = $options['label'];
		} else {
			if (isset($options['label']) && $options['label'] === false) {
				$labelStr = '';
			} else {
				$matches = array();
				if (preg_match('/([A-Z].*)\.(.*)/', $fieldName, $matches)) {
					if ($matches[1] === $matches[2]) {
						$labelStr = __d($this->defaultModel, $fieldName);
					} else {
						$labelStr = __d(Inflector::underscore($matches[1]), $fieldName);
					}
				} else {
					$labelStr = __d(Inflector::underscore($this->defaultModel), $this->defaultModel . '.' . $fieldName);
				}
			}
		}

		$label .=!empty($labelStr) ? $this->label($fieldName, $labelStr, array('class' => 'control-label')) : '';

		//construction du message d'erreur
		$error = $this->error($fieldName, null, array('class' => 'alert alert-danger'));

		//configuration des options pour ne pas afficher le conteneur (div) et le label (label) de FormHelper
		$options['label'] = false;
		$options['div'] = false;
		$options['error'] = false;

		//detection de champ de type mail
		$model = $this->_getModel($this->defaultModel);
		if($model) {
            $isEmail = in_array('email', Hash::extract($model->validate, $fieldName . '.{s}.rule.{n}'));
            if ($isEmail) {
                $options['type'] = 'email';
            }
        }

		//construction du champ
		$input = parent::input($fieldName, $this->addClass($options, 'input-xlarge'));

		//detection de champ caché
		$isHidden = preg_match('#type="hidden"#', $input) && !preg_match('#type="checkbox"#', $input);

		//preparation du champ email
		if (isset($isEmail) && $isEmail) {
			$input = $this->Html->tag('div', $this->Html->tag('span', $this->Html->tag('i', '', array('class' => array('fa', 'fa-envelope'))), array('class' => 'add-on')) . $input, array('class' => 'input-prepend'));
		}

		if ((!empty($divOptions['class']) && preg_match('#required#', $divOptions['class'])) || !empty($options['required'])) {
			$this->_hasRequiredField = true;
		}

		//construction des div bootstrap contenant le label et le chaùmp
		return $isHidden ? $input : $this->Html->tag('div', $label . $this->Html->tag('div', $input . (!empty($error) ? $error : ''), array('class' => 'controls')), array('class' => 'control-group ' . (!empty($divOptions['class']) ? $divOptions['class'] : '') . (!empty($options['required']) ? ' required' : '')));
	}

	/**
	 *
	 * @param type $options
	 * @return type
	 */
	public function end($options = null, $noFormInfo = false) {
		$buttons = '';
		$button = '';
		$output = '';

		if (isset($options['editMode']) && $options['editMode'] === true) {
			$validBtn = $this->Html->tag('button', $this->Html->tag('i', '', array('class' => array('icon-white', 'fa fa-floppy-o'))) . ' ' . __d('default', 'btn.save'), array('type' => 'submit', 'class' => array('btn', 'btn-inverse')));
			$cancelBtn = $this->Html->link($this->Html->tag('i', '', array('class' => array('fa fa-times-circle'))) . ' ' . __d('default', 'btn.cancel'), !empty($options['cancelUrl']) ? $options['cancelUrl'] : array('action' => 'index'), array('class' => 'btn', 'escape' => false));
			$buttons = $this->Html->tag('div', $validBtn . $cancelBtn, array('class' => 'btn-group'));
		} else if (!empty($options)) {
			$button = $this->Html->tag('div', $this->Html->tag('button', $options, array('type' => 'submit', 'class' => 'btn btn-inverse')), array('class' => 'submit'));
		}

		if (!empty($button)) {
			$output = $this->Html->tag('div', $this->Html->tag('div', $button, array('class' => 'controls')), array('class' => 'control-group'));
		} else if (!empty($buttons)) {
			$output = $this->Html->tag('div', $this->Html->tag('div', $buttons, array('class' => 'controls')), array('class' => 'control-group'));
		}

		$formInfo = '';
//		if (!$noFormInfo && $this->_hasRequiredField) {
//			$formInfo .= $this->Html->tag('div', $this->Html->tag('span', '*', array('style' => 'color:#f00;')) . ' : ' . __d('default', 'fields.required'), array('class' => array('alert')));
//		}

		return $formInfo . $output . parent::end();
	}

}

?>
