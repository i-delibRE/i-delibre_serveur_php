<?php

/**
 * Bootstrap Box Helper Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Helper/BootstrapBoxHelper.php $
 * $Id: BootstrapBoxHelper.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
class BootstrapBoxHelper extends AppHelper {

	/**
	 *
	 * @var type
	 */
	public $helpers = array('Html', 'Form');

	/**
	 *
	 * @param array $options
	 * example :
	 * array(
	 * 			'id' => css id,
	 * 			'title' => modal box title,
	 * 			'message' => modal box message,
	 * 			'btn.ok' => Ok button text,
	 * 			'btn.ok.url' => Ok button url,
	 * 			'btn.cancel' => Cancel button text
	 * );
	 *
	 * @return string bootstrap modal box generated string
	 */
	public function modalBox($options = array()) {
		$return = null;
		$settings = array(
			'id' => 'myModal',
			'title' => __d('bootstrap', 'modal.title'),
			'message' => __d('bootstrap', 'modal.message'),
			'btn.ok' => __d('bootstrap', 'modal.btn.ok'),
			'btn.ok.url' => __d('bootstrap', 'modal.btn.ok.url'),
			'btn.cancel' => __d('bootstrap', 'modal.btn.cancel')
		);

		if (!empty($options)) {
			$settings = array_merge($settings, $options);

			//header
			$titleBtnClose = $this->Html->tag('button', 'x', array('type' => 'button', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true'));
			$titleMessage = $this->Html->tag('h3', $settings['title'], array('id' => $settings['id'] . 'Label'));
			$header = $this->Html->tag('div', $titleBtnClose . $titleMessage, array('class' => 'modal-header'));

			//body
			$body = $this->Html->tag('div', $this->Html->tag('p', $settings['message']), array('class' => 'modal-body'));

			//footer
			$btnOk = $this->Html->tag('a', $settings['btn.ok'], array('class' => 'btn-ok btn btn-inverse', 'href' => $settings['btn.ok.url']));
			$btnCancel = !empty($settings['btn.cancel']) ? $this->Html->tag('button', $settings['btn.cancel'], array('class' => 'btn btn-dismiss', 'aria-hidden' => 'true', 'data-dismiss' => 'modal')) : '';
			$footer = $this->Html->tag('div', $this->Html->tag('div', $btnOk . $btnCancel, array('class' => 'btn-group')), array('class' => 'modal-body'));

			//modal
			$return = $this->Html->tag('div', $header . $body . $footer, array('id' => $settings['id'], 'class' => 'modal hide fade', 'tabindex' => '-1', 'role' => 'dialog', 'aria-labelledby' => $settings['id'] . 'Label', 'aria-hidden' => 'true'));
			$return .= '<script type="text/javascript">
				if (typeof modalBoxTargetUrl === "undefined"){
					function modalBoxTargetUrl(id, url){
						$("#" + id + " a.btn-ok").attr("href", url);
					}
				}
			</script>';
		}
		return $return;
	}

	/**
	 *
	 * @param type $options
	 * @return type
	 */
	public function msgBox($options = array()) {
		$return = null;
		$settings = array(
			'id' => 'myModal',
			'title' => __d('bootstrap', 'modal.title'),
			'message' => __d('bootstrap', 'modal.message'),
			'btn.ok' => __d('bootstrap', 'modal.btn.ok')
		);

		if (!empty($options)) {
			$settings = array_merge($settings, $options);

			//header
			$titleBtnClose = $this->Html->tag('button', 'x', array('type' => 'button', 'class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => 'true'));
			$titleMessage = $this->Html->tag('h3', $settings['title'], array('id' => $settings['id'] . 'Label'));
			$header = $this->Html->tag('div', $titleBtnClose . $titleMessage, array('class' => 'modal-header'));

			//body
			$body = $this->Html->tag('div', $this->Html->tag('p', $settings['message']), array('class' => 'modal-body'));

			//footer
			$btnOk = $this->Html->tag('button', $settings['btn.ok'], array('class' => 'btn btn-dismiss', 'aria-hidden' => 'true', 'data-dismiss' => 'modal'));
			$footer = $this->Html->tag('div', $btnOk, array('class' => 'modal-body'));

			//modal
			$return = $this->Html->tag('div', $header . $body . $footer, array('id' => $settings['id'], 'class' => 'modal hide fade', 'tabindex' => '-1', 'role' => 'dialog', 'aria-labelledby' => $settings['id'] . 'Label', 'aria-hidden' => 'true'));
		}
		return $return;
	}

	/**
	 * Generate button which can make modal box appear.
	 *
	 * @param string $id modal css id
	 * @param string $title button title for tooltip
	 * @param string $content button content
	 * @param string $class button extras classes
	 * @return string bootstrap button to call modal box
	 */
	public function modalBtn($id = null, $title = null, $content = null, $class = null, $targetUrl = '') {
		$return = null;
		if (!empty($id) && !empty($title) && !empty($content)) {
			$btnClasses = 'tooltiped btn';
			if (!empty($class)) {
				$btnClasses .= ' ' . $class;
			}

			$return = $this->Html->tag('a', $content, array('class' => $btnClasses, 'data-toggle' => 'modal', 'title' => $title, 'href' => '#' . $id, 'data-original-title' => $title, 'onclick' => 'modalBoxTargetUrl("' . $id . '", "' . $targetUrl . '")'));
		}
		return $return;
	}

}

?>
