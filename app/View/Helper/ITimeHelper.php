<?php

/**
 * ITime Helper Class
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Helper/ITimeHelper.php $
 * $Id: ITimeHelper.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
class ITimeHelper extends AppHelper {


	/**
	 *
	 * @param type $pg_timestamp
	 * @param type $showdate
	 * @param type $showtime
	 * @param type $showms
	 * @return string
	 */
	public function datetime($pg_timestamp, $showdate = true, $showtime = true, $showms = false) {
		$return = '';

		preg_match('#([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])#', $pg_timestamp, $date);
		preg_match('#([0-9][0-9]:[0-9][0-9]:[0-9][0-9])#', $pg_timestamp, $time);
		preg_match('#.*\.([0-9]*)#', $pg_timestamp, $ms);

		if ($showdate && !empty($date)) {
			$return .= implode('/', array_reverse(explode('-', $date[1])));
		}

		if ($showtime && !empty($time)) {
			$return .= ($return != '' ? ' ' : '') . $time[1];
		}

		if ($showms && !empty($ms)) {
			$return .= ($return != '' ? ' ' : '') . $ms[1];
		}

		return $return;
	}

}

?>
