<?php
/**
 *
 * Srvroles/edit View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Srvroles/edit.ctp $
 * $Id: edit.ctp 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
?>
<div class="srvroles form">
	<?php echo $this->Form->create('Srvrole'); ?>
	<fieldset>
		<legend><?php echo __d('srvrole', 'Srvrole.edit'); ?></legend>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		?>
	</fieldset>
	<?php echo $this->Form->end(array('editMode' => true)); ?>
</div>
