<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

?>
<div class="collectiviteSrvusers view">
	<h2><?php echo __('Collectivite Srvuser'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($collectiviteSrvuser['CollectiviteSrvuser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Srvuser'); ?></dt>
		<dd>
			<?php echo $this->Html->link($collectiviteSrvuser['Srvuser']['id'], array('controller' => 'srvusers', 'action' => 'view', $collectiviteSrvuser['Srvuser']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Collectivite'); ?></dt>
		<dd>
			<?php echo $this->Html->link($collectiviteSrvuser['Collectivite']['name'], array('controller' => 'collectivites', 'action' => 'view', $collectiviteSrvuser['Collectivite']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($collectiviteSrvuser['CollectiviteSrvuser']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($collectiviteSrvuser['CollectiviteSrvuser']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Collectivite Srvuser'), array('action' => 'edit', $collectiviteSrvuser['CollectiviteSrvuser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Collectivite Srvuser'), array('action' => 'delete', $collectiviteSrvuser['CollectiviteSrvuser']['id']), null, __('Are you sure you want to delete # %s?', $collectiviteSrvuser['CollectiviteSrvuser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Collectivite Srvusers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Collectivite Srvuser'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Srvusers'), array('controller' => 'srvusers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Srvuser'), array('controller' => 'srvusers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Collectivites'), array('controller' => 'collectivites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Collectivite'), array('controller' => 'collectivites', 'action' => 'add')); ?> </li>
	</ul>
</div>
