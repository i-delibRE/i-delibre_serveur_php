<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

?>
<div class="collectiviteSrvusers form">
	<?php echo $this->Form->create('CollectiviteSrvuser'); ?>
	<fieldset>
		<legend><?php echo __('Add Collectivite Srvuser'); ?></legend>
		<?php
		echo $this->Form->input('srvuser_id');
		echo $this->Form->input('collectivite_id');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Collectivite Srvusers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Srvusers'), array('controller' => 'srvusers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Srvuser'), array('controller' => 'srvusers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Collectivites'), array('controller' => 'collectivites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Collectivite'), array('controller' => 'collectivites', 'action' => 'add')); ?> </li>
	</ul>
</div>
