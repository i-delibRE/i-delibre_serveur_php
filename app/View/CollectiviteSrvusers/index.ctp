<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

?>
<div class="collectiviteSrvusers index">
	<h2><?php echo __('Collectivite Srvusers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('srvuser_id'); ?></th>
			<th><?php echo $this->Paginator->sort('collectivite_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		<?php foreach ($collectiviteSrvusers as $collectiviteSrvuser): ?>
			<tr>
				<td><?php echo h($collectiviteSrvuser['CollectiviteSrvuser']['id']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($collectiviteSrvuser['Srvuser']['id'], array('controller' => 'srvusers', 'action' => 'view', $collectiviteSrvuser['Srvuser']['id'])); ?>
				</td>
				<td>
					<?php echo $this->Html->link($collectiviteSrvuser['Collectivite']['name'], array('controller' => 'collectivites', 'action' => 'view', $collectiviteSrvuser['Collectivite']['id'])); ?>
				</td>
				<td><?php echo h($collectiviteSrvuser['CollectiviteSrvuser']['created']); ?>&nbsp;</td>
				<td><?php echo h($collectiviteSrvuser['CollectiviteSrvuser']['modified']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $collectiviteSrvuser['CollectiviteSrvuser']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $collectiviteSrvuser['CollectiviteSrvuser']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $collectiviteSrvuser['CollectiviteSrvuser']['id']), null, __('Are you sure you want to delete # %s?', $collectiviteSrvuser['CollectiviteSrvuser']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
	<div class="paging">
		<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Collectivite Srvuser'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Srvusers'), array('controller' => 'srvusers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Srvuser'), array('controller' => 'srvusers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Collectivites'), array('controller' => 'collectivites', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Collectivite'), array('controller' => 'collectivites', 'action' => 'add')); ?> </li>
	</ul>
</div>
