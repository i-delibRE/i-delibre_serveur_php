
<?php
/**
 * Monitor/index View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Jean Genot
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-04-01 09:55:53 +0200 (mar. 01 avril 2014) $
 * $Revision: 376 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/branches/1.0.x/app/View/Collectivites/edit.ctp $
 * $Id: edit.ctp 376 2014-04-01 07:55:53Z ssampaio $
 */
echo $this->Html->css('monitor');
?>

<div class="row" id="controls">
    <div class="span6 monitor">
        <div class="alert alert-danger" style="text-align: center;">
            <span class="pull-left"><?php echo __d('monitor', 'Monitor.monitor'); ?></span>
            <span class="monitorState pull-right">
                <span id="monitorOn"><i class="icon icon-play hide" title="<?php echo __d('monitor', 'Monitor.on') ?>"></i></span>
                <span id="monitorOff"><i class="icon icon-stop" title="<?php echo __d('monitor', 'Monitor.off') ?>"></i></span>
            </span>
            <span class="btn-group">
                <span class="btn btn-small btn-inverse" id="monitorStartBtn" title="<?php echo __d('monitor', 'Monitor.start'); ?>"><i class="icon icon-play"></i></span>
                <span class="btn btn-small btn-inverse" id="monitorStopBtn" title="<?php echo __d('monitor', 'Monitor.stop'); ?>"><i class="icon icon-stop"></i></span>
                <span class="btn btn-small btn-danger" id="monitorClearBtn" title="<?php echo __d('monitor', 'Monitor.clear'); ?>"><i class="icon icon-trash"></i></span>
            </span>
        </div>
    </div>
    <div class="span6 services">
        <div class="alert alert-danger" style="text-align: center;">
            <span class="pull-left"><?php echo __d('monitor', 'Monitor.services'); ?></span>
            <span class="servicesState pull-right">
                <span id="servicesOn"><i class="icon icon-play hide" title="<?php echo __d('monitor', 'Services.on') ?>"></i></span>
                <span id="servicesOff"><i class="icon icon-stop" title="<?php echo __d('monitor', 'Services.off') ?>"></i></span>
            </span>
            <span class="btn-group" style="visibility: hidden;">
                <span class="btn btn-small btn-inverse" id="servicesStartBtn" title="<?php echo __d('monitor', 'Services.start'); ?>"><i class="icon icon-play"></i></span>
                <span class="btn btn-small btn-inverse" id="servicesStopBtn" title="<?php echo __d('monitor', 'Services.stop'); ?>"><i class="icon icon-stop"></i></span>
                <span class="btn btn-small btn-inverse" id="servicesRefreshBtn" title="<?php echo __d('monitor', 'Services.restart'); ?>"><i class="icon icon-refresh"></i></span>
            </span>
        </div>
    </div>
</div>

<pre id="monitor"></pre>

<script type="text/javascript">
    /**
     * Switch services control render
     *
     * @param {type} state
     * @returns {undefined}
     */
    function setServicesState(state) {
        if (state === "on") {
            $('#controls .services .alert').removeClass('alert-danger').addClass('alert-success');
            $('#servicesOn i').removeClass('hide');
            $('#servicesOff i').addClass('hide');
        } else if (state === "off") {
            $('#controls .services .alert').removeClass('alert-success').addClass('alert-danger');
            $('#servicesOn i').addClass('hide');
            $('#servicesOff i').removeClass('hide');
        }
    }

    /**
     * Switch monitor control render
     *
     * @param {type} state
     * @returns {undefined}
     */
    function setMonitorState(state) {
        if (state === "on") {
            $('#controls .monitor .alert').removeClass('alert-danger').addClass('alert-success');
            $('#monitorOn i').removeClass('hide');
            $('#monitorOff i').addClass('hide');
        } else if (state === "off") {
            $('#controls .monitor .alert').removeClass('alert-success').addClass('alert-danger');
            $('#monitorOn i').addClass('hide');
            $('#monitorOff i').removeClass('hide');
        }
    }


<?php if ($wschecks['global']) { ?>
        setServicesState('on');
<?php } ?>


    /**
     * Actions de boutons de surveillance
     *
     */
    var interval;
    $('#monitorStartBtn').bind('click', function() {
        interval = setInterval(function() {

            $.ajax({
                url: "<?php echo Router::url(array('controller' => 'Monitor', 'action' => 'display')); ?>",
                success: function(data) {
                    $("#monitor").append(JSON.parse(data));
                    document.getElementById('monitor').scrollTop = document.getElementById('monitor').scrollHeight;
                }
            });
        }, 1000);

        $.ajax({
            url: "<?php echo Router::url(array('controller' => 'Monitor', 'action' => 'start')); ?>",
            success: function(data) {
                setMonitorState('on');
            }
        });
    });


    $('#monitorStopBtn').bind('click', function() {
        clearInterval(interval);
        $.ajax({
            url: "<?php echo Router::url(array('controller' => 'Monitor', 'action' => 'stop')); ?>",
            success: function(data) {
                setMonitorState('off');
            }
        });
    });

    $('#monitorClearBtn').bind('click', function() {
        $('#monitor').html('');
    });

</script>
