<?php
/**
 *
 * Types/edit View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-11-27 10:15:50 +0100 (mer. 27 nov. 2013) $
 * $Revision: 331 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Types/edit.ctp $
 * $Id: edit.ctp 331 2013-11-27 09:15:50Z ssampaio $
 *
 */
?>


<div class="types form">
    <?php echo $this->Form->create('Type'); ?>
    <fieldset>
        <legend><?php echo __d('type', 'Type.edit'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('name');

        echo $this->Form->input('selectAll', array(
                'type' => "checkbox",
                'label' => "Tout selectionner",
                'id' => 'selectActeurs',
                'checked' => false
        ));
        ?>


        <div id="acteurs">
            <?php echo $this->Form->input('User.User', array('type' => 'select', 'multiple' => 'checkbox')); ?>
        </div>
        <?php echo $this->Form->input('secretaire', array('type' => 'select', 'multiple' => 'checkbox', 'label' => 'Secretaire',  'value' => $selected)); ?>

    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

<script type="text/javascript">
    $('.controls').each(function() {
        if ($('div.input-xlarge', this).length > 0) {
            $(this).addClass('chkList');
        }
    })

    function switchDiv(checkbox, control) {
        if ($(checkbox).attr('checked') === "checked") {
            $(control).addClass('selected');
        } else {
            $(control).removeClass('selected');
        }
    }

    $('.controls div.input-xlarge input[type="checkbox"]').bind('change', function() {
        switchDiv(this, $(this).parent());
    });

    $('.controls div.input-xlarge').each(function() {
        switchDiv($('input[type="checkbox"]', $(this)), $(this));
    }).bind('click', function(e) {
        if ($(e.target).hasClass('input-xlarge')) {
            $('input[type="checkbox"]', $(e.target)).trigger('click');
        }
    });


    $('#selectActeurs').click(function(e){
       //var state = this.attr('selected');
       var ischecked = ($(this).is(':checked'));
       if(ischecked)
           selectAll();
       else
           unSelectAll();
    });


    function selectAll(){
        $('#acteurs input:checkbox').prop('checked', true);
        $('#acteurs input:checkbox').parent().addClass('selected');
    }


    function unSelectAll(){
        $('#acteurs input:checkbox').prop('checked', false);
        $('#acteurs input:checkbox').parent().removeClass('selected');
    }

</script>