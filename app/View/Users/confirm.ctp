<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

//debug($this->request->data);

$UsersChoice = $this->request->data['existing'];

//debug($UsersChoice);
?>


<h2><?php echo __d('user', 'User.detection.doublon'); ?></h2>


<div class="span10 offset1 well">
    <?php echo __d('user', 'User.choix'); ?>    

</div>
<div class="span6 offset3">

    <form id="UserConfirm"  method="post" action="/Users/confirm">

        <?php
        foreach ($UsersChoice as $key => $user) {

            if (!empty($user['Srvuser'])) {
                echo '<br>';

                echo '<table class="table">';
                echo '<tr><td>';
                echo '<input type="radio" name="keyChoice" value="' . $key . '">';
                echo '</td><td class="bg-grey">';
                //peut etre utiliser la key plutot

                echo 'Nom : ';
                echo '<b>' . $user['Srvuser']['firstname'] . '</b>' . '<br>';
                echo 'Prénom : ';
                echo '<b>' . $user['Srvuser']['lastname'] . '</b>' . '<br>';
                echo 'Date de naissance : ';
                // echo '<b>' . $user['Srvuser']['naissance'] . '</b>' . '<br>';
                echo '<b>' . $this->Time->format($user['Srvuser']['naissance'], '%d-%m-%Y', 'invalid') . '</b>' . '<br>';
        
                echo 'Username : ';
                echo '<b>' . $user['Srvuser']['username'] . '</b>' . '<br>';

                foreach ($user['Mandat'] as $Mandat) {
                    echo 'Collectivité : ';
                    echo '<b>' . $Mandat['Collectivite']['name'] . '</b>' . '<br>';
                }
                echo '</td></tr>';
                echo '</table>';

//echo $user['collectivite'] .'<br>';
            }
        }
        ?>
        <br>
        <table class="table">
            <tr><td>
                    <input type="radio" name="keyChoice" value="new">
                </td><td class="bg-grey">
                    <b>nouvel utilisateur </b>(homonyme)
                </td></tr>
        </table>

        <button type="submit" class="btn btn-success pull-right">Valider</button>
        <br>
    </form>


</div>

<?php
/*
echo $this->Form->create();


echo $this->Form->input('existing.Srvuser.firstname', array());

echo $this->Form->input('Test.field', array(
    'before' => '--avant--',
    'after' => '--après--',
    'between' => '--entre---',
    'separator' => '--séparateur--',
    'options' => array('1', '2')
));

/*
  options = array('H' => 'Homme', 'F' => 'Femme');
  $attributes = array('legend' => false);
  echo $this->Form->radio('genre', $options, $attributes);
 */
/*echo $this->Form->end();
*/