<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

$isOwner = true; // Todo remove isOwner tests
$isActeur = ($this->request->data['User']['group_id'] == IDELIBRE_GROUP_AGENT_ID);
?>


<?php if (!$isOwner) { ?>
    <div class="well bg-red">
        <?php echo __d('user', 'User.not.owner') ?>
    </div>



<?php } ?>


<style>
    .multi-input-hack{
        display: inline-block;
        margin-left: 5px;
        margin-right: 10px;
        margin-bottom: 15px;
        max-width: 75ch;
        font-size: 14px;

    }

    .border-hack{
    border-style: solid;
    }

</style>

<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __d('user', 'User.edit'); ?></legend>
        <?php

        //si ce n'est pas un élu
        if ($this->request->data['User']['group_id'] != IDELIBRE_GROUP_AGENT_ID) {
            echo $this->Form->input('id');
            echo $this->Form->input('civilite', array("label" => "Civilité",  "options" => array(NULL, "Madame", "Monsieur")  ));
            echo $this->Form->input('firstname');
            echo $this->Form->input('lastname');

            echo $this->Form->input('mail');
            echo $this->Html->tag('hr');
            echo $this->Form->input('active');
            echo $this->Form->input('username');
            echo $this->Form->input('group_id', array('type' => 'hidden'));
            echo $this->Html->tag('hr');
            echo $this->Form->input('User.newpass', array('type' => 'password', 'label' => __d('user', 'User.password.new'), 'value' => '', 'required' => false, 'autocomplete' => "off"));
            echo $this->Form->input('User.confirm', array('type' => 'password', 'autocomplete' => "off"));

            //si c'est un acteur
        } else {
            echo $this->Form->input('id');
            echo $this->Form->input('civilite', array("label" => "Civilité",  "options" => array(NULL, "Madame", "Monsieur")  ));
            echo $this->Form->input('firstname');
            echo $this->Form->input('lastname');
            echo $this->Form->input('group_id', array('type' => 'hidden'));

            echo $this->Form->input('titre', array(
                'label' => "Titre"
            ));

            echo $this->Form->input('groupepolitique_id', array(
                'options' => $groupepolitiques,
                'label' => __d('user', 'User.groupepolitique')
            ));


//            if ($isOwner) {
//                echo $this->Form->input('naissance', array(
//                    'label' => 'naissance',
//                    'dateFormat' => 'DMY',
//                    'timeFormat' => 'invalid',
//                    'minYear' => date('Y') - 110,
//                    'maxYear' => date('Y') - 18,
//                ));
//            } else {
//
//                echo $this->Form->input('naissance', array(
//                    'label' => 'naissance',
//                    'dateFormat' => 'DMY',
//                    'timeFormat' => 'invalid',
//                    'disabled' => 'true'
//                ));
//                echo $this->Form->input('naissance', array(
//                    'label' => 'naissance',
//                    'dateFormat' => 'DMY',
//                    'timeFormat' => 'invalid',
//                    'type' => 'hidden'
//                ));
//            }
            echo $this->Form->input('mail');

            echo $this->Html->tag('hr');




            echo $this->Form->input('active');

            if ($this->request->data['User']['group_id'] == IDELIBRE_GROUP_AGENT_ID) {
                echo $this->Form->input('blacklisted');
            }

            if ($isOwner) {
                echo $this->Form->input('username');
                echo $this->Html->tag('hr');
                echo $this->Form->input('User.newpass', array('type' => 'password', 'label' => __d('user', 'User.password.new'), 'value' => '', 'required' => false, 'autocomplete' => "off"));
                echo $this->Form->input('User.confirm', array('type' => 'password', 'autocomplete' => "off"));
            } else {
                echo $this->Form->input('username', array('disabled' => 'true'));
                echo $this->Form->input('username', array('type' => 'hidden'));
            }
        }
        //		echo $this->Form->input('userconf_id');
        ?>
    </fieldset>

<?php if(isset($authorized)){ ?>
    <fieldset>
        <legend> Types de Séances autorisés</legend>
        <div id="removeClass">
            <?php
            echo $this->Form->input('types', array(
                'type' => 'select',
                'multiple' => 'checkbox',
                'label' => 'Types de Séances',
                'value' => $authorized,
                "style" => "none" ));
            ?>
        </div>

    </fieldset>

<?php } ?>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

<?php
//$settings = array(
//    'id' => 'modalNotMatch',
//    'title' => __d('user', 'User.password.notmatch.title'),
//    'message' => __d('user', 'User.password.notmatch.message')
//);
//echo $this->BootstrapBox->msgBox($settings);
?>


<script type="text/javascript">
    var confirmOk = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-ok').addClass('icon-white')).addClass('label').addClass('label-success');
    var confirmNok = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-remove').addClass('icon-white')).addClass('label').addClass('label-important');

    $(document).ready(function(){
       $('#removeClass').find("*").removeClass("input-xlarge");
       $('#removeClass').find("label").addClass("multi-input-hack");
    });


    function checkPassConfirm() {
        if ($('#UserNewpass').val() !== $('#UserConfirm').val() || $('#UserNewpass').val() === '' || $('#UserConfirm').val() === '') {
            $('#UserConfirmInfo').empty().append(confirmNok);
        } else {
            $('#UserConfirmInfo').empty().append(confirmOk);
        }
    }

    $('#final-submit').click(function () {
        // on suprime k'ecouteur du formulaire sur submit pour eviter que  la vérification s'"ffectue de nouveau
        $("#UserEditForm").unbind('submit');
        $('#UserEditForm').submit();
    });


    $('#UserConfirm').after($('<span></span>').attr('id', 'UserConfirmInfo').css('margin-left', '5px')).on('keyup', function () {
        checkPassConfirm();
    });

    $('#UserNewpass').on('keyup', function () {
        checkPassConfirm();
    });

</script>
