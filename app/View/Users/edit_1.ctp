<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

debug($this->request->data);

/**
 *
 * Users/edit View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-04-01 09:55:53 +0200 (mar. 01 avril 2014) $
 * $Revision: 376 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Users/edit.ctp $
 * $Id: edit.ctp 376 2014-04-01 07:55:53Z ssampaio $
 *
 */
//$this->Html->addCrumb(__d('ctx', 'ctx.Users.index'));
//$this->Html->addCrumb(__d('user', 'User.list'), '/Users');
?>
<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __d('user', 'User.edit'); ?></legend>
        <?php
        // si l'user n'est pas un acteur
        if ($this->request->data['User']['group_id'] != IDELIBRE_GROUP_AGENT_ID) {
            echo $this->Form->input('id');
            echo $this->Form->input('firstname');
            echo $this->Form->input('lastname');
            echo $this->Form->input('naissance', array(
                'label' => 'naissance',
                'dateFormat' => 'DMY',
                'minYear' => date('Y') - 110,
                'maxYear' => date('Y') - 18,
            ));

            echo $this->Form->input('mail');
            echo $this->Html->tag('hr');
            echo $this->Form->input('active');
            echo $this->Form->input('username');
            echo $this->Form->input('group_id');
            echo $this->Html->tag('hr');
            echo $this->Form->input('User.newpass', array('type' => 'password', 'label' => __d('user', 'User.password.new'), 'value' => '', 'required' => false, 'autocomplete' => "off"));
            echo $this->Form->input('User.confirm', array('type' => 'password', 'autocomplete' => "off"));
//		echo $this->Form->input('userconf_id');
        } else {

            echo $this->Form->input('id');

            echo $this->Form->input('firstname', array('disabled' => 'true'));
            echo $this->Form->input('firstname', array('type' => 'hidden'));


            echo $this->Form->input('lastname', array('disabled' => 'true'));
            echo $this->Form->input('firstname', array('type' => 'hidden'));

            echo $this->Form->input('naissance', array(
                'label' => 'naissance',
                'dateFormat' => 'DMY',
                'minYear' => date('Y') - 110,
                'maxYear' => date('Y') - 18,
            ));

            echo $this->Form->input('mail');
            echo $this->Html->tag('hr');
            echo $this->Form->input('active');
            echo $this->Form->input('username');
            echo $this->Form->input('group_id');
            echo $this->Html->tag('hr');
            echo $this->Form->input('User.newpass', array('type' => 'password', 'label' => __d('user', 'User.password.new'), 'value' => '', 'required' => false, 'autocomplete' => "off"));
            echo $this->Form->input('User.confirm', array('type' => 'password', 'autocomplete' => "off"));
        }
        ?>
    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

<?php
$settings = array(
    'id' => 'modalNotMatch',
    'title' => __d('user', 'User.password.notmatch.title'),
    'message' => __d('user', 'User.password.notmatch.message')
);
echo $this->BootstrapBox->msgBox($settings);
?>


<script type="text/javascript">
    var confirmOk = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-ok').addClass('icon-white')).addClass('label').addClass('label-success');
    var confirmNok = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-remove').addClass('icon-white')).addClass('label').addClass('label-important');

    function checkPassConfirm() {
        if ($('#UserNewpass').val() !== $('#UserConfirm').val() || $('#UserNewpass').val() === '' || $('#UserConfirm').val() === '') {
            $('#UserConfirmInfo').empty().append(confirmNok);
        } else {
            $('#UserConfirmInfo').empty().append(confirmOk);
        }
    }

    $('#UserConfirm').after($('<span></span>').attr('id', 'UserConfirmInfo').css('margin-left', '5px')).on('keyup', function () {
        checkPassConfirm();
    });

    $('#UserNewpass').on('keyup', function () {
        checkPassConfirm();
    });

    $('#UserEditForm').on('submit', function (e) {
        if ($('#UserNewpass').val() !== '' && $('#UserNewpass').val() !== $('#UserConfirm').val()) {
            e.preventDefault();
            e.stopPropagation();
            $('#modalNotMatch').modal('show');
            $('#UserNewpass').focus();
        }
    });
</script>