<?php
/**
 *
 * Users/add View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-04-01 09:55:53 +0200 (mar. 01 avril 2014) $
 * $Revision: 376 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Users/add.ctp $
 * $Id: add.ctp 376 2014-04-01 07:55:53Z ssampaio $
 *
 */
//$this->Html->addCrumb(__d('ctx', 'ctx.Users.index'));
//$this->Html->addCrumb(__d('user', 'User.list'), '/Users');
//$this->Html->addCrumb(__d('user', 'User.add'));
?>


<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __d('user', 'User.add'); ?></legend>
        <?php

        echo $this->Form->input('civilite', array("label" => "Civilité",  "options" => array(NULL, "Madame", "Monsieur")  ));
        echo $this->Form->input('firstname');
        echo $this->Form->input('lastname');
//        echo $this->Form->input('naissance', array(
//            'label' => 'naissance',
//            'dateFormat' => 'DMY',
//            'timeFormat' => 'invalid',
//            'minYear' => date('Y') - 110,
//            'maxYear' => date('Y') - 18,
//        ));



        echo $this->Form->input('mail');
        echo $this->Html->tag('hr');
        echo $this->Form->input('username');
        echo $this->Form->input('group_id', array('empty' => true));

        echo '<div id = "gp_politique">';

        echo $this->Form->input('titre', array(
            'label' => "Titre"
        ));



        echo $this->Form->input('groupepolitique_id', array(
            'options' => $groupepolitiques,
            'label' => __d('user','User.groupepolitique')
        ));
        echo '</div>';
        echo $this->Html->tag('hr');
        echo $this->Form->input('password', array('autocomplete' => "off"));
        echo $this->Form->input('confirm', array('type' => 'password', 'required' => true));
        //		echo $this->Form->input('userconf_id');
        ?>
    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

<?php
$settings = array(
    'id' => 'modalNotMatch',
    'title' => __d('user', 'User.password.notmatch.title'),
    'message' => __d('user', 'User.password.notmatch.message')
);
echo $this->BootstrapBox->msgBox($settings);
?>


<script type="text/javascript">

    $(document).ready(function () {

        var IDELIBRE_GROUP_AGENT_ID = "<?php echo IDELIBRE_GROUP_AGENT_ID ?>"


        var confirmOk = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-ok').addClass('icon-white')).addClass('label').addClass('label-success');
        var confirmNok = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-remove').addClass('icon-white')).addClass('label').addClass('label-important');

        function checkPassConfirm() {
            if ($('#UserPassword').val() !== $('#UserConfirm').val() || $('#UserPassword').val() === '' || $('UserConfirm').val() === '') {
                $('#UserConfirmInfo').empty().append(confirmNok);
            } else {
                $('#UserConfirmInfo').empty().append(confirmOk);
            }
        }

        $('#UserConfirm').after($('<span></span>').attr('id', 'UserConfirmInfo').css('margin-left', '5px')).on('keyup', function () {
            checkPassConfirm();
        });

        $('#UserPassword').on('keyup', function () {
            checkPassConfirm();
        });

        $('#UserAddForm').on('submit', function (e) {
            if ($('#UserPassword').val() !== $('#UserConfirm').val()) {
                e.preventDefault();
                e.stopPropagation();
                $('#modalNotMatch').modal('show');
                $('#UserPassword').focus();
            }

        });

        $("#gp_politique").hide();
        
        $("#UserGroupId").change(function () {
            
            if($("#UserGroupId").val() === IDELIBRE_GROUP_AGENT_ID){
                $("#gp_politique").show();
            }else{
                $("#gp_politique").hide();
        }});


    });
</script>