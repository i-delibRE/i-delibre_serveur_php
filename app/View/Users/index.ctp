<?php

/**
 *
 * Users/index View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-04-01 09:55:53 +0200 (mar. 01 avril 2014) $
 * $Revision: 376 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Users/index.ctp $
 * $Id: index.ctp 376 2014-04-01 07:55:53Z ssampaio $
 *
 */
//$this->Html->addCrumb(__d('ctx', 'ctx.Users.index'));
//$this->Html->addCrumb(__d('user', 'User.list'));
echo $this->element('dataview');
?>