
<div style="margin-top: 20px">
    <?php
    echo $this->Form->create('Emailconvocation');
    ?>


    <fieldset>
        <legend>Modifier un email de convocation</legend>

    <?php
    echo $this->Form->input('Emailconvocation.id');
    ?>

    <?php
    if($this->request->data["Emailconvocation"]["id"] != 1)
        echo $this->Form->input('Emailconvocation.name', array(
            'label' => "Intitulé",
            'style' => "width:100%",
            'required' => true));
    ?>

    <?php
    echo $this->Form->input('Emailconvocation.sujet', array(
        'label' => 'Sujet',
        'style' => "width:100%",
        'required' => true));
    ?>

    <?php
    echo $this->Form->input('Emailconvocation.contenu', array(
        'style' => 'height:200px;width:100%',
        "type" =>"textarea",
        'class' => 'form-control',
        'label' => 'Contenu',
        'required' => true));
    ?>

    <?php
    if($this->request->data["Emailconvocation"]["id"] != 1)
        echo $this->Form->input('Type.id', array("label" => "Type",  "options" => $typeList , "value" => $this->request->data['Emailconvocation']['type_id'] , 'required' => true, 'style' => "width:100%"));
    ?>

    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>

<!--    <div>-->
<!--        <button type="submit" class="btn btn-success pull-right" title="Enregistrer"><span class="fa fa-floppy-o"></span> Enregistrer</button>-->
<!--    </div>-->
</div>

<br>



<div class="span3"></div>

<div class="span6 well">
    date de la séance : #dateseance# <br>
    heure de la séance : #heureseance# <br>
    lieu de la seance : #lieuseance# <br>
    type de la séance : #typeseance# <br>
    prénom de l'élu : #prenom# <br>
    nom de lélu : #nom# <br>
    titre de l'élu : #titre# <br>
    civilité de lélu : #civilite# <br>
</div>
