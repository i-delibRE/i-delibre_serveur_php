<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('Appchecks', 'Utility');

/**
 * Pages/appchecks View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-08-01 11:49:31 +0200 (ven. 01 août 2014) $
 * $Revision: 550 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Pages/appchecks.ctp $
 * $Id: appchecks.ctp 550 2014-08-01 09:49:31Z ssampaio $
 */
echo $this->Html->css('appchecks');
$allChecks = Appchecks::getAll();
?>

<!--<h1><?php echo __d('ctx', 'ctx.Pages.display.appchecks'); ?></h1>-->
<p>
    <span class="notice <?php echo $allChecks['all'] == 'ok' ? 'success' : ($allChecks['all'] == 'error' ? 'error-message' : ''); ?>">
        <i class="icon-<?php echo $allChecks['all'] == 'ok' ? 'ok' : ($allChecks['all'] == 'error' ? 'remove' : 'warning-sign'); ?>"></i> <?php echo __d('appchecks', 'Checks.all'); ?> <span class="btn btn-inverse" id="refresh"><i class="icon icon-refresh"></i> Vérifier à nouveau</span>

    </span>
</p>


<h2><?php echo __d('appchecks', 'Checks.versions'); ?></h2>
<p>
    <span class="notice <?php echo $allChecks['checks']['nginx']['check'] ? 'success' : 'error-message'; ?>">
        <i class="icon-<?php echo $allChecks['checks']['nginx']['check'] ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.versions.nginx'); ?> <span class="pull-right"><?php echo $allChecks['checks']['nginx']['version']; ?></span>
    </span>
</p>

<p>
    <span class="notice <?php echo $allChecks['checks']['php']['check'] ? 'success' : 'error-message'; ?>">
        <i class="icon-<?php echo $allChecks['checks']['php']['check'] ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.versions.php'); ?> <span class="pull-right"><?php echo $allChecks['checks']['php']['version']; ?></span>
    </span>
</p>
<p>
    <span class="notice <?php echo $allChecks['checks']['cakephp']['check'] == 1 ? 'success' : ($allChecks['checks']['cakephp']['check'] == 2 ? 'warning' : 'error-message'); ?>">
        <i class="icon-<?php echo $allChecks['checks']['cakephp']['check'] > 0 ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.versions.cakephp'); ?> <span class="pull-right"><?php echo $allChecks['checks']['cakephp']['version']; ?></span>
    </span>
</p>
<p>
    <span class="notice <?php echo $allChecks['checks']['postgres']['check'] ? 'success' : 'error-message'; ?>">
        <i class="icon-<?php echo $allChecks['checks']['postgres']['check'] ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.versions.postgres'); ?> <span class="pull-right"><?php echo $allChecks['checks']['postgres']['version']; ?></span>
    </span>
</p>

<h2><?php echo __d('appchecks', 'Checks.configs'); ?></h2>
<div class="notice <?php echo $allChecks['cfgs']['php']['file_uploads'] && $allChecks['cfgs']['php']['enought_memory'] && $allChecks['cfgs']['php']['enought_size'] ? 'success' : 'error-message'; ?>">
    <i class="icon-<?php echo $allChecks['cfgs']['php']['file_uploads'] && $allChecks['cfgs']['php']['enought_memory'] && $allChecks['cfgs']['php']['enought_size'] ? 'ok' : 'remove'; ?>"></i> <?php echo $allChecks['cfgs']['php']['file_uploads'] && $allChecks['cfgs']['php']['enought_memory'] && $allChecks['cfgs']['php']['enought_size'] ? __d('appchecks', 'Checks.configs.php.success') : __d('appchecks', 'Checks.configs.php.error'); ?>
    <ul>
        <li><?php echo __d('appchecks', 'Checks.php.max_file_uploads'); ?> : <?php echo $allChecks['cfgs']['php']['max_file_uploads']; ?></li>
        <li><?php echo __d('appchecks', 'Checks.php.upload_max_filesize'); ?> : <?php echo $allChecks['cfgs']['php']['upload_max_filesize']; ?>o</li>
        <li><?php echo __d('appchecks', 'Checks.php.post_max_size'); ?> : <?php echo $allChecks['cfgs']['php']['post_max_size']; ?>o</li>
        <li><?php echo __d('appchecks', 'Checks.php.memory_limit'); ?> : <?php echo $allChecks['cfgs']['php']['memory_limit']; ?>o</li>
        <li>&nbsp;</li>
        <li><?php echo __d('appchecks', 'Checks.php.enought_memory'); ?> (<?php echo $allChecks['cfgs']['php']['post_max_size']; ?> &le; <?php echo $allChecks['cfgs']['php']['memory_limit']; ?>) : <?php echo $allChecks['cfgs']['php']['enought_memory'] ? $this->element('ischeck') : $this->element('no'); ?></li>
<!--        <li>--><?php //echo __d('appchecks', 'Checks.php.enought_size'); ?><!-- (--><?php //echo $allChecks['cfgs']['php']['max_file_uploads']; ?><!-- &Cross; --><?php //echo $allChecks['cfgs']['php']['upload_max_filesize']; ?><!-- &le; --><?php //echo $allChecks['cfgs']['php']['memory_limit']; ?><!--) : --><?php //echo $allChecks['cfgs']['php']['enought_size'] ? $this->element('ischeck') : $this->element('no'); ?><!--</li>-->
    </ul>
</div>
<div class="notice <?php echo $allChecks['cfgs']['idelibre']['exists'] && $allChecks['cfgs']['idelibre']['readable'] ? 'success' : 'error-message'; ?>">
    <i class="icon-<?php echo $allChecks['cfgs']['idelibre']['exists'] && $allChecks['cfgs']['idelibre']['readable'] ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.configs.idelibre_inc'); ?>
    <ul>
        <li><?php echo __d('appchecks', 'Checks.configs.isFile'); ?> : <?php echo $allChecks['cfgs']['idelibre']['exists'] ? $this->element('ischeck') : $this->element('no'); ?></li>
        <li><?php echo __d('appchecks', 'Checks.configs.isReadable'); ?> : <?php echo $allChecks['cfgs']['idelibre']['readable'] ? $this->element('ischeck') : $this->element('no'); ?></li>
    </ul>
</div>
<div class="notice <?php echo $allChecks['cfgs']['email']['exists'] && $allChecks['cfgs']['email']['readable'] ? 'success' : 'error-message'; ?>">
    <i class="icon-<?php echo $allChecks['cfgs']['email']['exists'] && $allChecks['cfgs']['email']['readable'] ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.configs.email'); ?>
    <ul>
        <li><?php echo __d('appchecks', 'Checks.configs.isFile'); ?> : <?php echo $allChecks['cfgs']['email']['exists'] ? $this->element('ischeck') : $this->element('no'); ?></li>
        <li><?php echo __d('appchecks', 'Checks.configs.isReadable'); ?> : <?php echo $allChecks['cfgs']['email']['readable'] ? $this->element('ischeck') : $this->element('no'); ?></li>
    </ul>
</div>

<!--<h2>--><?php //echo __d('appchecks', 'Checks.pdfConvertTool'); ?><!--</h2>-->
<!--<p>-->
<!--    <span class="notice --><?php //echo $allChecks['pdfConvertTool']['ghostscript']['present'] ? 'success' : 'error-message'; ?><!--">-->
<!--        <i class="icon---><?php //echo $allChecks['pdfConvertTool']['ghostscript']['present'] ? 'ok' : 'remove'; ?><!--"></i> --><?php //echo __d('appchecks', 'Checks.pdfConvertTool.gs'); ?><!-- <span class="pull-right">--><?php //echo $allChecks['pdfConvertTool']['ghostscript']['present'] ? ' ' . __d('appchecks', 'Checks.pdfConvertTool.installed') : ' ' . __d('appchecks', 'Checks.pdfConvertTool.notInstalled'); ?><!--</span>-->
<!--    </span>-->
<!--</p>-->

<!--<h2>--><?php //echo __d('appchecks', 'Checks.webSocket'); ?><!-- <a href="--><?php //echo Router::url(array('controller' => 'Monitor')); ?><!--" class="pull-right btn btn-inverse"><i class="icon icon-search"></i> --><?php //echo __d('monitor', 'Monitor.monitor'); ?><!--</a></h2>-->
<div class="notice <?php echo $allChecks['webSocket']['wsServer']['installed'] && $allChecks['webSocket']['wsServer']['running'] && $allChecks['webSocket']['wsServer']['connected'] ? 'success' : 'error-message'; ?>">
    <i class="icon-<?php echo $allChecks['webSocket']['wsServer']['installed'] && $allChecks['webSocket']['wsServer']['running'] && $allChecks['webSocket']['wsServer']['connected'] ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.webSocket.server'); ?>
    <ul>
<!--        <li>--><?php //echo __d('appchecks', 'Checks.webService.server.installed'); ?><!-- : --><?php //echo $allChecks['webSocket']['wsServer']['installed'] ? $this->element('ischeck') : $this->element('no'); ?><!--</li>-->
<!--        <li>--><?php //echo __d('appchecks', 'Checks.webService.server.running'); ?><!-- : --><?php //echo $allChecks['webSocket']['wsServer']['running'] ? $this->element('ischeck') : $this->element('no'); ?><!--</li>-->
        <li><?php echo __d('appchecks', 'Checks.webService.server.running'); ?> : <?php echo $allChecks['webSocket']['wsServer']['connected'] ? $this->element('ischeck') : $this->element('no'); ?></li>
    </ul>
</div>

<h2><?php echo __d('appchecks', 'Checks.webservice.horodatage'); ?></h2>
<p>
    <span class="notice <?php echo $allChecks['horodatage'] ? 'success' : 'error-message'; ?>">
        <i class="icon-<?php echo $allChecks['horodatage'] ? 'ok' : 'remove'; ?>"></i> <?php echo __d('appchecks', 'Checks.horodatage'); ?> <span class="pull-right"><?php echo $allChecks['horodatage'] ? ' ' . __d('appchecks', 'Checks.horodatage.accessible') : ' ' . __d('appchecks', 'Checks.horodatage.inaccessible'); ?></span>
    </span>
</p>



<h2>Vérification de l'envoi des emails</h2>
<?php echo $this->Form->create(null, array(
   'url' => array('controller' => 'Essai', 'action' => 'checkMail'),

)); ?>

<div class="content">
<?php



echo $this->Form->input("Email.email",array(
        'label' =>" ",
        'placeholder' => 'email@exemple.fr'
    )
);
?>
<span class="pull-right">
<?php echo $this->form->end('Envoyer l\'email de test'); ?>
</span>
</div>




<?php echo $this->element('gotop'); ?>

<script type="text/javascript">
    $('#refresh').bind('click', function() {
        window.location.reload(true);
    });
</script>