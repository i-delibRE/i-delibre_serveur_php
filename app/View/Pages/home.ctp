<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
?>


<?php if (CakeSession::check('Auth.User.id')) { ?>
	<h1 class="homeTitle">
		<?php echo __d('default', 'welcome.logged'); ?>
		<div class="alert alert-info user-logged-message"><?php echo CakeSession::read('Auth.User.firstname') . ' ' . CakeSession::read('Auth.User.lastname'); ?></div>
	</h1>

	<!--	<a class="btn btn-large btn-inverse btn-action" href="/Environnements/menu">
			<i class="icon-th-large icon-white"></i>
	<?php echo __d('default', 'btn.menu'); ?>
		</a>
		<a class="btn btn-large btn-danger btn-action" href="/Srvusers/logout">
			<i class="icon-off icon-white"></i>
	<?php echo __d('default', 'btn.logout'); ?>
		</a>-->
<?php } else { ?>
	<h1 class="homeTitle">
		<?php echo __d('default', 'welcome.notlogged', MAINTITLE); ?>
	</h1>

	<p style="float:right;">Pour continuer, cliquez sur "<?php echo __d('default', 'btn.login'); ?>" &uarr;</p>
	<!--	<a class="btn btn-large btn-inverse btn-action" href="/Srvusers/login" data-context="params">
			<i class="icon-user icon-white"></i>
	<?php echo __d('default', 'btn.login'); ?>
		</a>-->

<?php } ?>

<img class="pull-right homeLogo" src="/img/logo_idelibre_server.png" style="clear: both;">
