<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('Appchecks', 'Utility');

/**
 * Pages/appchecks View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-05-22 16:05:09 +0200 (jeu. 22 mai 2014) $
 * $Revision: 431 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Pages/json/appchecks.ctp $
 * $Id: appchecks.ctp 431 2014-05-22 14:05:09Z ssampaio $
 */
echo json_encode(Appchecks::getAll());
?>
