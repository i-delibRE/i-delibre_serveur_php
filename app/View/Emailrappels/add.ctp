<div class="types form">
    <?php echo $this->Form->create('Emailrappel'); ?>
    <fieldset>
        <legend><?php echo "Ajout d'un type d'email"; ?></legend>
        <?php
            echo $this->Form->input('name', array("label" => "Type"));
            echo $this->Form->input('title', array("label" => "Titre"));
            echo $this->Form->input('content', array(
                "label" => "Message",
                "type" =>"textarea",
           ));
        ?>
    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

