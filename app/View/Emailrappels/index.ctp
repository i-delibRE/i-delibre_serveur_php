
<div>
    <h2 name="top">Emails de rappels</h2>

    <div class="table-header">
        <div class='control-group'>
            <a href='/emailrappels/add' class='btn btn-inverse'>
                <i class='icon-plus-sign icon-white'></i> Ajouter un message
</a>


        </div>
    </div>

    <div class="table-container">
        <table class='table table-hover table-bordered'>
            <tr>

                <th class="actions"><?php echo __('Intitulé'); ?>  </th>
<th class="actions"><?php echo __('Actions'); ?>  </th>

</tr>
<?php

foreach ($data as $item):
    ?>
    <tr>


        <td class ="actions span9">
            <?php echo $item['Emailrappel']['name']
            ?>
        </td>

        <td class="actions">
            <a
                data-toggle="tooltip"
                data-original-title="<?php echo __d('default', 'btn.edit'); ?>"
                class='tooltiped btn btn-inverse'
                href='<?php echo Router::url(array('action' => 'edit', $item['Emailrappel']['id'])); ?>'>
                <i class='fa fa-pencil icon-white'></i>
            </a>
            <?php
            echo $this->BootstrapBox->modalBtn('deleteModal', __d('default', 'btn.delete'), $this->Html->tag('i', '', array('class' => 'icon-trash icon-white')), 'btn-danger', Router::url(array('action' => 'delete', $item['Emailrappel']['id'])));
            ?>
        </td>
    </tr>
<?php endforeach; ?>
</table>
</div>
<div class="table-footer">
</div>
</div>

<a data-toggle="tooltip" data-placement="right" data-original-title="<?php echo __d('default', 'btn.gotop'); ?>" class='goTop' href="#top"><span class="triangle-up"></span></a>


<?php
$modalOptions = array(
    'id' => 'deleteModal',
    'title' => "Suppresion de message",
    'message' => "Confirmer" . $this->Html->tag('div', $this->Html->tag('i', '', array('class' => array('icon', 'icon-warning-sign'))) . ' ' . __d('groupepolitique', 'delete.confirm.message.tree'), array('class' => array('alert', 'alert-danger'))),
    'btn.ok' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-trash', 'icon-white'))) . ' ' . __d('default', 'btn.delete'),
    'btn.ok.url' => '/Emailrappels/index',
    'btn.cancel' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-remove-sign'))) . ' ' . __d('default', 'btn.cancel')
);
echo $this->BootstrapBox->modalBox($modalOptions);
?>