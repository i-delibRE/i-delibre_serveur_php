<div class="types form">
    <?php echo $this->Form->create('Emailrappel'); ?>
    <fieldset>
        <legend>Modification de message</legend>
        <?php

        echo $this->Form->input('id');

        echo $this->Form->input('name', array("label" => "Type"));
        echo $this->Form->input('title', array("label" => "Titre"));
        echo $this->Form->input('content', array(
            "label" => "Message",
            "type" =>"textarea",
        ));

        //echo $this->Form->input('User.User', array('type' => 'select', 'multiple' => 'checkbox'));
        ?>
    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

