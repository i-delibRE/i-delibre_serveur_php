
<div class="types form">
	<?php echo $this->Form->create('Groupepolitique'); ?>
	<fieldset>
		<legend><?php echo __d('groupepolitique', 'groupepolitique.add'); ?></legend>
		<?php
		echo $this->Form->input('name');
        echo $this->Form->input('selectAll', array(
            'type' => "checkbox",
            'label' => "Tout selectionner",
            'id' => 'selectActeurs',
            'checked' => false
        ));

        echo $this->Form->input('User.User', array('label' => 'Elus' ,'type' => 'select', 'multiple' => 'checkbox'));
		?>
	</fieldset>

<!--	--><?php //echo $this->Form->end("<span class='fa fa-floppy-o'></span> Enregistrer"); ?>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>


<script type="text/javascript">
    function switchDiv(checkbox, control) {
        if ($(checkbox).attr('checked') === "checked") {
            $(control).addClass('selected');
        } else {
            $(control).removeClass('selected');
        }
    }

    $('.controls div.input-xlarge input[type="checkbox"]').bind('change', function() {
        switchDiv(this, $(this).parent());
    });

    $('.controls div.input-xlarge').bind('click', function(e) {
        if ($(e.target).hasClass('input-xlarge')) {
            $('input[type="checkbox"]', $(e.target)).trigger('click');
        }
    });



    $('#selectActeurs').click(function(e){
        //var state = this.attr('selected');
        var ischecked = ($(this).is(':checked'));
        if(ischecked)
            selectAll();
        else
            unSelectAll();
    });


    function selectAll(){
        $('input:checkbox').prop('checked', true);
        $('input:checkbox').parent().addClass('selected');
    }

    function unSelectAll(){
        $('input:checkbox').prop('checked', false);
        $('input:checkbox').parent().removeClass('selected');
    }

</script>
