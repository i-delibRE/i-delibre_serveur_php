<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

echo $this->Html->css('pthemes');
?>


<div>
    <h2 name="top">Groupes Politiques</h2>

    <div class="table-header">
        <div class='control-group'>
            <a href='/groupepolitiques/add' class='btn btn-inverse' title="Ajouter un groupe">
                <i class='fa fa-plus-circle icon-white'></i> Ajouter un groupe
            </a>


        </div>
    </div>

    <div class="table-container">
        <table class='table table-hover table-bordered'>
            <tr>

                <th class="actions"><?php echo __('Intitulé'); ?>  </th>
                <th class="actions"><?php echo __('Actions'); ?>  </th>

            </tr>
            <?php
          
            foreach ($data as $item):
                ?>
                <tr>


                    <td class ="actions span9">
    <?php echo $item['Groupepolitique']['name']
    ?> 
                    </td>

                    <td class="actions">
                        <a
                            title="Modifier"
                            data-toggle="tooltip"
                            data-original-title="<?php echo __d('default', 'btn.edit'); ?>"
                            class='tooltiped btn btn-inverse'
                            href='<?php echo Router::url(array('action' => 'edit', $item['Groupepolitique']['id'])); ?>'>
                            <i class='fa fa-pencil icon-white'></i>
                        </a>
    <?php
    echo $this->BootstrapBox->modalBtn('deleteModal', __d('default', 'btn.delete'), $this->Html->tag('i', '', array('title'=>"Supprimer" , 'class' => 'fa fa-trash icon-white')), 'btn-danger', Router::url(array('action' => 'delete', $item['Groupepolitique']['id'])));
    ?>
                    </td>
                </tr>
                    <?php endforeach; ?>
        </table>
    </div>
    <div class="table-footer">
    </div>
</div>

<a data-toggle="tooltip" data-placement="right" data-original-title="<?php echo __d('default', 'btn.gotop'); ?>" class='goTop' href="#top"><span class="triangle-up"></span></a>


<?php
$modalOptions = array(
	'id' => 'deleteModal',
	'title' => __d('groupepolitique' , 'delete.confirm.title'),
	'message' => __d('groupepolitique', 'delete.confirm.message') . $this->Html->tag('div', $this->Html->tag('i', '', array('class' => array('icon', 'icon-warning-sign'))) . ' ' . __d('groupepolitique', 'delete.confirm.message.tree'), array('class' => array('alert', 'alert-danger'))),
	'btn.ok' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-trash', 'icon-white'))) . ' ' . __d('default', 'btn.delete'),
	'btn.ok.url' => '/groupepolitiques/index',
	'btn.cancel' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-remove-sign'))) . ' ' . __d('default', 'btn.cancel')
);
echo $this->BootstrapBox->modalBox($modalOptions);
?>