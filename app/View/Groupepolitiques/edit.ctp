


<div class="types form">
	<?php echo $this->Form->create('Groupepolitique'); ?>
	<fieldset>
		<legend><?php echo __d('groupepolitique', 'groupepolitique.edit'); ?></legend>
		<?php
                
		echo $this->Form->input('id');
		echo $this->Form->input('name');
        echo $this->Form->input('selectAll', array(
            'type' => "checkbox",
            'label' => "Tout selectionner",
            'id' => 'selectActeurs',
            'checked' => false
        ));
        ?>

        <div id="acteurs">
            <?php echo $this->Form->input('User.User', array('label' => "Acteurs",'type' => 'select', 'multiple' => 'checkbox')); ?>
        </div>

	</fieldset>
	<?php echo $this->Form->end(array('editMode' => true)); ?>
</div>



<script type="text/javascript">
    $('.controls').each(function() {
        if ($('div.input-xlarge', this).length > 0) {
            $(this).addClass('chkList');
        }
    })

    function switchDiv(checkbox, control) {
        if ($(checkbox).attr('checked') === "checked") {
            $(control).addClass('selected');
        } else {
            $(control).removeClass('selected');
        }
    }

    $('.controls div.input-xlarge input[type="checkbox"]').bind('change', function() {
        switchDiv(this, $(this).parent());
    });

    $('.controls div.input-xlarge').each(function() {
        switchDiv($('input[type="checkbox"]', $(this)), $(this));
    }).bind('click', function(e) {
        if ($(e.target).hasClass('input-xlarge')) {
            $('input[type="checkbox"]', $(e.target)).trigger('click');
        }
    });


    $('#selectActeurs').click(function(e){
        //var state = this.attr('selected');
        var ischecked = ($(this).is(':checked'));
        if(ischecked)
            selectAll();
        else
            unSelectAll();
    });


    function selectAll(){
        $('#acteurs input:checkbox').prop('checked', true);
        $('#acteurs input:checkbox').parent().addClass('selected');
    }


    function unSelectAll(){
        $('#acteurs input:checkbox').prop('checked', false);
        $('#acteurs input:checkbox').parent().removeClass('selected');
    }

    </script>