<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

echo $this->Html->css('collectivite_add');
?>
<div class="collectivites form" id="slides">
    <?php echo $this->Form->create('Collectivite', array('class' => 'form-horizontal')); ?>
<!--    <fieldset class="item">
        <legend><?php echo __d('collectivite', 'Collectivite.add.requirement'); ?></legend>
        <div class="content">
            <?php echo __d('collectivite', 'Collectivite.add.requirement.alert'); ?><br />
            <div class="well">
                <div id="configdb-div">
                    <input type="checkbox" required="required" id="configdb"><label for="configdb"><?php echo __d('collectivite', 'Collectivite.add.requirement.chk1'); ?></label>
                </div>
                <?php echo __d('collectivite', 'Collectivite.add.requirement.msg1'); ?>
            </div>

        </div>
    </fieldset>-->

    <fieldset class="item">
        <legend><?php echo __d('collectivite', 'Collectivite.add.collectivite'); ?></legend>
        <div class="content">
            <?php
            echo $this->Form->input('Collectivite.active', array('checked' => true, 'type' => 'hidden'));
            echo $this->Form->input('Collectivite.name');
            echo $this->Form->input('Collectivite.login_suffix');
     //       echo $this->Form->input('Collectivite.conn', array('type' => 'select', 'empty' => true));
        //    echo $this->Form->input('Collectivite.conn');
        //    echo $this->Form->input('Collectivite.bdd', array('label' => 'BDD', 'required' => 'required' ));
            ?>
        </div>
    </fieldset>

    <fieldset class="item">
        <legend><?php echo __d('collectivite', 'Collectivite.add.admin'); ?></legend>
        <div class="content">
            <?php
            echo $this->Form->input('User.firstname', array('required' => true));
            echo $this->Form->input('User.lastname', array('required' => true));
            echo $this->Form->input('User.mail', array('required' => true));
            echo $this->Html->tag('hr');
            echo $this->Form->input('User.username', array('required' => true));
            echo $this->Html->tag('hr');
            echo $this->Form->input('User.password', array('required' => true));
            echo $this->Form->input('User.confirm', array('type' => 'password', 'required' => true));
            ?>
        </div>
    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

<?php
$settings = array(
    'id' => 'modalNotMatch',
    'title' => __d('user', 'User.password.notmatch.title'),
    'message' => __d('user', 'User.password.notmatch.message')
);
echo $this->BootstrapBox->msgBox($settings);
?>

<script type="text/javascript">
    var confirmOk = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-ok').addClass('icon-white')).addClass('label').addClass('label-success');
    var confirmNok = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-remove').addClass('icon-white')).addClass('label').addClass('label-important');

    function checkPassConfirm() {
        if ($('#UserPassword').val() !== $('#UserConfirm').val() || $('#UserPassword').val() === '' || $('UserConfirm').val() === '') {
            $('#UserConfirmInfo').empty().append(confirmNok);
        } else {
            $('#UserConfirmInfo').empty().append(confirmOk);
        }
    }

    $('#UserConfirm').after($('<span></span>').attr('id', 'UserConfirmInfo').css('margin-left', '5px')).on('keyup', function() {
        checkPassConfirm();
    });

    $('#UserPassword').on('keyup', function() {
        checkPassConfirm();
    });

    $('#CollectiviteAddForm').on('submit', function(e) {
        if ($('#UserPassword').val() !== $('#UserConfirm').val()) {
            e.preventDefault();
            e.stopPropagation();
            $('#modalNotMatch').modal('show');
            $('#UserPassword').focus();
        }
    });
</script>