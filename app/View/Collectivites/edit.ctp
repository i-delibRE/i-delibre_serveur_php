<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

?>
<div class="collectivites form">
    <?php echo $this->Form->create('Collectivite'); ?>
    <fieldset>
        <legend><?php echo __d('collectivite', 'Collectivite.edit'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('active');
        echo $this->Form->input('name');
        //echo $this->Form->input('Collectivite.login_suffix');
        ?>
        <dl>
              <dt><?php echo __d('collectivite', 'Collectivite.login_suffix'); ?></dt><dd><?php echo $this->data['Collectivite']['login_suffix']; ?></dd>
            <dt><?php echo __d('collectivite', 'Collectivite.conn'); ?></dt><dd><?php echo $this->data['Collectivite']['conn']; ?></dd>
        </dl>
        <?php ?>
    </fieldset>
    <?php echo $this->Form->end(array('editMode' => true)); ?>
</div>

