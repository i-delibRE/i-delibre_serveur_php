<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

?>
<?php
echo $this->Form->create('Collectivite');
echo $this->Form->input('Collectivite.id');
?>
<fieldset>
    <legend><?php echo __d('collectivite', 'Collectivite.info'); ?></legend>
    <?php
    echo $this->Form->input('active');
    echo $this->Form->input('name');
    ?>
    <dl>
        <dt><?php echo __d('collectivite', 'Collectivite.login_suffix'); ?></dt><dd><?php echo $this->data['Collectivite']['login_suffix']; ?></dd>
        <dt><?php echo __d('collectivite', 'Collectivite.conn'); ?></dt><dd><?php echo $this->data['Collectivite']['conn']; ?></dd>
        <dt><?php echo __d('collectivite', 'Collectivite.created'); ?></dt><dd><?php echo h(CakeTime::i18nFormat($this->data['Collectivite']['created'], '%x %X')); ?></dd>
    </dl>
</fieldset>
<?php echo $this->Form->end("<span class='fa fa-floppy-o'></span> "  .__d('default', 'btn.save')); ?>