<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */
?>
<h2><?php echo __d('error', $name); ?></h2>
<p class="alert alert-danger">
	<strong><?php echo __d('error', 'Error'); ?>: </strong>
	<?php echo __d('error', 'An Internal Error Has Occurred.'); ?>
</p>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
?>
