<?php
/**
 * Elements/bootstrap_flash Element
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-11-08 10:58:35 +0100 (ven. 08 nov. 2013) $
 * $Revision: 316 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Elements/bootstrap_flash.ctp $
 * $Id: bootstrap_flash.ctp 316 2013-11-08 09:58:35Z ssampaio $
 */
?>
<div id="flashMessage" data-autoclose="yes" class="<?php echo $class; ?> fade in">
	<a class="close" data-dismiss="alert" href="#">&times;</a>
	<?php echo $message; ?>
	<span class="pull-right">
		<span class="first"><i class="icon-time"></i> <span class="secInfo">3</span></span>
		<span class="second"><i class="icon-time"></i> <span class="secInfo">2</span></span>
		<span class="third"><i class="icon-time"></i> <span class="secInfo">1</span></span>
	</span>
</div>

<script type="text/javascript">
	$('#flashMessage').alert().click(function() {
		$(this).attr('data-autoclose', 'no');
		$('#flashMessage .first').css('visibility', 'hidden');
		$('#flashMessage .second').css('visibility', 'hidden');
		$('#flashMessage .third').css('visibility', 'hidden');
	});

	setTimeout(function() {
		$('#flashMessage .first').css('display', 'none');
		$('#flashMessage .second').css('display', 'inline');
		setTimeout(function() {
			$('#flashMessage .second').css('display', 'none');
			$('#flashMessage .third').css('display', 'inline');
			setTimeout(function() {
				$('#flashMessage .third').css('visibility', 'hidden');
				setTimeout(function() {
					if ($('#flashMessage').attr('data-autoclose') == 'yes') {
						$('#flashMessage').alert('close');
					}
				}, 1);
			}, 1000);
		}, 1000);
	}, 1000);
</script>