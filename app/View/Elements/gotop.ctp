<a data-toggle="tooltip" data-placement="right" data-original-title="<?php echo __d('default', 'btn.gotop'); ?>" class='goTop' href="#top">
	<span class="triangle-up">
		<i class="icon icon-arrow-up icon-2x icon-white"></i>
	</span>
</a>

<script type="text/javascript">
	//action du bouton de retour en haut de page
	$('.goTop').hide();
	$(window).scroll(function() {
		if ($(window).scrollTop() > 0) {
			$('.goTop').show().addClass('hover');
		} else {
			$('.goTop').hide().removeClass('hover');
		}
	});
	$('.goTop').tooltip({delay: {show: 300, hide: 0},
		container: $('.goTop')
	});
</script>


