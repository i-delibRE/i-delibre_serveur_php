<?php

/**
 * Elements/inprogress Element
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Elements/inprogress.ctp $
 * $Id: inprogress.ctp 302 2013-10-21 15:57:34Z ssampaio $
 */
echo $this->Html->tag('span', '<i class="icon-refresh icon-white"></i>', array('class' => 'label label-warning')) . ' ' . (!empty($msg) ? $msg : __d('default', 'In Progress'));
?>
