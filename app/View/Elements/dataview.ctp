
<?php
/**
 * Elements/dataview Element
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-08-11 15:26:57 +0200 (lun. 11 août 2014) $
 * $Revision: 553 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Elements/dataview.ctp $
 * $Id: dataview.ctp 553 2014-08-11 13:26:57Z ssampaio $
 */
echo $this->Html->css('/vendors/bootstrap-datepicker/datepicker');
echo $this->Html->script('globalHelper');
echo $this->Html->script('/vendors/bootstrap-datepicker/bootstrap-datepicker');

if (empty($forbiddenActions)) {
    $forbiddenActions = array(
        'add' => false,
        'edit' => false,
        'delete' => false
    );
} else {
    $forbiddenActions['add'] = isset($forbiddenActions['add']) ? $forbiddenActions['add'] : false;
    $forbiddenActions['edit'] = isset($forbiddenActions['edit']) ? $forbiddenActions['edit'] : false;
    $forbiddenActions['delete'] = isset($forbiddenActions['delete']) ? $forbiddenActions['delete'] : false;
}
?>

<div class="<?php echo $controller; ?> <?php echo $action; ?>">

    <h1><?php echo $title; ?></h1>

    <div class="table-header">
        <div class='control-group'>
            <?php if (!empty($addurl) && !empty($addtitle) && !$forbiddenActions['add']) { //si les données sont renseignées et si l'ajout n'est pas désactivé ?>
                <a href='<?php echo $addurl; ?>' class='btn btn-inverse'>
                    <i class='fa  fa-plus-circle icon-white'></i> <?php echo $addtitle; ?>
                </a>
            <?php } else { ?>
                <a href='#' class='btn btn-inverse disabled' style="visibility:hidden;"></a>
            <?php } ?>
            <span class="pull-right">
                <form class="input-prepend input-append search-form" action="<?php echo Router::url(array('action' => 'index')); ?>" method="post">
                    <div class="btn-group">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">
                            <span class="search-field-label"><?php echo __d('default', 'search.field'); ?></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu search-list">
                            <?php
                            foreach ($fields as $field => $conf) {
                                if (($field == 'id' && Configure::read('debug') > 0) || $field != 'id') {
                                    $type = "text";
                                    if (!empty($conf['boolean'])) {
                                        $type = "boolean";
                                    } else if (!empty($conf['foreignkey'])) {
                                        $type = "foreignkey";
                                    } else if (!empty($conf['datetime'])) {
                                        $type = "datetime";
                                    }
                                    echo $this->Html->tag('li', $this->Html->tag('a', $conf['label'], array('href' => '#')), array('data-field' => $field, 'data-type' => $type, 'class' => !empty($conf['class']) ? $conf['class'] : array()));
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <input class="search-show" type="hidden" name="show" value="">
                    <input class="search-field" type="hidden" name="field" value="<?php echo __d('default', 'search.field'); ?>">
                    <input class="span2 search-type" type="hidden" name="search-type">
                    <input class="span2 search-text" type="text" name="search-text">

                    <div class="search-boolean">
                        <input type="hidden" name="search-boolean" value="1">
                        <div class="btn-group">
                            <button class="btn dropdown-toggle" data-toggle="dropdown">
                                <span class="yesNo"><?php echo $this->element('yes'); ?></span>
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu">
                                <li class="yesSearch" data-search-boolean="1"><a href="#"><?php echo $this->element('yes'); ?></a></li>
                                <li class="noSearch" data-search-boolean="0"><a href="#"><?php echo $this->element('no'); ?></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="search-foreignkey">
                        <input type="hidden" name="search-foreignkey" value="">
                        <input type="hidden" name="search-foreignkey-name" value="">
                        <div class="btn-group">
                            <button class="btn dropdown-toggle" data-toggle="dropdown">
                                <span class="fkchoice"><?php echo __d('default', 'search.fk.choice'); ?></span>
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu">
                                <?php
                                foreach ($fields as $field => $conf) {
                                    if (!empty($conf['foreignkey'])) {
                                        foreach ($conf['select'] as $id => $name) {
                                            ?><li class="fk <?php echo $field; ?>" data-search-foreignkey="<?php echo $id; ?>"><a href="#"><?php echo $name; ?></a></li><?php
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="input-prepend input-append search-datetime-container">
                        <span class="add-on"><?php echo __d('default', 'search.date.from'); ?></span>
                        <input class="span2 search-datetime" type="text" name="search-datetime" id="search-datetime" data-date-format="dd/mm/yyyy" data-date-weekStart="1">
                        <span class="add-on"><i class="icon-calendar"></i></span>
                    </div>
                    <div class="input-prepend input-append search-datetime-to-container">
                        <span class="add-on"><?php echo __d('default', 'search.date.to'); ?></span>
                        <input class="span2 search-datetime-to" type="text" name="search-datetime-to" id="search-datetime-to" data-date-format="dd/mm/yyyy" data-date-weekStart="1">
                        <span class="add-on"><i class="icon-calendar"></i></span>
                    </div>
                    <button  data-toggle="tooltip" data-original-title="<?php echo __d('default', 'btn.search'); ?>" class="btn btn-inverse tooltiped" type="submit"><i class=" fa fa-filter icon-white"></i></button>
                </form>
                <span data-toggle="tooltip" data-original-title="<?php echo __d('default', 'btn.table.td.more'); ?>" class="btn btn-more btn-inverse tooltiped"><i class="fa fa-expand  icon-white"></i></span>
                <span data-toggle="tooltip" data-original-title="<?php echo __d('default', 'btn.table.td.less'); ?>" class="btn btn-less btn-inverse tooltiped"><i class="fa fa-compress icon-white"></i></span>
            </span>
        </div>
        <?php if (!empty($critere)) { ?>
        <a href="<?php echo Router::url(array('action' => 'index', true)); ?>" class="btn btn-inverse" title="Réinitialiser"><span class="fa fa-undo" ></span> </a> <?php echo $critere; ?><br />

        <?php } ?>

    </div>

    <div class="table-container">
        <?php if (empty($data)) { ?>
            <div class="alert alert-info text-center"><?php echo __d('default', 'list.void'); ?></div>
        <?php } else { ?>
            <table class='table table-hover table-bordered'>
                <tr>
                    <?php
                    foreach ($fields as $field => $conf) {
                        if (($field == 'id' && Configure::read('debug') > 0) || $field != 'id') {
                            echo $this->Html->tag('th', $this->Paginator->sort($field, $conf['label']), array('class' => !empty($conf['class']) ? $conf['class'] : array()));
                        }
                    }
                    ?>
                    <?php if (!$forbiddenActions['edit'] || !$forbiddenActions['delete']) { ?>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    <?php } ?>
                </tr>
                <?php foreach ($data as $item): ?>



                    <tr>
                        <?php
                        foreach ($fields as $field => $conf) {
                            if (($field == 'id' && Configure::read('debug') > 0) || $field != 'id') {
                                if (!empty($conf['foreignkey'])) {
                                    //echo $this->Html->tag('td', $this->Html->link($item[$conf['linkedModel']][$conf['linkedDisplayField']], array('controller' => $conf['linkedController'], 'action' => $conf['linkedAction'], $item[$conf['linkedModel']]['id'])));
                                    echo $this->Html->tag('td', $item[$conf['linkedModel']][$conf['linkedDisplayField']]);
                                } else if (!empty($conf['datetime'])) {
                                    if (empty($item[$model][$field])) {
                                        echo $this->Html->tag('td', '', array('class' => !empty($conf['class']) ? $conf['class'] : array()));
                                    } else {
                                        echo $this->Html->tag('td', h(CakeTime::i18nFormat($item[$model][$field], '%x %X')), array('class' => !empty($conf['class']) ? $conf['class'] : array()));
                                    }
                                } else if (!empty($conf['boolean'])) {
                                    echo $this->Html->tag('td', $item[$model][$field] ? $this->element('yes') : $this->element('no'), array('class' => !empty($conf['class']) ? $conf['class'] : array()));
                                } else {
                                    echo $this->Html->tag('td', h($item[$model][$field]), array('class' => !empty($conf['class']) ? $conf['class'] : array()));
                                }
                            }
                        }
                        ?>

                        <?php if (!$forbiddenActions['edit'] || !$forbiddenActions['delete']) { ?>
                            <td class="actions">
                                <?php if (!$forbiddenActions['edit']) { //si l'édition n'est pas désactivée?>
                                    <a
                                        data-toggle="tooltip"
                                        data-original-title="<?php echo __d('default', 'btn.edit'); ?>"
                                        class='tooltiped btn btn-inverse'
                                        href='<?php echo Router::url(array('action' => 'edit', $item[$model]['id'])); ?>'>
                                        <i class='fa fa-pencil icon-white'></i>
                                    </a>
                                <?php } ?>
                                <?php
                                if (!$forbiddenActions['delete']  //si la suppression n'est pas désactivée
                                        && (empty($item['User']) || empty($item['Group']) || empty($item['Group']['admin']))) { //on ne supprime pas les utilisateur ayant droit d administration
                                    echo $this->BootstrapBox->modalBtn('deleteModal', __d('default', 'btn.delete'), $this->Html->tag('i', '', array('class' => 'fa fa-trash icon-white')), 'btn-danger', Router::url(array('action' => 'delete', $item[$model]['id'])));
                                }
                                ?>
                                <?php if (!empty($otherAction)) { ?>
                                    <a
                                        data-toggle="tooltip"
                                        data-original-title="<?php echo $otherAction['title']; ?>"
                                         class='<?php echo $otherAction['btn']  ?>'
                                        href='<?php echo $otherAction['link'] . $item[$model]['id']  ; ?>'>
                                        <i class='<?php echo $otherAction['icon'] ; ?>'></i>
                                    </a>

                                <?php }
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php } ?>
    </div>

    <div class="table-footer">
        <div>
            <?php
            echo $this->Paginator->counter(array(
                'format' => Configure::read('debug') > 0 ? __d('default', 'Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}') : __d('default', 'Page {:page} of {:pages}')
            ));
            ?>
        </div>
        <div class="paging pagination">
            <ul>
                <?php
                echo $this->Paginator->prev(' < ' . __d('default', 'prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
                echo $this->Paginator->next(__d('default', 'next') . ' > ', array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
</div>



<?php
$modalOptions = array(
    'id' => 'deleteModal',
    'title' => __d(Inflector::underscore($model), $model . '.delete.confirm.title'),
    'message' => __d(Inflector::underscore($model), $model . '.delete.confirm.message'),
    'btn.ok' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-trash', 'icon-white'))) . ' ' . __d('default', 'btn.delete'),
    'btn.ok.url' => $deleteUrl,
    'btn.cancel' => $this->Html->tag('i', '', array('class' => array('icon', 'icon-remove-sign'))) . ' ' . __d('default', 'btn.cancel')
);
echo $this->BootstrapBox->modalBox($modalOptions);
?>

<script type='text/javascript'>


    //gestion de l'affichage du nombre de colone
    function showmore() {
        $('.uuid, .datetime').addClass('show');
        $('.btn-more').hide();
        $('.btn-less').show();
        $('.pagination a').each(function () {
            if (typeof $(this).attr('href') != 'undefined' && $(this).attr('href') != '' && $(this).attr('href').search('/show:more') == -1) {
                $(this).attr('href', $(this).attr('href') + '/show:more');
            }
        });
        $('.search-show').val('more');
    }

    function showless() {
        $('.uuid, .datetime').removeClass('show');
        $('.btn-less').hide();
        $('.btn-more').show();
        $('.pagination a').each(function () {
            if (typeof $(this).attr('href') != 'undefined' && $(this).attr('href') != '') {
                $(this).attr('href', $(this).attr('href').replace('/show:more', ''));
            }
        });
        $('.search-show').val('');
    }

    //affichage par défaut du nombre de colone
<?php if (isset($this->params['named']) && isset($this->params['named']['show']) && ($this->params['named']['show'] == "more" || $this->data['show'] == 'more')) { ?>
        showmore();
<?php } else { ?>
        showless();
<?php } ?>

    //action des boutons de changement du nombre de colone
    $('.btn-more').click(function () {
        showmore();
    });
    $('.btn-less').click(function () {
        showless();
    });

    //initialisation des tooltips
    $('.tooltiped').tooltip();


    //set datepickers
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('.search-datetime').datepicker('setValue', now).on('changeDate', function (ev) {
        checkout.setValue(ev.date);
        checkin.hide();
    }).data('datepicker');

    var checkout = $('.search-datetime-to').datepicker({
        onRender: function (date) {
            return date.valueOf() < checkin.date.valueOf('setValue', now) ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');


    //gestion de l'affichage des options de recherche
    function showSearchText() {
        $('.search-boolean').hide();
        $('.search-datetime-container').hide();
        $('.search-datetime-to-container').hide();
        $('.search-foreignkey').hide();
        $('.search-foreignkey').attr('data-check', false);
        $('.search-text').attr('required', 'required');
        $('.search-text').show();
        $('.search-type').val('search-text');
    }

    function showSearchBoolean() {
        $('.search-text').removeAttr('required');
        $('.search-text').hide();
        $('.search-datetime-container').hide();
        $('.search-datetime-to-container').hide();
        $('.search-foreignkey').hide();
        $('.search-foreignkey').attr('data-check', false);
        $('.search-boolean').show();
        $('.search-type').val('search-boolean');
    }

    function showSearchDatetime() {
        $('.search-text').removeAttr('required');
        $('.search-text').hide();
        $('.search-boolean').hide();
        $('.search-foreignkey').hide();
        $('.search-foreignkey').attr('data-check', false);
        $('.search-datetime-container').show();
        $('.search-datetime-to-container').show();
        $('.search-type').val('search-datetime');
    }

    function showForeignkey() {
        $('.search-text').removeAttr('required');
        $('.search-text').hide();
        $('.search-datetime-container').hide();
        $('.search-datetime-to-container').hide();
        $('.search-boolean').hide();
        $('.search-foreignkey').show();
        $('.search-foreignkey').attr('data-check', true);
        $('.search-type').val('search-foreignkey');
    }

    //affichage par défaut
    showSearchText();

    //choix du champ
    $('.search-list li').click(function () {
        $('.search-field-label').parent('button').removeClass('btn-danger');

        $('.search-field-label').html($('a', this).html());
        $('.search-field').val($(this).attr('data-field'));
        if ($(this).attr('data-type') === 'boolean') {
            showSearchBoolean();
        } else if ($(this).attr('data-type') === 'foreignkey') {
            $('.search-foreignkey li.fk').hide();
            $('.search-foreignkey li.' + $(this).attr('data-field')).show();
            $('.search-foreignkey input[name=search-foreignkey]').val('');
            $('.search-foreignkey input[name=search-foreignkey-name]').val('');
            $('.search-foreignkey button').removeClass('btn-danger');
            $('.fkchoice').html('<?php echo __d('default', 'search.fk.choice'); ?>');
            showForeignkey();
        } else if ($(this).attr('data-type') === 'datetime') {
            showSearchDatetime();
        } else {
            showSearchText();
        }
    });

    //choix de la valeur d'un champ boolean
    $('.yesSearch, .noSearch').click(function () {
        $('.yesNo').html($("a", this).html());
        $('.search-boolean input[name=search-boolean]').val($(this).attr('data-search-boolean'));
    });

    //choix de la valeur d'un champ foreignkey
    $('.fk').click(function () {
        $('.search-foreignkey button').removeClass('btn-danger');
        $('.search-foreignkey input[name=search-foreignkey]').val($(this).attr('data-search-foreignkey'));
        $('.search-foreignkey input[name=search-foreignkey-name]').val($('a', this).html());
        $('.fkchoice').html($('a', this).html());
    });


    //validation du formulaire de recherche
    $('.search-form button[type=submit]').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        //verification de la selection du champ
        if ($('.search-field-label').html() === "<?php echo __d('default', 'search.field'); ?>") {
            $('.search-field-label').parent('button').addClass('btn-danger');
        } else {
            //verification de la selection de la valeur pour les champs foreignkey
            if ($('.search-foreignkey').attr('data-check') === "true" && $('.fkchoice').html() === "<?php echo __d('default', 'search.fk.choice'); ?>") {
                $('.search-foreignkey button').addClass('btn-danger');
            } else {
                //verification de la validité du formulaire (champ text required)
                if ($('.search-form')[0].checkValidity()) {
                    $('.search-form').submit();
                } else {
                    $('.search-form input[required]').focus();
                }
            }
        }
    });

</script>