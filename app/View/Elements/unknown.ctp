<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

echo $this->Html->tag('span', '<i class="icon-pause icon-white"></i>', array('class' => 'label label-warning'));
?>

