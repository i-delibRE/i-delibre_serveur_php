<?php
/**
 * Environnements/menu View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Environnements/menu.ctp $
 * $Id: menu.ctp 302 2013-10-21 15:57:34Z ssampaio $
 */
$menu = CakeSession::read('Auth.envData.menu');
?>

<div id="menu">
	<?php foreach ($menu as $name => $item) { ?>
		<a href="<?php echo $item['url']; ?>" class="<?php echo $item['class']; ?>">

            <i class="icon <?php echo $item['icon-class']; ?>"></i> <?php echo $name; ?>
		</a>
	<?php } ?>
</div>
