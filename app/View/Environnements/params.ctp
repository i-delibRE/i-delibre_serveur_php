<?php
/**
 * Environnements/params View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-11-25 11:35:47 +0100 (lun. 25 nov. 2013) $
 * $Revision: 328 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Environnements/params.ctp $
 * $Id: params.ctp 328 2013-11-25 10:35:47Z ssampaio $
 */
echo $this->Html->css(array('params'));
$userModel = $srvuser ? 'Srvuser' : 'User';
$poFile = Inflector::underscore($userModel);
?>

<h1><?php echo __d('params', 'Params.title'); ?></h1>
<div class="row-fluid">
	<div class="span7">
		<?php
		echo $this->Form->create($userModel);
		echo $this->Form->input('id');
		?>
		<!--		<fieldset>
					<legend><i class="icon icon-user"></i> <?php echo __d('params', 'account.info'); ?></legend>
		<?php
		echo $this->Form->input('username');
		?>
				</fieldset>-->
		<fieldset>
			<legend><i class="fa fa-user"></i> <?php echo __d('params', 'account.personal'); ?></legend>
			<?php
			echo $this->Form->input('firstname');
			echo $this->Form->input('lastname');
			echo $this->Form->input('mail');
			?>
		</fieldset>

		<!-- Language real input-->
		<!--		<fieldset>
					<legend><i class="icon icon-wrench"></i> <?php echo __d('params', 'account.conf'); ?></legend>
					<div class="control-group">
						<label class="control-label" for="language">Language</label>
						<div class="controls">
							<select id="language" class='input-xlarge'>
								<option>1</option>
								<option>2</option>
								<option>3</option>
							</select>
						</div>
					</div>
				</fieldset>-->
		<button class="btn btn-inverse" title="Enregistrer"><span class="fa fa-floppy-o"></span> <?php echo __d('default', 'btn.save'); ?></button>
		<?php echo $this->Form->end(); ?>

		<!--<form id="chpasswd" action="<?php echo Router::url(array('action' => 'params')); ?>" method="post" class='form-horizontal'>-->
		<?php echo $this->Form->create($userModel, array('id' => 'chpasswd')); ?>
		<fieldset>
			<legend><i class="fa fa-lock"></i> <?php echo __d('params', 'account.password.reset'); ?></legend>
			<?php
			echo $this->Form->input('id', array('type' => 'hidden'));
			echo $this->Form->input('password.old', array('empty' => true, 'required' => true, 'name' => 'data[' . $userModel . '][password_old]', 'type' => 'password'));
			echo $this->Form->input('password', array('empty' => true, 'required' => true));
			echo $this->Form->input('password.confirm', array('empty' => true, 'required' => true, 'type' => 'password', 'name' => 'data[' . $userModel . '][confirm]'));
			?>
		</fieldset>
		<button type="submit" class="btn btn-inverse" title="Enregistrer"> <span class="fa fa-floppy-o"></span> <?php echo __d('default', 'btn.save'); ?></button>
		<!--</form>-->
		<?php echo $this->Form->end(); ?>


		<?php
		$settings = array(
			'id' => 'modalNotMatch',
			'title' => __d('srvuser', $userModel . '.password.notmatch.title'),
			'message' => __d('srvuser', $userModel . '.password.notmatch.message')
		);
		echo $this->BootstrapBox->msgBox($settings);
		?>

		<script type="text/javascript">

			var confirmOk = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-ok').addClass('icon-white')).addClass('label').addClass('label-success');
			var confirmNok = $('<span></span>').append($('<i></i>').addClass('icon').addClass('icon-remove').addClass('icon-white')).addClass('label').addClass('label-important');

			function checkPassConfirm() {
				if ($('#<?php echo $userModel; ?>Password').val() !== $('#passwordConfirm').val() || $('#<?php echo $userModel; ?>Password').val() === '' || $('passwordConfirm').val() === '') {
					$('#passwordConfirmInfo').empty().append(confirmNok);
				} else {
					$('#passwordConfirmInfo').empty().append(confirmOk);
				}
			}

			$('#passwordConfirm').after($('<span></span>').attr('id', 'passwordConfirmInfo').css('margin-left', '5px')).on('keyup', function() {
				checkPassConfirm();
			});

			$('#<?php echo $userModel; ?>Password').on('keyup', function() {
				checkPassConfirm();
			});

			$('#chpasswd').on('submit', function(e) {
				if ($('#<?php echo $userModel; ?>Password').val() !== $('#passwordConfirm').val()) {
					e.preventDefault();
					e.stopPropagation();
					$('#modalNotMatch').modal('show');
					$('#<?php echo $userModel; ?>Password').focus();
				}
			});
//
//			$('#chpasswd button[type=submit]').click(function(e) {
//				e.preventDefault();
//				e.stopPropagation();
//				if ($("#confirm").val() !== $('#new').val()) {
//					$('#passwordMismatch').modal('show');
//					$('#confirm').focus();
//				} else {
//					if ($(this).parent('form')[0].checkValidity()) {
//						$(this).parent('form').submit();
//					} else {
//						var stop = false;
//						$('input[type=password]', $(this).parent('form')).each(function() {
//							if (!stop && $(this)[0].checkValidity() === false) {
//								$(this).focus();
//								stop = true;
//							}
//						});
//					}
//				}
//			});
		</script>
	</div>
	<div class="span4 offset1">
		<h2><i class="fa fa-info"></i> <?php echo __d('params', 'account.info'); ?></h2>
		<dl>
			<dt><?php echo __d($poFile, $userModel . '.username'); ?></dt><dd><?php echo $this->data[$userModel]['username']; ?></dd>
			<dt><?php echo __d($poFile, $userModel . '.created'); ?></dt><dd><?php echo!empty($this->data[$userModel]['created']) ? h(CakeTime::i18nFormat($this->data[$userModel]['created'], '%x %X')) : __d('default', 'date.void'); ?></dd>
			<dt><?php echo __d($poFile, $userModel . '.modified'); ?></dt><dd><?php echo!empty($this->data[$userModel]['modified']) ? h(CakeTime::i18nFormat($this->data[$userModel]['modified'], '%x %X')) : __d('default', 'date.void'); ?></dd>
			<dt><?php echo __d($poFile, $userModel . '.last_login'); ?></dt><dd><?php echo!empty($this->data[$userModel]['last_login']) ? h(CakeTime::i18nFormat($this->data[$userModel]['last_login'], '%x %X')) : __d('default', 'date.void'); ?></dd>
			<dt><?php echo __d($poFile, $userModel . '.last_logout'); ?></dt><dd><?php echo!empty($this->data[$userModel]['last_logout']) ? h(CakeTime::i18nFormat($this->data[$userModel]['last_logout'], '%x %X')) : __d('default', 'date.void'); ?></dd>
			<?php if (!$srvuser) { ?>
				<dt><?php echo __d($poFile, $userModel . '.last_import'); ?></dt><dd><?php echo!empty($this->data[$userModel]['last_import']) ? h(CakeTime::i18nFormat($this->data[$userModel]['last_import'], '%x %X')) : __d('default', 'date.void'); ?></dd>
			<?php } ?>
		</dl>

<!--		<h2><i class="fa fa-info"></i> --><?php //echo __d('params', 'account.info.group'); ?><!--</h2>-->
<!--		<dl>-->
<!--			--><?php //if (!$srvuser) { ?>
<!--				<dt>--><?php //echo __d('group', 'Group.name'); ?><!--</dt><dd>--><?php //echo $this->data['Group']['name']; ?><!--</dd>-->
<!--				<dt>--><?php //echo __d('group', 'Group.admin'); ?><!--</dt><dd>--><?php //echo $this->data['Group']['admin'] ? $this->element('yes') : $this->element('no'); ?><!--</dd>-->
<!--				<dt>--><?php //echo __d('group', 'Group.superuser'); ?><!--</dt><dd>--><?php //echo $this->data['Group']['superuser'] ? $this->element('yes') : $this->element('no'); ?><!--</dd>-->
<!--				<dt>--><?php //echo __d('group', 'Group.baseuser'); ?><!--</dt><dd>--><?php //echo $this->data['Group']['baseuser'] ? $this->element('yes') : $this->element('no'); ?><!--</dd>-->
<!--				<dt>--><?php //echo __d('group', 'Group.inseance'); ?><!--</dt><dd>--><?php //echo $this->data['Group']['inseance'] ? $this->element('yes') : $this->element('no'); ?><!--</dd>-->
<!---->
<!--			--><?php //} else { ?>
<!--				<dt>--><?php //echo __d('srvrole', 'Srvrole.name'); ?><!--</dt><dd>--><?php //echo $this->data['Srvrole']['name']; ?><!--</dd>-->
<!--			--><?php //} ?>
<!--		</dl>-->

	</div>
</div>