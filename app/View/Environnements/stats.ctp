<?php
/**
 * Environnements/stats View
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-06-05 17:16:20 +0200 (jeu. 05 juin 2014) $
 * $Revision: 447 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/View/Environnements/stats.ctp $
 * $Id: stats.ctp 447 2014-06-05 15:16:20Z ssampaio $
 */
echo $this->Html->css(array('stats'));
?>



<?php foreach ($seances as $seance) { ?>




    <table>
        <caption><?php echo $seance['Seance']['name'] . ' du ' . h(CakeTime::i18nFormat($seance['Seance']['date_seance'], '%x %X')); ?></caption>
        <tr>
            <th><?php echo __d('stats', 'Stats.convocs.users'); ?></th>
            <th><?php echo __d('stats', 'Stats.convocs.active'); ?></th>
            <th><?php echo __d('stats', 'Stats.convocs.ae_date'); ?></th>
            <th><?php echo __d('stats', 'Stats.convocs.read'); ?></th>
            <th><?php echo __d('stats', 'Stats.convocs.ar_date'); ?></th>
            <th><?php echo __d('convocation', 'Convocation.present_status'); ?></th>

        </tr>
        <?php foreach ($seance['Convocation'] as $convoc) { ?>
            <tr>


                <?php $convoc['active'] = !empty($convoc['ae_horodatage']); ?>

                <td><?php echo $convoc['User']['name']; ?></td>
                <td><?php echo $convoc['active'] ? $this->element('yes') : $this->element('no'); ?></td>

                <?php
                $ae_date = '';

                if (!empty($convoc['ae_horodatage'])) {
                    $ae_date = h(CakeTime::i18nFormat($convoc['ae_horodatage'], '%x %X')) . ' ' . $this->Html->tag('i', '', array('title' => __d('stats', 'tokendate'), 'class' => array('icon', ' icon-certificate')));
                } else if (!empty($convoc['ae_sent'])) {
                    $ae_date = h(CakeTime::i18nFormat($convoc['ae_sent'], '%x %X'));
                }
                ?>
                <td><?php echo $ae_date; ?></td>
                <td><?php echo $convoc['read'] ? $this->element('yes') : $this->element('no'); ?></td>

                <?php
                $ar_date = '';
                if (!empty($convoc['ar_horodatage'])) {
                    $ar_date = h(CakeTime::i18nFormat($convoc['ar_horodatage'], '%x %X')) . ' ' . $this->Html->tag('i', '', array('title' => __d('stats', 'tokendate'), 'class' => array('icon', ' icon-certificate')));
                } else if (!empty($convoc['ar_sent'])) {
                    $ar_date = h(CakeTime::i18nFormat($convoc['ar_sent'], '%x %X'));
                }
                ?>
                <td><?php echo $ar_date; ?></td>


                <td>
                    <?php
                    $pesenceIcon =  '<span class="fa fa-clock-o label label-warning"> </span>';

                    if($convoc['presentstatus'] == 'present'){
                        $pesenceIcon = $this->element('yes');
                    }
                    else if($convoc['presentstatus'] == 'absent'){
                        $pesenceIcon = $this->element('no');
                    }

                    echo $pesenceIcon;

                    ?>

                </td>

            </tr>
        <?php } ?>
    </table>
<?php } ?>
