
<h1>Import d'utilisateurs via fichier csv</h1>
<div class="well">
    <p>Information : Votre csv doit etre contruit de la maniere suivante :
    <ul>
        <li> Le séparateur est le caractère "<b>,</b>" </li>
        <li> champ 1 : Login </li>
        <li> champ 2 : Prenom </li>
        <li> champ 3 : Nom </li>
        <li> champ 4 : Email </li>
        <li> champ 5 : Mot de passe </li>
        <li> champ 6 : Type d'utilisateur
            <ul>
                <li>Secretaire : 1</li>
                <li>Administrateur : 2</li>
                <li>Acteur : 3</li>
                <li>Administratif : 4</li>
                <li>Invité : 5</li>
            </ul>
        </li>
        <li>Ne pas mettre les noms des colonnes mais directement les utilisateurs</li>

    </ul>

    </p>
</div>

<div class="well">
    <?php



    echo $this->Form->create(false, array(
        'url' => array('controller' => 'Importusers', 'action' => 'import'),
        'type' => 'file'));

    echo $this->Form->input('usersCSV', array('type' => 'file', 'name' => 'usersCSV', 'label' =>'fichier csv'));

    echo $this->Form->input('connection', array(
        'label' => 'connexion'
    ));


    echo $this->Form->end(array('editMode' => true));

    ?>



</div>