<?php ?>

<div class="bg-grey"><h3><?php echo __d('user', 'User.webdelib.check') ?></h3></div>
<div class="span6">
    <form id="UserConfirm"  method="post" action="/Confirmuser/saveSrvuser">

        <?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

foreach ($existantSrvusers as $key => $user) {
            if (!empty($user['Srvuser'])) {
                echo '<br>';
                echo '<table class="table">';
                echo '<tr><td>';
                echo '<input type="radio" name="keyChoice" value="' . $key . '">';
                echo '</td><td class="bg-grey">';
                echo 'Nom : ';
                echo '<b>' . $user['Srvuser']['firstname'] . '</b>' . '<br>';
                echo 'Prénom : ';
                echo '<b>' . $user['Srvuser']['lastname'] . '</b>' . '<br>';
                echo 'Date de naissance : ';
                //echo '<b>' . $user['Srvuser']['naissance'] . '</b>' . '<br>';
                echo '<b>' . $this->Time->format($user['Srvuser']['naissance'], '%d-%m-%Y', 'invalid') . '</b>' . '<br>';

                echo 'Username : ';
                echo '<b>' . $user['Srvuser']['username'] . '</b>' . '<br>';

                foreach ($user['Mandat'] as $Mandat) {
                    echo 'Collectivité : ';
                    echo '<b>' . $Mandat['Collectivite']['name'] . '</b>' . '<br>';
                }
                echo '</td></tr>';
                echo '</table>';

//echo $user['collectivite'] .'<br>';
            }
        }
        ?>

        <br>
        <table class="table">
            <tr><td>
                    <input type="radio" name="keyChoice" value="new"> 
                </td><td class="bg-grey">
                    <b>nouvel utilisateur  </b>(homonyme)
                </td></tr>
        </table>


        <br><br>
        <button type="submit" class="btn btn-success">Enregistrer</button>
    </form>
</div>
