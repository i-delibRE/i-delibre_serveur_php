/**
 * webroot/js/globalHelper.js
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-22 10:10:01 +0200 (mar. 22 oct. 2013) $
 * $Revision: 303 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/webroot/js/globalHelper.js $
 * $Id: globalHelper.js 303 2013-10-22 08:10:01Z ssampaio $
 */

/**
 *
 * @param {type} dateObject
 * @returns {String}
 */

function toFrString(dateObject) {
	return ((dateObject.getDate() > 9) ? '' : '0') + dateObject.getDate() + '/' + ((dateObject.getMonth() > 8) ? '' : '0') + (dateObject.getMonth() + 1) + '/' + dateObject.getFullYear();
}

/**
 * notificationStatus
 *
 * unread
 * read
 * accepted
 * delegated
 * refused
 */
var notificationStatus = {
	unread: {
		ico: 'envelope',
		rowClass: 'info'
	},
	read: {
		ico: 'eye-open',
		rowClass: ''
	},
	accepted: {
		ico: 'check',
		rowClass: 'success'
	},
	delegated: {
		ico: 'share',
		rowClass: 'success'
	},
	refused: {
		ico: 'ban-circle',
		rowClass: 'error'
	}
};

/**
 *
 * @param {type} url
 * @param {type} params
 * @param {type} success
 * @param {type} error
 * @returns {undefined}
 */
function ajaxPostRequest(url, params, success, error) {
	$.ajax({
		type: 'POST',
		url: url,
		data: params,
		success: success,
		error: error
	});
}



//user
var user = {
	loginName: 'adullact_user',
	fullName: 'Adullact user',
	currentFolder: {
		nodeRef: 'doc_id_value'
	},
	urlContext: ''
};

//sticky desktop
var circuitUtils = {
	getCurrentEtape: function() {
		return {bureau: 0};
	},
	getEtapeNum: function(index) {
		return {bureau: 0};
	}
};


/**
 *
 * @returns {Array}
 */
function UUID() {
	var nbr, randStr = "";
	do {
		randStr += (nbr = Math.random()).toString(16).substr(2);
	} while (randStr.length < 30);
	return [
		randStr.substr(0, 8), "-",
		randStr.substr(8, 4), "-4",
		randStr.substr(12, 3), "-",
		((nbr * 4 | 0) + 8).toString(16), // [89ab]
		randStr.substr(15, 3), "-",
		randStr.substr(18, 12)
	].join("");
}

