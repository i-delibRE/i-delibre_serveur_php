/**
 *
 * webroot/js/dateHelper.js
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-22 10:10:01 +0200 (mar. 22 oct. 2013) $
 * $Revision: 303 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/webroot/js/dateHelper.js $
 * $Id: dateHelper.js 303 2013-10-22 08:10:01Z ssampaio $
 */


/**
 *
 * @param {type} dateObject
 * @returns {String}
 */
function toFrString(dateObject) {
	return ((dateObject.getDate() > 9) ? '' : '0') + dateObject.getDate() + '/' + ((dateObject.getMonth() > 8) ? '' : '0') + (dateObject.getMonth() + 1) + '/' + dateObject.getFullYear();
}
