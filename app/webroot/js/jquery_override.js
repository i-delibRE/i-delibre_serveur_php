/**
 * webroot/js/jquery_override.js
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-22 10:10:01 +0200 (mar. 22 oct. 2013) $
 * $Revision: 303 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/webroot/js/jquery_override.js $
 * $Id: jquery_override.js 303 2013-10-22 08:10:01Z ssampaio $
 */


/**
 * $('un_element').attr();
 * retourne la liste des attributs html
 * http://forum.jquery.com/topic/retrieving-all-attributes-of-a-given-element-and-their-values#14737000002295973
 *
 * @param {type} $
 * @returns {undefined}
 */
(function($) {
	// duck-punching to make attr() return a map
	var _old = $.fn.attr;
	$.fn.attr = function() {
		var a, aLength, attributes, map;
		if (this[0] && arguments.length === 0) {

			//stephane sampaio
			//map = {};
			map = [];

			attributes = this[0].attributes;
			aLength = attributes.length;
			for (a = 0; a < aLength; a++) {
				map[attributes[a].name.toLowerCase()] = attributes[a].value;
			}
			return map;
		} else {
			return _old.apply(this, arguments);
		}
	}
}(jQuery));
