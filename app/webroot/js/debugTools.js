/**
 *
 * webroot/js/debugTools.js
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-22 10:10:01 +0200 (mar. 22 oct. 2013) $
 * $Revision: 303 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/webroot/js/debugTools.js $
 * $Id: debugTools.js 303 2013-10-22 08:10:01Z ssampaio $
 */

/**
 *
 * @returns {undefined}
 */
function debugTools() {
	var debugToolsDiv = $('<div></div>').attr('id', 'debugTools').addClass('btn').addClass('fold');

	var clearLocalStorageDiv = $('<div></div>').addClass('btn').addClass('btn-danger').attr('title', 'clear localStorage').html($('<i></i>').addClass('icon-trash')).click(function() {
		try {
			localStorage.clear();
			that.notify('localStorage cleared');
		} catch (err) {
			that.error('Impossible de vider localStorage', err);
		}
	});
	var clearSessionStorageDiv = $('<div></div>').addClass('btn').addClass('btn-danger').attr('title', 'clear sessionStorage').html($('<i></i>').addClass('icon-trash')).click(function() {
		try {
			sessionStorage.clear();
			that.notify('sessionStorage cleared');
		} catch (err) {
			that.error('Impossible de vider sessionStorage', err);
		}
	});

	var showLocalStorageDiv = $('<div></div>').addClass('btn').addClass('btn-inverse').attr('title', 'show localStorage').html($('<i></i>').addClass('icon-hdd').addClass('icon-white')).click(function() {
		that.debug(localStorage);
	});
	var showSessionStorageDiv = $('<div></div>').addClass('btn').addClass('btn-inverse').attr('title', 'show sessionStorage').html($('<i></i>').addClass('icon-hdd').addClass('icon-white')).click(function() {
		that.debug(sessionStorage);
	});
	var showStoredDataDiv = $('<div></div>').addClass('btn').addClass('btn-inverse').attr('title', 'show stored data').html($('<i></i>').addClass('icon-hdd').addClass('icon-white')).click(function() {
		that.debug(JSON.parse(localStorage.appUIData));
	});
	var reloadDiv = $('<div></div>').addClass('btn').addClass('btn-inverse').attr('title', 'reload page').html($('<i></i>').addClass('icon-refresh').addClass('icon-white')).click(function() {
		location.reload();
	});
	var fullReloadDiv = $('<div></div>').addClass('btn').addClass('btn-danger').attr('title', 'purge storages and reload page').html($('<i></i>').addClass('icon-refresh')).click(function() {
		clearLocalStorageDiv.trigger('click');
		clearSessionStorageDiv.trigger('click');
		location.reload(true);
	});

	var debugToolsDivContent = $('<div></div>').addClass('content').addClass('btn-group').addClass('btn-group-vertical');

	var showFPS = $('<div></div>').addClass('btn').addClass('btn-inverse').attr('title', 'show FPS').html($('<i></i>').addClass('icon-heart').addClass('icon-white')).click(function() {
		var script = document.createElement('script');
		script.src = '/app/webroot/idelibre_client/assets/annotations/stats.min.js';
		document.body.appendChild(script);
		script = document.createElement('script');
		script.innerHTML = 'var interval=setInterval(function(){if(typeof Stats==\'function\'){clearInterval(interval);var stats=new Stats();stats.domElement.style.position=\'fixed\';stats.domElement.style.left=\'0px\';stats.domElement.style.bottom=\'40px\';stats.domElement.style.zIndex=\'10000\';document.body.appendChild(stats.domElement);setInterval(function(){stats.update();},1000/60);}},100);';
		document.body.appendChild(script);

	});

	var showConsole = $('<a></a>').prop('href', "javascript:(function(F,i,r,e,b,u,g,L,I,T,E){if(F.getElementById(b))return;E=F[i+'NS']&&F.documentElement.namespaceURI;E=E?F[i+'NS'](E,'script'):F[i]('script');E[r]('id',b);E[r]('src',I+g+T);E[r](b,u);(F[e]('head')[0]||F[e]('body')[0]).appendChild(E);E=new%20Image;E[r]('src',I+L);})(document,'createElement','setAttribute','getElementsByTagName','FirebugLite','4','firebug-lite.js','releases/lite/latest/skin/xp/sprite.png','https://getfirebug.com/','#startOpened');").addClass('btn').addClass('btn-inverse').attr('title', 'show Console').html($('<i></i>').addClass('icon-comment').addClass('icon-white'));




	var makeFold = $('<div></div>').addClass('btn').addClass('btn-mini').addClass('btn-warning').attr('title', 'set foldable').html($('<i></i>').addClass('icon-lock').addClass('icon-white')).click(function() {
		if ($('#debugTools').hasClass('fold')) {
			$('#debugTools').removeClass('fold');
			$(this).removeClass('btn-warning');
			$(this).addClass('btn-info');
		} else {
			$('#debugTools').addClass('fold');
			$(this).removeClass('btn-info');
			$(this).addClass('btn-warning');
		}
	});

	var hideFixedBars = $('<div></div>').addClass('btn').addClass('btn-inverse').attr('title', 'show / hide fixedbars').html($('<i></i>').addClass('icon').addClass('icon-white').addClass('icon-fullscreen')).click(function() {
		$('#footer, .app-ui-header').slideToggle('fast');
		$(makeFold).trigger('click');
	});

	var gotoClient = $('<div></div>').addClass('btn').addClass('btn-inverse').attr('title', 'goto client').html($('<i></i>').addClass('icon').addClass('icon-white').addClass('icon-share-alt')).click(function() {
		window.location.href = "/idelibre_client";
	});

	debugToolsDivContent.append(makeFold)
	debugToolsDivContent.append($('<br />'));
	debugToolsDivContent.append(showLocalStorageDiv)
	debugToolsDivContent.append(clearLocalStorageDiv);
	debugToolsDivContent.append($('<br />'));
	debugToolsDivContent.append(showSessionStorageDiv);
	debugToolsDivContent.append(clearSessionStorageDiv);
	debugToolsDivContent.append($('<br />'));
	debugToolsDivContent.append(showStoredDataDiv);
	debugToolsDivContent.append($('<br />'));
	debugToolsDivContent.append(showFPS);
	debugToolsDivContent.append(showConsole);
	debugToolsDivContent.append($('<br />'));
	debugToolsDivContent.append(hideFixedBars);
	debugToolsDivContent.append($('<br />'));
	debugToolsDivContent.append(gotoClient);
	debugToolsDivContent.append($('<br />'));
	debugToolsDivContent.append(reloadDiv);
	debugToolsDivContent.append(fullReloadDiv);
	debugToolsDiv.append(debugToolsDivContent);

	$('body').append(debugToolsDiv);
}
/**
 *
 * @type type
 */
var _lastClick = {
	x: 0,
	y: 0
};

/**
 *
 * @param {type} variable
 * @returns {undefined}
 */
function debug(variable) {
	console.log('----- DEBUG ------');
	console.log(variable);
	var nd = new Date();
	var lastClick = '';
	if (_lastClick.x && _lastClick.y) {
		lastClick = '- ' + _lastClick.x + 'x' + _lastClick.y;
	}
	console.log(nd.toString() + ' <' + lastClick + '> : ' + variable.toString(), 1);
}


debugTools();