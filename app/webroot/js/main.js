/**
 * Core app init
 *
 * webroot/js/main.js
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-22 10:10:01 +0200 (mar. 22 oct. 2013) $
 * $Revision: 303 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/webroot/js/main.js $
 * $Id: main.js 303 2013-10-22 08:10:01Z ssampaio $
 *
 */

$(document).ready(function() {
	$('#nojs').remove();
});

//http://preloaders.net/en/search/spinning
//var waitMsg = $('<div></div>').addClass('alert').css('text-align', 'center').html('Chargement ...').append('<br /><br />').append($('<img src="/img/ajax_spin.gif" alt="Veuillez patienter"/>'));


//http://fgnass.github.com/spin.js/
if (typeof Spinner != 'undefined') {
	var opts = {
		lines: 17, // The number of lines to draw
		length: 10, // The length of each line
		width: 2, // The line thickness
		radius: 7, // The radius of the inner circle
		corners: 1.0, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		color: '#000', // #rgb or #rrggbb
		speed: 1.5, // Rounds per second
		trail: 25, // Afterglow percentage
		shadow: true, // Whether to render a shadow
		hwaccel: true, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 500, // The z-index (defaults to 2000000000)
		top: 'auto', // Top position relative to parent in px
		left: 'auto' // Left position relative to parent in px
	};
//var target = document.getElementById('foo');
//var spinner = new Spinner(opts).spin(target);
	var spinner = new Spinner(opts).spin();
	var waitTxt = $('<div></div>').css({
		'text-align': 'center',
		'font-size': '1.2em',
		'font-weight': 'bold'
	}).html('Veuillez patienter.');
	var waitMsg = $('<div></div>').addClass('alert').addClass('alert-info').html(spinner.el).append(waitTxt).css({
		'margin-left': 'auto',
		'margin-right': 'auto',
		top: '15px',
		width: '75%'
	});
}

/**
 *
 * @param {type} link
 * @param {type} target
 * @param {type} beforeAjax
 * @returns {undefined}
 */
function setAjaxLink(link, target, beforeAjax) {
	var url = $(link).attr('href');
	$(link).attr('href', '#');
	$(link).click(function(e) {
		e.preventDefault();
		e.stopPropagation();


		$(target).html(waitMsg);

		if (typeof beforeAjax != 'undefined') {
			beforeAjax();
		}

		$.ajax({
			url: url,
			method: 'get',
			success: function(data) {
				$(target).html(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (textStatus != "abort") {
					var errMsg = '<i class="icon-exclamation-sign"></i> ';
					if (jqXHR.status == 403) {
						errMsg += "Accès refusé.<br />Votre session a peut-être expirée.";
					} else if (jqXHR.status == 404) {
						errMsg += "URL invalide.";
					} else {
						errMsg += "Impossible d'accèder à cette page.";
					}
					var errAlert = $('<div></div>').html(errMsg).addClass('alert').addClass('alert-danger')
							.append($('<hr />'))
							.append($('<p class="well"></p>').html(jqXHR.responseText));
					$(target).html(errAlert);
				}
			}
		});
	});
}

/**
 *
 * @param {type} msg
 * @returns {undefined}
 */
function iconfirm(msg) {
	var modalBg = $('<div></div>').addClass('modal-bg').css({
		position: 'fixed',
		top: 0,
		bottom: 0,
		'background-color': 'rgba(0,0,0,127)',
		'z-index': 100000
	});
	var modalBox = $('<div></div>').addClass('modal-box').css({
		'width': '250px;',
		'height': '250px;',
		'background-color': 'rgba(255,255,255,255)'
	});

	var btnOk = $('<div></div>').addClass('btn').html('ok');
	var btnCancel = $('<div></div>').addClass('btn').html('cancel');

	modalBox.append(btnOk).append(btnCancel);
	modalBg.append(modalBox);
	$('body').append(modalBg);

}