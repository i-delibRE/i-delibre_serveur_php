<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * SrvusersMandatFixture
 *
 */
class SrvusersMandatFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => 'uuid_generate_v4()', 'length' => 36, 'key' => 'primary'),
		'srvuser_id' => array('type' => 'string', 'null' => true, 'length' => 36),
		'mandat_id' => array('type' => 'string', 'null' => false, 'length' => 36),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => 'now()'),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => 'now()'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '53a1ae05-bc74-424d-837b-5f8e293c0839',
			'srvuser_id' => 'Lorem ipsum dolor sit amet',
			'mandat_id' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-06-18 17:19:33',
			'modified' => '2014-06-18 17:19:33'
		),
	);

}
