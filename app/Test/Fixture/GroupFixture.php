<?php

/**
 * GroupFixture.php
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-12-31 16:16:07 +0100 (mar. 31 déc. 2013) $
 * $Revision: 356 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Test/Fixture/GroupFixture.php $
 * $Id: GroupFixture.php 356 2013-12-31 15:16:07Z ssampaio $
 */
class GroupFixture extends CakeTestFixture {

	/**
	 *
	 * @var type
	 */
	public $useDbConfig = "test_coll";

	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array(
		'model' => 'Group',
		'records' => false,
		'connection' => 'template'
	);

	/**
	 * Définition des enregistrements.
	 *
	 * @var array
	 */
	public $records = array(
	);

}

?>
