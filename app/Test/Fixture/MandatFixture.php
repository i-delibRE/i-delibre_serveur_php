<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * MandatFixture
 *
 */
class MandatFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => 'uuid_generate_v4()', 'length' => 36, 'key' => 'primary'),
		'user_id' => array('type' => 'string', 'null' => false, 'length' => 36),
		'conn' => array('type' => 'string', 'null' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => 'now()'),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => 'now()'),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '53a1ade1-d0b0-4045-9566-5f62293c0839',
			'user_id' => 'Lorem ipsum dolor sit amet',
			'conn' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-06-18 17:18:57',
			'modified' => '2014-06-18 17:18:57'
		),
	);

}
