<?php

/**
 * All Test Suite
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-01-27 11:56:46 +0100 (lun. 27 janv. 2014) $
 * $Revision: 360 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Test/Case/AllTestsTest.php $
 * $Id: AllTestsTest.php 360 2014-01-27 10:56:46Z ssampaio $
 */

/**
 * AllTests class
 *
 * This test group will run all tests.
 *
 * @see http://book.cakephp.org/2.0/en/development/testing.html
 * @package app.Test.Case
 */
class AllTests extends PHPUnit_Framework_TestSuite {

	/**
	 * Test suite with all test case files.
	 *
	 * @return void
	 */
	public static function suite() {
		$suite = new CakeTestSuite('All tests');
		$suite->addTestDirectoryRecursive(TESTS . DS . 'Case' . DS);

		return $suite;
	}

}

?>