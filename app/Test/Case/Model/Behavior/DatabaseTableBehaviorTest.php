<?php

/**
 * DatabaseTableBehavior test class.
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun., 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/Behavior/PasswordBehavior.php $
 * $Id: PasswordBehavior.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 *
 * @package			app
 * @subpackage		app.Model.Behavior
 *
 */
App::uses('DatabaseTableBehavior', 'Model/Behavior');

/**
 * Classe DatabaseTableBehaviorTest.
 *
 * @package app.Test.Case.Model.Behavior
 */
class DatabaseTableBehaviorTest extends CakeTestCase {

	/**
	 * ModÃ¨le Apple utilisÃ© par ce test.
	 *
	 * @var Model
	 */
	public $Apple = null;

	/**
	 * Fixtures utilisÃ©s par ces tests unitaires.
	 *
	 * @var array
	 */
	public $fixtures = array(
		'core.Apple'
	);

	/**
	 * PrÃ©paration du test.
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->Apple = ClassRegistry::init('Apple');
		$this->Apple->Behaviors->attach('DatabaseTable');

		$this->Apple->bindModel(
				array(
			'belongsTo' => array(
				'Parentapple' => array(
					'className' => 'Apple'
				)
			),
			'hasOne' => array(
				'Childapple' => array(
					'className' => 'Apple'
				)
			),
				), false
		);
	}

	/**
	 * Nettoyage postÃ©rieur au test.
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->Apple);
		parent::tearDown();
	}

	/**
	 * Test de la mÃ©thode DatabaseTableBehavior::fields()
	 *
	 * @return void
	 */
	public function testFields() {
		$result = $this->Apple->fields();
		$expected = array(
			'Apple.id',
			'Apple.apple_id',
			'Apple.color',
			'Apple.name',
			'Apple.created',
			'Apple.date',
			'Apple.modified',
			'Apple.mytime',
		);
		$this->assertEqual($result, $expected, var_export($result, true));
	}

	/**
	 * Test de la mÃ©thode DatabaseTableBehavior::sq()
	 *
	 * @return void
	 */
	public function testSq() {
		$result = $this->Apple->sq(
				array(
					'fields' => array('Apple.id'),
					'conditions' => array(
						'Apple.modified >' => '2012-10-31',
						'Apple.color' => 'red'
					),
					'joins' => array(
						$this->Apple->join('Parentapple')
					),
					'limit' => 3
				)
		);
		$expected = 'SELECT "Apple"."id" AS "Apple__id" FROM "public"."apples" AS "Apple" LEFT JOIN "public"."apples" AS "Parentapple" ON ("Apple"."parentapple_id" = "Parentapple"."id")  WHERE "Apple"."modified" > \'2012-10-31\' AND "Apple"."color" = \'red\'    LIMIT 3';
		$this->assertEqual($result, $expected, var_export($result, true));
	}

	/**
	 * Test de la mÃ©thode DatabaseTableBehavior::join()
	 *
	 * @return void
	 */
	public function testJoin() {
		$result = array(
			$this->Apple->join('Parentapple'),
			$this->Apple->join('Childapple'),
		);
		$expected = array(
			array(
				'table' => '"public"."apples"',
				'alias' => 'Parentapple',
				'type' => 'LEFT',
				'conditions' => '"Apple"."parentapple_id" = "Parentapple"."id"',
			),
			array(
				'table' => '"public"."apples"',
				'alias' => 'Childapple',
				'type' => 'LEFT',
				'conditions' => '"Childapple"."apple_id" = "Apple"."id"',
			),
		);
		$this->assertEqual($result, $expected, var_export($result, true));
	}

}

?>