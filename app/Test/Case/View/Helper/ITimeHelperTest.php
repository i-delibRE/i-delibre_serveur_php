<?php

/**
 * ITimeHelper test class.
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun., 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/Behavior/PasswordBehavior.php $
 * $Id: PasswordBehavior.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 *
 * @package			app
 * @subpackage		app.Model.Behavior
 *
 */
App::uses('View', 'View');
App::uses('ITimeHelper', 'View/Helper');

/**
 * Classe ITimeHelperTest.
 *
 * @package app.Test.Case.Model.Behavior
 */
class ITimeHelperTest extends CakeTestCase {

	/**
	 * Fixtures utilisés par ces tests unitaires.
	 *
	 * @var array
	 */
	public $fixtures = array();

	/**
	 * Préparation du test.
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$controller = null;
		$this->View = new View($controller);
		$this->ITime = new ITimeHelper($this->View);
	}

	/**
	 * Nettoyage postérieur au test.
	 *
	 * @return void
	 */
	public function tearDown() {
		parent::tearDown();
		unset($this->View, $this->App);
	}

	/**
	 * Test de la méthode ITimeHelper::_cacheKey()
	 *
	 * @return void
	 */
	public function testDatetime() {
		$pg_timestamp = "2013-12-27 17:31:01.491117+01";


		$result = $this->ITime->datetime($pg_timestamp);
		$expected = '27/12/2013 17:31:01';
		$this->assertEquals($result, $expected, var_export($result, true));


		$result = $this->ITime->datetime($pg_timestamp, false);
		$expected = '17:31:01';
		$this->assertEquals($result, $expected, var_export($result, true));


		$result = $this->ITime->datetime($pg_timestamp, true, false);
		$expected = '27/12/2013';
		$this->assertEquals($result, $expected, var_export($result, true));


		$result = $this->ITime->datetime($pg_timestamp, false, false);
		$expected = '';
		$this->assertEquals($result, $expected, var_export($result, true));


		$result = $this->ITime->datetime($pg_timestamp, true, true, true);
		$expected = '27/12/2013 17:31:01 491117';
		$this->assertEquals($result, $expected, var_export($result, true));


		$result = $this->ITime->datetime("choucroute");
		$expected = '';
		$this->assertEquals($result, $expected, var_export($result, true));
	}

}

?>