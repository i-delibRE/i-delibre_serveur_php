<?php

/**
 * SSLCertificates Tests
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Test/Case/Utility/SSLCertificatesTest.php $
 * $Id: SSLCertificatesTest.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
App::uses('SSLCertificates', 'Utility');

class SSLCertificatesTest extends CakeTestCase {

	/**
	 *
	 */
	public function setUp() {
		parent::setUp();
		$this->SSLCertificates = new SSLCertificates();
	}

	/**
	 *
	 */
	public function testHex2bin() {
		//736f6d655f737472696e67 : hexadecimal conversion of 'some_string\non two lines'
		$result = $this->SSLCertificates->hex2bin("736f6d655f737472696e670a6f6e2074776f206c696e6573");
		$expected = "some_string\non two lines";
		$this->assertEqual($result, $expected);
	}

	/**
	 *
	 */
	public function testVerifySSLSign() {
		/*
		 * test data
		 */

		//génération d'un certificat au format pem
		//openssl req -x509 -nodes -days 1 -subj "/C=FR/L=PARIS/CN=webmail.monsite.fr" -newkey rsa:1024 -keyout certificat.pem -out certificat.pem

		$certificat_pem = <<< EOL
-----BEGIN PRIVATE KEY-----
MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBALo53FAIqwiZtaNa
znq+IxcI28WuGOMSQ1gNYy7Wxtb6wD+4x8mXWS3+HgkWxZan20n2PaLJ8WnqVvF2
6npq29tFf7+cp5CEfuViw28r8NHhBJQO0aZamlLzPggngHrH0XEWT1BXAGbN4pfK
19I4KC4TdMc4pY+Q5GAAWNJRaW/5AgMBAAECgYBMp/6hT0AgFS2opXtzGQtWswXM
VILYDa988l3ObQiCe8thL43F1LAX94+jjCUfg88Hnf33IYtnXtgVIy8X3x7Kas/Z
Y9lmiZGhYmG6EwKWrN9uRzqi9Xrz4L+CgcgkzUdL/j1s0RTgNnuj4SoGXI+dI1YQ
laA4mI0NJzWhOvya9QJBAOWr3KymgRn3wPQe2WAiFZwuWtGPE0DfgT2wL7O4yZN7
xpXY8zFZ4Qu5LgdSPA0pOkyULSB7SN0L/nS6u7flkK8CQQDPkwOOVusK/EDkK40H
vzHK86sUUDpyqEXuRQBOQIu3dePXl3GmMvSttpElthUwkQcqHgdU60+RZFhu1AhD
uyPXAkBmyDfUqBsklOSsG1YqmHZVU8Ve/QmfwaZkBgIDCxJix0TC8Lk8/70f8vpv
WlT3itjFRpa/b4zXJ7PzZ1H05W6PAkBZy7BrICC39N4hltGAu25P7M9ijz5poW/3
HnR+ZZ/Lcr1o44PvsPV2PJdX6AgMpYCi8LCdRNp7/BvhfepBWrCrAkAkY8Ilhwm4
Cqc+L/jVIepK192JjVnXFmHG1JfLYHT8DnmxvA+jld0ZvLNRVGN/fESAK1MG0Q1R
01v8bAoiNmbs
-----END PRIVATE KEY-----

-----BEGIN CERTIFICATE-----
MIICQjCCAaugAwIBAgIJAMFkRkVuOvBUMA0GCSqGSIb3DQEBBQUAMDoxCzAJBgNV
BAYTAkZSMQ4wDAYDVQQHDAVQQVJJUzEbMBkGA1UEAwwSd2VibWFpbC5tb25zaXRl
LmZyMB4XDTEzMTAxNzEzMzcyNVoXDTEzMTAxODEzMzcyNVowOjELMAkGA1UEBhMC
RlIxDjAMBgNVBAcMBVBBUklTMRswGQYDVQQDDBJ3ZWJtYWlsLm1vbnNpdGUuZnIw
gZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALo53FAIqwiZtaNaznq+IxcI28Wu
GOMSQ1gNYy7Wxtb6wD+4x8mXWS3+HgkWxZan20n2PaLJ8WnqVvF26npq29tFf7+c
p5CEfuViw28r8NHhBJQO0aZamlLzPggngHrH0XEWT1BXAGbN4pfK19I4KC4TdMc4
pY+Q5GAAWNJRaW/5AgMBAAGjUDBOMB0GA1UdDgQWBBSBuDBeraZBuCxxRigZKjtn
jVWkyTAfBgNVHSMEGDAWgBSBuDBeraZBuCxxRigZKjtnjVWkyTAMBgNVHRMEBTAD
AQH/MA0GCSqGSIb3DQEBBQUAA4GBAD/fGKvHOyfhhYdHtB94ZpoVfsSpkL2tr6WQ
Q9VddaSnNuf4cvXBOWp+kWgy3I4X3/GVF5gNSH9nsjkSitB3ELbtLdUgFjGGYFxl
ecv2TQzX6uk2zuqL55C1FKE5qEH4bydiQZu032aOI3rrGFyNLmaMNfNbvCk59faU
qK6dHyCq
-----END CERTIFICATE-----
EOL;

		$proof = "This is the proof";
		$private_key = openssl_get_privatekey($certificat_pem);


		/*
		 * Good signature
		 */
		$goodSignature = '';
		openssl_sign($proof, $goodSignature, $private_key);
		$goodSignatureExpected = true;
		$goodSignatureResult = $this->SSLCertificates->verifySSLSign($proof, $goodSignature, $certificat_pem);
		$this->assertEqual($goodSignatureExpected, $goodSignatureResult);

		/*
		 * Bad signature
		 */
		$badSignature = "This is a bad SSL signature result";
		$badSignatureExpected = false;
		$badSignatureResult = $this->SSLCertificates->verifySSLSign($proof, $badSignature, $certificat_pem);
		$this->assertEqual($badSignatureExpected, $badSignatureResult);
	}

}

?>
