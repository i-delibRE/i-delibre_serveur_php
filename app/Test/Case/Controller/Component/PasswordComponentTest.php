<?php

/**
 * ITimeHelper test class.
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun., 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/Behavior/PasswordBehavior.php $
 * $Id: PasswordBehavior.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 *
 */
App::uses('Controller', 'Controller');
App::uses('PasswordComponent', 'Controller/Component');

/**
 * PasswordTestController class
 *
 * @package Search
 * @subpackage Test.Case.Controller.Component
 */
class PasswordTestController extends Controller {

	/**
	 * name property
	 *
	 * @var string 'PasswordTest'
	 */
	public $name = 'PasswordTest';

	/**
	 * uses property
	 *
	 * @var mixed null
	 */
	public $uses = null;

	/**
	 * components property
	 *
	 * @var array
	 */
	public $components = array('Password');

}

/**
 * PasswordTest class
 *
 * @package       app.Plugin.Search.Test.Case.Controller.Component
 */
class PasswordComponentTest extends CakeTestCase {

	/**
	 * Controller property
	 *
	 * @var PasswordTestController
	 */
	public $Controller;

	/**
	 * name property
	 *
	 * @var string 'Password'
	 */
	public $name = 'Password';

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->Controller = new PasswordTestController();
		$this->Controller->Components->init($this->Controller);
		$this->Controller->Password->initialize($this->Controller);
	}

	/**
	 * testRedirect method
	 *
	 * @return void
	 */
	public function testGeneratePasswordLength() {
		$result = $this->Controller->Password->generatePassword();
		$this->assertTrue(strlen($result) == 8);

		$result = $this->Controller->Password->generatePassword(4);
		$this->assertTrue(strlen($result) == 4);

		$result = $this->Controller->Password->generatePassword(12);
		$this->assertTrue(strlen($result) == 12);
	}

	/**
	 * testRedirect method
	 *
	 * @return void
	 */
	public function testGeneratePasswordUniqueChars() {
		$result = $this->Controller->Password->generatePassword();

		for ($i = 0; $i < strlen($result); $i++) {
			$this->assertFalse(strpos($result, $result[$i], $i + 1), var_export($result, true));
		}
	}

	/**
	 * @expectedException LogicException
	 */
	public function testGeneratePasswordException() {
		$this->Controller->Password->generatePassword(50000);
	}

	/**
	 * @expectedException LogicException
	 */
	public function testGeneratePasswordException2() {
		$this->Controller->Password->generatePassword(-1);
	}

}

?>