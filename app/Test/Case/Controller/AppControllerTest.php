<?php

/**
 * App Controller Tests
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Test/Case/Controller/AppControllerTest.php $
 * $Id: AppControllerTest.php 302 2013-10-21 15:57:34Z ssampaio $
 */
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

class AppControllerTest extends CakeTestCase {

	public $fixtures = array(
//		'app.User'
	);

	/**
	 *
	 */
	public function setUp() {
		parent::setUp();
		$this->AppController = new AppController();
	}

	/**
	 *
	 */
	public function testSetConn() {
		/*
		 * empty conn param
		 */
		$conn = "";
		$result = $this->AppController->setConn($conn);
		$expected = "";
		$this->assertEqual($result, $expected);


		/*
		 * default conn param
		 */
		$conn = "default";
		$this->AppController->setConn($conn);
		$result = Configure::read('conn');
		$expected = 'default';
		$this->assertEqual($result, $expected);

		/*
		 * test conn param
		 */
		$conn = "test";
		$this->AppController->setConn($conn);

		$result = array(
			'Configure' => array(
				'conn' => Configure::read('conn'),
				'Acl.database' => Configure::read('Acl.database')
			),
			'Models' => array(
				'Aro' => array(
					'loaded' => !empty($this->AppController->Aro),
					'useDbConfig' => !empty($this->AppController->Aro) && !empty($this->AppController->Aro->useDbConfig) ? $this->AppController->Aro->useDbConfig : false
				),
				'Aco' => array(
					'loaded' => !empty($this->AppController->Aco),
					'useDbConfig' => !empty($this->AppController->Aco) && !empty($this->AppController->Aco->useDbConfig) ? $this->AppController->Aco->useDbConfig : false
				),
				'Permission' => array(
					'loaded' => !empty($this->AppController->Permission),
					'useDbConfig' => !empty($this->AppController->Permission) && !empty($this->AppController->Permission->useDbConfig) ? $this->AppController->Permission->useDbConfig : false
				)
			)
		);

		$expected = array(
			'Configure' => array(
				'conn' => "test",
				'Acl.database' => "test"
			),
			'Models' => array(
				'Aro' => array(
					'loaded' => true,
					'useDbConfig' => "test"
				),
				'Aco' => array(
					'loaded' => true,
					'useDbConfig' => "test"
				),
				'Permission' => array(
					'loaded' => true,
					'useDbConfig' => "test"
				)
			)
		);
		$this->assertEqual($result, $expected);
	}

	public function testProcessLogin() {
		/*
		 * test empty login
		 */
		$this->AppController->request->data['Srvuser'] = array('username' => '');
		$result = $this->AppController->processLogin();
		$expected = array('userModel' => 'User', 'roleModel' => 'Group', 'login_suffix' => null);
		$this->assertEqual($result, $expected);

		/*
		 * test bad login
		 */
		$this->AppController->request->data['Srvuser'] = array('username' => 'badlogin');
		$result = $this->AppController->processLogin();
		$expected = array('userModel' => 'User', 'roleModel' => 'Group', 'login_suffix' => null);
		$this->assertEqual($result, $expected);

		/*
		 * test admin login
		 */
		$this->AppController->request->data['Srvuser'] = array('username' => 'user@admin');
		$result = $this->AppController->processLogin();
		$expected = array('userModel' => 'Srvuser', 'roleModel' => 'Srvrole', 'login_suffix' => "default");
		$this->assertEqual($result, $expected);

		/*
		 * test user login
		 */
		$this->AppController->request->data['Srvuser'] = array('username' => 'user@collectivite');
		$result = $this->AppController->processLogin();
		$expected = array('userModel' => 'User', 'roleModel' => 'Group', 'login_suffix' => "collectivite");
		$this->assertEqual($result, $expected);
	}

	public function testWriteLoginConfig() {
		/*
		 * test empty params
		 */
		$collectivite = array();
		$loginParams = array();
		$this->AppController->writeLoginConfig($collectivite, $loginParams);

		$result = array(
			'conn' => Configure::read('conn'),
			'userModel' => Configure::read('userModel'),
			'coll' => Configure::read('coll')
		);

		$expected = array(
			'conn' => false,
			'userModel' => false,
			'coll' => null
		);

		$this->assertEqual($result, $expected);



		/*
		 * test good params
		 */
		$collectivite = array('Collectivite' => array('conn' => 'collectivite'));
		$loginParams = array('userModel' => 'User', 'roleModel' => 'Group', 'login_suffix' => "collectivite");
		$this->AppController->writeLoginConfig($collectivite, $loginParams);

		$result = array(
			'conn' => Configure::read('conn'),
			'userModel' => Configure::read('userModel'),
			'coll' => Configure::read('coll')
		);

		$expected = array(
			'conn' => $collectivite['Collectivite']['conn'],
			'userModel' => $loginParams['userModel'],
			'coll' => $collectivite['Collectivite']
		);

		$this->assertEqual($result, $expected);




		/*
		 * test good loginParams with empty collectivite
		 */
		$collectivite = array();
		$loginParams = array('userModel' => 'User', 'roleModel' => 'Group', 'login_suffix' => "collectivite");
		$this->AppController->writeLoginConfig($collectivite, $loginParams);

		$result = array(
			'conn' => Configure::read('conn'),
			'userModel' => Configure::read('userModel'),
			'coll' => Configure::read('coll')
		);

		$expected = array(
			'conn' => $loginParams['login_suffix'],
			'userModel' => $loginParams['userModel'],
			'coll' => null
		);

		$this->assertEqual($result, $expected);
	}

}

?>
