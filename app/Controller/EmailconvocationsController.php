<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class EmailconvocationsController extends AppController {

    public function index() {
        $convocations = $this->Emailconvocation->find('all', array(
                'fields' => array(
                    'name', 'id'
                ),
                'order' => array('Emailconvocation.name'),
                'contain' => array(
                    'Type' => array(
                        'fields' => array('name')
                    )
                )
            )
        );
        $this->set('data', $convocations);
    }


    public function getAllJson(){
        $this->autoRender = false;
        $convocations = $this->Emailconvocation->find('all', array(
                'fields' => array(
                    'name', 'id'
                ),
                'order' => array('Emailconvocation.name'),
                'contain' => array(
                    'Type' => array(
                        'fields' => array('name')
                    )
                )
            )
        );
        echo json_encode($convocations);
    }


    public function getJson($id){
        $this->autoRender = false;
        $convocations = $this->Emailconvocation->find('first', array(
            'conditions'=> array(
                "Emailconvocation.id" => $id
            ),
            'contain' => array(
                'Type' => array(
                    'fields' => array('name', 'id')
                )
            )));

        $email = $convocations["Emailconvocation"];
        $currentType = $convocations["Type"];

        $types = $this->Emailconvocation->Type->find('all', array(
            "fields" => array(
                "name", "id"
            ),
            "order" => array("name")
        ));

        $types = Hash::extract($types,'{n}.Type');


        echo json_encode(["email" => $email, "currentType" => $currentType, "types" => $types]);


    }



    public function delete($id){
        $res = null;
        if($id != null){
            $res = $this->Emailconvocation->delete($id);
        }

        if($res){
            $this->Session->setFlash("Message supprimé", 'bootstrap_flash', array('class' => 'alert alert-success'));
        }else{
            $this->Session->setFlash("Erreur lors de la suppression", 'bootstrap_flash', array('class' => 'alert alert-error'));
        }

        $this->redirect(array('action' => "index"));
    }


    public function edit($id)
    {

        if ($this->request->is("POST") || $this->request->is("PUT")) {

            $emailconvocation = $this->request->data["Emailconvocation"];

            if(isset($this->request->data["Type"]))
                $emailconvocation["type_id"] = $this->request->data["Type"]["id"];


            $res = $this->Emailconvocation->save($emailconvocation);

            if ($res) {
                $this->Session->setFlash("Message modifié avec succès", 'bootstrap_flash', array('class' => 'alert alert-success'));
            } else {
                $this->Session->setFlash("Erreur lors de la modification", 'bootstrap_flash', array('class' => 'alert alert-error'));
            }

            $this->redirect(array('action' => "index"));
        }

        $data = $this->Emailconvocation->find('first', array(
            'conditions' => array('id' => $id)
        ));

        $this->request->data = $data;
        $typeList = $this->Emailconvocation->Type->find('list');
        $this->set('typeList', $typeList);


    }


    public function add(){

        if($this->request->is("POST")){

//            debug($this->request->data);

            $EmailInvitation = $this->request->data["Emailconvocation"];
            $EmailInvitation["type_id"] = $this->request->data["Type"]["id"];
            $res = $this->Emailconvocation->save($EmailInvitation);

            if($res){
                $this->Session->setFlash("Message enregistré", 'bootstrap_flash', array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash("Erreur lors de l'ajout", 'bootstrap_flash', array('class' => 'alert alert-error'));
            }

            $this->redirect(array('action' => "index"));

        }
        $typeList = $this->Emailconvocation->Type->find('list');
        $this->set('typeList', $typeList);
    }


}
