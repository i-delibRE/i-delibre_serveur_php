<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 */
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
App::uses('RestAuthenticate', 'Controller/Component/Auth');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
App::uses('DevTools', 'Utility');


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Components
     *
     * @var array
     */
    public $components = array(
        'RequestHandler',
        'DebugKit.Toolbar',
        'Session',
        'Auth',
        'Acl',
        'DbConnection',
    );

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array(
        'Base',
        'BootstrapBox',
        'ITime',
        'Form' => array('className' => 'BootstrapForm')
    );

    /**
     *
     * @var type
     */
    public $isJsonp = false;

    /**
     *
     * @var type
     */
    public $jsonpCallback;

    /**
     *
     * @var type
     */
    public $isAjax = false;

    /**
     *
     * Initialise la connexion afin d'utiliser la base de données de la collectivité choisie
     *
     * @access protected
     * @param string $conn
     * @return void
     */
    public function setConn($conn) {
        if (!empty($conn)) {
            $this->DbConnection->loadConnection($conn);
            Configure::write('conn', $conn);
            if ($conn != 'default') {
                Configure::write('Acl.database', $conn);
                $this->loadModel('Aro');
                $this->Aro->setDataSource($conn);
                $this->loadModel('Aco');
                $this->Aco->setDataSource($conn);
                $this->loadModel('Permission');
                $this->Permission->setDataSource($conn);
            }
        }
    }


    /**
     * Contruction des classes de l'application
     *
     * @access public
     * @return void
     */
    public function constructClasses() {
        parent::constructClasses();

        $conn = CakeSession::read('Auth.connName');
        $this->setConn($conn);
    }

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();

        //for dev from localhost
    //    $this->Auth->allow();
//        DevTools::loadConnection('agglo');
    //    $this->setConn('agglo');

        if ($this->Session->check('Config.language')) {
            Configure::write('Config.language', $this->Session->read('Config.language'));
        }

        $this->Session->write('ctxTitle', __d('ctx', 'ctx.' . $this->name . '.' . $this->action));

        if ($this->Session->check('Config.language')) {
            Configure::write('Config.language', $this->Session->read('Config.language'));
        }

        //Configure AuthComponent
        $this->Auth->loginAction = array('controller' => 'Srvusers', 'action' => 'login');
        $this->Auth->logoutRedirect = array('controller' => 'Environnements', 'action' => 'menu');
        $this->Auth->loginRedirect = array('controller' => 'Environnements', 'action' => 'menu');


        if ($this->Auth->User('id') && Configure::read('conn') != 'default') {
            $this->Auth->authorize = array(
                'Actions' => array('actionPath' => 'controllers')
            );
        }


        if (!empty($this->modelClass) && $this->action == "index") {
            $this->paginate = array(
                'limit' => 10,
                'order' => array(
                    $this->modelClass . '.' . (!empty($this->{$this->modelClass}->displayField) ? $this->{$this->modelClass}->displayField : 'created') => 'asc'
                )
            );
        }


        if (!empty($this->request->query['callback'])) {
            $this->isJsonp = true;
            $this->set('callback', $this->request->query['callback']);
        }

        if ($this->isJsonp && !empty($this->request->query['data'])) {
            $this->request->data = $this->request->query['data'];
        }

        if ($this->request->is('ajax')) {
            $this->isAjax = true;
        }
        $this->set('isJsonp', $this->isJsonp);
        $this->set('isAjax', $this->isAjax);
    }

    /**
     *
     * @return type
     */
    public function processLogin() {
        $userModel = 'User';
        $roleModel = 'Group';
        $db_login_suffix = null;

        if (isset($this->request->data['Srvuser'])) {
            $matches = array();
            if (preg_match('#(.*)@admin$#', $this->request->data['Srvuser']['username'], $matches)) {
                $this->request->data['Srvuser']['username'] = $matches[1];
                $db_login_suffix = 'default';
                $userModel = 'Srvuser';
                $roleModel = 'Srvrole';
            } else if (preg_match('#(.*)@(.*)$#', $this->request->data['Srvuser']['username'], $matches)) {
                $this->request->data['Srvuser']['username'] = $matches[1];
                $db_login_suffix = $matches[2];
            }
        }
        
        
        
        return array('userModel' => $userModel, 'roleModel' => $roleModel, 'login_suffix' => $db_login_suffix);
    }

    /**
     *
     * @param type $collectivite
     * @param type $loginParams
     */
    public function writeLoginConfig($collectivite, $loginParams) {
        if (!empty($loginParams)) {
         
            Configure::write('conn', !empty($collectivite) ? $collectivite['Collectivite']['conn'] : $loginParams['login_suffix']);
            Configure::write('userModel', $loginParams['userModel']);
            Configure::write('coll', !empty($collectivite) ? $collectivite['Collectivite'] : null);
        }
    }

    /**
     *
     * @param type $collectivite
     */
    public function writeUserConfig() {
        $env = 'superadmin';

        if (Configure::read('conn') == 'default') {
            $this->loadModel('Srvuser');

            $qd = array(
                'contain' => array('Srvrole'),
                'conditions' => array('Srvuser.id' => $this->Session->read('Auth.User.id'))
            );
            $srvuser = $this->Srvuser->find('first', $qd);

            if ($srvuser['Srvuser']['srvrole_id'] === IDELIBRE_ADMIN_GROUP_WALLETMANAGER_ID){
                $env = 'walletmanager';
            }
            
            if ($srvuser['Srvuser']['srvrole_id'] === IDELIBRE_ADMIN_GROUP_USER_ID){
                $env = 'baseuser';
            }
            


        } else {
            $this->loadModel('User');
            $this->User->useDbConfig = Configure::read('conn');
            $this->loadModel('Group');
            $this->Group->useDbConfig = Configure::read('conn');

            $qd = array(
                'contain' => false,
                'fields' => array('User.id', 'Group.admin', 'Group.superuser', 'Group.baseuser'),
                'joins' => array($this->User->join($this->Group->alias)),
                'conditions' => array('User.id' => $this->Session->read('Auth.User.id'))
            );
            $user = $this->User->find('first', $qd);

            if (!empty($user['Group']['admin'])) {
                $env = 'admin';
            } else if (!empty($user['Group']['superuser'])) {
                $env = 'superuser';
            } else if (!empty($user['Group']['baseuser'])) {
                $env = 'baseuser';
            }
        }
        Configure::write('env', $env);
    }

    /**
     *
     */
    public function writeLoginSession() {
        $this->Session->write('Auth.connName', Configure::read('conn'));
        $this->Session->write('Auth.userModel', Configure::read('userModel'));
        $this->Session->write('Auth.collectivite', Configure::read('coll'));
        $this->Session->write('Auth.env', Configure::read('env'));



    }

    /**
     *
     * Ajoute le nom et la valeur d'une variable dans le log debug
     *
     * @access protected
     * @param mixed $var
     * @return void
     */
    protected function _logDebug($var) {
        $debugvar = array(
            '================================================================',
            'date' => date('Y-m-d H:i:s'),
            'trace' => Debugger::trace(),
            'var' => $var,
            'type' => gettype($var)
        );
        $this->log(var_export($debugvar, true), 'debug');
    }

    /**
     *
     * @param type $var
     */
    public function ldebug($var) {
        $this->_logDebug($var);
    }

    /**
     *
     * @param type $var
     */
    public function ldebugHr() {
        $this->log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=', 'debug');
    }

    public function createBcryptPassword($password){
        $blowfish = BlowfishPasswordHasher::hash($password);
     // Checker !  debug(BlowfishPasswordHasher::check($password, $blowfish));
        $userModel = Configure::read('userModel');
        $this->loadModel($userModel);
        $this->{$userModel}->id = $this->Session->read('Auth.User.id');
        $this->{$userModel}->saveField('bcrypt', $blowfish);
    }



}
