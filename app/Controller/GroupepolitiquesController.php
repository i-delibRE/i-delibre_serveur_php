<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class GroupepolitiquesController extends AppController {



    public function index(){
        $data = $this->Groupepolitique->find('all', array(
            'recursive' => -1,
            'order' => 'name'
        ));

        $this->set(compact('data'));
    }


    public function listJson(){
        $this->autoRender= false;

        $data = $this->Groupepolitique->find('all', array(
            'recursive' => -1,
            'order' => 'name'
        ));
        echo json_encode($data);
    }


    public function add(){
        if($this->request->is('post') ||$this->request->is('put')){


            if($this->Groupepolitique->save($this->request->data)){

                $users = $this->Groupepolitique->User->find('all', array(
                    "fields" => array('id', 'groupepolitique_id')
                ));

                foreach ($users as &$user) {
                    if (in_array($user['User']['id'], $this->request->data['User']['User'])) {
                        $user['User']['groupepolitique_id'] = $this->Groupepolitique->id;

                    }
                }
                $this->Groupepolitique->User->saveAll($users);





                $this->Session->setFlash(__d('groupepolitique', 'groupepolitique.add.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__d('groupepolitique', 'groupepolitique.add.error'), 'bootstrap_flash', array('class' => 'alert alert-error'));
                $this->redirect(array('action' => 'index'));
            }
        }

        $qd = array(
            'fields' => array(
                'User.id',
                'User.name'
            ),
            'contain' => array(
                $this->Groupepolitique->User->Group->alias
            ),
            'conditions' => array(
                'Group.inseance' => true,
                'Group.baseuser' => true,
                'Group.superuser' => false,
                'Group.admin' => false
            ),
            'order' => array('User.lastname')
        );
        $users = $this->Groupepolitique->User->find('list', $qd);
        $this->set(compact('users'));

    }



    public function edit($id){
        if (!$this->Groupepolitique->exists($id)) {
            throw new NotFoundException(__('Invalid Groupe'));
        }

        if($this->request->is('post') ||$this->request->is('put')){

            if($this->Groupepolitique->save($this->request->data)){
                //update Associated users
                $users = $this->Groupepolitique->User->find('all', array(
                    "fields" => array('id', 'groupepolitique_id')
                ));

                foreach ($users as &$user) {
                    if (in_array($user['User']['id'], $this->request->data['User']['User'])) {
                        $user['User']['groupepolitique_id'] = $id;
                    } else if ($user['User']['groupepolitique_id'] == $id) {
                        $user['User']['groupepolitique_id'] = null;
                    }
                }

                $this->Groupepolitique->User->saveAll($users);

                $this->Session->setFlash(__d('groupepolitique', 'groupepolitique.edit.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__d('groupepolitique', 'groupepolitique.edit.error'), 'bootstrap_flash', array('class' => 'alert alert-error'));
                $this->redirect(array('action' => 'index'));
            }
        }else{
            $groupePolitiqueUser = $this->Groupepolitique->User->find('list', array('conditions' => array('User.groupepolitique_id' => $id), 'fields' => array('User.id')));
            $groupePolitiqueUser = array('User' => array('User' => array_values($groupePolitiqueUser)));
        }

        $qd = array(
            'fields' => array(
                'User.id',
                'User.name'
            ),
            'contain' => array(
                $this->Groupepolitique->User->Group->alias
            ),
            'conditions' => array(
                'Group.inseance' => true,
                'Group.baseuser' => true,
                'Group.superuser' => false,
                'Group.admin' => false
            ),
            'order' => array('User.lastname')
        );
        $users = $this->Groupepolitique->User->find('list', $qd);
        $this->set(compact('users'));


        $data = $this->Groupepolitique->find('first', array(
            'recursive' => -1,
            'conditions' => array('id' => $id)
        ));


        if(isset($groupePolitiqueUser)){
            $data = Hash::merge($data, $groupePolitiqueUser);
        }
        $this->request->data = $data;
    }


    public function detailJson($id){
        $this->autoRender= false;
        $groupePolitique = $this->Groupepolitique->find('first', array(
            'recursive' => -1,
            'conditions' => array('id' => $id),
            'contain' => array("User")
        ));




        $User = ClassRegistry::init("User");

        $users = $User->find("all", array(
            'conditions' => array(
                'User.group_id' => IDELIBRE_GROUP_AGENT_ID
            ),
            "fields" => array(
                'User.id', 'User.name', 'User.firstname', 'User.lastname'
            ),
            "order" => array('User.lastname')
        ));

        $users = Hash::extract($users,"{n}.User");

        $groupePol = $groupePolitique["Groupepolitique"];
        $selectedUsers = $groupePolitique["User"];


        echo json_encode(["groupePolitique" => $groupePol, 'users' => $users, 'selectedUsers' => $selectedUsers]);
    }


    /**e
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Groupepolitique->id = $id;
        if (!$this->Groupepolitique->exists()) {
            throw new NotFoundException(__('Invalid ptheme'));
        }
        $this->Groupepolitique->begin();
        if ($this->Groupepolitique->delete()) {
            $this->Groupepolitique->commit();
            $this->Session->setFlash(__d('groupepolitique', 'groupepolitique.delete.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Groupepolitique->rollack();
        $this->Session->setFlash(__d('groupepolitique', 'groupepolitique.delete.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }


    public function deleteJson($id = null) {
        $this->autoRender= false;
        $this->Groupepolitique->id = $id;
        if (!$this->Groupepolitique->exists()) {
            echo json_encode(["success" => false]);
            return;
        }

        if ($this->Groupepolitique->delete()) {
            echo json_encode(["success" => true, "id" => $id]);
        }else{
            echo json_encode(["success" => false]);
        }

    }



}