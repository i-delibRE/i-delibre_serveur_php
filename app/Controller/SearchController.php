<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class SearchController extends AppController {

    /*public function beforeFilter() {
        $this->Auth->allow();
    }*/

    public function index() {
        $Type = ClassRegistry::init('Type');

        //recherche par type de seance
        $types = $Type->find('all', array(
            'recursive' => -1,
            'fields' => array('Type.name', 'Type.id'),
        ));

        $listTypes = array();
        foreach ($types as $type) {
            $listTypes[$type['Type']['id']] = $type['Type']['name'];
        }


        //recherche des themes
        $Ptheme = ClassRegistry::init('Ptheme');
        $pthemes = $Ptheme->find('all', array(
            'recursive' => -1,
            'fields' => array('Ptheme.id', 'Ptheme.name'),
        ));

        $listThemes = array();
        foreach ($pthemes as $ptheme) {
            $listThemes[$ptheme['Ptheme']['id']] = $ptheme['Ptheme']['name'];
        }


        $this->set(compact('listTypes', 'listThemes'));
    }
    
    
    
     /**
     * initialise les variables pour la recherche (themes et types)
     */
    private function variableSearch() {


        $Type = ClassRegistry::init('Type');

        //recherche par type de seance
        $types = $Type->find('all', array(
            'recursive' => -1,
            'fields' => array('Type.name', 'Type.id'),
        ));

        $listTypes = array();
        foreach ($types as $type) {
            $listTypes[$type['Type']['id']] = $type['Type']['name'];
        }


        //recherche des themes
        $Ptheme = ClassRegistry::init('Ptheme');
        $pthemes = $Ptheme->find('all', array(
            'recursive' => -1,
            'fields' => array('Ptheme.id', 'Ptheme.name'),
        ));

        $listThemes = array();
        foreach ($pthemes as $ptheme) {
            $listThemes[$ptheme['Ptheme']['id']] = $ptheme['Ptheme']['name'];
        }


        $this->set(compact('listTypes', 'listThemes'));
    }
    

    public function searchByType() {
        $Seance = ClassRegistry::init("Seance");

        $typeId = $this->request->data['type'];
        $conditions = array();

        if (!empty($typeId)) {
            $conditions = array('Seance.type_id' => $typeId);
        }
        $seances = $Seance->find('all', array(
            'conditions' => $conditions,
            'order' => 'Seance.date_seance DESC',
        ));
        
        $this->variableSearch();
        
        $seanceredirectId = null;
        $this->set(compact('seanceredirectId'));
        $this->set(compact('seances'));
        $this->render('/Seances/liste');
    }

    public function searchByDate() {

        $Seance = ClassRegistry::init("Seance");

        $from = $this->request->data['dateFrom'];
        $to = $this->request->data['dateTo'];
        if (empty($from['day']) || empty($from['month']) || empty($from['year']) || empty($to['day']) || empty($to['month']) || empty($to['year'])) {
            $this->Session->setFlash(__d('seance', 'Seance.search.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            $this->redirect(array('controller' => 'Seances', 'action' => 'liste'));
        }


        $dateFrom = '' . $from['year'] . '-' . $from['month'] . '-' . $from['day'];
        $dateTo = '' . $to['year'] . '-' . $to['month'] . '-' . $to['day'];


        $seances = $Seance->find('all', array(
            'conditions' => array(
                'Seance.date_seance >' => $dateFrom,
                'Seance.date_seance <' => $dateTo
            ),
            'order' => 'Seance.date_seance DESC'));

        
        $this->variableSearch();
        
        $this->set(compact('seances'));
        $this->set('seanceredirectId', null);
        $this->render('/Seances/liste');
    }

    public function searchByTheme() {
        $Projet = ClassRegistry::init("Projet");
        $Seance = ClassRegistry::init("Seance");

        $themeId = $this->request->data['theme'];

        if (empty($themeId)) {
            $this->redirect(array('controller' => 'Seances', 'action' => 'liste'));
        }

        $projet = $Projet->find('all', array(
            'recursive' => -1,
            'fields' => array('distinct(Projet.seance_id)'),
            'conditions' => array(
                'Projet.ptheme_id' => $themeId
            ),
        ));

      //  debug($projet);
        
        $seanceId = Hash::extract($projet, '{n}.0.seance_id');

        //debug($seanceId);die;

        $seances = $Seance->find('all', array(
            'recursive' => -1,
            'conditions' => array(
                'Seance.id' => $seanceId
            ),
            'order' => 'Seance.date_seance DESC',
        ));
      
        
        $this->variableSearch();
        
        $this->set(compact('seances'));
        $this->set('seanceredirectId', null);
        $this->render('/Seances/liste');
    }

}
