<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');


class EmailrappelsController extends AppController
{
    public function beforeFilter() {
        parent::beforeFilter();
      //  $this->Auth->allow();

    }

    //requested from angular
//    public function getEmails(){
//
//
//
//        $this->autoRender = false;
//        $res = $this->Emailrappel->find('all');
//
//        $res = Hash::extract($res,"{n}.Emailrappel");
//        echo json_encode($res);
//    }



    public function sendEmail($seanceId){
        $this->autoRender = false;
        if(!$this->request->is("post") && !$this->request->is("put"))
            return ;

        $data = $this->request->data;
        //convocations already send :
        $Convocation = ClassRegistry::init('Convocation');
        $convocations = $Convocation->find("all", array(
            "fields" => array('user_id'),
            "conditions" => array (
                "seance_id" => $seanceId,
                'not' => array('ae_horodatage' => null)
            )
        ));
        $users = Hash::extract($convocations, "{n}.Convocation.user_id");
        foreach ($users as $user) {
            $res = $this->publipostage($user, $seanceId, $data['content']);
            $this->emailRappel($user, $data['title'], $res );
        }


        echo json_encode(["sucess" => true]);
    }


    public function emailRappel($userId, $title, $content ){
        $User = ClassRegistry::init("User");
        $User->id = $userId;

        $mailto = $User->field('mail');
        // Paramètres mail
        $Email = new CakeEmail('convocation');

        if (!empty($mailto)) {
            $Email->to($mailto);
            $Email->subject($title);
        }
        //envoi
        $result = null;
        try {
            $result = $Email->send($content);
        } catch (Exception $e) {
            $this->log($e->getMessage(), LOG_ERROR);
        }

        //traitement du retour
        return !empty($result);
    }


    public function publipostage($userId, $seanceId, $content) {
        $User = ClassRegistry::init("User");
        $User->id = $userId;

        $Seance = ClassRegistry::init("Seance");

        $seance = $Seance->find('first', array(
            'conditions' => array(
                'Seance.id' => $seanceId
            ),
            'fields' => array(
                'Seance.name', 'Seance.place', 'Seance.date_seance'
            ),
            'contain' => array(
                'Type.name'
            )
        ));


        // variable de la convocation
        $evaluate = array();
        $evaluate['nom'] = $User->field('lastname');
        $evaluate['prenom'] = $User->field('firstname');
        $evaluate['titre'] = $User->field('titre');

        $civiliteInt = $User->field('civilite');
        if($civiliteInt == 1)
            $evaluate['civilite'] = "madame";
        elseif ($civiliteInt == 2)
            $evaluate['civilite'] = "monsieur";
        else
            $evaluate['civilite'] = "";


        $evaluate['nomseance'] = $seance['Seance']['name'];
        $evaluate['lieuseance'] = $seance['Seance']['place'];
        $evaluate['typeseance'] = $seance['Type']['name'];

        $dateTime = $seance['Seance']['date_seance'];

        $predate = explode(' ', $dateTime);
        $date = $predate[0];

        $pretime = explode(' ', $dateTime);
        $time = $pretime[1];

        $exploded = explode('-', $date);

        $formatedDate = $exploded[2] . '/' . $exploded[1] . '/' . $exploded[0];

        $exploded = explode(':', $time);
        $formatedTime = $exploded[0] . 'h' . $exploded[1];
        $evaluate['dateseance'] = $formatedDate;
        $evaluate['heureseance'] = $formatedTime;
        $res = ConvertionBalise::evaluate($evaluate, $content);

        return $res;
    }



//
//    public function index(){
//        $data = $this->Emailrappel->find('all', array(
//            'recursive' => -1,
//            'order' => 'name'
//        ));
//        $this->set(compact('data'));
//    }

//
//    public function add(){
//        if($this->request->is('post') ||$this->request->is('put')){
//            if($this->Emailrappel->save($this->request->data)){
//                $this->Session->setFlash("Message enregistré", 'bootstrap_flash', array('class' => 'alert alert-success'));
//                $this->redirect(array('action' => 'index'));
//            }else{
//                $this->Session->setFlash("Erreur lors de l'ajout", 'bootstrap_flash', array('class' => 'alert alert-error'));
//                $this->redirect(array('action' => 'index'));
//            }
//        }
//
//    }



//    public function edit($id){
//        if (!$this->Emailrappel->exists($id)) {
//            throw new NotFoundException(__('Invalid Groupe'));
//        }
//
//        if($this->request->is('post') ||$this->request->is('put')){
//            if($this->Emailrappel->save($this->request->data)){
//                $this->Session->setFlash("Le message à été modifié", 'bootstrap_flash', array('class' => 'alert alert-success'));
//                $this->redirect(array('action' => 'index'));
//            }else{
//                $this->Session->setFlash("Erreur lors de la modification", 'bootstrap_flash', array('class' => 'alert alert-error'));
//                $this->redirect(array('action' => 'index'));
//            }
//        }
//
//        $data = $this->Emailrappel->find('first', array(
//            'recursive' => -1,
//            'conditions' => array('id' => $id)
//        ));
//
//        $this->request->data = $data;
//
//    }


//    public function delete($id = null) {
//        $this->Emailrappel->id = $id;
//        if (!$this->Emailrappel->exists()) {
//            throw new NotFoundException(__('Invalid ptheme'));
//        }
//        $this->Emailrappel->begin();
//        if ($this->Emailrappel->delete()) {
//            $this->Emailrappel->commit();
//            $this->Session->setFlash("Message supprimé", 'bootstrap_flash', array('class' => 'alert alert-success'));
//            $this->redirect(array('action' => 'index'));
//        }
//        $this->Groupepolitique->rollack();
//        $this->Session->setFlash("Erreur lors de la suppression", 'bootstrap_flash', array('class' => 'alert alert-danger'));
//        $this->redirect(array('action' => 'index'));
//    }
//




}