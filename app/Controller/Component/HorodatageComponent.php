<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('Security', 'Component');




/**
 * Regroupe les fonctions d'horodatage
 * type(s) supporté(s) : OPENSIGN, IAIK, CRYPTOLOG
 */
class HorodatageComponent extends Component {

	/**
	 * Controller
	 *
	 * @var type
	 */
	public $controller;

	/**
	 *
	 * @var type
	 */
	public $responseToText = '';

	/**
	 * Component initialization
	 *
	 * @access public
	 * @param type $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
	}

	/**
	 * fonction de vérification du fonctionnement du service d'horodatage
	 * @return boolean true, le service fonctionne, false dans le cas contraire
	 */
	public function check() {
		// initialisation
		$ret = false;

		// en fonction du type de service utilisé
		switch (Configure::read('Horodatage.type')) {
			case 'OPENSIGN' :
				try {
					$client = new SoapClient(Configure::read('Opensign.host'), array('wsdl_cache' => 0, 'trace' => 1));
					$ret = $client->getOpensslVersion();
					return (!empty($ret));
				} catch (Exception $e) {
					return false;
				}
				break;
			case 'IAIK' :
			case 'CRYPTOLOG' :
				// création du fichier test temporaire à horodater
				$tmpTestFileUri = tempnam(sys_get_temp_dir(), rand());
				file_put_contents($tmpTestFileUri, 'as@lae : fichier temporaire de test d\'horodatage');
				if (!file_exists($tmpTestFileUri))
					return false;
				// horodatage du fichier test
				$jh = $this->contentHorodatageFichier($tmpTestFileUri);
				unlink($tmpTestFileUri);
				return ($jh !== false);
				break;
			default :
				return false;
		}
	}

	/**
	 * fonction d'horodatage d'un fichier
	 * @param string $fichierSource chemin du du fichier à horodater
	 * @param string $repDest chemin du jeton d'horodatage (doit exister), si null, le jeton est produit dans le même répertoire que le fichier source
	 */
	public function horodateFichier($fichierSource, $repDest = null) {
		// si le nom du fichier est vide, on ne fait rien
		if (empty($fichierSource))
			return false;

		// initialisation du répertoire de destination
		if (empty($repDest))
			$repDest = dirname($fichierSource) . DS;

		$jh = $this->contentHorodatageFichier($fichierSource);
		if ($jh !== false) {
			$outrep = $repDest . basename($fichierSource) . '.rep.tsa';
			file_put_contents($outrep, $jh);
			return true;
		} else
			return false;
	}

	/**
	 * fonction d'horodatage des fichiers d'un répertoire et de ses sous répertoires
	 * les jetons d'horodatage peuvent être écrits dans un répertoire différent du répertoire source
	 * @param string $repSource chemin du répertoire des fichiers à horodater
	 * @param string $repDest chemin des jetons d'horodatage, si null, les jetons sont produit dans le même répertoire des sources
	 * @param booleen $recursive traite également les sous-dossiers si true
	 */
	public function horodateRepertoire($repSource, $repDest = null, $recursive = true) {
		// si repertoire source est vide, on ne fait rien
		if (empty($repSource) || ($repSource === DS))
			return;

		// lecture du répertoire
		$fichiersSource = glob($repSource . '*');

		// initialisation du répertoire de destination
		if (empty($repDest))
			$repDest = $repSource;
		else
		if (!is_dir($repDest))
			mkdir($repDest);

		foreach ($fichiersSource as $fichierSource) {
			if (is_file($fichierSource)) {
				$this->horodateFichier($fichierSource, $repDest);
			} else {
				// traitement du sous repertoire
				if ($recursive) {
					$this->horodateRepertoire($fichierSource . DS, $repDest . basename($fichierSource) . DS);
				}
			}
		}
	}

	/**
	 * retourne le contenu du jeton d'horodatage d'un fichier
	 * @param string $fileUri uri du fichier à horodater
	 * @return mixed false si l'horodatege a échoué, bitstream du jeton d'horodatage en cas de succès
	 */
	public function contentHorodatageFichier($fileUri) {
		// si le nom du fichier est vide, on ne fait rien
		if (empty($fileUri))
			return false;

		// création de l'empreinte du fichier
		$sha1 = sha1_file($fileUri);

		// en fonction du type de service utilisé
		switch (Configure::read('Horodatage.type')) {
			case 'OPENSIGN' :
				$params = array('sha1' => $sha1);
				$client = new SoapClient(Configure::read('Opensign.host'));

				$requete = $client->__soapCall("createRequest", array('parameters' => $params));
				$params = array('request' => $requete);
				$response = $client->__soapCall("createResponse", array('parameters' => $params));
				$params = array('response' => $response);
				$token = $client->__soapCall("extractToken", array('parameter' => $params));

				//<< idelibre
				$this->responseToText = $client->__soapCall("responseToText", array('parameter' => $params));
				//idelibre >>

				if (empty($token))
					return false;
				else
					return base64_decode($token);

				break;
			case 'IAIK' :
				require_once(APP . 'libs' . DS . 'TrustedTimestamps.php');
				$tsaUrl = Configure::read('IAIK.host');
				$requestFilePath = TrustedTimestamps::createRequestfile($sha1);

				try {
					$token = TrustedTimestamps::signRequestfile($requestFilePath, $tsaUrl);
				} catch (Exception $e) {
					$this->log('Exception reçue lors du timestamping Iaik: ' . $e->getMessage());
				}

				if (empty($token))
					return false;
				else
					return base64_decode($token);

				break;
			case 'CRYPTOLOG' :
				require_once(APP . 'libs' . DS . 'TrustedTimestamps.php');
				$tsaUrl = Configure::read('CRYPTOLOG.host');
				$tsaUserPwd = Configure::read('CRYPTOLOG.userpass');
				$requestFilePath = TrustedTimestamps::createRequestfile($sha1);

				try {
					$token = TrustedTimestamps::signRequestfile($requestFilePath, $tsaUrl, $tsaUserPwd);
				} catch (Exception $e) {
					$this->log('Exception reçue lors du timestamping Cryptolog: ' . $e->getMessage());
				}

				if (empty($token))
					return false;
				else
					return base64_decode($token);

				break;
			default :
				return false;
		}
	}

}

?>
