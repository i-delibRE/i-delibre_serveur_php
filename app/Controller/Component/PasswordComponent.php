<?php

/**
 *
 * PasswordComponent component class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 *
 * SVN Informations
 * $Date: 2013-12-31 16:16:07 +0100 (mar. 31 déc. 2013) $
 * $Revision: 356 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Controller/Component/PasswordComponent.php $
 * $Id: PasswordComponent.php 356 2013-12-31 15:16:07Z ssampaio $
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class PasswordComponent extends Component {

	/**
	 * Controller
	 *
	 * @var type
	 */
	public $controller;

	/**
	 * Component initialization
	 *
	 * @access public
	 * @param type $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
	}

	/**
	 * Password generator function
	 *
	 */
	function generatePassword($length = 8) {
		// inicializa variables
		$password = "";
		$i = 0;
		$possible = "0123456789bcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,;.!?*+-";

		if ($length <= 0 || $length > strlen($possible)){
			throw new LogicException();
		}

		// agrega random
		while ($i < $length) {
			$char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

			if (!strstr($password, $char)) {
				$password .= $char;
				$i++;
			}
		}
		return $password;
	}

}

?>