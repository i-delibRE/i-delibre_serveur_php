<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('Security', 'Component');


class DbFileComponent extends Component {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Security');

    /**
     * Controller
     *
     * @access public
     * @var Controller
     */
    public $controller;

    /**
     * * Component initialization
     *
     * @access public
     * @param Controller $controller
     * @return void
     */
    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    /**
     * Récupération de l'image stockée en base
     *
     * @param type $id
     * @throws NotFoundException
     */
    public function getFile($id = null, $base64 = false) {

        $collId = $_SESSION['Auth']['collectivite']['id'];

        $qd = array(
            'recursive' => -1,
            'conditions' => array(
                $this->controller->modelClass . '.id' => $id
            )
        );
        $file = $this->controller->{$this->controller->modelClass}->find('first', $qd);

        if (!empty($file)) {

            $fileInfos = $file[$this->controller->modelClass];
            $fullPath = $fileInfos["path"];
            $fileName = $fileInfos["name"];

            $this->controller->response->disableCache();
            $this->controller->response->file(
                $fullPath,
                array('download' => true, 'name' => $fileName )
            );
            return $this->controller->response;
        }

    }


}

?>
