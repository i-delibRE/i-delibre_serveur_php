<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class DataviewComponent extends Component {

	/**
	 * Controller
	 *
	 * @var type
	 */
	public $controller;

	/**
	 * Component initialization
	 *
	 * @access public
	 * @param type $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
	}

	/**
	 *
	 */
	function keepShowStyle() {
		if (!empty($this->controller->request->data)) {
			if ($this->controller->request->data['show'] === 'more') {
				$this->controller->request->params['named']['show'] = 'more';
			}
		}
	}

	/**
	 * Password generator function
	 *
	 */
	function processFilters() {
		$conditions = array();
		$critere = '';
		if (!empty($this->controller->request->data)) { //lecture des données de recherche envoyées
			$field = '';
			$search = '';
			$searchDate2 = '';
			$criteriaField = '';
			$criteriaSearch = '';
			//si on lance un recherche
			if (!empty($this->controller->request->data['field']) && $this->controller->request->data['field'] != __d('default', 'search.field')) {
				$criteriaField = $this->controller->request->data['field'];
				$field = $this->controller->request->data['field'];
				//en fonction du type de champ
				if ($this->controller->request->data['search-type'] === 'search-text') { //recherche sur champ texte
					$field .= ' ilike';
					$search = preg_replace('/\*/', '%', $this->controller->request->data['search-text']);
                    $search = '%'. $search .'%';
					$criteriaSearch = $this->controller->request->data['search-text'];
				} else if ($this->controller->request->data['search-type'] === 'search-foreignkey') { //recherche sur foreignkey
					$search = $this->controller->request->data['search-foreignkey'];
					$criteriaSearch = $this->controller->request->data['search-foreignkey-name'];
				} else if ($this->controller->request->data['search-type'] === 'search-boolean') { //recherche sur boolean
					$search = $this->controller->request->data['search-boolean'];
					$criteriaSearch = __d('default', $this->controller->request->data['search-boolean'] == "1" ? 'yes' : 'no');
				} else if ($this->controller->request->data['search-type'] === 'search-datetime') { //recherche sur champ date
					//date simple
					$tmp = explode('/', $this->controller->request->data['search-datetime']);
					$search = $tmp[2] . '-' . $tmp[1] . '-' . $tmp[0] . ' 00:00:00';
					$criteriaSearch = $this->controller->request->data['search-datetime'];

					//periode
					if (!empty($this->controller->request->data['search-datetime-to']) && $this->controller->request->data['search-datetime-to'] != $this->controller->request->data['search-datetime']) {
						$tmp2 = explode('/', $this->controller->request->data['search-datetime-to']);
						$searchDate2 = $tmp2[2] . '-' . $tmp2[1] . '-' . $tmp2[0] . ' 00:00:00';
						$criteriaSearch = __d('default', 'search.date.from') . ' ' . $this->controller->request->data['search-datetime'] . ' ' . __d('default', 'search.date.to') . ' ' . $this->controller->request->data['search-datetime-to'];
					}
				}
			}

			//dans tous les cas
			if (empty($searchDate2)) {
				$conditions = array($this->controller->modelClass . '.' . $field => $search);
			} else { //dans le cas d'une recherche sur une période
				$conditions = array(
					array(
						'and' => array(
							array($this->controller->modelClass . '.' . $field . ' >=' => $search),
							array($this->controller->modelClass . '.' . $field . ' <=' => $searchDate2),
						)
					)
				);
			}

			//definition du nom du fichier de traduction
			$poDomain = Inflector::underscore($this->controller->modelClass);
//			if ($poDomain == 'srvrole') {
//				$poDomain == 'group';
//			}
//			if ($poDomain == 'srvuser') {
//				$poDomain == 'user';
//			}
			//definition des critère à afficher pour rappel à l'utilisateur
			$critere = __d($poDomain, $this->controller->modelClass . '.' . $criteriaField) . ' : ' . $criteriaSearch;

			//enregistrement en session
			$this->_setToSession($conditions, $critere);
		} else if ($this->_checkSession()) {//si pas de donnée envoyée, on vérifie en session les paramètres enregistrés
			$this->_getFromSession($conditions, $critere);
		}

		return array('conditions' => $conditions, 'critere' => $critere);
	}

	/**
	 * Ecrit les données en session
	 *
	 * @param array $conditions
	 * @param string $critere
	 */
	private function _setToSession($conditions, $critere) {
		CakeSession::write('Search.' . $this->controller->name . '.' . $this->controller->action . '.conditions', $conditions);
		CakeSession::write('Search.' . $this->controller->name . '.' . $this->controller->action . '.critere', $critere);
	}

	/**
	 * Lit les données enregistrées en session
	 *
	 * @param array $conditions
	 * @param string $critere
	 */
	private function _getFromSession(&$conditions, &$critere) {
		$conditions = CakeSession::read('Search.' . $this->controller->name . '.' . $this->controller->action . '.conditions');
		$critere = CakeSession::read('Search.' . $this->controller->name . '.' . $this->controller->action . '.critere');
	}

	/**
	 * Vérifie si les données sont enregistrées en session
	 *
	 * @return boolean
	 */
	private function _checkSession() {
		return CakeSession::check('Search.' . $this->controller->name . '.' . $this->controller->action . '.conditions') &&
				CakeSession::check('Search.' . $this->controller->name . '.' . $this->controller->action . '.critere');
	}

	/**
	 * Vide les données enregistrées en session
	 *
	 */
	public function cancelSearch() {
		CakeSession::delete('Search.' . $this->controller->name . '.' . $this->controller->action . '.conditions');
		CakeSession::delete('Search.' . $this->controller->name . '.' . $this->controller->action . '.critere');
	}

	/**
	 * Ajoute les éléments pour fabriquer les select des model liés (pour le formulaire de recherche par exemple)
	 * et ajoute les class css en fonction du type
	 *
	 * @param array $fields
	 */
	public function processFields($fields) {
		foreach ($fields as $key => $conf) {
			if ($key == 'id') {
				$conf['class'] = 'uuid';
			}

			if (!empty($conf['boolean'])) {
				$conf['class'] = 'boolean';
			}
			if (!empty($conf['datetime'])) {
				$conf['class'] = 'datetime';
			}

			if (!empty($conf['foreignkey'])) {
				$conf['class'] = 'foreignkey';
				$model = $conf['linkedModel'];
                               
				$this->controller->loadModel($model);
				
                                $this->controller->{$model}->recursive = -1;
                                
				$conf['select'] = $this->controller->{$model}->find('list');
                                
			}
			$fields[$key] = $conf;
		}
                
		return $fields;
	}

}

?>