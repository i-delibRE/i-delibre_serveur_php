<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */


/**
 * Pour le chargement dinamique des connexions qui sont stockées dans config/connection
 */
class DbConnectionComponent extends Component {

    /**
     * 
     * @param type $name = collectivite.conn
     * @param type $database
     * @param type $suffix
     */
    public function createStringTab($name, $database, $suffix) {


        $connection = "<?php " . "$" . "{$name} = array('datasource' => 'Database/Postgres', \n"
                . "'persistent' => false, \n"
                . "'host' =>'" . DATABASE_HOST . "', \n"
                . "'login' =>'" . DATABASE_LOGIN . "',\n"
                . "'password' =>'" . DATABASE_PASSWORD . "',\n"
                . "'database' => '{$database}',\n"
                . "'prefix' => '',\n"
                . "'port' =>" . DATABASE_PORT . ",\n"
                . "'encoding' => 'utf8',\n"
                . "'inMenu' => true );";


        $file = new File(APP . 'Config/Connections/' . $name, true, 0777);
        $file->write($connection);


        $nodeConnection = '{"' . $suffix . '": "postgres:' . DATABASE_LOGIN . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . ':' . DATABASE_PORT . '/' . $database . '"' . '}';

        $fileNode = new File(NODEJS_CONNECTIONS . $name, true, 0777);
        $fileNode->write($nodeConnection);




        $url = IDELIBRE_WS_PROTOCOL . '://' . IDELIBRE_WS_HOST . ':' . IDELIBRE_WS_PORT . '/' . 'addCollectivite/';

        $data = array(
            $suffix => 'postgres:' . DATABASE_LOGIN . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . ':' . DATABASE_PORT . '/' . $database
        );

        $jsonData = jSON_encode($data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData))
        );

        curl_setopt($ch, CURLOPT_URL, $url);
 //       curl_setopt($ch, CURLOPT_POST, count($jsonData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Supprime els fichiers de conf de connection de la collectivite
     * @param type $name = collectivite.conn
     */
    public function deleteFilesConfig($name) {
        $path = APP . 'Config/Connections/' . $name;
        $rp = realpath($path);
        if (is_writable($rp)) {
            unlink($rp);
        }

        $pathNode = APP . 'Vendor/SocketIO/Connections/' . $name;
        $rpNode = realpath($pathNode);
        if (is_writable($rpNode)) {
            unlink($pathNode);
        };
    }

    public function loadAllConnections() {


        foreach (glob(APP . "Config/Connections/*") as $filename) {
            include_once $filename;

            $connectionName = basename($filename);
            try {
            ConnectionManager::create($connectionName, ${$connectionName});
            }
            catch(Exception $e){

                CakeLog::write('DbDynCon', 'pas de connexion');
                CakeLog::write(var_export($e, true));


            }
        }
    }




    public function loadConnection($name) {


        $filename = APP . "Config/Connections/" . $name;
        $fileExists = file_exists($filename);

        if ($fileExists) {
            include $filename;
            ConnectionManager::create($name, $fileExists ? ${$name} : array());
        }


        //$connectionName = basename($filename);
        //ConnectionManager::create($connectionName, ${$connectionName});


    }

    public function addConnection($name, $database) {
        
    }

}
