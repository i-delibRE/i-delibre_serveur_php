<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');


class EnvironnementsController extends AppController {

public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('goToClient');
}

    /**
     *
     * @var type
     */
    public $uses = null;

    /**
     *
     * @var type
     */
    public $envData = array(
        'loaded' => false,
        'menu' => array()
    );

    /**
     *
     */
    public function index() {
        $this->Session->write('ctxTitle', __d('ctx', 'ctx.env'));
    }

    /**
     *
     * @throws NotFoundException
     */
    public function menu() {

        //  $this->Session->write('ctxTitle', __d('ctx', 'ctx.menu'));

        if ($this->Session->read('Auth.env') == 'superadmin') {
            $this->_genSuperadminEnv();
        } else if ($this->Session->read('Auth.env') == 'walletmanager') {
            $this->_genWalletManagerEnv();
        } else if ($this->Session->read('Auth.env') == 'admin') {
            $this->_genAdminEnv();
        } else if ($this->Session->read('Auth.env') == 'superuser') {   //secretaire
            $this->_genSuperUserEnv();
        } else if ($this->Session->read('Auth.env') == 'baseuser') {  // elu acteur
            $this->_genBaseUserEnv();
        } else {
            throw new NotFoundException(__d('env', 'env.check.error'));
        }

        $this->envData['loaded'] = true;
        if (!$this->Session->check('Auth.envData') || !$this->Session->read('Auth.envData.loaded')) {
            $this->Session->write('Auth.envData', $this->envData);
        }
    }

    /**
     *
     */
    private function _genWalletManagerEnv() {
        $this->loadModel('Collectivite');
        $this->envData['menu'] = array(
            __d('ctx', 'ctx.Srvusers.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Srvusers',
                'icon-class' => 'fa fa-user'
            ),
            __d('ctx', 'ctx.Environnements.params') => array(
                'class' => 'btn btn-huge btn-inverse',
                'url' => '/Environnements/params',
                'icon-class' => 'fa fa-cogs icon-white'
            )
        );
    }

    /**
     *
     */
    private function _genSuperadminEnv() {
        $this->loadModel('Collectivite');
        $this->envData['menu'] = array(
            __d('ctx', 'ctx.Collectivites.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Collectivites',
                'icon-class' => 'fa fa-building'
            ),
            __d('ctx', 'ctx.Srvusers.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Srvusers',
                'icon-class' => 'fa fa-user'
            ),
            "Import des utilisateurs" => array(
                'class' => 'btn btn-huge',
                'url' => '/Importusers/import',
                'icon-class' => 'fa fa-user'
            ),
            __d('ctx', 'ctx.Environnements.params') => array(
                'class' => 'btn btn-huge btn-inverse',
                'url' => '/Environnements/params',
                'icon-class' => 'fa fa-cogs icon-white'
            ),
            __d('ctx', 'ctx.Pages.display.cakechecks') => array(
                'class' => 'btn btn-huge btn-success',
                'url' => '/Pages/display/cakechecks',
                'icon-class' => 'fa fa-check-square-o'
            ),
            __d('ctx', 'ctx.Pages.display.appchecks') => array(
                'class' => 'btn btn-huge btn-success',
                'url' => '/Pages/display/appchecks',
                'icon-class' => 'fa fa-check-square-o'
            ),
            __d('ctx', 'ctx.Environnements.statistics') => array(
                'class' => 'btn btn-huge',
                'url' => '/Statistics/lastSeances/3',
                'icon-class' => 'fa fa-bar-chart'
            )
        );
    }

    /**
     *
     */
    private function _genAdminEnv() {
        $this->envData['menu'] = array(
            __d('ctx', 'ctx.Environnements.stats') => array(
                'class' => 'btn btn-huge btn-primary',
                'url' => '/Environnements/stats',
                'icon-class' => 'fa fa-dashboard icon-white'
            ),
            __d('ctx', 'ctx.Users.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Users',
                'icon-class' => 'fa fa-user'
            ),
            __d('ctx', 'ctx.Groupepolitique.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Groupepolitiques',
                'icon-class' => 'fa fa-users'
            ),
            __d('ctx', 'ctx.Types.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Types',
                'icon-class' => 'fa fa-tag'
            ),
            __d('ctx', 'ctx.Pthemes.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Pthemes',
                'icon-class' => 'ls-theme'
            ),
            __d('ctx', 'ctx.Email.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Emailconvocations',
                'icon-class' => 'fa fa-envelope'
            ), __d('ctx', 'ctx.Emailinvitations.index') => array(
                'class' => 'btn btn-huge',
                'url' => '/Emailinvitations',
                'icon-class' => 'fa fa-envelope'
            ),
            __d('ctx', 'ctx.Seances.liste') => array(
                'class' => 'btn btn-huge btn-success',
                'url' => '/manageSeances',
                'icon-class' => 'ls-seance'
            ),
            __d('ctx', 'ctx.Collectivites.info') => array(
                'class' => 'btn btn-huge btn-inverse',
                'url' => '/Collectivites/info',
                'icon-class' => 'fa fa-info icon-white'
            ),
            __d('ctx', 'ctx.Environnements.params') => array(
                'class' => 'btn btn-huge btn-inverse',
                'url' => '/Environnements/params',
                'icon-class' => 'fa fa-cogs icon-white'
            )
        );
    }

    /**
     *
     */
    private function _genSuperUserEnv() {
        $this->envData['menu'] = array(
            __d('ctx', 'ctx.Environnements.stats') => array(
                'class' => 'btn btn-huge btn-primary',
                'url' => '/Environnements/stats',
                'icon-class' => 'fa fa-dashboard icon-white'
            ),
            __d('ctx', 'ctx.Seances.liste') => array(
                'class' => 'btn btn-huge btn-success',
                'url' => '/manageSeances',
                'icon-class' => 'ls-seance'
            ),
            __d('ctx', 'ctx.Environnements.params') => array(
                'class' => 'btn btn-huge btn-inverse',
                'url' => '/Environnements/params',
                'icon-class' => 'fa fa-cogs icon-white'
            )
        );
    }

    /**
     *
     */
    private function _genBaseUserEnv() {
        $this->envData['menu'] = array(
            __d('ctx', 'ctx.goClient') => array(
                'class' => 'btn btn-huge',
                //'url' => '/idelibre_client',
                'url' => '/Environnements/goToClient',
                'icon-class' => 'icon-share-alt'
            ),
            __d('ctx', 'ctx.Environnements.params') => array(
                'class' => 'btn btn-huge btn-inverse',
                'url' => '/Environnements/params',
                'icon-class' => 'fa fa-cogs icon-white'
            )
        );
    }
    
    
    public function goToClient(){
        $this->Auth->logout();
        $this->redirect('/idelibre_client');
    }

    /**
     *
     */
    public function params() {
        $model = ClassRegistry::init($this->Session->read('Auth.userModel'));
        $model->recursive = -1;

        $srvuser = $this->Session->read('Auth.userModel') == 'Srvuser' ? true : false;

        if (!empty($this->request->data)) {
            if (!empty($this->request->data[$model->name]['password'])) {//changement de mot de passe
                $user = $model->find('first', array('conditions' => array($model->name . '.id' => $this->request->data[$model->name]['id'])));
                if ($user[$model->name]['password'] === AuthComponent::password($this->request->data[$model->name]['password_old'])) {
                    $user[$model->name]['password'] = AuthComponent::password($this->request->data[$model->name]['password']);
                    $user[$model->name]['confirm'] = $this->request->data[$model->name]['confirm'];
                }

                if ($model->save($user)) {
                    $this->Session->setFlash(__d(Inflector::underscore($model->name), $model->name . '.password.new.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                    $this->redirect(array('action' => 'params'));
                } else {
                    $this->Session->setFlash(__d(Inflector::underscore($model->name), $model->name . '.password.new.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                }
            } else {//enrgistrement des informations utlisateur et configuration
                if ($model->save($this->request->data)) {
                    $this->Session->setFlash(__d(Inflector::underscore($model->name), $model->name . '.edit.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                    $this->redirect(array('action' => 'params'));
                } else {
                    $this->Session->setFlash(__d(Inflector::underscore($model->name), $model->name . '.edit.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                }
            }
        }

        $qdUserData = array(
            'fields' => array(
                $model->alias . '.id',
                $model->alias . '.firstname',
                $model->alias . '.lastname',
                $model->alias . '.username',
                $model->alias . '.last_login',
                $model->alias . '.created',
                $model->alias . '.modified',
                $model->alias . '.last_logout',
                $model->alias . '.mail',
                ($srvuser ? 'Srvrole' : 'Group') . '.id',
                ($srvuser ? 'Srvrole' : 'Group') . '.name'
            ),
            'contain' => array(
                $srvuser ? 'Srvrole' : 'Group'
            ),
            'conditions' => array(
                $model->alias . '.id' => $this->Session->read('Auth.User.id')
            )
        );

        if (!$srvuser) {
            $qdUserData['fields'][] = 'User.last_import';
            $qdUserData['fields'][] = 'Group.admin';
            $qdUserData['fields'][] = 'Group.inseance';
            $qdUserData['fields'][] = 'Group.superuser';
            $qdUserData['fields'][] = 'Group.baseuser';
        }

        $this->request->data = $model->find('first', $qdUserData);
        $this->set('srvuser', $srvuser);
    }

    /**
     *
     * @param type $id
     */
    public function stats() {
        $this->set('seances', ClassRegistry::init('Seance')->getConvocs());
        $res =  ClassRegistry::init('Seance')->getConvocs();
//        debug($res); die;
    }


    public function statsJson(){
        $this->autorender = false;
        $seances =  ClassRegistry::init('Seance')->getConvocs();
        echo json_encode($seances);
        die;
    }

    /**
     *
     */
    private function _userSyncs() {
        $this->loadModel('User');
        $qdUsers = array(
            'fields' => array(
                'User.id',
                'User.name',
                'User.rev',
                'User.rev_date'
            ),
            'recursive' => -1,
            'contain' => array(
                "Group" => array(
                    'fields' => array(
                        'Group.id'
                    )
                ),
            ),
            'order' => array('User.firstname', 'User.lastname'),
            'conditions' => array(
                'Group.inseance' => true,
                'Group.baseuser' => true
            )
        );
        $users = $this->User->find('all', $qdUsers);

        $syncs = array();
        foreach ($users as $user) {
            $syncs[$user['User']['id']] = array(
                'name' => $user['User']['name'],
                'syncNeeded' => $this->User->isSyncNeeded($user['User']['id']),
                'rev' => $user['User']['rev'],
                'rev_date' => $user['User']['rev_date']
            );
        }
        return $syncs;
    }

    /**
     *
     * @return boolean
     */
    private function _userConns() {
        $this->loadModel('ICakeSession');
        $this->ICakeSession->useDbConfig = 'default';
        $sessions = $this->ICakeSession->find('all');

        //lecture des sessions en base
        $currentDbSessions = array();
        for ($i = 0; $i < count($sessions); $i++) {
            $raw = $this->ICakeSession->unserialize($sessions[$i]['ICakeSession']['data']);

            if (Set::check($raw, 'Auth.collectivite.conn') && $raw['Auth']['collectivite']['conn'] == CakeSession::read('Auth.collectivite.conn')) {
                $user_uuid = $raw['Auth']['User']['id'];

                if (!isset($currentDbSessions[$user_uuid])) {
                    $currentDbSessions[$user_uuid] = array();
                }

                $currentDbSessions[$user_uuid][] = array(//pour chaque utilisateur, on recupere toutes les sessions
                    'userAgent' => $raw['Config']['userAgent'],
                    'expires' => (int) $sessions[$i]['ICakeSession']['expires'],
                    'isActiveUser' => (int) $sessions[$i]['ICakeSession']['expires'] > time() // on verifie les dates d expiration
                );
            }
        }

//on recupere les utilisateurs
        $this->loadModel('User');
        $qdUsers = array(
            'recursive' => -1,
            'fields' => array(
                'User.id',
                'User.name'
            ),
            'order' => array('User.firstname', 'User.lastname')
        );
        $users = $this->User->find('all', $qdUsers);

        for ($i = 0; $i < count($users); $i++) { // pour chaque utilisateur
            if (!empty($currentDbSessions[$users[$i]['User']['id']])) {
                //on verifie si au moins une des sessions de l'utilisateur n'est pas expirée
                $activeUserAgents = Hash::extract($currentDbSessions[$users[$i]['User']['id']], '{n}.isActiveUser');

                $users[$i]['User']['isActiveUser'] = false;
                if (in_array(true, $activeUserAgents, true)) {
                    $users[$i]['User']['isActiveUser'] = true;

                    $lastExpires = null; //recuperation de la date d'expiration la plus lointaine
                    for ($j = 0; $j < count($currentDbSessions[$users[$i]['User']['id']]); $j++) {
                        if ($lastExpires == null || ($currentDbSessions[$users[$i]['User']['id']][$j]['expires'] > $lastExpires)) {
                            $lastExpires = $currentDbSessions[$users[$i]['User']['id']][$j]['expires'];
                        }
                    }
                    $users[$i]['User']['lastExpires'] = $lastExpires;
                }
            } else { //si pas de session, on n'est pas connecté
                $users[$i]['User']['isActiveUser'] = false;
            }
        }

        return $users;
    }

}

?>
