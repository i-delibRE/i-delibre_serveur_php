<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');


class PthemesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
       // $this->Auth->allow();
    }

    /**
     *
     * @var type
     */
    public $helpers = array('Tree');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Ptheme->recursive = -1;


        $this->set('controller', $this->name);
        $this->set('action', $this->action);
        $this->set('title', __d('ptheme', 'Ptheme.list'));
        $this->set('addurl', Router::url(array('action' => 'add')));
        $this->set('deleteUrl', Router::url(array('controller' => 'Pthemes', 'action' => 'delete')));
        $this->set('addtitle', __d('ptheme', 'Ptheme.add'));
        $this->set('model', $this->modelClass);

        $fields = array(
            'id' => array(
                'label' => __d('ptheme', 'Ptheme.id'),
                'class' => 'uuid',
            ),
            'name' => array(
                'label' => __d('ptheme', 'Ptheme.name')
            )
        );
        $this->set('fields', $fields);



        //récupération du rang et de l'id du theme
        $ranks = $this->Ptheme->find('all', array(
            'fields' => array('id', 'rank')
        ));

        $rankById = array();
        foreach ($ranks as $rank) {
            $rankById[$rank['Ptheme']['id']] = $rank['Ptheme']['rank'];
        }


        //crée le décalage des sous themes
        $treelist = $this->Ptheme->generateTreeList();



        $data = array();
        foreach ($treelist as $id => $name) {
            $data[] = array('Ptheme' => array('id' => $id, 'name' => $name, 'rank' => $rankById[$id]));
        }


        $this->set('data', $data);
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->request->data['Ptheme']['fullname'] = $this->request->data['Ptheme']['name'];
            if(!empty($this->request->data['Ptheme']['parent_id'])) {
                $arrayThemeNames = Hash::extract($this->Ptheme->getPath($this->request->data['Ptheme']['parent_id']), "{n}.Ptheme.name");
                $fullThemeNames = trim(implode(",", $arrayThemeNames));
                $this->request->data['Ptheme']['fullname'] = $fullThemeNames . ', ' . $this->request->data['Ptheme']['name'];
            }


            $this->Ptheme->create();
            if ($this->Ptheme->save($this->request->data)) {
                $this->Session->setFlash(__d('ptheme', 'Ptheme.add.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('ptheme', 'Ptheme.add.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            }
        }
        $parentPthemes = $this->Ptheme->generateTreeList(null, null, null, ' -- ');
        $this->set(compact('parentPthemes'));
    }






    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {

        if (!$this->Ptheme->exists($id)) {
            throw new NotFoundException(__('Invalid ptheme'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $arrayThemeNames = Hash::extract($this->Ptheme->getPath($id), "{n}.Ptheme.name");
            $fullThemeNames = trim(implode(",", $arrayThemeNames));
            $this->request->data['Ptheme']['fullname'] = $fullThemeNames;




            if ($this->Ptheme->save($this->request->data)) {
                $this->Session->setFlash(__d('ptheme', 'Ptheme.edit.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('ptheme', 'Ptheme.edit.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('Ptheme.' . $this->Ptheme->primaryKey => $id));
            $this->request->data = $this->Ptheme->find('first', $options);
        }
//		$parentPthemes = $this->Ptheme->ParentPtheme->find('threaded', array('conditions' => array('ParentPtheme.' . $this->Ptheme->ParentPtheme->primaryKey . ' !=' => $id)));
        $parentPthemes = $this->Ptheme->generateTreeList(array('Ptheme.' . $this->Ptheme->primaryKey . ' !=' => $id), null, null, ' -- ');
        $this->set(compact('parentPthemes'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Ptheme->id = $id;
        if (!$this->Ptheme->exists()) {
            throw new NotFoundException(__('Invalid ptheme'));
        }
        if ($this->Ptheme->delete()) {
            $this->Session->setFlash(__d('ptheme', 'Ptheme.delete.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__d('ptheme', 'Ptheme.delete.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     *
     */
    public function getList() {

        if ($this->request->isAjax() && $this->request->is('get')) {
            $this->set('pthemes', $this->Ptheme->find('threaded', array(
                'order' => array('name')
            )));

            $th = $this->Ptheme->find('threaded', array(
                'order' => array('name')
            ));

            $this->log(var_export(json_encode($th), true), "ptheme");

        } else {
            throw new NotFoundException(__d('default', 'ajax.only'));
        }
    }




    public function getListJson() {
        $Ptheme = ClassRegistry::init('Ptheme');
        $th = $Ptheme->find('threaded', array(
            'order' => array('name')
        ));
        $this->autoRender = false;
        echo(json_encode($th));
    }


    public function getJson($id){
        $this->autoRender = false;
        $ptheme = $this->Ptheme->find("first", array(
            "conditions" => array(
                "Ptheme.id" => $id
            ),
            "contain" => array(
                "ParentPtheme", "ChildPtheme"
            ),

        ));

        echo json_encode(["theme" => $ptheme["Ptheme"],
            "parent" => $ptheme["ParentPtheme"],
            "children" => $ptheme["ChildPtheme"]
        ]);

    }

    public function rankit() {

        if ($this->request->is('post') || $this->request->is('post')) {
            //debug($this->request->data);
            $pThemes = $this->request->data['Ptheme'];



            $this->Ptheme->begin();

            $success = true;

            foreach ($pThemes as $theme) {
                $this->Ptheme->id = $theme['id'];
              //  debug($theme);
                $success = $this->Ptheme->save($theme) && $success;
            }

            if ($success) {
                $this->Ptheme->commit();
                $this->Session->setFlash(__d('ptheme', 'Ptheme.rank.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Ptheme->rollback();
                $this->Session->setFlash(__d('ptheme', 'Ptheme.rank.error'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }
        }



        $themes = $this->Ptheme->find('all', array(
            'fields' => array('name', 'rank', 'id'),
            'order' => 'rank',
        ));

        $this->set(compact('themes'));

        // $this->request->data = $themes;
    }

}
