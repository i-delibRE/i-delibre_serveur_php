<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 30/03/18
 * Time: 16:54
 */
App::uses('AppController', 'Controller');

require_once(APP . "Utility/ForceUtf8.php");
use \ForceUTF8\Encoding;

class ImportusersController extends AppController
{


    public function index(){
        $this->redirect(array('controller' => "Environnements",'action' => 'menu'));
    }

    public function import()
    {



        if ($this->request->is('post') || $this->request->is('put')) {

//debug($this->request->data);
            $conn = $this->request->data["connection"];
            $filename = $_FILES['usersCSV']['tmp_name'];




            if($conn == null || $filename == null){
                $this->Session->setFlash("Erreur lors de l'import", 'bootstrap_flash', array('class' => 'alert alert-danger'));
                $this->redirect(array('action' => 'import'));
                return;
            }

            $this->DbConnection->loadConnection($conn);

            $User = ClassRegistry::init('User');
            $User->useDbConfig = $conn;
            $User->Aro->useDbConfig = $conn;

            $handle = fopen($filename, 'r');

            $User->begin();
            $error = false;

            while (($row = fgetcsv($handle, 500, ',')) !== FALSE) {
                $User->create();
                // $User->useDbConfig = $conn;
                unset($User->validate['password']['checkIdenticalValues']);

                $groupeId ="";
                $grouptype = $row[5];

                if($grouptype == 1){ //secretaire
                    $groupeId = IDELIBRE_GROUP_USER_ID;
                }

                if($grouptype == 2){ //admin
                    $groupeId = IDELIBRE_GROUP_ADMIN_ID;
                }

                if($grouptype == 3){ //acteurs
                    $groupeId = IDELIBRE_GROUP_AGENT_ID;
                }
                if($grouptype == 4){ //adminitratifs
                    $groupeId = IDELIBRE_GROUP_ADMINISTRATIF_ID;
                }
                if($grouptype == 5){ //Invités
                    $groupeId = IDELIBRE_GROUP_INVITE_ID;
                }

                $user = array();


                $user['username'] = Encoding::fixUTF8(utf8_encode($row[0]));
                $user['firstname'] = Encoding::fixUTF8(utf8_encode($row[1]));
                $user['lastname'] = Encoding::fixUTF8(utf8_encode($row[2]));
                $user['mail'] = $row[3];

                $user['password'] =  AuthComponent::password($row[4]);
                $user['group_id'] = $groupeId;
                $user['groupepolitique_id'] = 1;
                $res = $User->save($user);

                if(!$res){
                    $error = true;
                    DevTools::logMe($User->validationErrors, IMPORT, true);
                    DevTools::logMe($user, IMPORT, true);
                }
            }
            fclose($handle);

            if(!$error){
                $User->commit();
                $this->Session->setFlash("Import réussi", 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'import'));
            } else {
                $User->rollBack();
                $this->Session->setFlash("Erreur lors de l'import", 'bootstrap_flash', array('class' => 'alert alert-danger'));
                $this->redirect(array('action' => 'import'));
            }


        }
    }


}