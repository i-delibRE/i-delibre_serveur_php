<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');
App::uses('File', 'Utility');

class CollectivitesController extends AppController {
    // a changer, gérer cela avec les acl
    /*    public function beforeFilter() {
      parent::beforeFilter();
      if($this->Session->read('Auth.env') !== 'superadmin'){
      throw new NotFoundException('acces interdit');
      }
      }
     */

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Dataview', 'DbConnection');

    /**
     * index method
     *
     * @return void
     */
    public function index($cancel = false) {

        if ($this->Session->read('Auth.env') !== 'superadmin') {
            throw new NotFoundException('acces interdit');
        }


        if ($cancel) {
            $this->Dataview->cancelSearch();
        }

        $this->Dataview->keepShowStyle();

        $dataviewInfos = $this->Dataview->processFilters();
        $this->set('critere', $dataviewInfos['critere']);

        $this->Collectivite->recursive = -1;
        $this->paginate = array_merge($this->paginate, array('conditions' => $dataviewInfos['conditions']));

        $this->set('controller', Inflector::camelize($this->name));
        $this->set('action', Inflector::camelize($this->action));
        $this->set('title', __d('collectivite', 'Collectivite.list'));
        $this->set('addurl', Router::url(array('action' => 'add')));
        $this->set('addtitle', __d('collectivite', 'Collectivite.add'));
        $this->set('deleteUrl', Router::url(array('controller' => 'Collectivites', 'action' => 'delete')));
        $this->set('model', $this->modelClass);
        $fields = array(
            'id' => array('label' => __d('collectivite', 'Collectivite.id')),
            'name' => array('label' => __d('collectivite', 'Collectivite.name')),
            'conn' => array('label' => __d('collectivite', 'Collectivite.conn')),
            'login_suffix' => array('label' => __d('collectivite', 'Collectivite.login_suffix')),
            'active' => array('boolean' => true, 'label' => __d('collectivite', 'Collectivite.active')),
            'created' => array('datetime' => true, 'label' => __d('collectivite', 'Collectivite.created')),
            'modified' => array('datetime' => true, 'label' => __d('collectivite', 'Collectivite.modified'))
        );
        $this->set('fields', $this->Dataview->processFields($fields));
        $this->set('data', $this->paginate());
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {

        if ($this->Session->read('Auth.env') !== 'superadmin') {
            throw new NotFoundException('acces interdit');
        }

        if ($this->request->is('post')) {

            $loginSuffix = $this->request->data['Collectivite']['login_suffix'];

            // on defini la connection comme etant le suffix en minuscule et sans carractere speciaux
            $conn = strtolower(preg_replace("/[^A-Za-z0-9 ]/", '', $loginSuffix));

            $bdd = 'idelibre_' . $conn;

            $this->request->data['Collectivite']['conn'] = $conn;
            $this->request->data['Collectivite']['bdd'] = $bdd;

            $data = $this->request->data;

            //création dynamique de la connexion à la bdd
            $this->DbConnection->createStringTab($data['Collectivite']['conn'], $data['Collectivite']['bdd'], $data['Collectivite']['login_suffix']);

            $valid = array(
                'success' => false,
                'msg' => __d('collectivite', 'Collectivite.add.error'),
                'class' => 'alert-danger'
            );
            $this->Collectivite->create();

            $conns = ConnectionManager::enumConnectionObjects();

            try {
                //$this->Collectivite->query('create database ' . $conns[$this->request->data['Collectivite']['conn']]['database'] . ' template ' . $conns['template']['database'] . ';');
                $this->Collectivite->query('create database ' . $data['Collectivite']['bdd'] . ' template ' . $conns['template']['database'] . ';');

                //on charge la nouvelle connection  TODO uniquement la nouvelle collectivité !!!!
                //$this->DbConnection->loadAllConnections();
                $this->DbConnection->loadConnection($data['Collectivite']['conn']);

                $this->Collectivite->begin();

                //on force la collectivité à être active par défaut
                $this->request->data['Collectivite']['active'] = true;

                if ($this->Collectivite->save($this->request->data)) {
                    //init model
                    $this->loadmodel('User');
                    $this->User->setDataSource($this->request->data['Collectivite']['conn']);
                    $this->loadmodel('Aro');
                    $this->Aro->setDataSource($this->request->data['Collectivite']['conn']);

                    //hash password
                    $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
                    //recup id admin
                    $this->request->data['User']['id'] = $this->User->field('id', array('username' => 'Administrateur'));

                    if ($this->User->save($this->request->data)) {
                        $valid['success'] = true;
                        $valid['msg'] = __d('collectivite', 'Collectivite.add.ok');
                        $valid['class'] = 'alert-success';
                    } else {
                        $valid['msg'] .= '<br /><br />' . __d('collectivite', 'Collectivite.add.error.admin_edit');
                    }
                }
            } catch (Exception $exc) {
                $valid['msg'] .= '<br /><br />' . __d('collectivite', 'Collectivite.add.error.db') . '<hr />' . (Configure::read('debug') > 0 ? $exc->getTraceAsString() : '');
            }

            $this->Session->setFlash($valid['msg'], 'bootstrap_flash', array('class' => 'alert ' . $valid['class']));
            if ($valid['success']) {
                $this->Collectivite->commit();
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Collectivite->rollback();
                //TODO: demander la confirmation pour la suppression de la base
            }
        }
//        $srvusers = $this->Collectivite->Srvuser->find('list');
//        $this->set(compact('srvusers'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {

        if ($this->Session->read('Auth.env') !== 'superadmin') {
            throw new NotFoundException('acces interdit');
        }

        if (!$this->Collectivite->exists($id)) {
            throw new NotFoundException(__d('collectivite', 'Collectivite.notfound'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Collectivite->save($this->request->data)) {
                $this->Session->setFlash(__d('collectivite', 'Collectivite.edit.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('collectivite', 'Collectivite.edit.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            }
        }
        $qd = array('conditions' => array('Collectivite.' . $this->Collectivite->primaryKey => $id));
        $this->request->data = $this->Collectivite->find('first', $qd);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {


        if ($this->Session->read('Auth.env') !== 'superadmin') {
            throw new NotFoundException('acces interdit');
        }


        $this->Collectivite->id = $id;
        $conn = $this->Collectivite->field('conn');
        if (!$this->Collectivite->exists()) {
            throw new NotFoundException(__d('collectivite', 'Collectivite.notfound'));
        }

        if ($this->Collectivite->delete()) {
            //on detruit les fichier de conf
            $this->DbConnection->deleteFilesConfig($conn);
            $this->Session->setFlash(__d('collectivite', 'Collectivite.delete.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }

        $this->Session->setFlash(__d('collectivite', 'Collectivite.delete.error'), 'bootstrap_flash', array('class' => 'alert alert-warning'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     *
     */
    public function info() {
        if (!empty($this->request->data)) {
            $collectivite = $this->Collectivite->create($this->request->data);
            $success = $this->Collectivite->save($collectivite);
            $this->Session->setFlash(__d('collectivite', 'Collectivite.edit.' . ($success ? 'ok' : 'error')), 'bootstrap_flash', array('class' => 'alert alert-' . ($success ? 'success' : 'danger')));
        }
        $qd = array(
            'fields' => array(
                'Collectivite.id',
                'Collectivite.name',
                'Collectivite.conn',
                'Collectivite.login_suffix',
                'Collectivite.created',
                'Collectivite.active'
            ),
            'conditions' => array(
                'Collectivite.id' => $this->Session->read('Auth.collectivite.id')
            )
        );

        $this->request->data = $this->Collectivite->find('first', $qd);
    }




    public function infoJson() {
        $this->autoRender = false;

        $qd = array(
            'fields' => array(
                'Collectivite.id',
                'Collectivite.name',
                'Collectivite.conn',
                'Collectivite.login_suffix',
                'Collectivite.created',
                'Collectivite.active'
            ),
            'conditions' => array(
                'Collectivite.conn' =>  Configure::read('conn')
            )
        );

        $coll =  $this->Collectivite->find('first', $qd);

        echo json_encode($coll["Collectivite"]);


    }

}
