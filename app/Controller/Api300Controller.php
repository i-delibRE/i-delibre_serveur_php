<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');
App::uses("ConnectionManager", "Model");
App::uses('DbDynCon', 'Utility');
App::uses('ConvertionBalise', 'Utility');

class Api300Controller extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    /**
     * simple hello test
     * @return string
     */
    public function ping() {
        $this->autoRender = false;
        return "ping";
    }


    public function version() {
        $this->autoRender = false;
        return VERSION;
    }

    /**
     * post : [
     *          conn: "Connection"
     *          username : "secretaire"
     *          password : "idelibre" 
     * @return string :(success, error_database //database missing, error_user //auth error)
     */
    public function check() {

        $this->autoRender = false;
        $data = $this->request->data;
        //load the connection
        DbDynCon::loadConnection($data["conn"]);

        
        //if no connection
        if (empty(ConnectionManager::enumConnectionObjects()[$data["conn"]])) {
            $this->response->statusCode(403);
            return "error_database";
        }

        $User = ClassRegistry::init('User');
        $User->useDbConfig = $data["conn"];

        $user = $User->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'User.username' => $data["username"],
                'User.password' => AuthComponent::password($data["password"])
            )
        ));
        
        if (empty($user)) {
            $this->response->statusCode(403);
            return "error_user";
        } else {
            $this->response->statusCode(200);
            return "success";
        }
    }

    public function pub(){
        $this->autoRender = false;
        $evaluate = [];
        $evaluate['foo'] = "un";
        $evaluate['bar'] = "deux";

        $convoc = "test de convocation avec variable #toto  et #fin ";


        $res = ConvertionBalise::evaluate($evaluate, $convoc);

        debug($res); die;

//        (#[^#| ]+#)
    }


}
