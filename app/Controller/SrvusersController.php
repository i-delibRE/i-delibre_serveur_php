<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('Password', 'Utility');
App::uses('SSLCertificates', 'Utility');

class SrvusersController extends AppController {

    /**
     * Controller components
     *
     * @access public
     * @var array
     */
    public $components = array('Dataview', 'DbConnection');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('login', 'logout', 'mdplost', 'get_proof')); //get_proof a supprimer (vérifier les méthodes et les éventuelles répercutions en base)
    }



    /**
     * index method
     *
     * @return void
     */
    public function index($cancel = false) {
        if ($cancel) {
            $this->Dataview->cancelSearch();
        }

        $this->Dataview->keepShowStyle();

        $dataviewInfos = $this->Dataview->processFilters();
        $this->set('critere', $dataviewInfos['critere']);


        $dataviewInfos['conditions'] = array( 'Srvuser.srvrole_id' => IDELIBRE_ADMIN_GROUP_ADMIN_ID );

        $this->Srvuser->recursive = -1;
        $this->paginate = array_merge($this->paginate, array('contain' => array('Srvrole'), 'conditions' => $dataviewInfos['conditions']));




        $this->set('controller', Inflector::camelize($this->name));
        $this->set('action', Inflector::camelize($this->action));
        $this->set('title', __d('srvuser', 'Srvuser.list'));
        $this->set('addurl', Router::url(array('action' => 'add')));
        $this->set('deleteUrl', Router::url(array('controller' => 'Srvusers', 'action' => 'delete')));
        $this->set('addtitle', __d('srvuser', 'Srvuser.add'));
        $this->set('model', $this->modelClass);
        $fields = array(
            'id' => array('label' => __d('srvuser', 'Srvuser.id')),
            'username' => array('label' => __d('srvuser', 'Srvuser.username')),
            'firstname' => array('label' => __d('srvuser', 'Srvuser.firstname')),
            'lastname' => array('label' => __d('srvuser', 'Srvuser.lastname')),
            'mail' => array('label' => __d('srvuser', 'Srvuser.mail')),
            'srvrole_id' => array(
                'label' => __d('srvuser', 'Srvuser.srvrole_id'),
                'foreignkey' => true,
                'linkedDisplayField' => $this->Srvuser->Srvrole->displayField,
                'linkedModel' => 'Srvrole',
                'linkedController' => 'srvroles',
                'linkedAction' => 'view'
            ),
            'last_login' => array('datetime' => true, 'label' => __d('srvuser', 'Srvuser.last_login')),
            'last_logout' => array('datetime' => true, 'label' => __d('srvuser', 'Srvuser.last_logout')),
            'created' => array('datetime' => true, 'label' => __d('srvuser', 'Srvuser.created')),
            'modified' => array('datetime' => true, 'label' => __d('srvuser', 'Srvuser.modified'))
        );


        $this->set('forbiddenActions', array('add' => false, 'edit' => false, 'delete' => true));
        if ($this->Session->read('Auth.env') != 'superadmin') {
            $this->set('forbiddenActions', array('add' => false, 'edit' => false, 'delete' => true));
        }
        $this->set('fields', $this->Dataview->processFields($fields));
        $this->set('data', $this->paginate());
    }

    private function addAdminColl($data) {
        $Collectivite = ClassRegistry::init('Collectivite');
        $conn = $Collectivite->field('conn', array('Collectivite.id' => $data['CollAdmin']['collId']));


        $this->DbConnection->loadConnection($conn);
        $this->setConn($conn);
        $User = ClassRegistry::init('User');
        $user['User']['firstname'] = $data['Srvuser']['firstname'];
        $user['User']['lastname'] = $data['Srvuser']['lastname'];
        $user['User']['mail'] = $data['Srvuser']['mail'];
        $user['User']['username'] = $data['Srvuser']['username'];

        $user['User']['password'] = AuthComponent::password($this->request->data['Srvuser']['password']);
        $user['User']['confirm'] = $this->request->data['Srvuser']['password'];
        $user['User']['group_id'] = IDELIBRE_GROUP_ADMIN_ID;

        $success = $User->save($user);

        if ($success) {

            $this->Session->setFlash(__d('srvuser', 'Srvuser.add.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__d('srvuser', 'Srvuser.add.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            $errors = $User->validationErrors;
            return($errors);
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        // changer pour des acl
        if ($this->Session->read('Auth.env') == 'baseuser') {
            //  throw new NotFoundException('acces interdit');
        }

        if ($this->request->is('post')) {

            if ($this->request->data['Srvuser']['srvrole_id'] == IDELIBRE_GROUP_ADMIN_ID) {
                $this->Srvuser->validationErrors = $this->addAdminColl($this->request->data);
            } else {
                $this->Srvuser->begin();
                $this->Srvuser->create();

                //traitement du mot de passe dans le cas d'un utilisateur superadmin
                if (!empty($this->request->data['Srvuser']['password'])) {
                    $this->request->data['Srvuser']['password'] = AuthComponent::password($this->request->data['Srvuser']['password']);
                }

                //creation de l'utilisateur
                if ($this->Srvuser->save($this->request->data)) {
                    $this->Session->setFlash(__d('srvuser', 'Srvuser.add.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                    $this->Srvuser->commit();
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Srvuser->rollback();
                    $this->Session->setFlash(__d('srvuser', 'Srvuser.add.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }

        $srvroles = $this->Srvuser->Srvrole->find('list');
        $Collectivite = ClassRegistry::init("Collectivite");
        $srvuserColls = $Collectivite->find('list');

        // on rajoute l'administrateur de collectivité dans la liste
        $srvroles[IDELIBRE_GROUP_ADMIN_ID] = "administrateur de collectivité";
        $this->set(compact('srvroles', 'srvuserColls'));
    }



    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        // changer pour des acl
        if ($this->Session->read('Auth.env') == 'baseuser') {
            throw new NotFoundException('acces interdit');
        }


        if (!$this->Srvuser->exists($id)) {
            throw new NotFoundException(__('Invalid srvuser'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            if (!empty($this->request->data['Srvuser']['newpass'])) {
                $this->request->data['Srvuser']['password'] = $this->request->data['Srvuser']['newpass'];
            }
            if (!empty($this->request->data['Srvuser']['password'])) {
                $this->request->data['Srvuser']['password'] = AuthComponent::password($this->request->data['Srvuser']['password']);
            }

            if (!empty($this->request->data['Srvuser']) &&
                $this->request->data['Srvuser']['srvrole_id'] === IDELIBRE_ADMIN_GROUP_ADMIN_ID) {

                $result = $this->editSuperAdmin($id);
            }


            if ($result) {
                //success
                $this->Session->setFlash(__d('srvuser', 'Srvuser.edit.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('srvuser', 'Srvuser.edit.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                $this->redirect(array('action' => 'index'));
            }
        }

        $this->setConn("default");
        $options = array('conditions' => array('Srvuser.' . $this->Srvuser->primaryKey => $id));
        $this->request->data = $this->Srvuser->find('first', $options);

        $qdSrvroles['conditions'] = array('id' => IDELIBRE_ADMIN_GROUP_USER_ID);
        $srvroles = $this->Srvuser->Srvrole->find('list', $qdSrvroles);
        $this->set(compact('srvroles'));


    }





    public function editSuperAdmin($id) {
        return $this->Srvuser->save($this->request->data);
    }



    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        // changer pour des acl
        if ($this->Session->read('Auth.env') == 'baseuser') {
            //throw new NotFoundException('acces interdit');
        }
        $this->Srvuser->id = $id;
        if (!$this->Srvuser->exists()) {
            throw new NotFoundException(__('Invalid srvuser'));
        }
        if ($this->Srvuser->delete()) {
            $this->Session->setFlash(__d('srvuser', 'Srvuser.delete.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__d('srvuser', 'Srvuser.delete.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     *
     * @param type $proof
     * @param type $sign
     * @return type
     */
    private function _selectCollWithCertificate($sign) {
        $selectedColl = array();
        $this->loadModel('Collectivite');
        $certsColls = $this->Collectivite->find('all', array('fields' => array('Collectivite.conn', 'Collectivite.name', 'Collectivite.public_key', 'Collectivite.login_suffix'), 'conditions' => array('Collectivite.active' => true, 'Collectivite.use_cert' => true)));
        foreach ($certsColls as $collitem) {
            if (SSLCertificates::verifySSLSign($this->Session->read('proof'), SSLCertificates::hex2bin($sign), $collitem['Collectivite']['public_key'])) {
                $selectedColl = $collitem;
            }
        }
        $loginParams = array('userModel' => 'User', 'roleModel' => 'Group', 'login_suffix' => !empty($selectedColl) ? $selectedColl['Collectivite']['login_suffix'] : '');
        $this->Session->delete('proof');
        return array($selectedColl, $loginParams);
    }

    /**
     *
     * @param type $loginParams
     * @return type
     */
    private function _selectColl() {
        $this->loadModel('Collectivite');
        $loginParams = $this->processLogin();
        $selectedColl = $this->Collectivite->find('first', array('conditions' => array('Collectivite.active' => true, 'Collectivite.use_cert' => false, 'Collectivite.login_suffix' => $loginParams['login_suffix'])));
        return array($selectedColl, $loginParams);
    }

    /**
     * Genere une chaîne texte aléatoire de 512 caractères
     * @deprecated since version 1.1
     * @return string
     */
    private function _genProof() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = 512;
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    /**
     * @deprecated since version 1.1
     */
    public function get_proof() {
        $proof = $this->_genProof();

        $this->Session->write('proof', $proof);
        $this->set('proof', $proof);

        if ($this->isJsonp) {
            $this->set('callback', $this->request->query['callback']);
        }
    }

    /**
     *
     */
    public function login() {

        $this->Session->write('ctxTitle', __d('ctx', 'ctx.login'));
        $this->layout = 'login';

        if (!empty($this->request->data)) {

            //rajoute le @admin si il n'y a rien
            $matches = array();
            (preg_match('#(.*)@(.*)$#', $this->request->data['Srvuser']['username'], $matches));

            if (empty($matches)) {
                $this->request->data['Srvuser']['username'] = $this->request->data['Srvuser']['username'] . '@admin';
            }

            list($selectedColl, $loginParams) = $this->_selectColl();

            //connexion
            $logged = false;

            //todo: pour un log sans aucun @ envoyer au writelogin() params uniquement la collectivite correspondante au login et le srvrole
            // si le login n'existe pas envoyer une erreur !

            $this->writeLoginConfig($selectedColl, $loginParams);

            if (!empty($selectedColl) || $loginParams['login_suffix'] == "default") {
                $this->Auth->authenticate = array('Form' => array('userModel' => $loginParams['userModel']));
                $this->Auth->constructAuthenticate();
                if (Configure::read('conn') != 'default') {
                    $this->request->data['User'] = $this->request->data['Srvuser'];
                }
                if ($this->Auth->login()) {
                    $logged = true;


                }
            }

            $userInfos = array(
                'id' => '',
                'prenom' => '',
                'nom' => '',
                'logged' => false
            );

            //traitement post connexion
            if ($logged) {
                $this->writeUserConfig();
                $this->writeLoginSession();
                $this->setConn(Configure::read('conn'));


                $userModel = $this->Session->read('Auth.userModel');
                $this->loadModel($userModel);
                $this->{$userModel}->id = $this->Session->read('Auth.User.id');
                $this->{$userModel}->saveField('last_login', 'NOW()');
                $this->Session->setFlash(__d('login', 'login.success', true), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->createBcryptPassword($this->request->data["Srvuser"]['password']);

                if (!$this->isAjax && !$this->isJsonp) {
                    $redirect = $this->Auth->redirect();
                } else {
                    $userInfos = array(
                        'id' => $this->{$userModel}->id,
                        'prenom' => $this->{$userModel}->field('firstname'),
                        'nom' => $this->{$userModel}->field('lastname'),
                        'logged' => $logged
                    );
                }
            } else if ($this->Session->read('Message.flash.message') != __d('default', 'login.logout')) {
                if (!$this->isAjax && !$this->isJsonp) {
                    $this->Session->setFlash(__d('login', 'login.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                    $redirect = "/Srvusers/login";
                }
            }

            if ($this->isAjax || $this->isJsonp) {
                $this->set("userInfos", $userInfos);
            } else {
                $this->redirect($redirect);
            }
        }
    }

    /**
     *
     */
    public function logout() {
        if ($this->isAjax || $this->isJsonp) {
//			$this->autoRender = false;
        } else {
            $this->Session->setFlash(__d('login', 'login.logout'), 'bootstrap_flash', array('class' => 'alert alert-info'));
        }

        if ($this->Session->check('Auth.userModel')) {
            $userModel = $this->Session->read('Auth.userModel');
            $this->loadModel($userModel);
            $this->{$userModel}->id = $this->Session->read('Auth.User.id');
            $this->{$userModel}->saveField('last_logout', 'NOW()');
        }
        $this->Session->delete('Auth');
        $this->Session->destroy();

        if (!$this->isAjax && !$this->isJsonp) {
            $this->Auth->logout();

            $this->redirect(array('controller' => "srvusers"  ,'action' => 'login'));

        } else {
            $this->set("userInfos", array('logout', 'ok'));
        }
    }


    /**
     * Fonction permettant la gestion du mot de passe oublié
     *
     * @access public
     * @return void
     */
    public function mdplost($from = null) {
        $this->layout = 'login';
        $this->Session->write('ctxTitle', __d('ctx', 'ctx.mdplost'));

        $this->set('from', $from);

        if (!empty($this->request->data)) {
            $from_client = (isset($this->request->data['from_client']) && $this->request->data['from_client'] == 1);
            $redirect = $from_client ? '/idelibre_client' : $this->referer();
            $redirectSuccess = $from_client ? '/idelibre_client' : '/Srvusers/login';

            $this->loadModel('Collectivite');
            $conns = $this->Collectivite->find('list', array('fields' => array('Collectivite.conn', 'Collectivite.name'), 'conditions' => array('Collectivite.active' => true)));
            $this->set('conns', $conns);
            // Récupération du suffix permettant de connaître la collectivité sur laquelle se connecter
            $loginParams = $this->processLogin();
            $this->loadModel($loginParams['userModel']);

            if (empty($loginParams['login_suffix'])) {

                $this->Session->setFlash(__d('mdplost', 'save_error_suffix'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            } else {

                $qdIsActive = array(
                    'conditions' => array(
                        'Collectivite.active' => true,
                        'Collectivite.login_suffix' => $loginParams['login_suffix']
                    )
                );
                $selectedColl = $this->Collectivite->find('first', $qdIsActive);


                if (!empty($selectedColl)) {

                    // Connexion à la collectivité en question pour rechercher l'utilisateur en question
                    $this->setConn($selectedColl['Collectivite']['conn']);
                    $this->{$loginParams['userModel']}->useDbConfig = $selectedColl['Collectivite']['conn'];

                    $this->{$loginParams['userModel']}->begin();
                    $qdUser = array(
                        'fields' => array(
                            $loginParams['userModel'] . '.id',
                            $loginParams['userModel'] . '.username',
                            $loginParams['userModel'] . '.active',
                            $loginParams['userModel'] . '.mail'
                        ),
                        'conditions' => array(
                            $loginParams['userModel'] . '.username' => $this->request->data['Srvuser']['username'],
                            $loginParams['userModel'] . '.mail' => $this->request->data['Srvuser']['mail']
                        ),
                        'contain' => false
                    );
                    $user = $this->{$loginParams['userModel']}->find('first', $qdUser);



                    // Si l'utilisateur existe, on vérifie le couple login/mot de passe
                    if (empty($user)) {

                        $this->Session->setFlash(__d('mdplost', 'save_error_couple'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                        $this->{$loginParams['userModel']}->rollback();
                        $this->redirect($redirect);
                    } else if (!$user[$loginParams['userModel']]['active']) {

                        $this->Session->setFlash(__d('mdplost', 'save_error_inactif'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                        $this->{$loginParams['userModel']}->rollback();
                        $this->redirect($redirect);
                    } else {

                        // génération du nouveau mot de passe
                        $newpass = Password::generatePassword();

                        $success = false;
                        $errorMessage = 'save_error';
                        $this->{$loginParams['userModel']}->id = $user[$loginParams['userModel']]['id'];
                        if ($this->{$loginParams['userModel']}->saveField('password', AuthComponent::password($newpass))) {

                            if ($this->{$loginParams['userModel']}->saveField('pwdmodified', true)) {
                                //                              debug("into the success"); die;
                                $success = true;
                                // Paramètres mail
                                try {
                                    $Email = new CakeEmail('convocation');
                                    $Email->to(Configure::read('debug') == 0 ? $this->{$loginParams['userModel']}->field('mail') : $Email->from());
                                    $Email->subject('Changement de mot de passe dans i-delibRE');
                                    $mailBody = "Bonjour,\nsuite à votre demande, veuillez trouver ci-dessous vos nouveaux identifiants: " . "\n" . "  Rappel de votre identifiant : " . $user[$loginParams['userModel']]['username'] . "@" . $selectedColl['Collectivite']['login_suffix'] . "\n" . "  Votre nouveau mot de passe : " . $newpass . "\n\nA votre première connexion, il vous sera demandé de modifier ce mot de passe.";
                                    $result = $Email->send($mailBody);
                                    $success = !empty($result) && $success;
                                } catch (Exception $e) {
                                    $this->log($e->getMessage(), LOG_ERROR);
                                    $success = false;
                                    $errorMessage = 'save_error_mail';
                                }
                            }
                        }
                        //Sauvegarde du nouveau mot de passe de l'utilisateur et envoi du mail
                        if ($success) {
                            $this->{$loginParams['userModel']}->commit();
                            $this->Session->setFlash(__d('mdplost', 'save_success'), 'bootstrap_flash', array('class' => 'alert alert-info'));
                            $this->redirect($redirectSuccess);
                        } else {
                            $this->{$loginParams['userModel']}->rollback();
                            $this->Session->setFlash(__d('mdplost', $errorMessage), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                        }
                    }
                }
            }
        }
    }

}
