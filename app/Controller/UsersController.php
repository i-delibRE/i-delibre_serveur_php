<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');
App::uses('Password', 'Utility');


class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Dataview');

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('login', 'logout', 'verif'));
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($cancel = false) {
        if ($cancel) {
            $this->Dataview->cancelSearch();
        }

        $this->Dataview->keepShowStyle();

        $dataviewInfos = $this->Dataview->processFilters();
        $this->set('critere', $dataviewInfos['critere']);



        $this->User->recursive = -1;
        $this->paginate = array_merge($this->paginate, array('contain' => array('Group', 'Groupepolitique'), 'conditions' => $dataviewInfos['conditions']));


        $this->set('controller', Inflector::camelize($this->name));
        $this->set('action', Inflector::camelize($this->action));
        $this->set('title', __d('user', 'User.list'));
        $this->set('addurl', Router::url(array('action' => 'add')));
        $this->set('deleteUrl', Router::url(array('controller' => 'Users', 'action' => 'delete')));
        $this->set('addtitle', __d('user', 'User.add'));
        $this->set('model', $this->modelClass);

        $fields = array(
            'id' => array('label' => __d('user', 'User.id')),
            'username' => array('label' => __d('user', 'User.username')),
            'firstname' => array('label' => __d('user', 'User.firstname')),
            'lastname' => array('label' => __d('user', 'User.lastname')),
            'mail' => array('label' => __d('user', 'User.mail')),
            'group_id' => array(
                'label' => __d('user', 'User.group_id'),
                'foreignkey' => true,
                'linkedDisplayField' => $this->User->Group->displayField,
                'linkedModel' => 'Group',
                //'linkedController' => 'groups',
                //'linkedAction' => 'view'
            ),
            'groupepolitique_id' => array(
                'label' => "Groupe politique",
                'foreignkey' => true,
                'linkedDisplayField' => 'name',
                'linkedModel' => 'Groupepolitique',
                // 'linkedController' => 'groupecontrollers',
            ),
            'active' => array('boolean' => true, 'label' => __d('user', 'User.active')),
            'last_login' => array('datetime' => true, 'label' => __d('user', 'User.last_login')),
            'last_logout' => array('datetime' => true, 'label' => __d('user', 'User.last_logout')),
            'created' => array('datetime' => true, 'label' => __d('user', 'User.created')),
            'modified' => array('datetime' => true, 'label' => __d('user', 'User.modified'))
        );


        $otherAction = array(
//            'title' => 'logs de connexions',
//            'link' => '/Logacteur/view/',
//            'btn' => 'tooltiped btn btn-primary',
//            'icon' => 'icon-eye-open'
        );

        $this->set(compact('otherAction'));


        $this->set('fields', $this->Dataview->processFields($fields));
        $this->set('data', $this->paginate());



    }

    public function listJson(){
        $this->autoRender = false;
        $users = $this->User->find("all", [
           "contain" => ["Groupepolitique", "Group"]


        ]);
        echo json_encode($users);
    }


    /**
     * add method
     *
     * @return void
     */
    public function add($from = '') {

//throw new NotFoundException('moment indisponible');
        if ($this->request->is('post')) {
            //si le compte est celui d'un utilisateur normal alors le groupepolitique est null
            if ($this->request->data['User']['group_id'] != IDELIBRE_GROUP_AGENT_ID) {
                $this->request->data['User']['groupepolitique_id'] = null;
            }

            $this->User->create();
            $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            unset($this->User->validate['password']['checkIdenticalValues']);

            $this->User->begin();

            if ($res = $this->User->save($this->request->data)) {
                $this->Session->setFlash(__d('user', 'User.add.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->User->commit();
                $this->redirect(array('action' => 'index'));

            } else {
                $this->User->rollback();
                $this->Session->setFlash(__d('user', 'User.add.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
                $this->redirect(array('action' => 'index'));
            }
        }
        $groups = $this->User->Group->find('list');
        $groupepolitiques = $this->User->Groupepolitique->find('list');

        $this->set(compact('groups', 'from', 'groupepolitiques'));
    }


    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //throw new NotFoundException('moment indisponible');

        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {


            $this->User->id = $id;

            //on verifie si ce user est un acteur, dans ce cas on ne peut modifier ni son username ni son groups
            $userEdited = $this->User->find('first', array(
                'recursive' => -1,
                'conditions' => array(
                    'User.id' => $id
                )
            ));

            if (!empty($this->request->data['User']['newpass'])) {
                $this->request->data['User']['password'] = $this->request->data['User']['newpass'];
            }

            if (!empty($this->request->data['User']['password'])) {
                //update the bcrypt !
                $this->request->data['User']['bcrypt'] = BlowfishPasswordHasher::hash($this->request->data['User']['password']);
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            }

            //if it's a simple user
            if($userEdited['User']['group_id'] === IDELIBRE_GROUP_USER_ID){
                $this->request->data['Authorized'] = $this->request->data['User']["types"];
                if(empty($this->request->data['Authorized'])){
                    $this->request->data['Authorized'] = [];
                }

            }



            if ($this->User->saveAll($this->request->data)) {

                $this->Session->setFlash(__d('user', 'User.edit.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));


            } else {
                $this->Session->setFlash(__d('user', 'User.edit.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $groups = $this->User->Group->find('list');
        $groupepolitiques = $this->User->Groupepolitique->find('list');

        $this->set(compact('groups', 'userconfs', 'groupepolitiques'));

        if($this->request->data['User']["group_id"] == IDELIBRE_GROUP_USER_ID){
            $Types = ClassRegistry::init("Type");
            $types = $Types->find("list", array(
                "fields" => array("Type.id", "Type.name")
            ));

            $User = ClassRegistry::init('User');
            $authorized = $User->TypesAuthorized->find('all',array(
                'fields' => array('TypesAuthorized.type_id'),
                'conditions' => array(
                    "TypesAuthorized.user_id" => $this->request->data['User']["id"]
                )
            ));

            $authorized = Hash::extract($authorized, "{n}.TypesAuthorized.type_id");

            $this->set(compact('types', 'authorized'));
        }
    }



    public function getUserJson($id){
        $this->autoRender = false;

        if(!$this->User->exists($id)){
           echo json_encode(["error" => true]); return;
        }

        $user = $this->User->find('first', [
            'conditions' => ['User.id' => $id],
            'contain' => ['Group', 'Groupepolitique']
        ]);

        echo json_encode($user);
    }


    public function listActorsJson(){
        $this->autoRender = false;
        $users = $this->User->find("all", array(
            'conditions' => array(
                'User.group_id' => IDELIBRE_GROUP_AGENT_ID
            ),
            "fields" => array(
                'User.id', 'User.name', 'User.firstname', 'User.lastname'
            ),
            "order" => array('User.lastname')
        ));

        $users = Hash::extract($users, '{n}.User');

        echo json_encode(["users" => $users]);

    }


    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {
            $this->Session->setFlash(__d('user', 'User.delete.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }else {
            $this->Session->setFlash(__d('user', 'User.delete.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     *
     */
    public function login() {
        $this->redirect('/Srvusers/login');
    }

    /**
     *
     */
    public function logout() {
        $this->redirect('/Srvusers/logout');
    }

    /**
     *
     */
    public function mdplost() {
        $this->redirect('/Srvusers/mdplost');
    }


}
