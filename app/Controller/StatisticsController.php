<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class StatisticsController extends AppController {


    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
//        $this->Auth->allow(); //get_proof a supprimer (vérifier les méthodes et les éventuelles répercutions en base)
    }



    public function users()
    {
        $this->autoRender = false;

        Configure::write('debug', 2);
        $Collectivite = ClassRegistry::init("Collectivite", "default");
        $collectivites = $Collectivite->find('all', array(
            "fields" => Array('conn', 'name')
        ));

        $allCollectivity = array();
        foreach ($collectivites as $coll) {
            $allCollectivity[$coll["Collectivite"]["name"]] = $this->usersByConn($coll["Collectivite"]["conn"]);
        }
        $this->createUsersCsv($allCollectivity);
    }




    private function createUsersCsv($collArray){
        $status = "Collectivité;type : nb;type : nb;type : nb \r\n";

        foreach ($collArray as $key => $coll) {
            $status = $status . $key . ';' . $coll[0]['group_id'] . $coll[0]['count'] . ';'  . $coll[1]['group_id'] . $coll[1]['count'] . ';' . $coll[1]['group_id'] . $coll[1]['count'] . "\r\n";
        }

        $status = chr(239) . chr(187) . chr(191) . $status;


        $filePath = "/tmp/" . "userscollectivite" .'.csv';
        $file = new File($filePath, true, 0777);
        $file->write($status);

        $this->response->file(
            $filePath,
            array('download' => true, 'name' => "userscollectivite" . ".csv")
        );
        return $this->response;
    }





    public function lastSeances($month)
    {
        $this->autoRender = false;

        Configure::write('debug', 2);
        $Collectivite = ClassRegistry::init("Collectivite", "default");
        $collectivites = $Collectivite->find('all', array(
            "fields" => Array('conn', 'name')
        ));

        $allCollectivity = array();
        foreach ($collectivites as $coll) {
            $allCollectivity[$coll["Collectivite"]["name"]] = $this->seancesByConn($coll["Collectivite"]["conn"], $month);
        }
        $this->createCsv($allCollectivity);
    }


    private function createCsv($collArray){
        $status = "Collectivité;nombre de séances;Lues;Non lue\r\n";

        foreach ($collArray as $key => $coll) {
            $status = $status . $key . ';' . $coll['seances'] . ';' . $coll['ar']  . ';' . $coll['notAR']  . "\r\n";
        }

        $status = chr(239) . chr(187) . chr(191) . $status;


        $filePath = "/tmp/" . "collectivite" .'.csv';
        $file = new File($filePath, true, 0777);
        $file->write($status);

        $this->response->file(
            $filePath,
            array('download' => true, 'name' => "collectivite" . ".csv")
        );
        return $this->response;
    }




    private function usersByConn($conn){
        ClassRegistry::flush();
        $this->setConn($conn);

        $User = ClassRegistry::init("User");
        $User->setDataSource($conn);


        $users = $User->query("select group_id, count(group_id) from users group by group_id");

        foreach ($users as &$user){
            if ($user[0]["group_id"] == IDELIBRE_GROUP_ADMIN_ID){
                $user[0]["group_id"] = "admin";
            }
            if ($user[0]["group_id"] == IDELIBRE_GROUP_AGENT_ID){
                $user[0]["group_id"] = "acteur";
            }
            if ($user[0]["group_id"] == IDELIBRE_GROUP_USER_ID){
                $user[0]["group_id"] = "utilisateur";
            }
        }


        $users = Hash::extract($users, '{n}.{n}');
        return $users;

    }


    private function seancesByConn($conn, $month){
        ClassRegistry::flush();
        $this->setConn($conn);


        $date = new DateTime();
        date_sub($date, date_interval_create_from_date_string($month . ' months') );
        $dateFrom = $date->format('Y-m-d');

        $Seance = ClassRegistry::init("Seance");
        $Seance->setDataSource($conn);
        $seances = $Seance->find("all", array(
            "conditions" => array(
                "date_seance >" =>  $dateFrom
            ),
            "fields" => "Seance.id",
            "contain" => array("Convocation")
        ));




        $read = 0;
        $notRead = 0;
        $seanceNb = count($seances);

        foreach ($seances as $seance){
            foreach ($seance["Convocation"] as $convoc){
                if($convoc["ar_horodatage"])
                    $read++;
                else
                    $notRead++;
            }
        }

        $ret = array(
            "seances" => $seanceNb,
            "ar" => $read,
            "notAR" => $notRead
        );
        return $ret;
    }


}