<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');


class TypesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Dataview');

    /**
     * index method
     *
     * @return void
     */
    public function index($cancel = false) {

        if ($cancel) {
            $this->Dataview->cancelSearch();
        }

        $this->Dataview->keepShowStyle();

        $dataviewInfos = $this->Dataview->processFilters();
        $this->set('critere', $dataviewInfos['critere']);

        $this->Type->recursive = -1;
        $this->paginate = array_merge($this->paginate, array('conditions' => $dataviewInfos['conditions']));

        $this->set('controller', Inflector::camelize($this->name));
        $this->set('action', Inflector::camelize($this->action));
        $this->set('title', __d('type', 'Type.list'));
        $this->set('addurl', Router::url(array('action' => 'add')));
        $this->set('deleteUrl', Router::url(array('controller' => 'Types', 'action' => 'delete')));
        $this->set('addtitle', __d('type', 'Type.add'));
        $this->set('model', $this->modelClass);
        $fields = array(
            'id' => array('label' => __d('type', 'Type.id')),
            'name' => array('label' => __d('type', 'Type.name')),
            'created' => array('datetime' => true, 'label' => __d('type', 'Type.created')),
            'modified' => array('datetime' => true, 'label' => __d('type', 'Type.modified'))
        );

        $this->set('fields', $this->Dataview->processFields($fields));
        $this->set('data', $this->paginate());
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Type->create();
            if ($this->Type->saveAll($this->request->data)) {
                $this->Session->setFlash(__d('type', 'Type.add.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('type', 'Type.add.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            }
        }
        $qd = array(
            'fields' => array(
                'User.id',
                'User.name'
            ),
            'contain' => array(
                $this->Type->User->Group->alias
            ),
            'conditions' => array(
                'Group.inseance' => true,
                'Group.baseuser' => true,
                'Group.superuser' => false,
                'Group.admin' => false
            ),
            'order' => array('User.lastname')
        );
        $users = $this->Type->User->find('list', $qd);
        $this->set(compact('users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Type->exists($id)) {
            throw new NotFoundException(__('Invalid type'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data["Authorized"] = $this->request->data ["Type"]['secretaire'];



            if(empty($this->request->data["Authorized"])){
                $this->request->data["Authorized"] = [];
            }


            if ($res = $this->Type->saveAll($this->request->data)) {

                $this->Session->setFlash(__d('type', 'Type.edit.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__d('type', 'Type.edit.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
            }
        } else {
            $data = $this->Type->TypesUser->find('list', array('conditions' => array('TypesUser.type_id' => $id), 'fields' => array('TypesUser.id', 'TypesUser.user_id')));
            $data = array('User' => array('User' => array_values($data)));
            $options = array('conditions' => array('Type.' . $this->Type->primaryKey => $id));
            $data = Hash::merge($data, $this->Type->find('first', $options));
            $this->request->data = $data;

        }


        $qd = array(
            'fields' => array(
                'User.id',
                'User.name'
            ),
            'contain' => array(
                $this->Type->User->Group->alias
            ),
            'conditions' => array(
                'Group.inseance' => true,
                'Group.baseuser' => true,
                'Group.superuser' => false,
                'Group.admin' => false
            ),
            'order' => array('User.lastname')

        );
        $users = $this->Type->User->find('list', $qd);
        //$this->set(compact('users', 'selected'));
        $this->set(compact('users'));


//debug($this->Type); die;

        $auth = $this->Type->TypesAuthorized->find('all', array('conditions' => array('TypesAuthorized.type_id' => $id), 'fields' => array('TypesAuthorized.id', 'TypesAuthorized.user_id')));
        //debug($auth); die;

        $selected = Hash::extract($auth, "{n}.TypesAuthorized.user_id");
        //$autho = array("TypesAutorized" => $formated);
        $this->set(compact('selected'));

        $secretaires =  $this->Type->User->find('list', array(
            'fields' => array('User.id', 'User.name'),
            'conditions' => array(
                'User.group_id' => IDELIBRE_GROUP_USER_ID
            ),
            'order' => array('User.lastname')
        ));

        $this->set(compact('secretaires'));

    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Type->id = $id;
        if (!$this->Type->exists()) {
            throw new NotFoundException(__('Invalid type'));
        }
        if ($this->Type->delete()) {
            $this->Session->setFlash(__d('type', 'Type.delete.ok'), 'bootstrap_flash', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__d('type', 'Type.delete.error'), 'bootstrap_flash', array('class' => 'alert alert-danger'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     * Récupération de la liste des utilisateurs liés à un type
     *
     * @param string $id UUID
     * @throws NotFoundException
     */
    public function users($id = null) {
        if (!$this->request->is('ajax') || !$this->request->is('get') || !$this->Type->exists($id)) {
            throw new NotFoundException();
        }
        $this->layout = '';
        $this->set('users', $this->Type->getUsersFromTypeId($id));
    }




    public function usersJson($id = null) {
        $return = array();
        if (!empty($id)) {
            $qd = array(
                'contain' => array(
                    'User' => array(
                        'conditions' => array('User.active' => true),
                        'fields' => array('User.id', 'User.name'),
                        'order' => 'User.lastname'
                    )
                ),
                'conditions' => array(
                    'Type.id' => $id,
                ),
                //
            );
            $type = $this->Type->find('first', $qd);
            $return = $type['User'];
        }
        $return = Hash::remove($return,"{n}.TypesUser");
        $this->autoRender = false;
        echo(json_encode($return));
    }

    public function listJson(){
        $this->autoRender = false;
        $types = $this->Type->find("all");
        $types = Hash::extract($types, '{n}.Type');

        echo json_encode(["types" => $types]);
    }

    public function deleteJson($id = null) {
        $this->autoRender = false;
        $this->Type->id = $id;
        if (!$this->Type->exists()) {
            echo json_encode(["success" => false]);
            return;
        }
        if ($this->Type->delete()) {
            echo json_encode(["success" => true, "id" => $id]);
        }else{
            echo json_encode(["success" => false]);
        }
    }

    public function detailJson($id){
        $this->autoRender = false;
        $type = $this->Type->find('first', array(
            "conditions" => array("Type.id" => $id),
            "contain" => array("TypesAuthorized"
            )
        ));

        $typeAuth = Hash::extract($type, "TypesAuthorized");

        $type = Hash::extract($type, "Type");



        $User = ClassRegistry::init("User");
        $users = $User->Find("all", array(
            "conditions" => array('User.group_id' => IDELIBRE_GROUP_USER_ID),
            "fields" => array("User.id", "User.name", 'User.firstname', 'User.lastname', 'User.name'),
            'order' => array('User.lastname')
        ));

        $users = Hash::extract($users,'{n}.User');

        echo json_encode(["type" => $type, "authorized" => $typeAuth, "users" => $users ]);
    }



}
