<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');

class SeancesController extends AppController {


    /**
     * Components
     *
     * @var array
     */
    public $components = array('Dataview', 'Horodatage', 'DbFile');

    /**
     * 	Identifiant de l'utilisateur effectuant une importation
     *
     * @var string (UUID)
     */
    private $_WsImportUserId;

    /**
     *
     * @throws ForbiddenException
     */
    /**
     *
     * @throws ForbiddenException
     */
    public function beforeFilter() {
        parent::beforeFilter();
        if ($this->action == 'add') {
            App::uses('Security', 'Utility');
            $data = array(
                'username' => filter_input(INPUT_POST, 'username'),
                'password' => Security::hash(filter_input(INPUT_POST, 'password'), null, true),
                'conn' => filter_input(INPUT_POST, 'conn')
            );
            $this->setConn($data['conn']);
            $userModel = ClassRegistry::init('User');
            $userModel->useDbConfig = $data['conn'];
            $qdUser = array(
                'recursive' => -1,
                'contain' => array(
                    'Group'
                ),
                'conditions' => array(
                    'User.username' => $data['username'],
                    'User.password' => $data['password'],
                    'User.active' => true,
                    'Group.superuser' => true
                )
            );
            $user = $userModel->find('first', $qdUser);

            if (empty($user) || !$this->Auth->login($user)) {
                throw new ForbiddenException();
            }

            //Add collectiviteId to session
            $Collectivite = ClassRegistry::init('Collectivite');
            $Collectivite->useDbConfig = "default";
            $selectedColl = $Collectivite->find('first', array(
                'conditions' => array(
                    'Collectivite.conn' => $data['conn']
                )));

            $_SESSION['Auth']['collectivite'] = array();
            $_SESSION['Auth']['collectivite']['id']= $selectedColl['Collectivite']['id'];

            $this->_WsImportUserId = $user['User']['id'];
        }
    }



    /**
     * add method
     *
     * @return void
     */
    // WebService Call
    public function add() {

        // if ($this->request->is('post') && $this->request->is('ssl')) {
        if ($this->request->is('post')) {
            DevTools::ldebug($this->request->data, true);
            $return = $this->Seance->genSeance(filter_input_array(INPUT_POST), $_FILES, true);
            $this->set('return', $return);
            if ($return['success'] && !empty($this->_WsImportUserId)) {
                DevTools::ldebug($return, true);
                $this->loadModel('User');
                $this->User->id = $this->_WsImportUserId;
                $this->User->saveField('last_import', date('Y-m-d H:i:s'));
            }
        } else {
            $this->autoRender = false;
        }
    }




    public function modifyUserPresence($seanceId) {
        $Convocation = ClassRegistry::init('Convocation');

        if($this->request->is("Post") || $this->request->is("Put")){
            if(key_exists("value", $this->request->data["Convocation"])) {


                if ($this->request->data["Convocation"]["value"] == "") {
                    $this->request->data["Convocation"]["value"] = null;
                }

                $Convocation->id = $this->request->data["Convocation"]["id"];
                $Convocation->saveField('presentstatus', $this->request->data["Convocation"]["value"]);
            }

        }

        $convocations = $Convocation->find('all', array(
            'conditions' => array(
                "Convocation.seance_id" => $seanceId
            ),
            "fields" => array("Convocation.id",'presentstatus'),
            "contain" => array(
                "User" => array(
                    "fields" => array( "User.id", "User.firstname", "User.lastname")
                )
            )
        ));

        $this->set("seanceId", $seanceId);
        $this->set("convocations", $convocations);

    }


    public function changePresence(){
        $this->autoRender = false;

        if($this->request->is("Post") || $this->request->is("Put")){
            $Convocation = ClassRegistry::init('Convocation');
            $convocations = $this->request->data;

            foreach ($convocations as $convocation) {
                $Convocation->create();
                $Convocation->id = $convocation["id"];
                $Convocation->saveField("presentstatus", $convocation["presentstatus"]);
                $Convocation->saveField("procuration_name", $convocation["procuration_name"]);
            }
            echo json_encode(["success" => true]);
        }
    }


    /**
     * Add new seance (post + get)
     */
    public function ajoutJson(){
        if (!empty($this->request->data)) {
            $result =  $this->Seance->genSeanceJson(filter_input_array(INPUT_POST), $_FILES);
            $this->autoRender = false;
            echo json_encode($result);
        }
        else {
            $User = ClassRegistry::init('User');
            $Type = ClassRegistry::init('Type');

            $qdUser = array(
                'recursive' => -1,
                'fields' => array(
                    'User.id',
                    'User.firstname',
                    'User.lastname',
                    'User.active',
                    'Group.name',
                    'Group.inseance'
                ),
                'order' => array('User.lastname'),
                'conditions' => array(
                    'User.active' => true,
                    'Group.inseance' => true
                ),
                'contain' => array(
                    'Group'
                )
            );
            $users = $User->find('all', $qdUser);

            $usersToSend = array();
            foreach ($users as $user) {
                $usersToSend[$user['User']['id']] = $user['User']['firstname'] . ' ' . $user['User']['lastname'];
            }

            $qdInvites = array(
                'recursive' => -1,
                'fields' => array(
                    'User.id',
                    'User.firstname',
                    'User.lastname',
                    'User.active',
                    'Group.name',
                    'Group.inseance'
                ),
                'order' => array('User.lastname'),
                'conditions' => array(
                    'User.active' => true,
                    'Group.id' => IDELIBRE_GROUP_INVITE_ID
                ),
                'contain' => array(
                    'Group'
                )
            );
            $invites = $User->find('all', $qdInvites);

            $invitesToSend = array();
            foreach ($invites as $invite) {
                $invitesToSend[$invite['User']['id']] = $invite['User']['firstname'] . ' ' . $invite['User']['lastname'];
            }

            $qdAdministratif = array(
                'recursive' => -1,
                'fields' => array(
                    'User.id',
                    'User.firstname',
                    'User.lastname',
                    'User.active',
                    'Group.name',
                    'Group.inseance'
                ),
                'order' => array('User.lastname'),
                'conditions' => array(
                    'User.active' => true,
                    'Group.id' => IDELIBRE_GROUP_ADMINISTRATIF_ID
                ),
                'contain' => array(
                    'Group'
                )
            );


            $administratifs = $User->find('all', $qdAdministratif);

            $administratifsToSend = array();
            foreach ($administratifs as $administratif) {
                $administratifsToSend[$administratif['User']['id']] = $administratif['User']['firstname'] . ' ' . $administratif['User']['lastname'];
            }

            $types = $Type->find('list');

            //if it's an user
            if($this->Session->read('Auth.env') == 'superuser'){
                $typeIds = $this->authorizedSeance($this->Auth->User('id'));
                foreach ($types as $key => $value){
                    if(!in_array($key, $typeIds)){
                        unset($types[$key]);
                    }
                }
            }

            $toSend = array(
                "users" => $usersToSend,
                "invites" => $invitesToSend,
                "administratifs" => $administratifsToSend,
                "types" => $types
            );

            $this->autoRender = false;
            echo json_encode($toSend);
        }
    }

    /**
     * initialise les variables pour la recherche (themes et types)
     */
    private function variableSearch() {


        $Type = ClassRegistry::init('Type');

        //recherche par type de seance
        $types = $Type->find('all', array(
            'recursive' => -1,
            'fields' => array('Type.name', 'Type.id'),
        ));

        $listTypes = array();
        foreach ($types as $type) {
            $listTypes[$type['Type']['id']] = $type['Type']['name'];
        }


        //recherche des themes
        $Ptheme = ClassRegistry::init('Ptheme');
        $pthemes = $Ptheme->find('all', array(
            'recursive' => -1,
            'fields' => array('Ptheme.id', 'Ptheme.name'),
        ));

        $listThemes = array();
        foreach ($pthemes as $ptheme) {
            $listThemes[$ptheme['Ptheme']['id']] = $ptheme['Ptheme']['name'];
        }


        $this->set(compact('listTypes', 'listThemes'));
    }


    /**
     * @param $userId
     * @return array
     * List authorized seance type for a given userId
     */
    private function authorizedSeance($userId){
        $User = ClassRegistry::init('User');
        $authorized = $User->TypesAuthorized->find('all',array(
            'fields' => array('TypesAuthorized.type_id'),
            'conditions' => array(
                "TypesAuthorized.user_id" => $userId
            )
        ));
        $formated = Hash::extract($authorized, "{n}.TypesAuthorized.type_id");
        return $formated;
    }


    /**
     * return the list of seances (get)
     */
    public function listeJson(){
        $this->autoRender = false;
        $seances = $this->Seance->find('all', array(
            'order' => 'Seance.date_seance DESC',
        ));
        $ret = Hash::extract($seances, '{n}.Seance');

        if($this->Session->read('Auth.env') == 'superuser'){
            $typeIds = $this->authorizedSeance($this->Auth->User('id'));
            for($i = count($ret) - 1; $i >= 0; $i--){
                if(!in_array($ret[$i]['type_id'], $typeIds)){
                    array_splice($ret, $i, 1);
                }
            }
        }

        echo json_encode($ret);
    }


    /**
     * @param $id
     * return detail for a giver seanceId
     */
    public function getSeanceDataJson($id){
        $this->autoRender = false;

        $qd = array(
            'fields' => array(
                'Seance.id',
                'Seance.name',
                'Seance.type_id',
                'Seance.date_seance',
                'Seance.document_id',
                'Seance.place',
                'Seance.archive'
            ),
            'contain' => array(
                'Document' => array(
                    'fields' => array(
                        'Document.id',
                        'Document.name'
                    )
                ),
                'Convocation' => array(
                    'fields' => array(
                        'Convocation.id',
                        'Convocation.read',
                        'Convocation.ar_horodatage',
                        'Convocation.active',
                        'Convocation.ae_horodatage',
                        'Convocation.seance_id',
                        'Convocation.presentstatus',
                        'Convocation.istoken',
                        'Convocation.isemailed',
                        'Convocation.procuration_name'
                    ),
                    'User' => array(
                        'fields' => array(
                            "User.id",
                            "User.name",
                            'User.lastname'
                        ),
                        'order' => array(
                            'User.lastname'
                        )
                    )
                ),
                'Projet' => array(
                    'order' => array('Projet.rank'),
                    'fields' => array(
                        "Projet.id",
                        "Projet.name",
                        "Projet.document_id",
                        "Projet.seance_id",
                        "Projet.vote",
                        "Projet.ptheme_id",
                        "Projet.rank",
                        "Projet.user_id"
                    ),
                    'Document' => array(
                        'fields' => array(
                            'Document.id',
                            'Document.name',
                            'Document.size'
                        )
                    ),
                    'Annex' => array(
                        'fields' => array(
                            'Annex.id',
                            'Annex.name',
                            'Annex.size',
                            'Annex.rank',
                        ),
                        'order' => 'Annex.rank ASC'
                    ),
                    'Ptheme' =>array(
                        "fields" => array(
                            "Ptheme.fullname"
                        )
                    )
                )
            ),
            'conditions' => array(
                'Seance.id' => $id
            ),
            'order' => 'Seance.date_seance DESC'
        );

        $seance = $this->Seance->find('first', $qd);


        @usort($seance['Convocation'], function($convA, $convB) {
            DevTools::logMe( $convA['User']['lastname'], "seance", true);
            return $convA['User']['lastname'] > $convB['User']['lastname'];
        });


        $qdUser = array(
            'recursive' => 2,
            'fields' => array(
                'User.id',
                'User.firstname',
                'User.lastname',
                'Group.name'
            ),
            'contain' => array(
                'Group' => array(
                )
            ),
            "conditions" => array(
                "User.group_id" => IDELIBRE_GROUP_AGENT_ID
            ),
            "order" => array(
                "User.lastname"
            )
        );

        $users = $this->Seance->Type->TypesUser->User->find('all', $qdUser);

        $usersToSend = array();
        foreach ($users as $user) {
            $usersToSend[$user['User']['id']] = $user['User']['firstname'] . ' ' . $user['User']['lastname'];
        }

        $Ptheme = ClassRegistry::init('Ptheme');

        $th = $Ptheme->find('threaded', array(
            'order' => array('name')
        ));

        $invites = $this->Seance->Invitation->find('all', array(
            'conditions' => array(
                "Invitation.seance_id" => $id
            ),
            'contain' => array(
                'User' => array(
                    "fields" => array(
                        'User.firstname', "User.lastname", "User.group_id", "User.id", "User.name"
                    )
                ),
                'Document' => array(
                    "fields" => array(
                        "Document.name", "Document.id"
                    )
                )
            )
        ));

        $invitesToSend = array();
        $administratifsToSend = array();
        foreach ($invites as $invite){
            if($invite['User']['group_id'] == IDELIBRE_GROUP_INVITE_ID) {
                $invitesToSend[] = $invite;
            }

            if($invite['User']['group_id'] == IDELIBRE_GROUP_ADMINISTRATIF_ID) {
                $administratifsToSend[] = $invite;
            }
        }

        $existingInvites = $this->Seance->Invitation->User->find('all', array(
            'fields' => array(
                'User.id',
                'User.name',
            ),
            'conditions' => array(
                'User.active' => true,
                'User.group_id' => IDELIBRE_GROUP_INVITE_ID
            ),
        ));

        $existingInvites = Hash::extract($existingInvites, "{n}.User");


        $existingAdministratifs = $this->Seance->Invitation->User->find('all', array(
            'fields' => array(
                'User.id',
                'User.name',
            ),
            'conditions' => array(
                'User.active' => true,
                'User.group_id' => IDELIBRE_GROUP_ADMINISTRATIF_ID
            ),
        ));

        $existingAdministratifs = Hash::extract($existingAdministratifs, "{n}.User");

        $toSend = array();
        $toSend["seance"] = $seance;
        $toSend["users"] = $users;
        $toSend["types"] = $this->Seance->Type->find('list');
        $toSend["themes"] = $th;
        $toSend["invites"] = $invitesToSend;
        $toSend["administratifs"] = $administratifsToSend;
        $toSend["existingInvites"] = $existingInvites;
        $toSend["existingAdministratifs"] = $existingAdministratifs;

        echo json_encode($toSend);
    }


    /**
     * @param $seance_id
     * Send all convocations for a given seanceId
     */
    public function sendConvocationsJson($seance_id) {
        if (empty($seance_id) || !$this->Seance->exists($seance_id)) {
            throw new NotFoundException(__d('seance', 'Seance.notFound'));
        }
        $this->autoRender = false;
        $qd = array(
            'recursive' => -1,
            'fields' => array(
                'Seance.id'
            ),
            'contain' => array(
                'Convocation' => array(
                    'fields' => array(
                        'Convocation.id'
                    ),
                    'conditions' => array(
                        'Convocation.active' => false
                    )
                )
            ),
            'conditions' => array(
                'Seance.id' => $seance_id
            )
        );

        $results = array();

        // Recherche toutes convocations associées
        $convocs = Hash::extract($this->Seance->find('first', $qd), "Convocation.{n}.id");
        for ($i = 0; $i < count($convocs); $i++) {
            $send = $this->Seance->Convocation->sendJson($convocs[$i]);
            array_push($results, $send );
        }
        echo json_encode($results);
    }



    /**
     *
     * @param type $seance_id
     * @throws NotFoundException
     *  refresh acteurs seance
     *
     */
    private function refreshSeance($seance_id) {
        if (empty($seance_id) || !$this->Seance->exists($seance_id)) {
            throw new NotFoundException(__d('seance', 'Seance.notFound'));
        }
        $this->autoRender = false;
        $qd = array(
            'recursive' => -1,
            'fields' => array(
                'Seance.id'
            ),
            'contain' => array(
                'Convocation' => array(
                    'fields' => array(
                        'Convocation.id'
                    ),
                    'conditions' => array(
                        'Convocation.active' => true
                    )
                )
            ),
            'conditions' => array(
                'Seance.id' => $seance_id
            )
        );

        $result = array();

        // Recherche toutes convocations associées
        $convocs = Hash::extract($this->Seance->find('first', $qd), "Convocation.{n}.id");

        for ($i = 0; $i < count($convocs); $i++) {
            //TODO utiliser un chemin qui ne passe pas par l'horodatage
            //  $result[] = array($i => $this->Seance->Convocation->resend($convocs[$i], $seance_id));
            $result[] = array($i => $this->Seance->Convocation->reSend($convocs[$i]));
        }

    }


    /**
     * @param $id
     * Delete a seance
     */
    public function deleteJson($id){
        $this->autoRender = false;

        $this->Seance->id = $id;
        if (!$this->Seance->exists()) {
            echo json_encode(["result" => false]);
            return;
        }

        if ($this->Seance->delete()) {
            echo json_encode(["result" => true]);
        } else {
            echo json_encode(["result" => false]);

        }
    }


    /**
     * @param $id
     * archive a seance (classer!)
     */
    public function archiveJson($id) {

        $this->autoRender = false;

        if (!$this->Seance->exists($id)) {
            echo json_encode(["result" => false]);
            return;
        }

        $seance = $this->Seance->find('first', array(
            'recursive' => -1,
            'conditions' => array('id' => $id),
            'contain' => array('Convocation', 'Invitation')
        ));



        // on en fait une archive
        $seance['Seance']['archive'] = true;
        // on précise que la convocation n'est plus active
        if (!empty($seance['Convocation'])) {
            foreach ($seance['Convocation'] as $key => $convocation) {
                $seance['Convocation'][$key]['active'] = false;
            }
        }

        if (!empty($seance['Invitation'])) {
            foreach ($seance['Invitation'] as $key => $invitation) {
                $seance['Invitation'][$key]['isactive'] = false;
            }
        }

        // on sauvegarde la seance
        $success = $this->Seance->saveAssociated($seance);
        // on envoie la notification de supression de cette seances au tablettes !
        if (!empty($seance['Convocation'])) {
            foreach ($seance['Convocation'] as $convocation) {
                $this->Seance->WSDeleteSeance($id, $convocation['user_id']);
            }
        }

        if (!empty($seance['Invitation'])) {
            foreach ($seance['Invitation'] as $invitation) {
                $this->Seance->WSDeleteSeance($id, $invitation['user_id']);
            }
        }


        if ($success) {
            echo  json_encode(["result" => true]);
        } else {
            echo json_encode(["result" => false]);
        }
    }


    /**
     * @param $seanceId
     * @return CakeResponse|null
     * return csv
     */
    public function printInfoUsers($seanceId) {
        $seance = $this->Seance->find('first', array(
                'conditions' => array(
                    "Seance.id" => $seanceId
                ),
                'contain' => array(
                    'Convocation' => array(
                        'fields' => array('Convocation.id', 'Convocation.presentstatus', 'Convocation.ae_horodatage', 'Convocation.ar_horodatage', 'Convocation.procuration_name' ),
                        'User' => array(
                            'fields' => array("User.firstname", "User.lastname", "User.mail"),
                            'Groupepolitique' => array(
                                "fields" => array("Groupepolitique.name")
                            )
                        )
                    )
                )
            )
        );


        $usersStatusArray = array();
        foreach ($seance['Convocation'] as $convoc) {
            if (empty($convoc['presentstatus'])) {
                $convoc['presentstatus'] = "Sans réponse";
            }
            $userArray = array(
                "status" => $convoc['presentstatus'],
                "firstname" => $convoc['User']['firstname'],
                "lastname" => $convoc['User']['lastname'],
                "ae" => (!empty($convoc['ae_horodatage']))? CakeTime::format('d/m/Y (H\hi)', $convoc['ae_horodatage']): null,
                "ar" => (!empty($convoc['ar_horodatage']))? CakeTime::format('d/m/Y (H\hi)', $convoc['ar_horodatage']): null,
                "groupePol" =>  (!empty($convoc['User']['Groupepolitique']))? $convoc['User']['Groupepolitique']['name'] : null,
                "email" =>  $convoc['User']['mail'],
                "mandataire" => $convoc['procuration_name']
            );
            array_push($usersStatusArray, $userArray);
        }



        $usersStatusArray = Hash::sort($usersStatusArray, '{n}.lastname', 'asc');

        $presentStatus = "Prenom;Nom;Présence;Envoi;Réception;GroupePolitique;Email;Mandataire\r\n";
        foreach ($usersStatusArray as $user) {
            $presentStatus = $presentStatus . $user['firstname'] . ';' . $user['lastname'] . ';' . $user['status']  . ';' . $user['ae']  . ';' . $user['ar'] . ';' . $user['groupePol'] . ';' . $user['email'] .  ';' . $user['mandataire'] . "\r\n";
        }

        $presentStatus = chr(239) . chr(187) . chr(191) . $presentStatus;

        $filePath = "/tmp/" . $seanceId .'.csv';
        $file = new File($filePath, true, 0777);
        $file->write($presentStatus);

        $this->response->file(
            $filePath,
            array('download' => true, 'name' => $seanceId. ".csv")
        );
        return $this->response;
    }

    //should not be here
    private function ColoredTable($header,$data, $pdf) {
        // Colors, line width and bold font
        $pdf->SetFillColor(58, 139, 215);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(0.3);
        $pdf->SetFont('', 'B');
        // Header
        $w = array(50,50,40,40,40,40);
        $num_headers = count($header);

        for($i = 0; $i < $num_headers; ++$i) {
            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }

        $pdf->Ln();
        // Color and font restoration
        $pdf->SetFillColor(224, 235, 255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('');
        // Data
        $fill = 0;

        $pageHeight = $pdf->GetPageHeight();
        foreach($data as $row) {

            $maxnocells = 0;
            $cellcount = 0;
            //write text first
            $startX = $pdf->GetX();
            $startY = $pdf->GetY();
            //draw cells and record maximum cellcount
            //cell height is 6 and width is 80
            $cellcount = $pdf->MultiCell($w[0],6,$row['firstname'],0,'L',0,0);
            if ($cellcount > $maxnocells ) {$maxnocells = $cellcount;}

            $cellcount = $pdf->MultiCell($w[1],6,$row['lastname'],0,'L',0,0);
            if ($cellcount > $maxnocells ) {$maxnocells = $cellcount;}

            $pdf->SetXY($startX,$startY);
            $pdf->Multicell($w[0], 6 *$maxnocells, $row['firstname'], 'LR', 'L', $fill, 0);
            $pdf->Multicell($w[1], 6 *$maxnocells, $row['lastname'], 'LR', 'L', $fill,0);
            $pdf->Multicell($w[2], 6 *$maxnocells, $row['status'], 'LR',  'L', $fill,0);
            $pdf->Multicell($w[3], 6 *$maxnocells, $row['ae'], 'LR',  'L', $fill,0);
            $pdf->Multicell($w[4], 6 *$maxnocells, $row['ar'], 'LR', 'L', $fill,0);
            $pdf->Multicell($w[4], 6 *$maxnocells, $row['mandataire'], 'LR', 'L', $fill,0);
            if($pdf->getY() > ($pageHeight -50)) {
                $pdf->Ln();
                $pdf->Cell(array_sum($w), 0, '', 'T');
                $pdf->addPage();

            }
            $pdf->Ln();
            $fill = !$fill;

        }
        $pdf->Cell(array_sum($w), 0, '', 'T');
    }

    /**
     * @param $seanceId
     * @return CakeResponse|null
     * return pdf for a given seance
     */
    public function printInfoUsersPdf($seanceId) {

        require_once(APP . "Vendor/TCPDF/tcpdf_import.php");
        require_once(APP . "Vendor/TCPDF/tcpdf.php");

        $seance = $this->Seance->find('first', array(
                'conditions' => array(
                    "Seance.id" => $seanceId
                ),
                'contain' => array(
                    'Convocation' => array(
                        'fields' => array('Convocation.id', 'Convocation.presentstatus', 'Convocation.ae_horodatage', 'Convocation.ar_horodatage','Convocation.procuration_name'),
                        'User' => array(
                            'fields' => array("User.firstname", "User.lastname"),
                            'Groupepolitique' => array(
                                "fields" => array("Groupepolitique.name")
                            )
                        )
                    )
                )
            )
        );


        $usersStatusArray = array();
        foreach ($seance['Convocation'] as $convoc) {
            if (empty($convoc['presentstatus'])) {
                $convoc['presentstatus'] = "Sans réponse";
            }
            $userArray = array(
                "status" => $convoc['presentstatus'],
                "firstname" => $convoc['User']['firstname'],
                "lastname" => $convoc['User']['lastname'],
                "ae" => (!empty($convoc['ae_horodatage']))? CakeTime::format('d/m/Y (H\hi)', $convoc['ae_horodatage']): null,
                "ar" => (!empty($convoc['ar_horodatage']))? CakeTime::format('d/m/Y (H\hi)', $convoc['ar_horodatage']): null,
                "groupePol" =>  (!empty($convoc['User']['Groupepolitique']))? $convoc['User']['Groupepolitique']['name'] : null,
                "mandataire" => $convoc['procuration_name']
            );
            array_push($usersStatusArray, $userArray);
        }
        $usersStatusArray = Hash::sort($usersStatusArray, '{n}.lastname', 'asc');

        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Libriciel');
        $pdf->SetTitle('Seance');
        $pdf->SetSubject('Résumé');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $now = new DateTime();

        $formatedNow = CakeTime::format('d/m/Y (H\hi)',$now);
        $pdf->SetHeaderData('idelibre.png', 40, $seance['Seance']["name"] ." ". CakeTime::format('d/m/Y (H\hi)', $seance['Seance']["date_seance"]), "Généré le " . $formatedNow );
// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set font
        $pdf->SetFont('helvetica', '', 9);
// add a page
        $pdf->AddPage();
// column titles
        $header = array('Prenom', 'Nom', 'Présence', 'Envoi', 'Réception', 'Mandataire');
// data loading
        $data = $usersStatusArray;
// print colored table
        $this->ColoredTable($header, $data, $pdf);
        $path = "/tmp/";
        $filename = $seanceId . ".pdf";
        $full_path = $path . '/' . $filename;
        $pdf->Output($full_path, 'F');

        $this->response->file(
            $full_path,
            array('download' => true, 'name' => $seanceId. ".pdf")
        );
        return $this->response;

    }


    /**
     * @param $seanceId
     * @return CakeResponse|null
     * return zip for a given seance
     */
    public function getzipSeance($seanceId){
        //DEV
        $collId = $_SESSION['Auth']['collectivite']['id'];
        //$collId = '5810a34f-c540-4ad1-9caa-189c5cde535b';

        $filePath = ZIP . $collId .DS. $seanceId . ".zip";

        $this->response->file(
            $filePath,
            array('download' => true, 'name' => $seanceId .".zip")
        );
        return $this->response;


    }

    /**
     * Cat all the pdf in one
     * @param $seanceId
     * @return CakeResponse|null
     */
    public function genPdfSeance($seanceId){
        $seance = $this->Seance->find('first', array(
            "conditions" => array(
                "Seance.id" => $seanceId
            ),
            "contain" => array(
                "Document" => array(
                    "fields" => array("id", "path", "name")
                ),
                "Projet" => array(
                    "fields" => array("id", "name", "rank"),
                    "order" => array("Projet.rank ASC"),
                    "Document" => array(
                        "fields" => array("id", "path", "name")
                    ),
                    "Annex" => array(
                        "fields" => array("id", "path", "name", "rank"),
                        "order" => array("Annex.rank ASC"),
                        "conditions" => array( "Annex.type" => "application/pdf")
                    )
                ),
            )));



        $cmd = "pdftk "  . $seance["Document"]["path"]. " ";
        foreach ($seance["Projet"] as $projet){
            $cmd .= $projet["Document"]["path"] ." ";
            foreach ($projet["Annex"] as $annexe){
                $cmd .= $annexe["path"]. " ";
            }
        }

        $cmd .= " cat output " . "/tmp/" . $seanceId . ".pdf";
        shell_exec($cmd);

        $filePath = "/tmp/" . $seanceId .".pdf";

        $this->response->file(
            $filePath,
            array('download' => true, 'name' => 'seanceComplete.pdf')
        );
        return $this->response;
    }


    /**
     * @param $id
     * edit seance function
     */
    public function editJson($id){
        $this->autoRender = false;

        if (empty($id) || !$this->Seance->exists($id) || empty($this->request->data)) {
            return; //une erreur
        }
        $return = $this->Seance->editSeanceJson($id, $_POST, $_FILES);
        DevTools::logMe($return, EDIT_SEANCE, __LINE__, __FILE__);

        if ($return['success']) {
            // on verifie si la seance à deja etait envoyée
            $seance = $this->Seance->find('first', array(
                'recursive => -1',
                'contain' => array(
                    'Convocation'
                ),
                'conditions' => array(
                    'Seance.id' => $id
                )
            ));

            // variable de vérification si au moins une convoc à été envoyée
            $isAlreadySent = false;
            foreach ($seance['Convocation'] as $convoc) {
                if ($convoc['active'] == true) {
                    $isAlreadySent = true;
                }
            }

            if ($isAlreadySent) {
                $this->refreshSeance($id);
            }

            echo json_encode($return);
            return ;
        } else {
            echo json_encode($return);
            return ;
        }
    }

    /**
     * @param $seanceId
     * @return CakeResponse|null
     * return zip of tokens
     */
    public function getTokenAEJson($seanceId){
        $seance = $this->Seance->find("first", array(
            'conditions' => array( 'id' => $seanceId),
            'fields' => array('date_seance')
        ));
        $year = explode('-', $seance['Seance']['date_seance'])[0];
        $collId = CakeSession::read('Auth.collectivite.id');
        $folder =  TOKEN . $collId . DS . $year . DS . $seanceId;

        $cmd = "nohup zip -r  /tmp/" .  $seanceId . ".zip " . $folder. DS .  "* >/dev/null 2>&1";
        shell_exec($cmd);

        $this->response->file(
            "/tmp/" . $seanceId .".zip",
            array('download' => true, 'name' => "jetons_" . $seanceId .".zip")
        );
        return $this->response;
    }
}
