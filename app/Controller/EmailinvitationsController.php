<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class EmailinvitationsController extends AppController
{

    public function index() {
        $invitations = $this->Emailinvitation->find('all', array(
                'fields' => array(
                    'name', 'id'
                ),
                'order' => array("Emailinvitation.name"),
                'contain' => array(
                    'Type' => array(
                        'fields' => array('name')
                    )
                )
            )
        );
        $this->set('data', $invitations);
    }


    public function add(){
        if($this->request->is("POST")){
            $EmailInvitation = $this->request->data["Emailinvitation"];
            $EmailInvitation["type_id"] = $this->request->data["Type"]["id"];
            $res = $this->Emailinvitation->save($EmailInvitation);

            if($res){
                $this->Session->setFlash("Message enregistré", 'bootstrap_flash', array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash("Erreur lors de l'ajout", 'bootstrap_flash', array('class' => 'alert alert-error'));
            }

            $this->redirect(array('action' => "index"));

        }
        $typeList = $this->Emailinvitation->Type->find('list');
        $this->set('typeList', $typeList);
    }


    public function delete($id){
        $res = null;
        if($id != null){
            $res = $this->Emailinvitation->delete($id);
        }

        if($res){
            $this->Session->setFlash("Message supprimé", 'bootstrap_flash', array('class' => 'alert alert-success'));
        }else{
            $this->Session->setFlash("Erreur lors de la suppression", 'bootstrap_flash', array('class' => 'alert alert-error'));
        }

        $this->redirect(array('action' => "index"));
    }


    public function edit($id){

        if($this->request->is("POST") || $this->request->is("PUT")) {
            $EmailInvitation = $this->request->data["Emailinvitation"];

            if(isset($this->request->data["Type"]))
                $EmailInvitation["type_id"] = $this->request->data["Type"]["id"];

            $res = $this->Emailinvitation->save($EmailInvitation);


            if($res){
                $this->Session->setFlash("Message modifié avec succès", 'bootstrap_flash', array('class' => 'alert alert-success'));
            }else{
                $this->Session->setFlash("Erreur lors de la modification", 'bootstrap_flash', array('class' => 'alert alert-error'));
            }

            $this->redirect(array('action' => "index"));
        }


        $data = $this->Emailinvitation->find('first', array(
            'conditions' => array('id' => $id)
        ));

        $this->request->data = $data;
        $typeList = $this->Emailinvitation->Type->find('list');
        $this->set('typeList', $typeList);

    }


    public function getAllJson(){
        $this->autoRender = false;
        $invitations = $this->Emailinvitation->find('all', array(
                'fields' => array(
                    'name', 'id'
                ),
                'order' => array('Emailinvitation.name'),
                'contain' => array(
                    'Type' => array(
                        'fields' => array('name')
                    )
                )
            )
        );
        echo json_encode($invitations);
    }



    public function getJson($id){
        $this->autoRender = false;
        $convocations = $this->Emailinvitation->find('first', array(
            'conditions'=> array(
                "Emailinvitation.id" => $id
            ),
            'contain' => array(
                'Type' => array(
                    'fields' => array('name', 'id')
                )
            )));

        $email = $convocations["Emailinvitation"];
        $currentType = $convocations["Type"];

        $types = $this->Emailinvitation->Type->find('all', array(
            "fields" => array(
                "name", "id"
            ),
            "order" => array("name")
        ));

        $types = Hash::extract($types,'{n}.Type');


        echo json_encode(["email" => $email, "currentType" => $currentType, "types" => $types]);


    }



}