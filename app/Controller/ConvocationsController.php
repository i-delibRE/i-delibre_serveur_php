<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');
App::uses('Security', 'Component');

class ConvocationsController extends AppController {


    public function beforeFilter() {
        parent::beforeFilter();
    }


    /**
     *
     * @var type
     */
    public $components = array('Horodatage', 'Security');

    /**
     *
     * @var type
     */
    public $TSFile;

    /**
     *
     * @var type
     */
    public $TSToken;

    /**
     * Enregistre l'accusé de lecture de la convocation par l'utilisateur de l'application cliente
     *
     * @param string $seance_id UUID
     * @param string $user_id UUID
     * @throws NotFoundException
     */
    //TODO: utiliser la classe utilitaire Horodatage
    public function setRead($seance_id, $user_id) {
        $this->layout = 'ajax';

        if (!$this->Convocation->User->exists($user_id) || !$this->Convocation->Seance->exists($seance_id)) {
            throw new NotFoundException();
        }

        $return = array(
            'success' => false,
            'errorMsg' => __d('convocation', 'setRead.error')
        );

        $convoc = $this->Convocation->find('first', array('conditions' => array('Convocation.seance_id' => $seance_id, 'Convocation.user_id' => $user_id)));

        if (empty($convoc)) {
            throw new NotFoundException();
        }

        $convoc['Convocation']['read'] = true;
        $convoc['Convocation']['ar_received'] = date('Y-m-d H:i:s');

        $baseFileName = Security::hash($convoc['Convocation']['id'] . $convoc['Convocation']['ar_horodatage']);
        $this->TSFile = new File(TMP . DS . 'horodatage' . $baseFileName);

        $tsFileContent = "i-delibRE - Timestamp\n" .
            "controller : " . $this->name . "\n" .
            "action : " . $this->action . "\n" .
            "organisation : " . CakeSession::read('Auth.collectivite.name') . " : " . CakeSession::read('Auth.collectivite.id') . "\n" .
            "user : " . CakeSession::read('Auth.User.name') . " : " . CakeSession::read('Auth.User.id') . "\n" .
            "seance : " . $seance_id . "\n" .
            "convocation : " . $convoc['Convocation']['id'] . "\n" .
            "server timestamp : " . $convoc['Convocation']['ar_received'];
        $this->TSFile->write($tsFileContent);

        try {
            if ($this->Horodatage->check() && $this->Horodatage->horodateFichier($this->TSFile->path)) {
                $resp = $this->Horodatage->responseToText;
                for ($i = 0; $i < count($resp); $i++) {
                    $matches = array();
                    if (preg_match("#Time stamp: (.*)#", $resp[$i], $matches)) {
                        if (count($matches) > 1) {
                            $date = DateTime::createFromFormat('M j H:i:s Y O', $matches[1]);
                            $convoc['Convocation']['ar_horodatage'] = date('Y-m-d H:i:s', CakeTime::convert($date->getTimestamp(), null));
                        }
                    }
                }
                $convoc['Convocation']['ar_horodatage_file'] = $this->TSFile->read();
                $this->TSToken = new File($this->TSFile->path . '.rep.tsa');
                $convoc['Convocation']['ar_horodatage_token'] = $this->TSToken->read();
            } else {
                $convoc['Convocation']['ar_received'] = null;
                $convoc['Convocation']['ar_horodatage'] = date('Y-m-d H:i:s');
            }

            $this->Convocation->create();
            if ($this->Convocation->save($convoc)) {
                $return['success'] = true;
                $return['errorMsg'] = "";
            }

            if (!empty($this->TSFile)) {
                $this->TSFile->delete();
            }
            if ($this->Horodatage->check() && !empty($this->TSToken)) {
                $this->TSToken->delete();
            }
        } catch (Exception $exc) {
            $data['errorMsg'] = __d('convocation', 'setRead.error:' + ' ' + var_export($exc, true));
        }


        $this->set('return', $return);
    }


    /**
     * @param $convocation_id
     * send convocation
     */
    public function sendJson($convocation_id) {
        $this->autoRender = false;
        $res =  $this->Convocation->sendJson($convocation_id);
        echo json_encode($res);
    }

}
