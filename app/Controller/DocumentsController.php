<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppController', 'Controller');

class DocumentsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('DbFile');

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
//        $this->Auth->allow(array('getFile', 'getFileJson'));
    }



    /**
     *
     * @param type $id
     */
    public function getFile($id) {
        return $this->DbFile->getFile($id);
    }


    public function getFileJson($id)
    {
        $qd = array(
            'recursive' => -1,
            'conditions' => array(
                'Document.id' => $id
            )
        );

        $file = $this->Document->find('first', $qd);

        if (!empty($file)) {
            $fullPath = $file["Document"]["path"];
            $fileName = $file["Document"]["name"];

            $this->response->file(
                $fullPath,
                array('download' => true, 'name' => $fileName )
            );
            return $this->response;
        }

    }


}
