<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('ConvertionBalise', 'Utility');
App::uses('CakeEmail', 'Network/Email');
class InvitationsController extends AppController
{

    public function beforeFilter() {
        parent::beforeFilter();
    }



    public function sendJson($invitationId){
        $this->autoRender = false;
        $return = array();
        $this->Invitation->id = $invitationId;
        //  if already timestamped do nothing !
        if(!empty($this->Invitation->field("ae_horodatage"))){
            return json_encode(array("success" => false));
        }
        $timestamp = date('Y-m-d H:i:s');
        $res = $this->Invitation->saveField('ae_horodatage',$timestamp);


        if($res){
            $this->Invitation->saveField('isactive',true);
            $return =  array(
                "success" => true,
                "invitationId" => $invitationId,
                "ae_horodatage" => $timestamp
            );
        }else{
            $return =  array(
                "success" => false
            );
        }

        $this->sendMail($invitationId);


        $ch = curl_init("http://localhost:". IDELIBRE_WS_PORT ."/pushSeance/" . $this->Invitation->field("user_id") ."/" . PASSPHRASE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);




        return json_encode($return);
    }



    private function sendAllJson($seanceId, $userType){
        $invitations = $this->Invitation->find('all', array(
            'fields' => array("Invitation.id"),
            'conditions' => array(
                "Invitation.seance_id" => $seanceId,
                "User.group_id" => $userType
            ),
            'contain' => array("User")
        ));


        $toReturn = array();
        foreach ($invitations as $invitation){
            $this->Invitation->id = $invitation['Invitation']['id'];
            //  if already timestamped do nothing !
            if(!empty($this->Invitation->field("ae_horodatage"))){
                //return json_encode(array("success" => false, "message" => "ae"));
                //Do nothing
            }else {


                $timestamp = date('Y-m-d H:i:s');
                $res = $this->Invitation->saveField('ae_horodatage', $timestamp);


                if ($res) {
                    $this->Invitation->saveField('isactive', true);
                    $invit = array(
                        "invitationId" => $invitation['Invitation']['id'],
                        "ae_horodatage" => $timestamp
                    );
                    array_push($toReturn, $invit);
                    $this->sendMail($invitation['Invitation']['id']);

                }
            }
        }

        return $toReturn;

    }

    public function sendAdministratifsJson($seanceId){
        $this->autoRender = false;
        $return = $this->sendAllJson($seanceId, IDELIBRE_GROUP_ADMINISTRATIF_ID);

        echo json_encode($return);
    }


    public function sendInvitesJson($seanceId){
        $this->autoRender = false;
        $return = $this->sendAllJson($seanceId, IDELIBRE_GROUP_INVITE_ID);

        echo json_encode($return);
    }



    public function sendMail($invitationId) {

        $invitation = $this->Invitation->find('first', array(
            "conditions" => array(
                'id' => $invitationId
            )
        ));




        $User = ClassRegistry::init('User');
        $User->id = $invitation["Invitation"]["user_id"];



        $invit = $this->prepareTypeInvitation($invitation);

        $mailto = $User->field('mail');
        // Paramètres mail
        $Email = new CakeEmail('convocation');

        if (!empty($mailto)) {

            $Email->to($mailto);
            $Email->subject($invit['Emailinvitation']['sujet']);
        }
        //envoi
        $result = null;
        try {
            $result = $Email->send($invit['Emailinvitation']['content']);
        } catch (Exception $e) {
            $this->log($e->getMessage(), LOG_ERROR);
        }

        //traitement du retour
        return !empty($result);
    }





    public function prepareTypeInvitation($invitation) {
        $User = ClassRegistry::init('User');
        $User->id = $invitation["Invitation"]["user_id"];
        $seance = $this->Invitation->Seance->find('first', array(
            'conditions' => array(
                'Seance.id' => $invitation['Invitation']['seance_id']
            ),
            'fields' => array(
                'Seance.name', 'Seance.place', 'Seance.date_seance', 'Seance.type_id'
            ),
            'contain' => array(
                'Type.name'
            )
        ));
        $typeId = $seance['Seance']['type_id'];
        //recupération de la convocation
        $EmailInvit = ClassRegistry::init('Emailinvitation');
        $emailInvit = $EmailInvit->find('first', array(
            'conditions' => array(
                'type_id' => $typeId
            )
        ));
//si empty alors prendre le default !
        if(empty($emailInvit)){
            $emailInvit = $EmailInvit->find('first', array(
                'conditions' => array(
                    'type_id IS NULL'
                )
            ));
        }
        // variable de la convocation
        $evaluate = array();
        $evaluate['nom'] = $User->field('lastname');
        $evaluate['prenom'] = $User->field('firstname');
        $evaluate['titre'] = $User->field('titre');

        $civiliteInt = $User->field('civilite');

        if($civiliteInt == 1)
            $evaluate['civilite'] = "madame";
        elseif ($civiliteInt == 2)
            $evaluate['civilite'] = "monsieur";
        else
            $evaluate['civilite'] = "";

        $evaluate['nomseance'] = $seance['Seance']['name'];
        $evaluate['lieuseance'] = $seance['Seance']['place'];
        $evaluate['typeseance'] = $seance['Type']['name'];

        $dateTime = $seance['Seance']['date_seance'];
        //non compatible php ubuntu 12.04
        //$date = explode(' ', $dateTime)[0];
        $predate = explode(' ', $dateTime);
        $date = $predate[0];

        //non compatible php ubuntu 12.04
        //$time = explode(' ', $dateTime)[1];
        $pretime = explode(' ', $dateTime);
        $time = $pretime[1];

        $exploded = explode('-', $date);
        $formatedDate = $exploded[2] . '/' . $exploded[1] . '/' . $exploded[0];



        $exploded = explode(':', $time);
        $formatedTime = $exploded[0] . 'h' . $exploded[1];

        $evaluate['dateseance'] = $formatedDate;
        $evaluate['heureseance'] = $formatedTime;

        $res = ConvertionBalise::evaluate($evaluate, $emailInvit);

        return $res;
    }










}