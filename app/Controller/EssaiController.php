<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EssaiController
 *
 * @author rdubourget
 */

App::uses('CakeEmail', 'Network/Email');
class EssaiController extends AppController {
    public function beforeFilter() {
        $this->Auth->allow();
    }


    public function check(){

        $Email = new CakeEmail('convocation');
        $Email->from('noreply@adullact.org');
        $Email->subject("email test idelibre");
        $Email->to("remi.dubourget@libriciel.coop");

        $result = null;

        debug("envoie de l'eamail");
        try {
            $result = $Email->send("contenu du mail de convocation");
        } catch (Exception $e) {
            debug($e->getMessage());
        }

        debug($result);
        $this->autoRender = false;
    }


    public function checkMail()
    {
        if ($this->request->is('POST') || $this->request->is('GET')) {
            $destination = $this->request->data['Email']['email'];

            if(empty($destination)){
                $this->redirect(array('controller' => 'Environnements', 'action' => 'menu'));
            }


            $Email = new CakeEmail('convocation');
            $Email->from('noreply@idelibre-api.fr');
            $Email->subject("email test idelibre");
            $Email->to($destination);

            $result = null;
            try {
                $result = $Email->send("Envoie d'un message test depuis le serveur idelibre");
            } catch (Exception $e) {
                debug($e->getMessage());
            }
        }
        $this->redirect(array('controller' => 'Environnements', 'action' => 'menu'));
    }




}
