<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');

class Ptheme extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     *
     * @var type
     */
    public $actsAs = array('Tree');

    /**
     *
     * @var type
     */
    public $validationDomain = 'ptheme';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Ptheme.name.notempty',
            )//,
//            'isUnique' => array(
//                'rule' => array('isUnique'),
//                'message' => 'Ptheme.name.isUnique'
//            )
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'ParentPtheme' => array(
            'className' => 'Ptheme',
            'foreignKey' => 'parent_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ChildPtheme' => array(
            'className' => 'Ptheme',
            'foreignKey' => 'parent_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Projet' => array(
            'className' => 'Projet',
            'foreignKey' => 'ptheme_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    //faire un getparent et check si le get parent est le bon !

    /**
     * Recherche une branche de l'arbre des themes et la crée si elle n'existe pas
     *
     * @param string $strPtheme concatenation d'une branche de l'arbre des themes (separateur : voir IDELIBRE_WSIMPORT_THEME_SEPARATOR dans idelibre.inc.php, valeur par defaut : ",")
     * @return integer (false if error)
     */


    // get the subtheme that is the real subtheme of the parent
    private function getTrueSubtheme($themeName, $parentId){
        $pthemes = $this->find("all", array(
            "conditions" => array(
                "Ptheme.name" => $themeName,
            ),
            "fields"=> array(
                "Ptheme.name", "Ptheme.id"
            ),
            "contain" => array(
                "ParentPtheme"
            ),
        ));

        //return l'id du ptheme ayant comme theme parent celui avec le bon Id
        foreach ($pthemes as $theme){
            if($theme["ParentPtheme"]["id"] == $parentId) {
                return $theme["Ptheme"]["id"];
            }
        }
        return null;
    }



    public function treeCreate($strPtheme) {
        $parentId = null;
        $exploded = explode(IDELIBRE_WSIMPORT_THEME_SEPARATOR, $strPtheme);

        DevTools::logMe($exploded,CONNECTEUR, true);

        foreach ($exploded as $pos=>$item) {
            $result = null;
            if ($parentId == null) {
                $result = $this->find('first', array('conditions' => array('Ptheme.name' => $item)));
            }else{
                $result = $this->getTrueSubtheme($exploded, $parentId);
            }
            if (empty($result)) {
                $ptheme = $this->create();
                $ptheme['Ptheme']['name'] = $item;
                $ptheme['Ptheme']['parent_id'] = $parentId;

                $fullThemeName = $exploded[0];
                for($i=1; $i<=$pos; $i++){
                    $fullThemeName .=  ", " . trim($exploded[$i]);
                }
                $ptheme['Ptheme']['fullname'] = $fullThemeName;

                $this->save($ptheme);
                $parentId = $this->id;
            } else {
                if ($parentId == null) {
                    $parentId = $result['Ptheme']['id'];
                }else{
                    $parentId = $result;
                }
            }
        }

        return is_null($parentId) ? false : $parentId;
    }



    public function treeCreate2($strPtheme) {
        $parentId = null;
        $exploded = explode(IDELIBRE_WSIMPORT_THEME_SEPARATOR, $strPtheme);

        $this->getParentTheme($exploded[1]);
        debug($exploded); die;


        foreach ($exploded as $item) {
            $result = $this->find('first', array('conditions' => array('Ptheme.name' => $item)));
            if (empty($result)) {
                $ptheme = $this->create();
                $ptheme['Ptheme']['name'] = $item;
                $ptheme['Ptheme']['parent_id'] = $parentId;
                $this->save($ptheme);
                $parentId = $this->id;
            } else {
                $parentId = $result['Ptheme']['id'];
            }
        }
        return is_null($parentId) ? false : $parentId;
    }

}
