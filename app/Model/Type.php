<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');

class Type extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     *
     * @var type
     */
    public $validationDomain = 'type';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Type.name.notempty',
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Type.name.isUnique'
            )
        )
    );


    public $hasOne = array(
        'Emailinvitation' => array(
            'className' => 'Emailinvitation',
            'foreignKey' => 'type_id',
            'dependent' => false
        ),
        'EmailConvocation' => array(
            'className' => 'EmailConvocation',
            'foreignKey' => 'type_id',
            'dependent' => false
        )

    );


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Seance' => array(
            'className' => 'Seance',
            'foreignKey' => 'type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Seancesave' => array(
            'className' => 'Seancesave',
            'foreignKey' => 'type_id',
        ),
        'TypesAuthorized' => array(
            'className' => 'TypesAuthorized',
            'foreignKey' => 'type_id',
        )
    );




    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'User' => array(
            'className' => 'User',
            'joinTable' => 'types_users',
            'foreignKey' => 'type_id',
            'associationForeignKey' => 'user_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'TypesUser'
        ),
        'Authorized' => array(
            'className' => 'User',
            'joinTable' => 'types_authorizeds',
            'foreignKey' => 'type_id',
            'associationForeignKey' => 'user_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'TypesAuthorized'
        )

    );

    /**
     * Récupération de l'id type à partir du nom du type
     * (utilisée pour la création automatique via web-service)
     *
     * @param string $type_name
     * @param boolean $create si $create vaut true et que $type_name n'existe pas, on crée un nouveau type et on en revoie l'id
     * @return string UUID
     */
    public function getTypeIdFromName($type_name, $create = false) {
        $return = $this->field('Type.id', array('Type.name' => $type_name));

        if (empty($return) && $create) {
            $type = $this->create();
            $type['Type']['name'] = $type_name;
            if ($this->save($type)) {
                $return = $this->id;
            }
        }

        return $return;
    }

    /**
     * Récupération des utlisateurs associés à un type
     *
     * @param string $typeId UUID
     * @return array
     */
    public function getUsersFromTypeId($typeId) {
        $return = array();
        if (!empty($typeId)) {
            $qd = array(
                'contain' => array(
                    'User' => array(
                        'conditions' => array('User.active' => true)
                    )
                ),
                'conditions' => array(
                    'Type.id' => $typeId,

                )
            );
            $type = $this->find('first', $qd);
            $return = $type['User'];
        }
        return $return;
    }

    public function getUsersFromTypeIdJson($typeId) {
        $return = array();
        if (!empty($typeId)) {
            $qd = array(
                'contain' => array(
                    'User' => array(
                        'conditions' => array('User.active' => true),
                        'fields' => array('User.id', 'User.name')
                    )
                ),
                'conditions' => array(
                    'Type.id' => $typeId,

                )
            );
            $type = $this->find('first', $qd);
            $return = $type['User'];
        }
        $return = Hash::remove($return,"{n}.TypesUser");

        return json_encode($return);
        //  return $return;
    }




    /**
     * Retourne les utilisateurs non associés à un type donné
     *
     * @param integer $typeId
     * @param array $users
     * @return array User
     */
    public function getNotAssociated($typeId, $users) {
        $return = array();
        foreach ($users as $user) {
            $check = $this->TypesUser->find('first', array('conditions' => array('TypesUser.type_id' => $typeId, 'TypesUser.user_id' => $user['User']['id'])));
            if (empty($check)) {
                $return[] = $user;
            }
        }
        return $return;
    }

    /**
     * Associe une liste d'utilisateurs à un type
     *
     * @param integer $typeId
     * @param array $users
     * @return boolean
     */
    public function associateUsers($typeId, $users) {
        $saved = array();
        foreach ($users as $user) {
            $typesuser = $this->TypesUser->create();
            $typesuser['TypesUser']['type_id'] = $typeId;
            $typesuser['TypesUser']['user_id'] = $user['User']['id'];
            $saved[] = $this->TypesUser->save($typesuser);
        }
        return !in_array(false, $saved, true);
    }

}
