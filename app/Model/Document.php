<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class Document extends AppModel {

    /**
     *
     * @var type
     */
    public $actsAs = array('DbFile');

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';



    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            ),
        ),
        'size' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            ),
        ),
        'type' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasOne associations
     *
     * @var array
     */
    public $hasOne = array(
        'Seance' => array(
            'className' => 'Seance',
            'foreignKey' => 'document_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Projet' => array(
            'className' => 'Projet',
            'foreignKey' => 'document_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Invitation' => array(
            'className' => 'Invitation',
            'foreignKey' => 'document_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );


    /**
     * cration du document dna s son workspace
     * @param type $documentId
     * @param type $filename
     */
    public function createDoc($documentId, $filename) {
        //get the collectivite id
        //DEV
        $collId = $_SESSION['Auth']['collectivite']['id'];
        //$collId = '5810a34f-c540-4ad1-9caa-189c5cde535b';

        $year = date("Y");
        //if the directorydoesnt exist create it maybe in the /data ??
        $folder = WORKSPACE.$collId.DS.$year;
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        rename($filename, $folder.DS.$documentId);

        $this->id = $documentId;
        $this->saveField("path", $folder.DS.$documentId);
    }

}
