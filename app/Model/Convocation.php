<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');
App::uses('Horodatage', 'Utility');
App::uses('Security', 'Utility');
App::uses('ConvertionBalise', 'Utility');

class Convocation extends AppModel {


    /**
     *
     * @var array
     */
    public $return = array(
        'horodatage' => array(
            'success' => false,
            'message' => ''
        ),
        'save' => array(
            'success' => false,
            'message' => ''
        ),
        'mail' => array(
            'success' => false,
            'message' => ''
        ),
        'TSFile' => array(
            'exists' => false,
            'delete' => false
        ),
        'TSToken' => array(
            'exists' => false,
            'delete' => false
        )
    );


    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'seance_id' => array(
            'uuid' => array(
                'rule' => array('uuid'),
            ),
        ),
        'user_id' => array(
            'uuid' => array(
                'rule' => array('uuid'),
            ),
        ),
        'read' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            ),
        ),
        'presence' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            ),
        ),
        'delegation' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            ),
        ),
        'procuration' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Seance' => array(
            'className' => 'Seance',
            'foreignKey' => 'seance_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );




    public function sendJson($convocation_id) {
        Configure::write("debug", 0);
        if (empty($convocation_id) || !$this->exists($convocation_id)) {

            throw new NotFoundException(__d('convocation', 'Convocation.notFound'));
        }


        //check if convocation already sent

        $aeStatus = $this->find('first', array(
            'conditions' => array(
                'id' => $convocation_id
            ),
            'fields' => array(
                'ae_horodatage'
            )
        ));

        //if already send return an error !
        if($aeStatus["Convocation"]["ae_horodatage"] !=null){
            $return["isSaved"] = false;
            return $return;
        }



        //récupération de la convocation
        $convocToSend = $this->find('first', array('conditions' => array('Convocation.id' => $convocation_id)));
        //préparation des nouvelles informations de la convocation
        $convocToSend['Convocation']['active'] = true;


        $year = date("Y");
        $collId = $_SESSION['Auth']['collectivite']['id'];

        $folder = TOKEN . $collId . DS . $year;
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $folder = TOKEN . $collId . DS . $year . DS .$convocToSend['Convocation']['seance_id'];
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $TSFile = new File($folder . DS . 'idelibre_horodatage_' . Security::hash($convocToSend['Convocation']['id']));


        //préparation des nouvelles informations de la convocation
        $convocToSend['Convocation']['active'] = true;
        $convocToSend['Convocation']['ae_received'] = date('Y-m-d H:i:s');

        //Ecriture du fichier d'horodatage
        $tsFileContent = "i-delibRE - Timestamp\n" .
            "controller : " . $this->name . "\n" .
            "action : " . $this->action . "\n" .
            "organisation : " . CakeSession::read('Auth.collectivite.name') . " : " . CakeSession::read('Auth.collectivite.id') . "\n" .
            "user : " . CakeSession::read('Auth.User.name') . " : " . CakeSession::read('Auth.User.id') . "\n" .
            "seance : " . $convocToSend['Convocation']['seance_id'] . "\n" .
            "convocation : " . $convocToSend['Convocation']['id'] . "\n" .
            "server timestamp : " . $convocToSend['Convocation']['ae_received'];
        $TSFile->write($tsFileContent);

        //Horodatagevim
        try {
            if (Horodatage::horodateFichier($TSFile->path)) { //si l horodatage est configuré et fonctionnel
                $return['horodatage']['success'] = true;
                $convocToSend['Convocation']['ae_horodatage'] = date('Y-m-d H:i:s');
                $return['horodatage']['ae_horodatage'] = $convocToSend['Convocation']['ae_horodatage'];
            } else { //sinon on utilise la date systeme
                $convocToSend['Convocation']['ae_horodatage'] = date('Y-m-d H:i:s');
                $return['horodatage']['success'] = false;
                $return['horodatage']['ae_horodatage'] = $convocToSend['Convocation']['ae_horodatage'];
            }
        } catch (Exception $exc) {
            $return['horodatage']['message'] = __d('convocation', 'Convocation.horodatage.error');
            if (Configure::read('debug') > 0) {
                $return['horodatage']['exception'] = $exc->getTraceAsString();
                $return['horodatage']['success'] = false;
            }
        }

        //Sauvegarde des informations

        if ($return['horodatage']['success']) {
            $convocToSend["Convocation"]['istoken'] = true;
        }

        //Envoi du mail
        try {
            $return['mail']['success'] = $this->sendMail($convocToSend);
            if($return['mail']['success']){
                $convocToSend["Convocation"]['isemailed'] = true;
            }


        } catch (Exception $exc) {
            $return['mail']['message'] = __d('convocation', 'Convocation.mail.error');
            if (Configure::read('debug') > 0) {
                $return['mail']['exception'] = $exc->getTraceAsString();
            }
        }

        $isSaved = $this->save($convocToSend);

        if($isSaved){
            $return["isSaved"] = true;
        }else{
            $return["isSaved"] = false;
        }



        //envoi de l info de sync au client
        try {
            $this->WSTellSync($convocToSend);
        } catch (Exception $exc) {

            if (Configure::read('debug') > 0) {
                DevTools::ldebug($exc->getTraceAsString());
            }
        }

        $return["convocation_id"] = $convocToSend["Convocation"]["id"];
        return $return;
    }








    /**
     * transformation des varialbes de la convocation
     */
    public function prepareConvocation() {
        $this->User->id = $this->convocToSend['Convocation']['user_id'];

        $seance = $this->Seance->find('first', array(
            'conditions' => array(
                'Seance.id' => $this->convocToSend['Convocation']['seance_id']
            ),
            'fields' => array(
                'Seance.name', 'Seance.place', 'Seance.date_seance'
            ),
            'contain' => array(
                'Type.name'
            )
        ));

        //recupération de la convocation
        $Convoc = ClassRegistry::init('Emailconvocation');
        $convoc = $Convoc->find('first', array(
            'conditions' => array(
                'id' => 1
            )
        ));

        // variable de la convocation
        $evaluate = array();
        $evaluate['nom'] = $this->User->field('lastname');
        $evaluate['prenom'] = $this->User->field('firstname');
        $evaluate['titre'] = $this->User->field('titre');

        $civiliteInt = $this->User->field('civilite');

        if($civiliteInt == 1)
            $evaluate['civilite'] = "madame";
        elseif ($civiliteInt == 2)
            $evaluate['civilite'] = "monsieur";
        else
            $evaluate['civilite'] = "";

        $evaluate['nomseance'] = $seance['Seance']['name'];
        $evaluate['lieuseance'] = $seance['Seance']['place'];
        $evaluate['typeseance'] = $seance['Type']['name'];

        $dateTime = $seance['Seance']['date_seance'];

        $predate = explode(' ', $dateTime);
        $date = $predate[0];
        $pretime = explode(' ', $dateTime);
        $time = $pretime[1];


        $exploded = explode('-', $date);
        $formatedDate = $exploded[2] . '/' . $exploded[1] . '/' . $exploded[0];
        $exploded = explode(':', $time);
        $formatedTime = $exploded[0] . 'h' . $exploded[1];

        $evaluate['dateseance'] = $formatedDate;
        $evaluate['heureseance'] = $formatedTime;
        $res = ConvertionBalise::evaluate($evaluate, $convoc);
        return $res;
    }



    public function prepareTypeConvocation($convocToSend) {
        $this->User->id = $convocToSend['Convocation']['user_id'];
        $seance = $this->Seance->find('first', array(
            'conditions' => array(
                'Seance.id' => $convocToSend['Convocation']['seance_id']
            ),
            'fields' => array(
                'Seance.name', 'Seance.place', 'Seance.date_seance', 'Seance.type_id'
            ),
            'contain' => array(
                'Type.name'
            )
        ));

        $typeId = $seance['Seance']['type_id'];
        //recupération de la convocation
        $Convoc = ClassRegistry::init('Emailconvocation');
        $convoc = $Convoc->find('first', array(
            'conditions' => array(
                'type_id' => $typeId
            )
        ));
//si empty alors prendre le default !
        if(empty($convoc)){
            $convoc = $Convoc->find('first', array(
                'conditions' => array(
                    'type_id IS NULL'
                )
            ));
        }
        // variable de la convocation
        $evaluate = array();
        $evaluate['nom'] = $this->User->field('lastname');
        $evaluate['prenom'] = $this->User->field('firstname');
        $evaluate['titre'] = $this->User->field('titre');

        $civiliteInt = $this->User->field('civilite');

        if($civiliteInt == 1)
            $evaluate['civilite'] = "madame";
        elseif ($civiliteInt == 2)
            $evaluate['civilite'] = "monsieur";
        else
            $evaluate['civilite'] = "";

        $evaluate['nomseance'] = $seance['Seance']['name'];
        $evaluate['lieuseance'] = $seance['Seance']['place'];
        $evaluate['typeseance'] = $seance['Type']['name'];

        $dateTime = $seance['Seance']['date_seance'];
        //non compatible php ubuntu 12.04
        //$date = explode(' ', $dateTime)[0];
        $predate = explode(' ', $dateTime);
        $date = $predate[0];
        //non compatible php ubuntu 12.04
        //$time = explode(' ', $dateTime)[1];
        $pretime = explode(' ', $dateTime);
        $time = $pretime[1];

        $exploded = explode('-', $date);
        $formatedDate = $exploded[2] . '/' . $exploded[1] . '/' . $exploded[0];
        $exploded = explode(':', $time);
        $formatedTime = $exploded[0] . 'h' . $exploded[1];

        $evaluate['dateseance'] = $formatedDate;
        $evaluate['heureseance'] = $formatedTime;
        $res = ConvertionBalise::evaluate($evaluate, $convoc);
        return $res;
    }


    /**
     *
     * @return type
     */
    public function sendMail($convocToSend) {

        $this->User->id = $convocToSend['Convocation']['user_id'];
        $conv = $this->prepareTypeConvocation($convocToSend);
        $mailto = $this->User->field('mail');
        // Paramètres mail
        $Email = new CakeEmail('convocation');

        if (!empty($mailto)) {
            $Email->to($mailto);
            $Email->subject($conv['Emailconvocation']['sujet']);
        }
        //envoi
        $result = null;
        try {
            $result = $Email->send($conv['Emailconvocation']['contenu']);
        } catch (Exception $e) {
            $this->log($e->getMessage(), LOG_ERROR);
        }

        //traitement du retour
        return !empty($result);
    }



    /**
     * push de la convocation !
     */
    public function WSTellSync($convocToSend) {

        $url = IDELIBRE_WS_PROTOCOL . '://' . IDELIBRE_WS_HOST . ':' . IDELIBRE_WS_PORT . "/pushSeance/" . $convocToSend['Convocation']['user_id'] ."/" . PASSPHRASE;
        $ch = curl_init($url);


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);

    }

    // TODO : doit etre envoyé sur wstellREsync  (on peut aussi virer la seance_id !)
    public function reSend($convocation_id /* , $seance_id */) {
        $convocToSend = $this->find('first', array('conditions' => array('Convocation.id' => $convocation_id)));
        // $this->WSTellReSync($seance_id);
        $this->WSTellReSync($convocToSend);
    }

    /*
     * Envoie la demande de resynchro de la séance
     * @param $seance_id numero de la seance
     */

    public function WSTellReSync($convocToSend) {
        $url = IDELIBRE_WS_PROTOCOL . '://' . IDELIBRE_WS_HOST . ':' . IDELIBRE_WS_PORT ."/pushModifiedSeance/" . $convocToSend['Convocation']['user_id'] ."/" . PASSPHRASE;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        curl_close($ch);
    }


}
