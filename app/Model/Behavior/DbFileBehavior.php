<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class DbFileBehavior extends ModelBehavior {

	/**
	 *
	 * @var type
	 */
	private $_file;

	/**
	 * Définition des champs "lourds" (contenus de fichier)
	 *
	 * @access public
	 * @var array
	 */
	private $_heavyFields = array(
		'content'
	);

	/**
	 * Initialisation du behavior
	 *
	 * @access public
	 * @param Model $model
	 * @param array $config
	 * @return void
	 */
	public function setup(Model $model, $config = array()) {
		parent::setup($model, $config);
	}

	/**
	 * Récupération des champs "légers" (sans les champs "lourds")
	 *
	 * @access public
	 * @param Model $model
	 * @return array
	 */
	public function getLightFields(Model $model) {
		$heavyFields = array();
		foreach ($this->_heavyFields as $field) {
			$heavyFields[] = $model->alias . '.' . $field;
		}
		return array_diff($model->fields(), $heavyFields);
	}

	/**
	 * CakePHP callback beforeSave
	 * Permet d'enregistrer le contenu d'un fichier en base
	 *
	 * @access public
	 * @param type $model
	 * @param array $options
	 * @return boolean
	 */
	public function beforeSave(Model $model, $options = array()) {
		$parentReturn = parent::beforeSave($model, $options);

		$this->_file = null;
		if (!empty($model->data['httpSentFile'])) {
			$this->_file = new File($model->data['httpSentFile']['tmp_name']);
			$model->data[$model->alias]['name'] = $model->data['httpSentFile']['name'];
		}

		if (!empty($model->data['fileToStore'])) {
			$this->_file = new File($model->data['fileToStore']['filename']);
			$model->data[$model->alias]['name'] = $this->_file->name() . '.' . $this->_file->ext();
		}

		if (!empty($this->_file)) {
			$model->data[$model->alias]['size'] = $this->_file->size();
			$model->data[$model->alias]['type'] = $this->_file->mime();

			$model->data[$model->alias]['checksum'] = $this->_file->md5();
		}

		return $parentReturn;
	}

	public function afterSave(Model $model, $created, $options = array()) {
		parent::afterSave($model, $created);

//	$this->log(var_export($model->id, true), 'debug');

		if ($created && !empty($this->_file)) {
//			$this->_file->delete();
		}
	}

	/**
	 * Récupération du contenu d'un fichier stocké en base
	 *
	 * @access public
	 * @param integer $id identifiant du document
	 * @throws NotFoundException
	 * @return string
	 */
	public function getFileContent(Model $model, $id) {
		if (empty($id)) {
			throw new NotFoundException();
		}

		$doc = $model->find('first', array('recursive' => -1, 'fields' => array($model->alias . '.content', $model->alias . '.id'), 'conditions' => array($model->alias . '.id' => $id)));
		$filecontent = $doc[$model->alias]['content'];
		return $filecontent;
	}

}

?>
