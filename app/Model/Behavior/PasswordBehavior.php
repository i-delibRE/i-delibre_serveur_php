<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class PasswordBehavior extends ModelBehavior {

	/**
	 * Vérification que la valeur du champ soit égale à la valeur de référence.
	 *
	 * @param mixed $check Les valeurs à vérifier.
	 * @param string $referenceField Le nom du champ de référence.
	 * @return boolean
	 */
	public function checkIdenticalValues(Model $model, $check, $referenceField) {
		if (!is_array($check) || empty($model->data[$model->alias][$referenceField])) {
			return false;
		}
		$result = true;
		foreach (Set::normalize($check) as $value) {
			$result = ( $value == AuthComponent::password($model->data[$model->alias][$referenceField]));
		}
		return $result;
	}

}

?>
