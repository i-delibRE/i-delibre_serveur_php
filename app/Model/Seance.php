<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');
App::uses('FolderTools', 'Utility');

//App::uses('Security', 'Utility');

class Seance extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Permet de stocker la séance à créer lors de la génération
     *
     * @var array
     */
    private $_seanceToCreate;

    /**
     * Permet de stocker les données $_POST pour traitement lors de la génération
     *
     * @var array
     */
    private $_post;

    /**
     * Permet de stocker les données $_FILES pour traitement lors de la génération
     *
     * @var array
     */
    private $_docs;

    /**
     * Permet de stocker les utilisateurs asssociés à un type lors de la génération
     *
     * @var array
     */
    private $_usersFromType;

    /**
     * Permet de stocker l'id du groupe "Représentatn" lors de la génération
     *
     * @var string UUID
     */
    private $_representantGroupId;

    /**
     * Liste des utilisateurs non associés à un type lors de la génération
     *
     * @var array
     */
    private $_usersNotAssociated;

    /**
     * Liste des utilisateurs non enregistrés en base lors de la génération
     *
     * @var array
     */
    private $_usersNotInDb;

    /**
     * Liste des utilisateurs enregistrés en base lors de la génération
     *
     * @var array
     */
    private $_usersInDb;

    /**
     * Contient les numéros de projets triés par ordre croissant
     *
     * @var array
     */
    private $_projetsRanks;

    /**
     *
     * @var type
     */
    private $_json;

    /**
     *
     * @var type
     */
    public $virtualFields = array(
        'fullname' => 'Seance.name || \' \' || Seance.date_seance'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //      'required' => true
            )
        ),
        'alias' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique')
            )
        ),
        'date_seance' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                //'message' => 'Your custom message here',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'document_id' => array(
            'uuid' => array(
                'rule' => array('uuid'),
                //'message' => 'Your custom message here',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'type_id' => array(
            'uuid' => array(
                'rule' => array('uuid'),
                //'message' => 'Your custom message here',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Document' => array(
            'className' => 'Document',
            'foreignKey' => 'document_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Convocation' => array(
            'className' => 'Convocation',
            'foreignKey' => 'seance_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Projet' => array(
            'className' => 'Projet',
            'foreignKey' => 'seance_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Invitation' => array(
            'className' => 'Invitation',
            'foreignKey' => 'seance_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     *
     * @param array $options
     */
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        DevTools::logMe("beforeSave",CONNECTEUR, true);

        if (empty($this->data['Seance']['name'])) {
            DevTools::logMe("beforeSave into the if",CONNECTEUR, true);
             if(isset($this->data['Seance']['type_id'])) {
                 $this->data['Seance']['alias'] = Inflector::slug($this->Type->field('name', array('Type.id' => $this->data['Seance']['type_id'])) . $this->data['Seance']['date_seance']);
                 $this->data['Seance']['name'] = $this->Type->field('name', array('Type.id' => $this->data['Seance']['type_id']));
             }
        }
    }

    /**
     * verification de l'éxistence d'une convocation en fonction de son nom et de sa date
     *
     * @param string $name
     * @param date $date
     * @return mixed (false si la convocation n'existe pas, uuid de la convocation si elle existe)
     */
    public function seanceExists($name, $date) {
        $qdSeance = array(
            'conditions' => array(
                'Seance.name' => $name,
                'Seance.date_seance' => $date,
            )
        );
        $result = $this->find('first', $qdSeance);

        $return = false;
        if (!empty($result)) {
            $return = $result['Seance']['id'];
        }
        return $return;
    }


    /**
     * Génération d'une séance et des convocations si la liste des Représentant et des projets (incluant les documents) ne sont pas vides.
     *
     * @param array $post
     * @param array $docs
     * @return array
     */
    public function genSeance($post, $docs, $webdelib = false) {
//        DevTools::logMe($post,'coll', true);
//        DevTools::logMe($docs,CONNECTEUR, true);

        //initialisation du message de retour
        $return = array(
            'success' => false,
            'code' => 'Seance.add.error',
            'message' => __d('seance', 'Seance.add.error'),
            'uuid' => null
        );

        $this->_docs = $docs;
        $this->_post = $post;

        if (empty($this->_post['jsonData'])) {
            $return['code'] = 'Seance.add.error.jsonData.void';
            $return['message'] = __d('seance', 'Seance.add.error.jsonData.void');
        } else {
            //preparation des données
            $this->_post = json_decode($this->_post['jsonData'], true);


            //DevTools::ldebug($this->_post);
            //Traitement des informations de base pour la séance
            try {
                $this->_genBaseSeance();
            } catch (Exception $exc) {
                DevTools::logMe($exc->getMessage(),CONNECTEUR, true);
                $return['code'] = 'Seance.add.error.genBaseSeance';
                $return['message'] = __d('seance', 'Seance.add.error.genBaseSeance') . ' ' . $exc;
            }

            //traitement des acteurs (ajout si besoin et liaison avec la seance)
            try {
                $this->_setUsers($webdelib);
            } catch (Exception $exc) {
                DevTools::logMe($exc->getMessage(),CONNECTEUR, true);
                $return['code'] = 'Seance.add.error.setUsers';
                $return['message'] = __d('seance', 'Seance.add.error.setUsers') . ' ' . $exc;
            }

            //creation et association du document principal (convocation)
            try {
                $this->_seanceToCreate['Document']['id'] = $this->_createDoc('Document', $docs['convocation']);
            } catch (Exception $exc) {
                DevTools::logMe($exc->getMessage(),CONNECTEUR, true);
                $return['code'] = 'Seance.add.error.createDoc.convocation';
                $return['message'] = __d('seance', 'Seance.add.error.createDoc.convocation') . ' ' . $exc;
            }

            //traitement des projets
            try {
                $this->_setProjets();
            } catch (Exception $exc) {
                DevTools::logMe($exc->getMessage(),CONNECTEUR, true);
                $return['code'] = 'Seance.add.error.setProjets';
                $return['message'] = __d('seance', 'Seance.add.error.setProjets') . ' ' . $exc;
            }



            //ajout du lieu de la seance
            if (!empty($post['data']['Seance']['place'])) {
                $this->_seanceToCreate['Seance']['place'] = $post['data']['Seance']['place'];
            }

            //ajout du lieu de la seance depuis le webservice
            if (!empty($this->_post['place'])) {
                $this->_seanceToCreate['Seance']['place'] = $this->_post['place'];
            }

            //enregistrment global de la seance
            try {
                $resultSaveSeance = $this->saveAll($this->_seanceToCreate, array('deep' => true));
                //verification des erreurs
                if ($resultSaveSeance) {
                    $return['success'] = true;
                    $return['code'] = 'Seance.add.ok';
                    $return['message'] = __d('seance', 'Seance.add.ok');
                    $return['uuid'] = $this->id;
                };
            } catch (Exception $exc) {
                DevTools::logMe($exc->getMessage(),CONNECTEUR, true);
                //Detection de l'erreur de duplication de la date de seance (PostgreSQL)
                if (preg_match('#7:.*#', $this->getDataSource()->lastError())) {
//					$return['message'] = __d('seance', 'Seance.add.error.alreadyExists');
                    $return['code'] = 'Seance.post.noop';
                    $return['message'] = __d('seance', 'Seance.post.noop');
                }
            }
        }
        return $return;
    }


    // v3.1
    public function genSeanceJson($jsonPost, $files, $webdelib = false){

        $post = json_decode($jsonPost['json_data'], true);



        //check files format !
        $projets = $post["projets"];
        foreach ($projets as $projet){
            if($projet["type"] !="application/pdf"){
                return array(
                    'success' => false,
                    'code' => "add.projects.error",
                    'message' => "erreur de type de fichier"
                );
            }
        }

        if($post["convocation"]["type"] != "application/pdf"){
            return array(
                'success' => false,
                'code' => "add.convocation.error",
                'message' => "erreur de type de fichier"
            );
        }

        //set minimum infos
        $seanceToCreate = $this->setInfoSeanceJson($post);

        //set convocations
        $seanceToCreate['Convocation'] = array();
        foreach ($post["acteurs_convoques"] as $acteurId) {
            $seanceToCreate['Convocation'][] = array('user_id' => $acteurId);
        }

        // set place
        if (!empty($post['place'])) {
            $seanceToCreate['Seance']['place'] = $post['place'];
        }

        //set convocations files
        $seanceToCreate['Document']['id'] = $this->_createDoc('Document', $files['convocation']);


        //if invitation file createDocument
        if(isset($files['invitation'])){
            $invitationDocumentId = $this->_createDoc('Document', $files['invitation']);

            $seanceToCreate['Invitation'] = array();

            foreach ($post["invites"] as $inviteId) {
                $seanceToCreate['Invitation'][] = array('user_id' => $inviteId, 'document_id' => $invitationDocumentId);
            }


            foreach ($post["administratifs"] as $administratifId) {
                $seanceToCreate['Invitation'][] = array('user_id' => $administratifId, 'document_id' => $invitationDocumentId);
            }
        }

        //set project files
        $resCreateProjets = $this->_setProjetsJson($post, $files);

        if($resCreateProjets === false){
            return array(
                'success' => false,
                'code' => "modify.projects.error",
                'message' => "Erreur lors de l'ajout des projets"
            );
        }

        $seanceToCreate["Projet"] = $resCreateProjets;

        //save seance !
        try {
            $resultSaveSeance = $this->saveAll($seanceToCreate, array('deep' => true));
            //verification des erreurs
            if ($resultSaveSeance) {
                $return['success'] = true;
                $return['code'] = 'Seance.add.ok';
                $return['message'] = __d('seance', 'Seance.add.ok');
                $return['uuid'] = $this->id;

                $this->genZipSeanceAsync($this->id);
            };
        } catch (Exception $exc) {
            //Detection de l'erreur de duplication de la date de seance (PostgreSQL)
            if (preg_match('#7:.*#', $this->getDataSource()->lastError())) {
                $return['code'] = 'Seance.post.noop';
                $return['message'] = __d('seance', 'Seance.post.noop');
                $return['success'] = false;
            }
        }

        return $return;
    }


    public function genZipSeanceAsync($seanceId){
        $seance = $this->find('first', array(
            "conditions" => array(
                "Seance.id" => $seanceId
            ),
            "contain" => array(
                "Document" => array(
                    "fields" => array("id", "path", "name")
                ),
                "Projet" => array(
                    "fields" => array("id", "name"),
                    "Document" => array(
                        "fields" => array("id", "path", "name")
                    ),
                    "Annex" => array(
                        "fields" => array("id", "path", "name")
                    )
                ),
            )));

        // DEV
        $collId = $_SESSION['Auth']['collectivite']['id'];
        //$collId = '5810a34f-c540-4ad1-9caa-189c5cde535b';

        $folder = ZIP.$collId. DS. $seanceId;
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        //delete zip if exist
        if (file_exists($folder . ".zip")) {
            unlink($folder . ".zip");
        }


        //copy convocationDocument
        copy($seance["Document"]["path"], $folder.DS.$seance["Document"]["name"] );


        //create a folder for each projet with project and annexes inside
        foreach ($seance["Projet"] as $projet){
            //create directory
            $projetFolder =  $folder . DS . $projet["name"];
            if (!file_exists($projetFolder)) {
                mkdir($projetFolder, 0777, true);
            }
            //copy project file
            copy($projet["Document"]["path"], $projetFolder .DS. $projet["Document"]["name"] );

            foreach ($projet["Annex"] as $annexe){
                copy($annexe["path"], $projetFolder .DS. $annexe["name"]);
            }
        }

        //les && sont la pour le do in backround
        //   On se met dans le bon repertoire                      on zip recursivmeent la séance                                              On supprime les fichiers temporaires
        $cmd = "cd ".  ZIP . $collId . " > /dev/null 2>&1 && nohup zip -r  " . $seanceId . ".zip " . $seanceId . DS .  "* >/dev/null 2>&1 &&  rm -rf " .ZIP. $collId. DS . $seanceId .DS ;
        shell_exec($cmd);
    }



    // v3.1
    private function _setProjetsJson($post, $files) {
        $seanceToCreate['Projet'] = array();
        DevTools::logMe("ADDSEANCE",ADD_SEANCE);


        if (!empty($post['projets'])) {
            foreach ($post['projets'] as $projet) {
                //traitement des annexes
                $tabAnnexes = array();
                if (isset($projet['annexes'])) {
                    foreach ($projet['annexes'] as $annexe) {
                        $documentId = $this->_createDoc('Annex', $files['projet_' . $projet['rank'] . '_' . $annexe['rank'] . '_annexe']);
                        $tabAnnexes[] = array(
                            'id' => $documentId,
                            'rank' => $annexe['rank']
                        );

                    }
                }
                $rapporteurId = null;
                // en provenance de l'application
                if (isset($projet['rapporteurId']) &&  $projet['rapporteurId'] !=='') {
                    $rapporteurId = $projet['rapporteurId'];
                }

                //en provenance du connecteur
                if (isset($projet['rapporteurlastname']) && isset($projet['rapporteurfirstname'])) {
                    $User = ClassRegistry::init('User');
                    $user = $User->find('first', array(
                        'field' => array('id'),
                        'conditions' => array(
                            'and' => array(
                                'User.firstname ilike' => $projet['rapporteurfirstname'],
                                'User.lastname ilike' => $projet['rapporteurlastname'],
                            )
                        )
                    ));
                    $rapporteurId = $user['id'];
                }

                //creation du projet
                if (isset($files['projet_' . $projet['rank'] . '_rapport'])) {
                    $documentId = $this->_createDoc('Document', $files['projet_' . $projet['rank'] . '_rapport']);

                    if(!isset($projet['theme'])) {
                        DevTools::logMe(array("message" =>  "MISSING THEME","var" => $projet), ADD_SEANCE, true);
                        return false;
                    }


                    $seanceToCreate['Projet'][] = array(
                        'name' => $projet['fullName'],
                        'user_id' => $rapporteurId,
                        'ptheme_id' => $projet['theme']['id'],
                        'document_id' => $documentId,
                        'rank' => $projet['rank'],
                        'Annex' => $tabAnnexes
                    );

                } else {
                    $seanceToCreate['Projet'][] = array(
                        'id' => $projet['uuid'],
                        'rank' => $projet['ordre']
                    );
                }
            }
        }

        // $this->genZipSeanceAsync($docIds);


        DevTools::logMe($seanceToCreate["Projet"],ADD_SEANCE);

        return $seanceToCreate["Projet"];



    }



    private function setInfoSeanceJson($post) {

        //verification de la présence de la date
        if (empty($post['date_seance'])) {

            throw new CakeException(__d('seance', 'Seance.genError.genBaseSeance.date_seance'));
        }

        //detection du type de séance
        $typeId = null;
        if (!empty($post['type_id'])) {
            $typeId = $post['type_id'];
        } else if (!empty($post['type_seance'])) { //cas du web-service
            $typeId = $this->Type->getTypeIdFromName($post['type_seance'], true); //recherche de l'identifiant du type (creation si le type n'existe pas)
        }

        if (!$this->Type->exists($typeId)) {
            throw new CakeException(__d('seance', 'Seance.genError.genBaseSeance.type_id'));
        }
        $seanceToCreate = $this->create();
        $seanceToCreate['Seance']['type_id'] = $typeId;
        $seanceToCreate['Seance']['date_seance'] = $this->_normalizeDate($post['date_seance']);
        return $seanceToCreate;
    }



    /**
     * Génération d'une séance et des convocations si la liste des Représentant et des projets (incluant les documents) ne sont pas vides.
     *
     * @param array $post
     * @param array $docs
     * @return array
     */
    public function editSeance($seanceId, $post, $docs) {
        //initialisation du message de retour
        $return = array(
            'success' => false,
            'message' => __d('seance', 'Seance.edit.error'),
            'uuid' => null
        );

        /*
                debug($post);
                debug($docs); die;
        */

        $this->_docs = $docs;
        $this->_post = $post;

        $docsToDrop = array();

        if (empty($seanceId) || !$this->exists($seanceId)) {
            $return['message'] = __d('seance', 'Seance.notFound');
        } else if (empty($this->_post['jsonData'])) {
            $return['message'] = __d('seance', 'Seance.edit.error.jsonData.void');
        } else {
            $this->_post = json_decode($this->_post['jsonData'], true);
            $this->_seanceToCreate = $this->find('first', array('contain' => array('Projet' => array('fields' => array('Projet.id'))), 'conditions' => array('Seance.id' => $seanceId)));



            // on incremente le numero de revision
            $this->_seanceToCreate["Seance"]["rev"] +=1;

            // creation et association du document principal (convocation)
            if ($docs['convocation']['error'] == 0) {
                try {
                    $docsToDrop[] = $this->_seanceToCreate['Seance']['document_id'];
                    unset($this->_seanceToCreate['Seance']['document_id']);
                    $this->_seanceToCreate['Document']['id'] = $this->_createDoc('Document', $docs['convocation']);
                } catch (Exception $exc) {
                    $return['message'] = __d('seance', 'Seance.edit.error.createDoc.convocation') . ' ' . $exc;
                }
            }

            try {
                // projet present dans la bdd
                $oldProjets = Hash::extract($this->_seanceToCreate['Projet'], '{n}.id');
                //debug($this->_seanceToCreate);
                $this->_setProjets();
                //debug($this->_seanceToCreate); die;
                $newProjets = Hash::extract($this->_seanceToCreate['Projet'], '{n}.id');
                $this->_dropProjets($oldProjets, $newProjets);
            } catch (Exception $exc) {
                $return['message'] = __d('seance', 'Seance.edit.error.setProjets') . ' ' . $exc;
            }

            //enregistrment global de la seance
            try {
                $resultSaveSeance = $this->saveAll($this->_seanceToCreate, array('deep' => true));
                //verification des erreurs
                if ($resultSaveSeance) {
                    $this->_dropDocs($docsToDrop);

                    $return['success'] = true;
                    $return['message'] = __d('seance', 'Seance.edit.ok');
                    $return['uuid'] = $this->id;
                };
            } catch (Exception $exc) {
                //Detection de l'erreur de duplication de la date de seance (PostgreSQL)
                if (preg_match('#7:.*#', $this->getDataSource()->lastError())) {
//					$return['message'] = __d('seance', 'Seance.add.error.alreadyExists');
                    $return['message'] = __d('seance', 'Seance.post.noop');
                }
            }
        }
        return $return;
    }


    // v3.1
    public function editSeanceJson($seanceId, $jsonPost, $files) {
        $data = json_decode($jsonPost["json_data"], true);
//dateSeance / place/  ne sont pas maj


        //check files extensions
        $projets = $data["projets"];
        foreach ($projets as $projet){
            $extension = substr($projet["documentName"], -3);
            if($extension != "pdf"){
                return array(
                    'success' => false,
                    'code' => "add.projects.error",
                    'message' => "erreur de type de fichier"
                );
            }
        }


        $convocationExtension = substr($data["convocation"]["name"], -3);
        if($convocationExtension != "pdf"){
            return array(
                'success' => false,
                'code' => "add.projects.error",
                'message' => "erreur de type de fichier"
            );
        }

        //update Convocations
        if(!$this->updateConvocations($data["convocation"], $data["seanceId"], $files)){
            return array(
                'success' => false,
                'message' => "impossible de mettre à jour la convocation",
                'uuid' => $seanceId
            );
        }


        if(!$this->updateBaseSeance($data)){
             return array(
                'success' => false,
                'message' => "impossible de mettre à jour les informations de la séance",
                'uuid' => $seanceId
            );
        }




        $this->begin();

        // Update users
        if(!$this->updateConvocatedUsers($data['acteurs_convoques'], $data['seanceId'])){
            $this->rollback();
            return array(
                'success' => false,
                'message' => "impossible de mettre à jour les acteurs convoqués",
                'uuid' => $seanceId
            );
        }



        //update invitations
        if(!$this->updateInvitations($data['invites'], $data['administratifs'] , $data['seanceId'], $files)){
            $this->rollback();
            return array(
                'success' => false,
                'message' => "impossible de mettre à jour les utilisateurs invités",
                'uuid' => $seanceId
            );
        }

        //update Projets
        if(!$this->updateProjets($data["projets"], $data["seanceId"], $files)){
            $this->rollback();
            return array(
                'success' => false,
                'message' => "impossible de mettre à jour les projets ou annexes",
                'uuid' => $seanceId
            );
        }



        // TODO regen the zip
        $this->genZipSeanceAsync($seanceId);



        $this->commit();

        return array(
            'success' => true,
            'message' => "La séance a bien été modifiée",
            'uuid' => $seanceId
        );


    }


    private function updateBaseSeance($data){
        $this->id = $data["seanceId"];
        $rev = $this->field('rev');
        $newRev = $rev +1 ;
        $date = $this->_normalizeDate($data['date_seance']);
        $place = $data["place"];
        $res = $this->saveField("place", $place);
        $res = $res && $this->saveField("date_seance", $date);
        $res = $res && $this->saveField("rev", $newRev);

        return $res;
    }




    private function updateConvocations($convocation, $seanceId, $files){
        if(isset($convocation['file'])){
            $documentId = $this->_createDoc('Document', $files['convocation']);
            $this->create();
            $this->id = $seanceId;
            return $this->saveField("document_id", $documentId);
        }else{
            // if no convocation file
            return true;
        }
    }



    private function findProjetIndex($projets, $documentId){
        for($i=0; $i<= count($projets); $i++){
            if(isset($projets[$i]["documentId"]) && $projets[$i]["documentId"] == $documentId){
                return $i;
            }
        }
        return -1;
    }



    private function updateProjets($projets, $seanceId, $files){

        $documentsId = Hash::extract($projets, "{n}.documentId");
        $existingProjets = $this->Projet->find("all", array(
            "conditions" => array(
                "Projet.seance_id" => $seanceId
            ),
            "contain" => array("Annex")
        ));

        // Remove projets
        $toRemoveProjets = array();
        foreach ($existingProjets as $existingProjet){
            if(!in_array($existingProjet["Projet"]["document_id"], $documentsId)){
                array_push($toRemoveProjets, $existingProjet["Projet"]["id"]);
            }
        }


        // update rank
        foreach ($existingProjets as $existingProjet){
            $index = $this->findProjetIndex($projets, $existingProjet["Projet"]["document_id"]);
            if($index >=0){

                if(!isset($projets[$index]["userId"]) || $projets[$index]["userId"] == "Aucun")
                    $projets[$index]["userId"] = null;

                $existingProjet["Projet"]["rank"] = $projets[$index]["rank"];
                $existingProjet["Projet"]["ptheme_id"] = $projets[$index]["themeId"];
                $existingProjet["Projet"]["user_id"] = $projets[$index]["userId"];
                $existingProjet["Projet"]["name"] = $projets[$index]["name"];
                $existingProjet["Projet"]["theme"] = $projets[$index]["fulltheme"];
                $this->Projet->create();
                $res = $this->Projet->save($existingProjet);

                if(!$res)
                    return false;


                //update annexes :
//                if(!empty($existingProjet["Annex"])){
                if(!$this->updateAnnexes($existingProjet,  $projets[$index], $files))
                    return false;
//                }
            }
        }

        //add new projets
        foreach ($projets as $key => $projet){
            if(!isset($projet["documentId"])){
                $tabAnnexes = array();
                if (isset($projet['annexes'])) {
                    foreach ($projet['annexes'] as $annexe) {
                        $tabAnnexes[] = array(
                            'id' => $this->_createDoc('Annex', $files['projet_' . $projet['rank'] . '_' . $annexe['rank'] . '_annexe']),
                            'rank' => $annexe['rank']
                        );
                    }
                }

                if(!isset($projet['themeId'])) {
                    DevTools::logMe(array(
                        "message" =>  "MISSING THEME",
                        "var" => $projet
                    ), EDIT_SEANCE);
                    return false;
                }

                if(!isset( $projet['userId']))
                    $projet['userId'] = null;


                $prj = array(
                    'name' => $projet['name'],
                    'user_id' => $projet['userId'],
                    'ptheme_id' => $projet['themeId'],
                    'document_id' => $this->_createDoc('Document', $files['projet_' . $projet['rank'] . '_rapport']),
                    'rank' => $projet['rank'],
                    'seance_id' => $seanceId,
                    'Annex' => $tabAnnexes,

                );

                $this->Projet->create();

                $res = $this->Projet->saveAll($prj);
                if(!$res)
                    return false;
            }
        }

        //TODO remove Annex with them
        if(count($toRemoveProjets) > 0) {
            $res = $this->Projet->delete(
                $toRemoveProjets
            );
            if (!$res)
                return false;
        }

        return true;
    }


    // v3.1
    private function updateAnnexes($existingProjet, $sendProjet, $files){


        $sendIds = Hash::extract($sendProjet, "annexes.{n}.id");
        //to remove Annexes
        $toRemove = array();
        foreach ($existingProjet["Annex"] as $existingAnnexe){
            //          array_push($existingAnnexesId, $existingAnnexe["id"]);
            if(!in_array($existingAnnexe["id"], $sendIds)){
                array_push($toRemove, $existingAnnexe["id"]);
            }
        }

        if (count($toRemove) > 0) {
            DevTools::logMe($toRemove, "annexe");
            $res = $this->Projet->Annex->deleteAll(array(
                'Annex.id' => $toRemove
            ));
            DevTools::logMe("after delete", "annexe");
            if(!$res)
                return false;


        }


        // FIXME BUG CRASH If REMOVE ANNEXE !!!!  //TODO check if annexe is not already removed !!!!
        foreach ($existingProjet["Annex"] as $existingAnnexe) {
            //update annexes position
            $index = array_search($existingAnnexe["id"], $sendIds);

            if ($index >= 0) { //if annexe already exist
                $existingAnnexe["rank"] = $sendProjet["annexes"][$index]['rank'];

                $this->Projet->Annex->create();
                $this->Projet->Annex->id = $existingAnnexe["id"];

                if ($this->Projet->Annex->exists($existingAnnexe["id"])) {
                    if (!$this->Projet->Annex->saveField("rank", $sendProjet["annexes"][$index]['rank']))
                        return false;
                }
            }
        }

        foreach ($sendProjet["annexes"] as $annexe){

            if(isset($annexe["file"])){
                $AnnexeId = $this->_createDoc('Annex', $files['projet_' . $sendProjet['rank'] . '_' . $annexe['rank'] . '_annexe']);

                $this->Projet->Annex->create();

                $res = $this->Projet->Annex->updateAll(
                    array('rank' => $annexe['rank'], 'Annex.projet_id' => "'" . $sendProjet["id"] . "'"),
                    array("Annex.id" => $AnnexeId)
                );

                if(!$res)
                    return false;

            }
        }
        return true;
    }


    // v3.1
    private function updateConvocatedUsers($usersId, $seanceId){
        $seance =  $this->find('first', array(
            'conditions' => array(
                "Seance.id" => $seanceId
            ),
            'contain' => array("Convocation"),

        ));

        $alreadyInUsersId = Hash::extract($seance, "Convocation.{n}.user_id");

        $userToAdd = array();
        foreach ($usersId as $userId){
            if(!in_array($userId,$alreadyInUsersId)){
                array_push($userToAdd, $userId);
            }
        }

        $userToRemove = array();
        foreach ($alreadyInUsersId as $userNewId){
            if(!in_array($userNewId, $usersId)){
                array_push($userToRemove, $userNewId);
            }
        }


        $error = false;
        $deleteResult = $this->Convocation->deleteAll(array(
            "seance_id" => $seanceId,
            "user_id" => $userToRemove
        ));
        if(!$deleteResult){
            return false;
        }

        foreach ($userToAdd as $user) {
            $this->Convocation->create();
            $conv = array();
            $conv['seance_id'] = $seanceId;
            $conv['user_id'] = $user;
            if(!$this->Convocation->save($conv))
                $error = true;
        }

        if(!$error)
            return true;
        else
            return false;


    }


    //TODO Check if no invitation document before
    // V3.1
    private function updateInvitations($invites, $administratifs , $seanceId, $fileInvitation){

        DevTools::logMe($fileInvitation, EDIT_SEANCE, __LINE__, __FILE__);

        $totalUserIds = array_merge($invites, $administratifs);

        $invitation = $this->Invitation->find("all", array(
            'conditions' => array(
                "seance_id" => $seanceId
            ),
            'fields' => array('user_id', 'document_id')
        ));







        $alreadyInUsersId = Hash::extract($invitation, "{n}.Invitation.user_id");


        $userToAdd = array();
        foreach ($totalUserIds as $userId){
            if(!in_array($userId,$alreadyInUsersId)){
                array_push($userToAdd, $userId);
            }
        }

        $userToRemove = array();
        foreach ($alreadyInUsersId as $userNewId){
            if(!in_array($userNewId, $totalUserIds)){
                array_push($userToRemove, $userNewId);
            }
        }

        $errors = false;

        // delete invitation for removed users
        $res = $this->Invitation->deleteAll(array(
            "seance_id" => $seanceId,
            "user_id" => $userToRemove
        ));

        if(!$res)
            return false;

        //add new invitations with the old document
        if(isset($invitation[0]["Invitation"]["document_id"])) {

            foreach ($userToAdd as $user) {
                $this->Invitation->create();
                $inv = array();
                $inv['seance_id'] = $seanceId;
                $inv['user_id'] = $user;
                $inv['document_id'] = $invitation[0]["Invitation"]["document_id"];
                $res = $this->Invitation->save($inv);

                if (!$res)
                    return false;
            }


        }

        //if new invitation files ()
        if(isset($fileInvitation['invitation'])){
            DevTools::logMe("infileinvitation", EDIT_SEANCE, __LINE__, __FILE__);
            //save document
            $documentId = $this->_createDoc('Document', $fileInvitation['invitation']);
            //update all invitations
            $this->Invitation->create();
            $res = $this->Invitation->updateAll(
                array("Invitation.document_id" =>"'" . $documentId ."'"),
                array("Invitation.seance_id" => $seanceId)
            );
            DevTools::logMe($res, EDIT_SEANCE, __LINE__, __FILE__);

            if(!$res)
                $errors = true;

            DevTools::logMe("afterSave", EDIT_SEANCE, __LINE__, __FILE__);


            if(!isset($invitation[0]["Invitation"]["document_id"])) {
                foreach ($userToAdd as $user) {
                    $this->Invitation->create();
                    $inv = array();
                    $inv['seance_id'] = $seanceId;
                    $inv['user_id'] = $user;
                    $inv['document_id'] = $documentId;
                    $res = $this->Invitation->save($inv);
                    if (!$res)
                        return false;
                }
            }
        }

        if($errors)
            return false;
        else
            return true;
    }





    /**
     *
     * @param type $docsToDrop
     */
    private function _dropDocs($docsToDrop) {
        for ($i = 0; $i < count($docsToDrop); $i++) {
            $this->Projet->Document->delete($docsToDrop[$i]);
        }
    }

    /**
     *
     * @param type $old
     * @param type $new
     */
    private function _dropProjets($old, $new) {
        $work = array_diff($old, $new);
        $tab = array();
        foreach ($work as $key => $value) {
            array_push($tab, $value);
        }
        for ($i = 0; $i < count($tab); $i++) {
            $this->Projet->delete($tab[$i]);
        }
    }

    /**
     * push de la convocation !
     */
    public function WSDeleteSeance($seanceId, $userId) {
        $ch = curl_init("http://localhost:" . IDELIBRE_WS_PORT . "/removeSeance/" . $userId . "/" . PASSPHRASE);
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     *
     * @param type $id
     * @param type $cascade
     */
    public function delete($id = null, $cascade = true) {
        if ($id === null) {
            $id = $this->id;
        }
        $qd = array(
            'fields' => array(
                'Convocation.id',
                'Convocation.user_id'
            ),
            'conditions' => array(
                'Convocation.seance_id' => $id,
                'Convocation.active' => true
            )
        );

        $convocatedUserIds = array_values($this->Convocation->find('list', $qd));


        $qd = array(
            'fields' => array(
                'Invitation.id',
                'Invitation.user_id'
            ),
            'conditions' => array(
                'Invitation.seance_id' => $id,
                'Invitation.isactive' => true
            )
        );


        $invitedUserIds = array_values($this->Invitation->find('list', $qd));

        $userIds = array_merge($convocatedUserIds, $invitedUserIds);




        //remove document from serveur
        $invitation = $this->Invitation->find('first', array(
            'conditions' =>array(
                'Invitation.seance_id' => $id
            ),
            'contain' => array("Document")
        ));


        if(isset($invitation["Document"]["path"])){
            $isdeleted = unlink ($invitation['Document']['path']);
            if(!$isdeleted){
                CakeLog::write('deleteError', $invitation['Document']['path'] );
            }
        }


        //remove document from serveur
        $ConvocationPath = $this->Document->find('first', array(
            'fields' => array('path'),
            'conditions' =>array(
                "id" => $this->field("document_id")
            )
        ));

        $isdeleted = unlink ($ConvocationPath['Document']['path']);
        if(!$isdeleted){
            CakeLog::write('deleteError', $ConvocationPath['Document']['path'] );
        }


        // remove projetDocuments and annexes from fileSystem
        $filePaths =  $this->find('first', array(
            "conditions" => array("Seance.id" => $id),
            "fields" => array("id"),
            "contain" => array(
                "Projet" => array(
                    "fields" => array("Projet.id"),
                    "Document" => array("fields" => array("Document.path")),
                    "Annex" => array("fields" => array("Annex.path"))
                ))
        ));


        $projetPaths = Hash::extract($filePaths, "Projet.{n}.Document.path");
        $annexePaths = Hash::extract($filePaths, "Projet.{n}.Annex.{n}.path");



        foreach ($projetPaths as  $projetPath){
            $isdeleted = unlink ($projetPath);
            if(!$isdeleted){
                CakeLog::write('deleteError', $projetPath );
            }

        }
        foreach ($annexePaths as $annexePath){
            $isdeleted = unlink ($annexePath);
            if(!$isdeleted){
                CakeLog::write('deleteError',$annexePath);
            }
        }



        //delete zip file
        //DEV
        $collId = $_SESSION['Auth']['collectivite']['id'];
        // $collId = '5810a34f-c540-4ad1-9caa-189c5cde535b';

        $zipPath = ZIP.$collId.DS.$id.".zip";
        DevTools::logMe($zipPath, "deleteCheck");

        $isDeleted = unlink($zipPath);
        DevTools::logMe($isDeleted, "deleteCheck");

        if(!$isDeleted)
            CakeLog::write('deleteError',$zipPath);

        $resultDelete = parent::delete($id, $cascade);

        if ($resultDelete && !empty($userIds)) {
            foreach ($userIds as $userId) {
                $this->WSDeleteSeance($id, $userId);
            }
        }
        return $resultDelete;
    }

    /**
     * creation de la séance à enregistrer et traitement des informations de base de la séance
     *
     * @throws CakeException
     */
    private function _genBaseSeance() {
        //verification de la présence de la date
        if (empty($this->_post['date_seance'])) {
            throw new CakeException(__d('seance', 'Seance.genError.genBaseSeance.date_seance'));
        }

        //detection du type de séance
        $typeId = null;
        if (!empty($this->_post['type_id'])) { //cas du formulaire i-delibRE
            $typeId = $this->_post['type_id'];
        } else if (!empty($this->_post['type_seance'])) { //cas du web-service
            $typeId = $this->Type->getTypeIdFromName($this->_post['type_seance'], true); //recherche de l'identifiant du type (creation si le type n'existe pas)
        }

        if (!$this->Type->exists($typeId)) {
            throw new CakeException(__d('seance', 'Seance.genError.genBaseSeance.type_id'));
        }

        $this->_seanceToCreate = $this->create();
        $this->_seanceToCreate['Seance']['type_id'] = $typeId;
        $this->_seanceToCreate['Seance']['date_seance'] = $this->_normalizeDate($this->_post['date_seance']);


    }

    /**
     *
     * @param type $strDate
     * @return type
     */
    private function _normalizeDate($strDate) {
        $strDateTime = explode(' ', $strDate);
        $date = explode('-', $strDateTime[0]);
        $time = explode(':', $strDateTime[1]);
        return date('Y-m-d H:i:s', mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]));
    }

    /**
     * Traitement des utilisateurs associés à la scéance
     */
    private function _setUsers($webdelib) {
        //récupération des utilisateurs en fonction du type de séance
        $this->_getUsersFromType();

        //récupération de l'id du groupe "Représentant"
        $this->_getRepresentantGroupId();

//        DevTools::ldebug($this->_post);
        //récupération des utilisateurs envoyés par le formulaire ou le web-service
        if (is_string($this->_post['acteurs_convoques'])) {
            $users = json_decode($this->_post['acteurs_convoques'], true);

        } else if (is_array($this->_post['acteurs_convoques'])) {
            $users = $this->_post['acteurs_convoques'];
        }

        // récupération des utilisateurs non enregistrés en base du formulaire
        $this->_usersInDb = array();
        $this->_usersNotInDb = $this->Convocation->User->getNotInDB($users, $this->_usersInDb);

        // récupération des utilisateurs non associés du formulaire (utilisateurs non assignés et non enregistrés)
        $notAssociated = $this->Type->getNotAssociated($this->_seanceToCreate['Seance']['type_id'], $this->_usersInDb);

        $freshCreatedUsers = $this->Convocation->User->createUsers($this->_usersNotInDb, $this->_representantGroupId);

        $this->_usersNotAssociated = array_merge($notAssociated, $freshCreatedUsers);


        if ($webdelib) {
            //on associe les nouveaux utilisateurs et ceux non associés au type
            $this->Type->associateUsers($this->_seanceToCreate['Seance']['type_id'], $this->_usersNotAssociated);
        }
        //association de chaque user à la séance via la table convocations
        $users = $this->Type->getUsersFromTypeId($this->_seanceToCreate['Seance']['type_id']);


        if (!$webdelib && !empty($this->_usersNotAssociated)) {
            $newusers = Hash::extract($this->_usersNotAssociated, '{n}.User');
            $users = array_merge($users, $newusers);
        }



        //find black listed users
        $User = ClassRegistry::init('User');
        $blackListedIds = $User->find('all', array(
            "conditions" => array(
                'blacklisted' => true
            ),
            'fields' => array("id")
        ));
        $blackListedIds = Hash::extract($blackListedIds, "{n}.User.id");

        //remove black listed users
        foreach ($users as $key=>$user) {
            if(in_array($user['id'], $blackListedIds)){
                unset($users[$key]);
            }
        }



        $this->_seanceToCreate['Convocation'] = array();
        foreach ($users as $user) {

            $this->_seanceToCreate['Convocation'][] = array('user_id' => $user['id']);
        }






//		$this->_seanceToCreate['Convocation']['User'] = $this->Type->getUsersFromTypeId($this->_seanceToCreate['Seance']['type_id']);
    }

    /**
     * traitement des projets
     */
    private function _setProjets() {
        $this->_seanceToCreate['Projet'] = array();
        if (!empty($this->_post['projets'])) {
            foreach ($this->_post['projets'] as $projet) {

                DevTools::logMe( $projet,'coll', true);

                //traitement des annexes
                $tabAnnexes = array();
                if (isset($projet['annexes'])) {

                    foreach ($projet['annexes'] as $annexe) {
                        $tabAnnexes[] = array(
                            'id' => $this->_createDoc('Annex', $this->_docs['projet_' . $projet['ordre'] . '_' . $annexe['ordre'] . '_annexe']),
                            'rank' => $annexe['ordre']
                        );
                    }
                }

                $rapporteurId = null;
                // en provenance de l'application
                if (isset($projet['rapporteurId']) &&  $projet['rapporteurId'] !=='') {
                    $rapporteurId = $projet['rapporteurId'];
                }

                //en provenance du connecteur
                if ( isset($projet['Rapporteur']) && isset($projet['Rapporteur']['rapporteurlastname']) && isset($projet['Rapporteur']['rapporteurfirstname'])) {
                    $this->log("OKOKOKOKOKOKO", "coll");
                    $User = ClassRegistry::init('User');
                    $user = $User->find('first', array(
                        'field' => array('id'),
                        'conditions' => array(
                            'and' => array(
                                'User.firstname ilike' => $projet['Rapporteur']['rapporteurfirstname'],
                                'User.lastname ilike' => $projet['Rapporteur']['rapporteurlastname'],
                            )
                        )
                    ));

                    if(!empty($user['User']['id'])) {
                        $rapporteurId = $user['User']['id'];
                        $this->log("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "coll");
                    }
                }

                //creation du projet
                if (isset($this->_docs['projet_' . $projet['ordre'] . '_rapport'])) {
                    $this->_seanceToCreate['Projet'][] = array(
                        'name' => $projet['libelle'],
                        'user_id' => $rapporteurId,
                        'ptheme_id' => $this->Projet->Ptheme->treeCreate($projet['theme']),
                        'document_id' => $this->_createDoc('Document', $this->_docs['projet_' . $projet['ordre'] . '_rapport']),
                        'rank' => $projet['ordre'],
                        'Annex' => $tabAnnexes
                    );




                } else {
                    $this->_seanceToCreate['Projet'][] = array(
                        'id' => $projet['uuid'],
                        'rank' => $projet['ordre']
                    );
                }
            }
        }
    }

    /**
     * Récupération des utilisateurs du type de séance
     */
    private function _getUsersFromType() {
        $this->_usersFromType = $this->Type->getUsersFromTypeId($this->_seanceToCreate['Seance']['type_id']);
    }

    /**
     * Récupération de l'id du groupe "Représentant"
     */
    private function _getRepresentantGroupId() {
        //$this->_representantGroupId = $this->Convocation->User->Group->field('Group.id', array('Group.name' => 'Acteur'));
        $this->_representantGroupId = IDELIBRE_GROUP_AGENT_ID;
    }

    /**
     * Créer un document enregistré en base
     *
     * @param string $modelAlias
     * @param array $uploadedFile file item from $_FILES
     * @return mixed return false on error, id (or uuid) else
     */
    private function _createDoc($modelAlias, $uploadedFile) {
        $return = false;
        $document = $this->Projet->{$modelAlias}->create();
        $document[$modelAlias]['name'] = $uploadedFile['name'];
        $document['httpSentFile'] = $uploadedFile;

        if ($this->Projet->{$modelAlias}->save($document)) {

            if ($modelAlias == "Document") {
                $this->Projet->Document->createDoc($this->Projet->{$modelAlias}->id, $uploadedFile['tmp_name']);
            }

            if ($modelAlias == "Annex") {
                // DEV
                $collId = $_SESSION['Auth']['collectivite']['id'];
                //$collId = '5810a34f-c540-4ad1-9caa-189c5cde535b';

                $year = date("Y");
                //if the directorydoesnt exist create it maybe in the /data ??
                $folder = WORKSPACE . $collId . DS . $year;
                if (!file_exists($folder)) {
                    mkdir($folder, 0777, true);
                }
                rename($uploadedFile['tmp_name'], $folder . DS . $this->Projet->{$modelAlias}->id);

                $this->Projet->Annex->id = $this->Projet->{$modelAlias}->id;
                $this->Projet->Annex->saveField("path", $folder . DS . $this->Projet->{$modelAlias}->id);
            }

            $return = $this->Projet->{$modelAlias}->id;
        }
        return $return;
    }

    /**
     *
     * @return type
     */
    public function getConvocs() {
        $qd = array(
            'contain' => array(
                'Convocation' => array(
                    'fields' => array(
                        'Convocation.read',
                        'Convocation.ar_received',
                        'Convocation.ar_horodatage',
                        'Convocation.active',
                        'Convocation.ae_sent',
                        'Convocation.ae_horodatage',
                        'Convocation.presence',
                        'Convocation.delegation',
                        'Convocation.created',
                        'Convocation.presentstatus'
                    ),
                    'User' => array(
                        'fields' => array('User.firstname', 'User.lastname', 'User.name'),
                        'order' => array('User.firstname', 'User.lastname')
                    )
                )
            ),
            'recursive' => -1,
            'fields' => array(
                'Seance.name',
                'Seance.date_seance',
                'Seance.archive'
            ),
            'order' => array('Seance.date_seance desc')
        );
        return $this->find('all', $qd);
    }

}
