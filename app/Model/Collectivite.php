<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');

class Collectivite extends AppModel {

    /**
     *
     * @var type
     */
    public $useDbConfig = 'default';

    /**
     *
     * @var type
     */
    public $validationDomain = 'collectivite';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Collectivite.name.notempty'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Collectivite.name.isUnique'
            )
        ),
        'conn' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Collectivite.conn.notempty'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Collectivite.conn.isUnique'
            )
        ),
        'login_suffix' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Collectivite.login_suffix.notempty'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Collectivite.login_suffix.isUnique'
            )
        )
    );

}
