<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');

class Group extends AppModel {
    

	/**
	 *
	 * @var type
	 */
	public $actsAs = array('Acl' => array('type' => 'requester'));

	/**
	 *
	 * @var type
	 */
	public $validationDomain = 'group';

	/**
	 *
	 * @return null
	 */
	public function parentNode() {
		return null;
	}

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Group.name.notempty'
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Group.name.isUnique'
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
