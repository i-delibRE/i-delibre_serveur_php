<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

class Groupepolitique extends AppModel {
    
    public $displayField = 'name';

    public $hasMany = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'groupepolitique_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
    ));

}
