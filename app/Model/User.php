<?php

/**
 * User Model
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2014-05-09 13:55:36 +0200 (ven. 09 mai 2014) $
 * $Revision: 407 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/User.php $
 * $Id: User.php 407 2014-05-09 11:55:36Z ssampaio $
 *
 * @property Group $Group
 * @property Convocation $Convocation
 */
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
App::uses('Password', 'Utility');

class User extends AppModel {

    /**
     *
     * @var type
     */
    public $actsAs = array('Acl' => array('type' => 'requester'), 'Password');

    /**
     *
     * @var type
     */
    public $displayField = 'username';

    /**
     *
     * @var type
     */
    public $validationDomain = 'user';

    /**
     *
     * @var type
     */
    public $virtualFields = array(
        'name' => 'User.firstname || \' \' || User.lastname'
    );

    /**
     *
     * @return null
     */
    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'firstname' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'User.firstname.notempty'
            ),
        ),
        'lastname' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'User.lastname.notempty'
            ),
        ),
        'username' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'User.username.notempty'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'User.username.isUnique'
            )
        ),
        'mail' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => 'User.mail.invalid'
            ),
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'User.mail.notempty'
            ),
        ),
        'password' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'User.password.notempty'
            ),
            'checkIdenticalValues' => array(
                'rule' => array('checkIdenticalValues', 'confirm'),
                'message' => 'User.password.check'
            )
        ),
        'group_id' => array(
            'uuid' => array(
                'rule' => array('uuid'),
                'message' => 'User.group_id.notempty'
            ),
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Groupepolitique' => array(
            'className' => 'Groupepolitique',
            'foreignKey' => 'groupepolitique_id',
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Convocation' => array(
            'className' => 'Convocation',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Projet' => array(
            'className' => 'Projet',
            'foreignKey' => 'user_id'
        ),
        'TypesAutorized' => array(
            'className' => 'TypesAutorized',
            'foreignKey' => 'type_id',
        ),
        'Invitation' => array(
            'className' => 'Invitation',
            'foreignKey' => 'user_id',
        )
        
    );

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Type' => array(
            'className' => 'Type',
            'joinTable' => 'types_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'type_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'TypesUser'
        ),
        'Authorized' => array(
            'className' => 'Type',
            'joinTable' => 'types_authorizeds',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'type_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'TypesAuthorized'
        )
    );

    /**
     * Retourne les utilisateurs non présents en base
     *
     * @param array $users
     * @param array $usersInDb permet de récupérer les utilisateurs présents en base
     * @return array User
     */
    public function getNotInDB($users, &$usersInDb = array()) {
        $return = array();
        foreach ($users as $user) {
            $check = $this->find('first', array('conditions' => array('firstname ilike' => $user['Acteur']['prenom'], 'lastname ilike' => $user['Acteur']['nom'])));
            if (empty($check)) {
                $return[] = $user;
            } else {
                $usersInDb[] = $check;
            }
        }
        return $return;
    }

    /**
     * Créer les utilisateurs de la liste $users et retourne le tableau contenant tous les utilisateurs créés
     *
     * @param array $users
     * @param integer $groupId
     * @return array
     */
    public function createUsers($users, $groupId) {
        $result = array();
        foreach ($users as $user) {
            $username = strtolower(substr(Inflector::slug($user['Acteur']['prenom']), 0, 1) . '.' . Inflector::slug($user['Acteur']['nom']));

            $userToSave = $this->create();
            $userToSave['User']['firstname'] = $user['Acteur']['prenom'];
            $userToSave['User']['lastname'] = $user['Acteur']['nom'];
            if (!empty($user['Acteur']['email'])) {
                $userToSave['User']['mail'] = $user['Acteur']['email'];
            } else {
                $userToSave['User']['mail'] = $username . '@example.org';
            }
            $userToSave['User']['username'] = $username;

            $newPassword = strtolower(Inflector::slug($user['Acteur']['prenom']));

            $userToSave['User']['password'] = AuthComponent::password($newPassword);
            $userToSave['User']['confirm'] = $newPassword;

            $userToSave['User']['group_id'] = $groupId;
            //TODO: a prendre en compte plus tard
            // $user['Acteur']['salutation'];
            // $user['Acteur']['titre']
            // $user['Acteur']['ville']
            // $user['Acteur']['cp']

            try {
                if ($this->save($userToSave)) {
                    $userToSave['User']['id'] = $this->id;
                    $result[] = $userToSave;
                } else {
                    debug($this->validationErrors);
                }
            } catch (Exception $e) {
                $this->log($e->getMessage());
                $this->log(__LINE__, LOG_ERROR);
                throw $e;
            }
        }
        return $result;
    }

    public function getLastSync($userId = null) {
        $return = '';
        if (empty($userId)) {
            if (CakeSession::check('Auth.User.id')) {
                $userId = CakeSession::read('Auth.User.id');
            }
        }
        return $return;
    }

}
