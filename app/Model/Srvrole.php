<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');

class Srvrole extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $useDbConfig = 'default';

	/**
	 *
	 * @var type
	 */
	public $displayField = 'name';

	/**
	 *
	 * @var type
	 */
	public $validationDomain = 'srvrole';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Srvrole.name.notempty'
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Srvrole.name.isUnique'
			)
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'Srvuser' => array(
			'className' => 'Srvuser',
			'foreignKey' => 'srvrole_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
