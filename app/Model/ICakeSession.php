<?php

/**
 * CakeSession Model
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 5
 * @author Stéphane Sampaio
 * @copyright Adullact Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun. 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://rdubourget@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/ICakeSession.php $
 * $Id: ICakeSession.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 */
App::uses('AppModel', 'Model');

class ICakeSession extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $useDbConfig = "default";

	/**
	 *
	 * @var type
	 */
	public $useTable = 'cake_sessions';

	/**
	 *
	 * @var type
	 */
	public $displayField = 'name';

	/**
	 *
	 * @var type
	 */
	public $primaryKey = 'id';

	/**
	 *
	 * @var type
	 */
	public $recursive = -1;

	/**
	 *
	 * @var type
	 */
	public $_schema = array(
		'id' => array(
			'type' => 'string',
			'length' => 255
		),
		'data' => array(
			'type' => 'text'
		),
		'expires' => array(
			'type' => 'integer'
		)
	);

	/**
	 *
	 * @var type
	 */
	public $virtualFields = array(
		'name' => "ICakeSession.data"
	);

	/**
	 *
	 * @param type $session_data
	 * @return type
	 * @throws Exception
	 */
	public static function unserialize($session_data) {
		$method = ini_get("session.serialize_handler");
		switch ($method) {
			case "php":
				return self::unserialize_php($session_data);
				break;
			case "php_binary":
				return self::unserialize_phpbinary($session_data);
				break;
			default:
				throw new Exception("Unsupported session.serialize_handler: " . $method . ". Supported: php, php_binary");
		}
	}

	/**
	 *
	 * @param type $session_data
	 * @return type
	 * @throws Exception
	 */
	private static function unserialize_php($session_data) {
		$return_data = array();
		$offset = 0;
		while ($offset < strlen($session_data)) {
			if (!strstr(substr($session_data, $offset), "|")) {
				throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
			}
			$pos = strpos($session_data, "|", $offset);
			$num = $pos - $offset;
			$varname = substr($session_data, $offset, $num);
			$offset += $num + 1;
			$data = unserialize(substr($session_data, $offset));
			$return_data[$varname] = $data;
			$offset += strlen(serialize($data));
		}
		return $return_data;
	}

	/**
	 *
	 * @param type $session_data
	 * @return type
	 */
	private static function unserialize_phpbinary($session_data) {
		$return_data = array();
		$offset = 0;
		while ($offset < strlen($session_data)) {
			$num = ord($session_data[$offset]);
			$offset += 1;
			$varname = substr($session_data, $offset, $num);
			$offset += $num;
			$data = unserialize(substr($session_data, $offset));
			$return_data[$varname] = $data;
			$offset += strlen(serialize($data));
		}
		return $return_data;
	}

}
