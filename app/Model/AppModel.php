<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('Model', 'Model');
App::uses('DevTools', 'Utility');
App::uses('DbDynCon', 'Utility');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    /**
     *
     * @var type
     */
    public $actsAs = array('Containable', 'DatabaseTable');

    /**
     *
     * @var type
     */
    public $recursive = -1;

    /**
     *
     * @var type
     */
    public $adminModels = array('Srvuser', 'Srvrole', 'Collectivite', 'CollectiviteSrvuser', 'SrvusersMandat', 'Mandat');

    /**
     * Override of the Model's __construct method.
     *
     *
     * @param mixed $id Set this ID for this model on startup, can also be an array of options, see above.
     * @param string $table Name of database table to use.
     * @param string $ds DataSource connection name.
     * @link http://bakery.cakephp.org/articles/doze/2010/03/12/use-multiple-databases-in-one-app-based-on-requested-url
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $conn = Configure::read('conn');
        if (!empty($conn) && !in_array($this->name, $this->adminModels)) {

            //load dynamique de la connexion !
            DbDynCon::loadConnection($conn);
            $this->setDataSource($conn);
        }
    }

    /**
     *
     * @param type $var
     */
    public function ldebug($var) {
        $this->log(var_export($var, true), 'debug');
    }

    /**
     * Débute une transaction.
     *
     * @return boolean
     */
    public function begin() {
        return $this->getDataSource()->begin();
    }

    /**
     * Valide une transaction.
     *
     * @return boolean
     */
    public function commit() {
        return $this->getDataSource()->commit();
    }

    /**
     * Annule une transaction.
     *
     * @return boolean
     */
    public function rollback() {
        return $this->getDataSource()->rollback();
    }

    /**
     *
     * @return type
     */
    public function getQueries() {

        $db = ConnectionManager::getDataSource(Configure::read('conn'));

        if (method_exists($db, 'getLog')) {
            return array();
        }

        $log = $db->getLog();
        return $log;
    }

}
