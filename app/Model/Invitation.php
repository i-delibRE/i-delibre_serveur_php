<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');

class Invitation extends  AppModel
{



    public $validate = array(
        'document_id' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            ),
        ),
        'user_id' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),

            ),
        ),
        'seance_id' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            ),
        ),

    );






    public $belongsTo = array(
        'Seance' => array(
            'className' => 'Seance',
            'foreignKey' => 'seance_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Document' => array(
            'className' => 'Document',
            'foreignKey' => 'document_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );









}