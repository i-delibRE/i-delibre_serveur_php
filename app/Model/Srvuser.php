<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * i-delibRE 3.1
 * LICENCE CeCILL v2
 *
 */

App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
App::uses('Password', 'Utility');

class Srvuser extends AppModel {

    /**
     *
     * @var type
     */
    public $useDbConfig = 'default';

    /**
     *
     * @var type
     */
    public $displayField = 'username';

    /**
     *
     * @var type
     */
    public $validationDomain = 'srvuser';

    /**
     *
     * @var type
     */
    public $actsAs = array('Password');

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'firstname' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Srvuser.firstname.notempty'
            ),
        ),
        'lastname' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Srvuser.lastname.notempty'
            ),
        ),
        'username' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Srvuser.username.notempty'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Srvuser.username.isUnique'
            )
        ),
        'mail' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => 'Srvuser.mail.invalid'
            ),
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Srvuser.mail.notempty'
            ),
        ),
        'password' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Srvuser.password.notempty'
            ),
            'checkIdenticalValues' => array(
                'rule' => array('checkIdenticalValues', 'confirm'),
                'message' => 'Srvuser.password.check'
            )
        ),
        'srvrole_id' => array(
            'uuid' => array(
                'rule' => array('uuid'),
                'message' => 'Srvuser.srvrole_id.notempty'
            ),
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Srvrole' => array(
            'className' => 'Srvrole',
            'foreignKey' => 'srvrole_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
