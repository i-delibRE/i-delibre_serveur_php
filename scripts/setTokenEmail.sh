#!/bin/bash

while read p; do

  echo $p
  dbname=$p
  username="idelibre"

PGPASSWORD='idelibre'  psql $dbname $username << 'EOF'
  update convocations set istoken = true;
  update convocations set isemailed = true;

EOF

done <listDB