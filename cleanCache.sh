#!/bin/bash

echo "clear cache"
rm ./app/tmp/cache/persistent/* 2> /dev/null
rm ./app/tmp/cache/models/* 2> /dev/null
rm ./app/tmp/cache/views/* 2> /dev/null
rm ./app/tmp/cache/* 2> /dev/null
rm ./app/tmp/logs/* 2> /dev/null
echo "Done !"